import { Component, OnDestroy, OnInit, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { navItems } from '../../_nav';
import { UsuarioService } from '../../views/usuario/usuario.service';
import { ToastrService } from 'ngx-toastr';
import { ExportacaoService } from '../../core/service/exportacao.service';
import * as fileSaver from 'file-saver';
import { UnidadeBasicaSaudeService } from '../../core/service/unidade-basica-saude.service';
import * as jwt_decode from 'jwt-decode';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent implements OnInit, OnDestroy {

  public navItems;
  
  public sidebarMinimized = true;
  
  private changes: MutationObserver;
  
  public element: HTMLElement;

  public perfil: string;

  public nome: string;

  public descricao: string;

  constructor(private usuarioSerice: UsuarioService, private toastrService: ToastrService, private exportacaoService: ExportacaoService, 
    private unidadeSaudeBasicaService: UnidadeBasicaSaudeService, @Inject(DOCUMENT) _document?: any) {
    this.changes = new MutationObserver((mutations) => {
      this.sidebarMinimized = _document.body.classList.contains('sidebar-minimized');
    });
    
    this.element = _document.body;
    
    this.changes.observe(<Element>this.element, {
      attributes: true,
      attributeFilter: ['class']
    });
  }

  ngOnInit(): void {  
    this.usuarioSerice.getCurrentUserInfo().subscribe(
      data => {
        this.navItems = (navItems.filter(e => e.attributes?.profiles?.includes(data.perfil.perfil) 
        || e.attributes?.profiles?.includes("ROLE_USER")));
        this.perfil = data.perfil.perfil;
        this.nome = data.nome + " " + data.sobrenome;
        this.unidadeSaudeBasicaService.findByCnes(jwt_decode(sessionStorage.getItem('access_token')).cnes)
          .subscribe(ubs => this.descricao = ubs.nome + " - " + data.perfil.descricao)
      },
      error => console.log('Could not retrive current user details')
    );
  }

  gerarExportacao() {
    this.exportacaoService.countByImportada()
      .subscribe(
        data => {
          if (data > 0) {
            this.exportacaoService.gerarExportacao()
              .subscribe(
                response => {
                  console.log('exportacao gerada com sucesso');
                  fileSaver.saveAs(response.body, "fichas-exportacao.zip");
                },
                error => {
                  console.log('erro ao gerar exportacao ' + error);
                  this.toastrService.error('Error ao exportar fichas.', 'Error');
                }
              );
          } else {
            this.toastrService.warning('Não tem fichas para exportar!', 'Exportação');
          }
        },
        error => {
          this.toastrService.error('Error ao exportar fichas.', 'Error');
          console.log(error);
        }
      )
  }

  ngOnDestroy(): void {
    this.changes.disconnect();
  }
  
}
