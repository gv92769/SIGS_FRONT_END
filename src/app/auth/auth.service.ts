import { Injectable } from '@angular/core';
import { UsuarioLogin } from '../views/login/usuario-login';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map} from 'rxjs/operators';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
import { LotacaoDto } from '../core/model/lotacao-dto';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private readonly ACCESS_TOKEN = "access_token";

  private readonly REFRESH_TOKEN = "refresh_token";

  private readonly url: string;

  constructor(private http: HttpClient, private router: Router) {
    this.url = environment.apiBaseUrl + '/autenticacao';
  }

  isLoggedIn() : boolean {
    if(this.getAccessToken() !== null && this.getRefreshToken() !== null)
      return true;
    else
      return false
  }

  login(usuario: UsuarioLogin): Observable<string> {
    return this.http.post<any>(this.url + '/login',usuario).pipe(
      map(data => {
        this.setAccessToken(data.access_token);
        this.setRefreshToken(data.refresh_token);
        return data.access_token;
      })
    )
  }

  lotacao(lotacaoDto: LotacaoDto): Observable<string> {
    lotacaoDto.token = this.getRefreshToken();
    return this.http.post<any>(this.url+'/lotacao', lotacaoDto).pipe(
      map(data => {
        this.setAccessToken(data.access_token);
        this.setRefreshToken(data.refresh_token);
        return data.access_token;
      })
    );
  }

  logout() : Observable<any> {
    this.deleteTokens();
    this.router.navigate(['/login']);
    return Observable.throw("logout");
  }

  refreshToken() : Observable<string> {
    let refreshToken = this.getRefreshToken();
    return this.http.post<any>(this.url + '/refreshtoken',refreshToken).pipe(
      map(data => {
          this.setAccessToken(data.access_token);
          return data.access_token;
      })
    )
  }

  deleteTokens(){
      this.deleteAccessToken();
      this.deleteRefreshToken();
  }

  getAccessToken(){
    return sessionStorage.getItem(this.ACCESS_TOKEN);
  }

  private getRefreshToken(){
    return sessionStorage.getItem(this.REFRESH_TOKEN);
  }

  private deleteAccessToken(){
    sessionStorage.removeItem(this.ACCESS_TOKEN);
  }

  private deleteRefreshToken(){
    sessionStorage.removeItem(this.REFRESH_TOKEN);
  }

  private setAccessToken(token){
    sessionStorage.setItem(this.ACCESS_TOKEN,token);
  }

  private setRefreshToken(token){
    sessionStorage.setItem(this.REFRESH_TOKEN,token);
  }

}
