import { Injectable } from '@angular/core';
import { HttpClient, HttpInterceptor, HttpRequest, HttpHandler, HttpSentEvent, HttpHeaderResponse, HttpProgressEvent, HttpResponse, HttpUserEvent, HttpErrorResponse } from "@angular/common/http";

import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/finally';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/take';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from "rxjs/BehaviorSubject";

import { AuthService } from '../auth/auth.service';

@Injectable()
// HttpInterceptor interface.
export class RequestInterceptor implements HttpInterceptor {


    isRefreshingToken: boolean = false;
    tokenSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);

    constructor(private authService: AuthService) {}
    // addToken will add the Bearer token to the Authorization header
    addToken(req: HttpRequest<any>, token: string): HttpRequest<any> {
        //console.log("HTTP INTERCEPTOR - ADD TOKEN");
        return req.clone({ setHeaders: { Authorization: 'Bearer ' + token }})
    }
    // The RequestInterceptorService will implement HttpInterceptor which has only one method:
    // intercept.  It will add a token to the header on each call and catch any errors that might occur.
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpSentEvent | HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any>> {
        // In the intercept method, we return next.handle and pass in the cloned request with a header added
        // Get the auth token from the AuthService
        if(req.url.includes('/autenticacao/login') || req.url.includes('/autenticacao/refreshtoken')){
          //console.log("HTTP INTERCEPTOR - LOGIN OR REFRESH");
          return next.handle(req);
        }

        return next.handle(this.addToken(req, this.authService.getAccessToken()))
          .catch(error => {
             if (error instanceof HttpErrorResponse) {
               if(error.error.status === 401){
                  if(error.error.message == "expired_token"){
                    return this.handle401ExpiredTokenError(req,next);
                  }
                }
                return Observable.throw(error);
             } else {
               return Observable.throw(error);
            }
          });
    }

    // The code to handle the 401 error is the most important.
        handle401ExpiredTokenError(req: HttpRequest<any>, next: HttpHandler) {
            // If isRefreshingToken is false (which it is by default) we will
            // enter the code section that calls authService.refreshToken
            if (!this.isRefreshingToken) {
                // Immediately set isRefreshingToken to true so no more calls
                // come in and call refreshToken again – which we don’t want of course
                this.isRefreshingToken = true;

                // Reset here so that the following requests wait until the token
                // comes back from the refreshToken call.
                this.tokenSubject.next(null);
                // Call authService.refreshToken (this is an Observable that will be returned)
                return this.authService.refreshToken()
                    .switchMap((newToken: string) => {
                        if (newToken) {
                            // When successful, call tokenSubject.next on the new token,
                            // this will notify the API calls that came in after the refreshToken
                            // call that the new token is available and that they can now use it
                            this.tokenSubject.next(newToken);
                            // Return next.handle using the new token
                            return next.handle(this.addToken(req, newToken));
                        }

                        // If we don't get a new token, we are in trouble so logout.
                        return this.authService.logout();
                    })
                    .catch(error => {
                        // If there is an exception calling 'refreshToken', bad news so logout.
                        return this.authService.logout();
                    })
                    .finally(() => {
                        // When the call to refreshToken completes, in the finally block,
                        // reset the isRefreshingToken to false for the next time the token needs to be refreshed
                        this.isRefreshingToken = false;
                    });
            // Note that no matter which path is taken, we must return an Observable that ends up
            // resolving to a next.handle call so that the original call is matched with the altered call
            }
            // If isRefreshingToken is true, we will wait until tokenSubject has a non-null value
            // – which means the new token is ready
            else {

                return this.tokenSubject
                    .filter(token => token != null)
                    // Only take 1 here to avoid returning two – which will cancel the request
                    .take(1)
                    .switchMap(token => {
                        // When the token is available, return the next.handle of the new request
                        return next.handle(this.addToken(req, token));
                    });
            }
        }

}
