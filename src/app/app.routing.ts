import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import Containers
import { DefaultLayoutComponent } from './containers';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/register/register.component';
import { AuthGuard } from './auth/auth.guard';
import { SelecionarPerfilComponent } from './views/selecionar-perfil/selecionar-perfil.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: '404',
    component: P404Component,
    data: {
      title: 'Page 404'
    }
  },
  {
    path: '500',
    component: P500Component,
    data: {
      title: 'Page 500'
    }
  },
  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Login Page'
    }
  },
  {
    path: 'logout',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: 'register',
    component: RegisterComponent,
    data: {
      title: 'Register Page'
    }
  },
  {
    path: 'lotacao/selecionar-perfil',
    component: SelecionarPerfilComponent,
    data: {
      title: 'Lotação'
    },
    canActivate: [AuthGuard],
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    data: {
      title: 'Início'
    },
    canActivate: [AuthGuard],
    children: [
      {
        path: 'usuario',
        loadChildren: () => import('./views/usuario/usuario.module').then(m => m.UsuarioModule)
      },
      {
        path: 'cidadao',
        loadChildren: () => import('./views/cidadao/cidadao.module').then(m => m.CidadaoModule)
      },
      {
        path: 'cadastrodomiciliar',
        loadChildren: () => import('./views/cadastro-domiciliar/cadastro-domiciliar.module').then(m => m.CadastroDomiciliarModule)
      },
      {
        path: 'atendimentoindividual',
        loadChildren: () => import('./views/atendimento-individual/atendimento-individual.module').then(m => m.AtendimentoIndividualModule)
      },
      {
        path: 'fichavisitadomiciliar',
        loadChildren: () => import('./views/ficha-visita-domiciliar/ficha-visita-domiciliar.module').then(m => m.FichaVisitaDomiciliarModule)
      },
      {
        path: 'evolucao',
        loadChildren: () => import('./views/evolucao/evolucao.module').then(m => m.EvolucaoModule)
      },
      {
        path: 'ectoscopia',
        loadChildren: () => import('./views/ectoscopia/ectoscopia.module').then(m => m.EctoscopiaModule)
      },
      {
        path: 'exame',
        loadChildren: () => import('./views/exame/exame.module').then(m => m.ExameModule)
      },
      {
        path: 'atividadecoletiva',
        loadChildren: () => import('./views/atividade/atividade.module').then(m => m.AtividadeModule)
      },
      {
        path: 'atendimentoodontologico',
        loadChildren: () => import('./views/atendimento-odontologico/atendimento-odontologico.module').then(m => m.AtendimentoOdontologicoModule)
      },
      {
        path: 'fichaprocedimento',
        loadChildren: () => import('./views/ficha-procedimento/ficha-procedimento.module').then(m => m.FichaProcedimentoModule)
      },
      {
        path: 'dashboard',
        loadChildren: () => import('./views/dashboard/dashboard.module').then(m => m.DashboardModule)
      }
    ]
  },
  { path: '**', component: P404Component }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
