interface NavAttributes {
  [propName: string]: any;
}
interface NavWrapper {
  attributes: NavAttributes;
  element: string;
}
interface NavBadge {
  text: string;
  variant: string;
}
interface NavLabel {
  class?: string;
  variant: string;
}

export interface NavData {
  name?: string;
  url?: string;
  icon?: string;
  badge?: NavBadge;
  title?: boolean;
  children?: NavData[];
  variant?: string;
  attributes?: NavAttributes;
  divider?: boolean;
  class?: string;
  label?: NavLabel;
  wrapper?: NavWrapper;
}

export const navItems: NavData[] = [
  {
    name: 'Página Inicial',
    url: '/dashboard',
    icon: 'icon-home',
    attributes: { profiles: ['ROLE_USER'] },
  },
  {
    title: true,
    name: 'Usuário',
    attributes: { profiles: ['ROLE_ADMINISTRADOR'] },
  },
  {
    name: 'Adicionar',
    url: '/usuario/adicionar',
    icon: 'cui-user',
    attributes: { profiles: ['ROLE_ADMINISTRADOR'] },
  },
  {
    name: 'Pesquisar',
    url: '/usuario/pesquisar',
    icon: 'cui-magnifying-glass',
    attributes: { profiles: ['ROLE_ADMINISTRADOR'] },
  },
  {
    title: true,
    name: 'Cidadão',
    attributes: { profiles: ['ROLE_ADMINISTRATIVO', 'ROLE_AGENTE_SAUDE', 'ROLE_ENFERMEIRO'] },
  },
  {
    name: 'Adicionar',
    url: '/cidadao/adicionar',
    icon: 'cui-user',
    attributes: { profiles: ['ROLE_ADMINISTRATIVO', 'ROLE_AGENTE_SAUDE', 'ROLE_ENFERMEIRO'] },
  },
  {
    name: 'Pesquisar',
    url: '/cidadao/pesquisar',
    icon: 'cui-magnifying-glass',
    attributes: { profiles: ['ROLE_ADMINISTRATIVO', 'ROLE_AGENTE_SAUDE', 'ROLE_ENFERMEIRO'] },
  },
  {
    title: true,
    name: 'Cadastro Domiciliar',
    attributes: { profiles: ['ROLE_AGENTE_SAUDE', 'ROLE_ENFERMEIRO'] },
  },
  {
    name: 'Adicionar',
    url: '/cadastrodomiciliar/adicionar',
    icon: 'cui-user',
    attributes: { profiles: ['ROLE_AGENTE_SAUDE', 'ROLE_ENFERMEIRO'] },
  },
  {
    name: 'Pesquisar',
    url: '/cadastrodomiciliar/pesquisar',
    icon: 'cui-magnifying-glass',
    attributes: { profiles: ['ROLE_AGENTE_SAUDE', 'ROLE_ENFERMEIRO'] },
  },
  {
    title: true,
    name: 'Ficha Visita Domiciliar',
    attributes: { profiles: ['ROLE_USER'] },
    // attributes: {
    //   profiles: ['ROLE_ENFERMEIRO', 'ROLE_FARMACEUTICO', 'ROLE_FISIOTERAPIA', 'ROLE_MASSOTERAPEUTA',
    //     'ROLE_FONOAUDIOLOGO', 'ROLE_MEDICO', 'ROLE_NUTRICIONISTA', 'ROLE_PSICOLOGO']
    // },
  },
  {
    name: 'Adicionar',
    url: '/fichavisitadomiciliar/adicionar',
    icon: 'cui-user',
    attributes: { profiles: ['ROLE_USER'] },
    // attributes: {
    //   profiles: ['ROLE_ENFERMEIRO', 'ROLE_FARMACEUTICO', 'ROLE_FISIOTERAPIA', 'ROLE_MASSOTERAPEUTA',
    //     'ROLE_FONOAUDIOLOGO', 'ROLE_MEDICO', 'ROLE_NUTRICIONISTA', 'ROLE_PSICOLOGO']
    // },
  },
  {
    name: 'Pesquisar',
    url: '/fichavisitadomiciliar/pesquisar',
    icon: 'cui-magnifying-glass',
    attributes: { profiles: ['ROLE_USER'] },
    // attributes: {
    //   profiles: ['ROLE_ENFERMEIRO', 'ROLE_FARMACEUTICO', 'ROLE_FISIOTERAPIA', 'ROLE_MASSOTERAPEUTA',
    //     'ROLE_FONOAUDIOLOGO', 'ROLE_MEDICO', 'ROLE_NUTRICIONISTA', 'ROLE_PSICOLOGO']
    // },
  },
  {
    title: true,
    name: 'Evolução',
    attributes: {
      profiles: ['ROLE_ENFERMEIRO', 'ROLE_FARMACEUTICO', 'ROLE_FISIOTERAPIA', 'ROLE_MASSOTERAPEUTA',
        'ROLE_FONOAUDIOLOGO', 'ROLE_MEDICO', 'ROLE_NUTRICIONISTA', 'ROLE_PSICOLOGO', 'ROLE_ASSISTENTE_SOCIAL']
    },
  },
  {
    name: 'Adicionar',
    url: '/evolucao/adicionar',
    icon: 'cui-user',
    attributes: {
      profiles: ['ROLE_ENFERMEIRO', 'ROLE_FARMACEUTICO', 'ROLE_FISIOTERAPIA', 'ROLE_MASSOTERAPEUTA',
        'ROLE_FONOAUDIOLOGO', 'ROLE_MEDICO', 'ROLE_NUTRICIONISTA', 'ROLE_PSICOLOGO', 'ROLE_ASSISTENTE_SOCIAL']
    },
  },
  {
    name: 'Pesquisar',
    url: '/evolucao/pesquisar',
    icon: 'cui-magnifying-glass',
    attributes: {
      profiles: ['ROLE_ENFERMEIRO', 'ROLE_FARMACEUTICO', 'ROLE_FISIOTERAPIA', 'ROLE_MASSOTERAPEUTA',
        'ROLE_FONOAUDIOLOGO', 'ROLE_MEDICO', 'ROLE_NUTRICIONISTA', 'ROLE_PSICOLOGO', 'ROLE_ASSISTENTE_SOCIAL']
    },
  },
  {
    title: true,
    name: 'Atendimento Individual',
    attributes: {
      profiles: ['ROLE_ENFERMEIRO', 'ROLE_FARMACEUTICO', 'ROLE_FISIOTERAPIA', 'ROLE_MASSOTERAPEUTA',
        'ROLE_FONOAUDIOLOGO', 'ROLE_MEDICO', 'ROLE_NUTRICIONISTA', 'ROLE_PSICOLOGO']
    },
  },
  {
    name: 'Adicionar',
    url: '/atendimentoindividual/adicionar',
    icon: 'cui-user',
    attributes: {
      profiles: ['ROLE_ENFERMEIRO', 'ROLE_FARMACEUTICO', 'ROLE_FISIOTERAPIA', 'ROLE_MASSOTERAPEUTA',
        'ROLE_FONOAUDIOLOGO', 'ROLE_MEDICO', 'ROLE_NUTRICIONISTA', 'ROLE_PSICOLOGO']
    },
  },
  {
    name: 'Pesquisar',
    url: '/atendimentoindividual/pesquisar',
    icon: 'cui-magnifying-glass',
    attributes: {
      profiles: ['ROLE_ENFERMEIRO', 'ROLE_FARMACEUTICO', 'ROLE_FISIOTERAPIA', 'ROLE_MASSOTERAPEUTA',
        'ROLE_FONOAUDIOLOGO', 'ROLE_MEDICO', 'ROLE_NUTRICIONISTA', 'ROLE_PSICOLOGO']
    },
  },
  {
    title: true,
    name: 'Ectoscopia',
    attributes: {
      profiles: ['ROLE_ENFERMEIRO', 'ROLE_FARMACEUTICO', 'ROLE_FISIOTERAPIA', 'ROLE_MASSOTERAPEUTA',
        'ROLE_FONOAUDIOLOGO', 'ROLE_MEDICO', 'ROLE_NUTRICIONISTA', 'ROLE_PSICOLOGO']
    },
  },
  {
    name: 'Adicionar',
    url: '/ectoscopia/adicionar',
    icon: 'cui-user',
    attributes: {
      profiles: ['ROLE_ENFERMEIRO', 'ROLE_FARMACEUTICO', 'ROLE_FISIOTERAPIA', 'ROLE_MASSOTERAPEUTA',
        'ROLE_FONOAUDIOLOGO', 'ROLE_MEDICO', 'ROLE_NUTRICIONISTA', 'ROLE_PSICOLOGO']
    },
  },
  {
    name: 'Pesquisar',
    url: '/ectoscopia/pesquisar',
    icon: 'cui-magnifying-glass',
    attributes: {
      profiles: ['ROLE_ENFERMEIRO', 'ROLE_FARMACEUTICO', 'ROLE_FISIOTERAPIA', 'ROLE_MASSOTERAPEUTA',
        'ROLE_FONOAUDIOLOGO', 'ROLE_MEDICO', 'ROLE_NUTRICIONISTA', 'ROLE_PSICOLOGO']
    },
  },
  {
    title: true,
    name: 'Exame',
    attributes: { 
      profiles: ['ROLE_ENFERMEIRO', 'ROLE_FARMACEUTICO', 'ROLE_FISIOTERAPIA', 'ROLE_MASSOTERAPEUTA',
    'ROLE_FONOAUDIOLOGO', 'ROLE_MEDICO', 'ROLE_NUTRICIONISTA', 'ROLE_PSICOLOGO'] },
  },
  {
    name: 'Solicitar',
    url: '/exame/solicitacao/adicionar',
    icon: 'cui-user',
    attributes: { 
      profiles: ['ROLE_ENFERMEIRO', 'ROLE_FARMACEUTICO', 'ROLE_FISIOTERAPIA', 'ROLE_MASSOTERAPEUTA',
    'ROLE_FONOAUDIOLOGO', 'ROLE_MEDICO', 'ROLE_NUTRICIONISTA', 'ROLE_PSICOLOGO'] },
  },
  {
    name: 'Pesquisar',
    url: '/exame/pesquisar',
    icon: 'cui-user',
    attributes: { 
      profiles: ['ROLE_ENFERMEIRO', 'ROLE_FARMACEUTICO', 'ROLE_FISIOTERAPIA', 'ROLE_MASSOTERAPEUTA',
    'ROLE_FONOAUDIOLOGO', 'ROLE_MEDICO', 'ROLE_NUTRICIONISTA', 'ROLE_PSICOLOGO'] },
  },
  {
    name: 'Laudo',
    url: '/exame/solicitacao/laudo',
    icon: 'cui-user',
    attributes: { 
      profiles: ['ROLE_ENFERMEIRO', 'ROLE_FARMACEUTICO', 'ROLE_FISIOTERAPIA', 'ROLE_MASSOTERAPEUTA',
    'ROLE_FONOAUDIOLOGO', 'ROLE_MEDICO', 'ROLE_NUTRICIONISTA', 'ROLE_PSICOLOGO'] },
  },
  {
    title: true,
    name: 'Atividade Coletiva',
    attributes: {
      profiles: ['ROLE_ENFERMEIRO', 'ROLE_FARMACEUTICO', 'ROLE_FISIOTERAPIA', 'ROLE_MASSOTERAPEUTA', 'ROLE_FONOAUDIOLOGO',
        'ROLE_MEDICO', 'ROLE_NUTRICIONISTA', 'ROLE_PSICOLOGO']
    }
  },
  {
    name: 'Adicionar',
    url: '/atividadecoletiva/adicionar',
    icon: 'cui-user',
    attributes: {
      profiles: ['ROLE_ENFERMEIRO', 'ROLE_FARMACEUTICO', 'ROLE_FISIOTERAPIA', 'ROLE_MASSOTERAPEUTA', 'ROLE_FONOAUDIOLOGO',
        'ROLE_MEDICO', 'ROLE_NUTRICIONISTA', 'ROLE_PSICOLOGO']
    }
  },
  {
    name: 'Pesquisar',
    url: '/atividadecoletiva/pesquisar',
    icon: 'cui-magnifying-glass',
    attributes: {
      profiles: ['ROLE_ENFERMEIRO', 'ROLE_FARMACEUTICO', 'ROLE_FISIOTERAPIA', 'ROLE_MASSOTERAPEUTA', 'ROLE_FONOAUDIOLOGO',
        'ROLE_MEDICO', 'ROLE_NUTRICIONISTA', 'ROLE_PSICOLOGO']
    }
  },
  {
    title: true,
    name: 'Atendimento Odontológico',
    attributes: { profiles: ['ROLE_CIRURGIAO_DENTISTA'] }
  },
  {
    name: 'Adicionar',
    url: '/atendimentoodontologico/adicionar',
    icon: 'cui-user',
    attributes: { profiles: ['ROLE_CIRURGIAO_DENTISTA'] }
  },
  {
    name: 'Pesquisar',
    url: '/atendimentoodontologico/pesquisar',
    icon: 'cui-magnifying-glass',
    attributes: { profiles: ['ROLE_CIRURGIAO_DENTISTA'] }
  },
  {
    title: true,
    name: 'Ficha de Procedimentos',
    attributes: {
      profiles: ['ROLE_AUXILIAR_SAUDE', 'ROLE_ENFERMEIRO', 'ROLE_FARMACEUTICO', 'ROLE_FISIOTERAPIA', 'ROLE_MASSOTERAPEUTA',
        'ROLE_FONOAUDIOLOGO', 'ROLE_MEDICO', 'ROLE_NUTRICIONISTA', 'ROLE_PSICOLOGO', 'ROLE_TECNICO_SAUDE']
    },
  },
  {
    name: 'Adicionar',
    url: '/fichaprocedimento/adicionar',
    icon: 'cui-user',
    attributes: {
      profiles: ['ROLE_ENFERMEIRO', 'ROLE_AUXILIAR_SAUDE', 'ROLE_FARMACEUTICO', 'ROLE_FISIOTERAPIA', 'ROLE_MASSOTERAPEUTA',
        'ROLE_FONOAUDIOLOGO', 'ROLE_MEDICO', 'ROLE_NUTRICIONISTA', 'ROLE_PSICOLOGO', 'ROLE_TECNICO_SAUDE']
    },
  },
  {
    name: 'Pesquisar',
    url: '/fichaprocedimento/pesquisar',
    icon: 'cui-magnifying-glass',
    attributes: {
      profiles: ['ROLE_AUXILIAR_SAUDE', 'ROLE_ENFERMEIRO', 'ROLE_FARMACEUTICO', 'ROLE_FISIOTERAPIA', 'ROLE_MASSOTERAPEUTA',
        'ROLE_FONOAUDIOLOGO', 'ROLE_MEDICO', 'ROLE_NUTRICIONISTA', 'ROLE_PSICOLOGO', 'ROLE_TECNICO_SAUDE']
    },
  },
];
