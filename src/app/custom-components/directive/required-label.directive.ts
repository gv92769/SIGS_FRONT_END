import { Directive, ElementRef, OnInit } from '@angular/core';

@Directive({
  selector: '[requiredLabel]'
})
export class RequiredLabelDirective implements OnInit {

  constructor(private el: ElementRef) { }

  ngOnInit() {
    //this.el.nativeElement.innerHTML += ' <span class="text-danger">*</span>';
    this.el.nativeElement.innerHTML += ' <span class="text-muted">*</span>';
  }



}
