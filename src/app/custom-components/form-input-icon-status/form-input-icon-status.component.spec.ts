import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormInputIconStatusComponent } from './form-input-icon-status.component';

describe('FormInputIconStatusComponent', () => {
  let component: FormInputIconStatusComponent;
  let fixture: ComponentFixture<FormInputIconStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormInputIconStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormInputIconStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
