import { Component, OnInit, Input } from '@angular/core';

enum Status{
  READY = 1,
  LOADING,
  SUCCESS,
  ERROR
}

@Component({
  selector: 'form-input-icon-status',
  templateUrl: './form-input-icon-status.component.html',
  styleUrls: ['./form-input-icon-status.component.scss']
})
export class FormInputIconStatusComponent implements OnInit {

  static readonly Status = Status;

  @Input('status') status: Status = Status.READY;

  constructor() { }

  ngOnInit() {
  }

  ready(): void{
    this.status = Status.READY;
  }

  loading(): void{
    this.status = Status.LOADING;
  }

  success(): void{
    this.status = Status.SUCCESS;
  }

  error(): void{
    this.status = Status.ERROR;
  }

  get Status(){
    return Status;
  }

  isSuccessStatus(): boolean {
    return this.status == Status.SUCCESS;
  }

}


