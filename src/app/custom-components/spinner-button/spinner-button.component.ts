import { Component, OnInit, Input, ElementRef, Renderer2 } from '@angular/core';


@Component({
  selector: 'spinner-button',
  template: `<ng-content select="button"></ng-content>`
})
export class SpinnerButtonComponent implements OnInit {

  @Input() spinnerPos: number = 0; //0: left, 1: right, 2: replace content
  @Input() spinnerType: number = 0; //0: spinner-border,  1: spinner-grow
  @Input() spinnerSize: number = 0; //0: small,  1: normal (default size)

  private _showSpinner: boolean;

  spinnerElement: any;
  buttonElement: any;
  buttonChildren: NodeList;
  tempElement: any;
  tempElementChildren: NodeList;


  constructor(private renderer: Renderer2, private elRef: ElementRef) {

  }


  ngOnInit() {
    this.buttonElement = this.elRef.nativeElement.querySelector('button');
    this.buttonChildren = this.buttonElement && this.buttonElement.childNodes;
    if (this.buttonElement) {
      this.createSpinnerElement();
      if (this.spinnerPos == 2) {
        this.createTempElementAndChildren();
      }
    }
  }


  @Input()
  set showSpinner(showSpinner: boolean) {
    this._showSpinner = showSpinner;
    this.onShowSpinnerChange();
  }

  onShowSpinnerChange(): void {
    if (this.buttonElement) {
      if (this._showSpinner) {
        if (this.spinnerPos == 1) {
          this.renderer.appendChild(this.buttonElement, this.spinnerElement);
        }
        else if (this.spinnerPos == 2) {
          let count = this.buttonChildren.length;
          while (count > 0) {
            this.renderer.appendChild(this.tempElement, this.buttonChildren[0]);
            count = this.buttonChildren.length;
          }
          this.renderer.appendChild(this.buttonElement, this.spinnerElement);
        }
        else {
          this.renderer.insertBefore(this.buttonElement, this.spinnerElement, this.buttonChildren[0]);
        }
      }
      else {
        this.renderer.removeChild(this.buttonElement, this.spinnerElement);
        if (this.spinnerPos == 2) {
          let count = this.tempElementChildren.length;
          while (count > 0) {
            this.renderer.appendChild(this.buttonElement, this.tempElementChildren[0]);
            count = this.tempElementChildren.length;
          }
        }
      }
    }
  }

  private createTempElementAndChildren(): void {
    this.tempElement = this.renderer.createElement('span');
    this.tempElementChildren = this.tempElement.childNodes;
  }

  private createSpinnerElement(): void {
    this.spinnerElement = this.renderer.createElement('span');
    this.createSpinnerTypeClass();
    this.createSpinnerSizeClass();
    this.renderer.setAttribute(this.spinnerElement, "role", "status");
    this.renderer.setAttribute(this.spinnerElement, "aria-hidden", "true");
  }

  private getSpinnerTypeClass(): string {
    switch (this.spinnerType) {
      case 0: return 'spinner-border';
      case 1: return 'spinner-grow';
      default: return 'spinner-border';
    }
  }

  private createSpinnerTypeClass(): void {
    let typeString = this.getSpinnerTypeClass();
    this.renderer.addClass(this.spinnerElement, typeString);
  }

  private createSpinnerSizeClass(): void {
    let typeString = this.getSpinnerTypeClass();
    switch (this.spinnerSize) {
      case 0: this.renderer.addClass(this.spinnerElement, typeString + '-sm'); break;
      case 1: break;
      default: this.renderer.addClass(this.spinnerElement, typeString + '-sm'); break;
    }
  }
}
