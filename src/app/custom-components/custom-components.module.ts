// Angular
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SpinnerButtonComponent } from './spinner-button/spinner-button.component';
import { RequiredLabelDirective } from './directive/required-label.directive';
import { FormInputIconStatusComponent } from './form-input-icon-status/form-input-icon-status.component';



@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    SpinnerButtonComponent,
    RequiredLabelDirective,
    FormInputIconStatusComponent
  ],
  exports: [
    SpinnerButtonComponent,
    RequiredLabelDirective,
    FormInputIconStatusComponent
  ]
})
export class CustomComponentsModule { }
