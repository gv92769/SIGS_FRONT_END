export class ProcedimentoOdonto {
  id: number;
  sigtap: string;
  codigoab: string;
  descricao: string;
  local: string;
  max: number;
}
