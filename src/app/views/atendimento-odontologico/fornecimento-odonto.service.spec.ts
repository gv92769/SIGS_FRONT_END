import { TestBed } from '@angular/core/testing';

import { FornecimentoOdontoService } from './fornecimento-odonto.service';

describe('FornecimentoOdontoService', () => {
  let service: FornecimentoOdontoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FornecimentoOdontoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
