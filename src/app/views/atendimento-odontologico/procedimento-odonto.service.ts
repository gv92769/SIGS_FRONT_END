import { Injectable } from '@angular/core';
import { ProcedimentoOdonto } from './procedimento-odonto';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProcedimentoOdontoService {

  readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/procedimentoodonto';
  }

  public findAll(): Observable<ProcedimentoOdonto[]> {
    return this.http.get<ProcedimentoOdonto[]>(this.url);
  }

}
