import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdicionarAtendimentoOdontologicoComponent } from './adicionar-atendimento-odontologico.component';

describe('AdicionarAtendimentoOdontologicoComponent', () => {
  let component: AdicionarAtendimentoOdontologicoComponent;
  let fixture: ComponentFixture<AdicionarAtendimentoOdontologicoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdicionarAtendimentoOdontologicoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdicionarAtendimentoOdontologicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
