import { Component, OnInit, ViewChild } from '@angular/core';
import { FichaAtendimentoOdontologico } from '../ficha-atendimento-odontologico';
import { BehaviorSubject } from 'rxjs';
import { UsuarioService } from '../../usuario/usuario.service';
import { UnidadeBasicaSaudeService } from '../../../core/service/unidade-basica-saude.service';
import { ProfissionalService } from '../../../core/service/profissional.service';
import * as jwt_decode from 'jwt-decode';
import { AtendimentoOdontologicoService } from '../atendimento-odontologico.service';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CboService } from '../../../core/service/cbo.service';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-adicionar-atendimento-odontologico',
  templateUrl: './adicionar-atendimento-odontologico.component.html',
  styleUrls: ['./adicionar-atendimento-odontologico.component.css']
})
export class AdicionarAtendimentoOdontologicoComponent implements OnInit {

  formData$: BehaviorSubject<FichaAtendimentoOdontologico> = new BehaviorSubject<FichaAtendimentoOdontologico>(new FichaAtendimentoOdontologico());

  @ViewChild('formAtendimentoOdontologico', { static: false }) formAtendimentoOdontologico: FichaAtendimentoOdontologico;

  isLoadingSubmit: boolean = false;

  constructor(private usuarioService: UsuarioService, private profissionalService: ProfissionalService, private cboService: CboService,
    private atendimentoOdontologicoService: AtendimentoOdontologicoService, private router: Router, private toastrService: ToastrService,
    private unidadeSaudeBasicaService: UnidadeBasicaSaudeService) { }

  ngOnInit(): void {
    let fichaAtendimentoOdontologico: FichaAtendimentoOdontologico = new FichaAtendimentoOdontologico();

    this.usuarioService.getCurrentUserInfo().subscribe(
      data => this.profissionalService.findById(data.id)
        .subscribe(profissional => fichaAtendimentoOdontologico.profissionalResponsavel = profissional)
    );

    this.unidadeSaudeBasicaService.findByCnes(jwt_decode( sessionStorage.getItem('access_token')).cnes)
      .subscribe(ubs => fichaAtendimentoOdontologico.ubs = ubs)

    this.cboService.findByCodigo(jwt_decode(sessionStorage.getItem('access_token')).cbo)
      .subscribe(cbo => fichaAtendimentoOdontologico.cbo = cbo);

    fichaAtendimentoOdontologico.dataHorarioInicio = formatDate(new Date(), "yyyy-MM-dd'T'HH:mm:ss", 'en-US');

    this.formData$.next(fichaAtendimentoOdontologico);
  }

  onSubmitted(atendimentoOdontologico: FichaAtendimentoOdontologico) {
    this.isLoadingSubmit = true;
    this.atendimentoOdontologicoService.create(atendimentoOdontologico)
      .pipe(finalize(() => this.isLoadingSubmit = false))
      .subscribe({
        next: (response) => {
          this.toastrService.success('Atendimento odontológico salvo com sucesso!', 'Sucesso');
          this.router.navigate(['atendimentoodontologico/adicionar']);
        },
        error: (err) => {
          this.toastrService.error('Error ao salvar atendimento odontológico.', 'Error');
          console.log(err);
        }
      });
  }

}
