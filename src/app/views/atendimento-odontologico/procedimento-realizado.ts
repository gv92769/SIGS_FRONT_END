import { Procedimento } from '../../core/model/procedimento';
import { ProcedimentoOdonto } from './procedimento-odonto';

export class ProcedimentoRealizado {
  id: number;
  procedimento: ProcedimentoOdonto;
  proced: Procedimento;
  quantidade: number = 0;
}
