import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { AdicionarAtendimentoOdontologicoComponent } from './adicionar-atendimento-odontologico/adicionar-atendimento-odontologico.component';
import { EditarAtendimentoOdontologicoComponent } from './editar-atendimento-odontologico/editar-atendimento-odontologico.component';
import { PesquisarAtendimentoOdontologicoComponent } from './pesquisar-atendimento-odontologico/pesquisar-atendimento-odontologico.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Atendimento Odontológico'
    },
    children: [
      {
        path: 'adicionar',
        component: AdicionarAtendimentoOdontologicoComponent,
        data: {
          title: 'Adicionar'
        }
      },
      {
        path: 'editar/:id',
        component: EditarAtendimentoOdontologicoComponent,
        data: {
          title: 'Editar'
        }
      },
      {
        path: 'pesquisar',
        component: PesquisarAtendimentoOdontologicoComponent,
        data: {
          title: 'Pesquisar'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AtendimentoOdontologicoRoutingModule {}
