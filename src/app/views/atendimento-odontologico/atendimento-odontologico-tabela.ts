export class AtendimentoOdontologicoTabela {
  id: number;
  data: string;
  nome: string;
  tipoAtendimento: string;
  cpf: string;
  cns: string;
}
