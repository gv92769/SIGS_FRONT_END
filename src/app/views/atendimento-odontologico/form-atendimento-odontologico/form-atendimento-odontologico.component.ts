import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { FichaAtendimentoOdontologico } from '../ficha-atendimento-odontologico';
import { FormGroup } from '@angular/forms';
import { LocalAtendimento } from '../../../core/model/local-atendimento';
import { LocalAtendimentoService } from '../../../core/service/local-atendimento.service';
import { CidadaoService } from '../../cidadao/cidadao.service';
import { Cidadao } from '../../cidadao/cidadao';
import { TipoAtendimento } from '../../../core/model/tipo-atendimento';
import { TipoAtendimentoService } from '../../../core/service/tipo-atendimento.service';
import { TipoConsultaOdontoService } from '../tipo-consulta-odonto.service';
import { VigilanciaSaudeBucalService } from '../vigilancia-saude-bucal.service';
import { FornecimentoOdontoService } from '../fornecimento-odonto.service';
import { CondutaEncaminhamentoOdontoService } from '../conduta-encaminhamento-odonto.service';
import { ProcedimentoOdonto } from '../procedimento-odonto';
import { ProcedimentoOdontoService } from '../procedimento-odonto.service';
import { ProcedimentoRealizado } from '../procedimento-realizado';
import { ObjectCodigoDescricao } from '../../../core/model/object-codigo-descricao';
import { Config } from '../../../core/model/config';
import { Procedimento } from '../../../core/model/procedimento';
import { ProcedimentoService } from '../../../core/service/procedimento.service';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { Page } from '../../../core/model/page';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-form-atendimento-odontologico',
  templateUrl: './form-atendimento-odontologico.component.html',
  styleUrls: ['./form-atendimento-odontologico.component.css']
})
export class FormAtendimentoOdontologicoComponent implements OnInit {

  @Input('formData') formData$: Observable<FichaAtendimentoOdontologico>;

  fichaAtendimentoOdontologico: FichaAtendimentoOdontologico = new FichaAtendimentoOdontologico();

  @ViewChild('form', { static: false }) form: FormGroup;

  locais: LocalAtendimento[] = new Array;

  tiposAtendimento: TipoAtendimento[] = new Array;

  tiposConsulta: ObjectCodigoDescricao[] = new Array;

  listVigilacia: ObjectCodigoDescricao[] = new Array;

  fornecimentos: ObjectCodigoDescricao[] = new Array;

  condutasDesfecho: ObjectCodigoDescricao[] = new Array;

  condutasEncaminhamento: ObjectCodigoDescricao[] = new Array;

  @Output('onSubmit') onSubmitEventEmitter = new EventEmitter();

  procedimentos: ProcedimentoOdonto[] = new Array;

  opCidadao: Cidadao[] = new Array;

  configCidadao: Config = new Config();

  procedimentoSelecionado: ProcedimentoRealizado = new ProcedimentoRealizado();

  page: Page<ProcedimentoRealizado> = new Page();

  idProcedimentoSelecionado: number;

  config: Config = new Config();

  opProcedimentos: Procedimento[] = new Array;

  columnMode = ColumnMode;

  constructor(private localAtendimentoService: LocalAtendimentoService, private cidadaoService: CidadaoService,
    private tipoConsultaOdontoService: TipoConsultaOdontoService, private vigilanciaSaudeBucalService: VigilanciaSaudeBucalService,
    private fornecimentoOdontoService: FornecimentoOdontoService, private condutaEncaminhamentoOdontoService: CondutaEncaminhamentoOdontoService,
    private procedimentoOdontoService: ProcedimentoOdontoService, private tipoAtendimentoService: TipoAtendimentoService,
    private procedimentoService: ProcedimentoService) { }

  ngOnInit(): void {
    this.formData$.subscribe({
      next: (value) => {
        this.fichaAtendimentoOdontologico = value;
        this.page.content = value.outrosProcedimentos;
        this.page.totalElements = value.outrosProcedimentos.length;
      },
      error: (err) => {
        console.log('erro behavior subject');
        console.log(err);
      }
    });

    this.localAtendimentoService.findAll().subscribe(
      data => this.locais = data,
      error => console.log("error in findAllLocalAtendimento()")
    );

    let ids: number[] = [2, 4, 5, 6];

    this.tipoAtendimentoService.findAllById(ids).subscribe(
      data => this.tiposAtendimento = data,
      error => console.log("error in findAllTipoAtendimento()")
    );

    this.tipoConsultaOdontoService.findAll().subscribe(
      data => this.tiposConsulta = data,
      error => console.log("error in findAllTipoConsultaOdonto()")
    );

    this.vigilanciaSaudeBucalService.findAll().subscribe(
      data => this.listVigilacia = data,
      error => console.log("error in findAllVigilanciaSaudeBucal()")
    );

    this.fornecimentoOdontoService.findAll().subscribe(
      data => this.fornecimentos = data,
      error => console.log("error in findAllFornecimentoOdonto()")
    );

    ids = [16, 12, 13, 14, 15, 17];

    this.condutaEncaminhamentoOdontoService.findAllById(ids).subscribe(
      data => this.condutasDesfecho = data,
      error => console.log("error in findAllTipoAtendimento()")
    );

    ids = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];

    this.condutaEncaminhamentoOdontoService.findAllById(ids).subscribe(
      data => this.condutasEncaminhamento = data,
      error => console.log("error in findAllTipoAtendimento()")
    );

    this.procedimentoOdontoService.findAll().subscribe(
      data => this.procedimentos = data,
      error => console.log("error in findAllProcedimentoOdonto()")
    );

    this.configCidadao.displayKey = 'nome';
    this.configCidadao.placeholder = 'Digite o CPF/CNS';

    this.config.displayKey = 'descricao';
    this.config.placeholder = 'Escolha um ou mais Procedimentos';
    this.config.searchOnKey = 'descricao';
  }

  buscarCidadao(valor: string) {
    if (valor != null && valor != '')
      this.cidadaoService.filtro(valor)
        .subscribe({
          next: (data) => {
            this.opCidadao = data;
          },
          error: (err) => {
            console.log(err);
          }
        });
  }

  gestanteDisabled() {
    if (this.fichaAtendimentoOdontologico.cidadao != null && this.fichaAtendimentoOdontologico.cidadao.sexo == 'FEMININO')
      return false;

    return true;
  }

  vigilanciaSaudeChecked(vigilancia) {
    if (this.fichaAtendimentoOdontologico.listVigilanciaSaudeBucal.findIndex(x => x.codigo == vigilancia.codigo) != -1)
      return true;

    return false;
  }

  vigilanciaSaudeChange(vigilancia) {
    let index = this.fichaAtendimentoOdontologico.listVigilanciaSaudeBucal.findIndex(x => x.codigo == vigilancia.codigo)
    if (index == -1) {
      this.fichaAtendimentoOdontologico.listVigilanciaSaudeBucal.push(vigilancia);
    }
    else {
      this.fichaAtendimentoOdontologico.listVigilanciaSaudeBucal.splice(index, 1);
    }
  }

  procedimentoChange(procedimento) {
    let index = this.fichaAtendimentoOdontologico.procedimentos.findIndex(x => x.procedimento != null && x.procedimento.id == procedimento.id)
    if (index == -1) {
      let procedimentoRealizado: ProcedimentoRealizado = new ProcedimentoRealizado();
      procedimentoRealizado.procedimento = procedimento;
      this.fichaAtendimentoOdontologico.procedimentos.push(procedimentoRealizado);
    }
    else {
      this.fichaAtendimentoOdontologico.procedimentos.splice(index, 1);
    }
  }

  procedimentoChecked(procedimento) {
    if (this.fichaAtendimentoOdontologico.procedimentos.findIndex(x => x.procedimento != null && x.procedimento.id == procedimento.id) != -1)
      return true;

    return false;
  }

  numProcedimento(procedimento, event) {
    let index = this.fichaAtendimentoOdontologico.procedimentos.findIndex(x => x.procedimento != null && x.procedimento.id == procedimento.id);
    this.fichaAtendimentoOdontologico.procedimentos[index].quantidade = event.path[0].value;
  }

  valueNumProcedimento(procedimento) {
    let index = this.fichaAtendimentoOdontologico.procedimentos.findIndex(x => x.procedimento != null && x.procedimento.id == procedimento.id);
    return this.fichaAtendimentoOdontologico.procedimentos[index].quantidade;
  }

  fornecimentoChecked(fornecimento) {
    if (this.fichaAtendimentoOdontologico.fornecimentos.findIndex(x => x.codigo == fornecimento.codigo) != -1)
      return true;

    return false;
  }

  fornecimentoChange(fornecimento) {
    let index = this.fichaAtendimentoOdontologico.fornecimentos.findIndex(x => x.codigo == fornecimento.codigo)
    if (index == -1) {
      this.fichaAtendimentoOdontologico.fornecimentos.push(fornecimento);
    }
    else {
      this.fichaAtendimentoOdontologico.fornecimentos.splice(index, 1);
    }
  }

  condutaChecked(conduta) {
    if (this.fichaAtendimentoOdontologico.condutas.findIndex(x => x.codigo == conduta.codigo) != -1)
      return true;

    return false;
  }

  condutaChange(conduta) {
    let index = this.fichaAtendimentoOdontologico.condutas.findIndex(x => x.codigo == conduta.codigo)
    if (index == -1) {
      this.fichaAtendimentoOdontologico.condutas.push(conduta);
    }
    else {
      this.fichaAtendimentoOdontologico.condutas.splice(index, 1);
    }
  }

  condutaDesfechoDisabled(codigo) {
    let compare = [1, 2];
    if (codigo == 15) {
      if (compare.findIndex(x => x == this.fichaAtendimentoOdontologico.tipoConsultaOdonto.codigo) == -1)
        return false;
      return true;
    }
    if (codigo == 17) {
      if (compare.findIndex(x => x == this.fichaAtendimentoOdontologico.tipoConsultaOdonto.codigo) == -1)
        return true;
      return false;
    }

    return true;
  }

  tipoConsulta(codigo) {
    if (this.fichaAtendimentoOdontologico.tipoAtendimento.codigo == 6) {
      if (codigo == 2)
        return false;
      return true;
    }
    return true;
  }

  validarCampoList() {
    if (this.fichaAtendimentoOdontologico.condutas == null || this.fichaAtendimentoOdontologico.condutas.findIndex(x => x.codigo == 16 || x.codigo == 12 || x.codigo == 13 ||
      x.codigo == 14 || x.codigo == 15 || x.codigo == 17) == -1)
      return false;

    if (this.fichaAtendimentoOdontologico.procedimentos.length != 0 && this.fichaAtendimentoOdontologico.procedimentos.findIndex(x => x.quantidade == 0 ||
      x.quantidade == null || x.quantidade > x.procedimento.max) != -1)
      return false;

    return true;
  }

  onFormSubmit() {
    if (this.form.valid && this.validarCampoList()) {
      this.fichaAtendimentoOdontologico.dataHorarioFinal = formatDate(new Date(), "yyyy-MM-dd'T'HH:mm:ss", 'en-US');
      this.onSubmitEventEmitter.emit(this.fichaAtendimentoOdontologico);
    }
  }

  searchProcedimentoChange(descricao: string) {
    let procedimentoFiltro = {
      texto: descricao,
      sexo: this.fichaAtendimentoOdontologico.cidadao.sexo == 'MASCULINO' ? true : false
    }
    this.procedimentoService.searchOdonto(procedimentoFiltro)
      .subscribe(
        data => this.opProcedimentos = data,
        error => {
          console.log(error);
          this.opProcedimentos = new Array;
        }
      )
  }

  salvarProced() {
    let index = this.fichaAtendimentoOdontologico.outrosProcedimentos.findIndex(x => x.proced.id == this.idProcedimentoSelecionado == null ? 
      this.procedimentoSelecionado.proced.id : this.idProcedimentoSelecionado);

    if (index == -1) 
      this.fichaAtendimentoOdontologico.outrosProcedimentos.push(this.procedimentoSelecionado);
    else 
      this.fichaAtendimentoOdontologico.outrosProcedimentos[index] = this.procedimentoSelecionado;

    this.page.content = this.fichaAtendimentoOdontologico.outrosProcedimentos;
    this.page.totalElements = this.fichaAtendimentoOdontologico.outrosProcedimentos.length;
    this.idProcedimentoSelecionado = null;
    this.procedimentoSelecionado = new ProcedimentoRealizado();
  }

  onEdit(proced: ProcedimentoRealizado) {
    this.procedimentoSelecionado = proced;
    this.idProcedimentoSelecionado = proced.proced.id;
  }

  onDelete(proced: ProcedimentoRealizado) {
    let index = this.fichaAtendimentoOdontologico.outrosProcedimentos.findIndex(x => x.proced.id == proced.proced.id);
    this.fichaAtendimentoOdontologico.outrosProcedimentos.splice(index, 1);
    this.page.content = this.fichaAtendimentoOdontologico.outrosProcedimentos;
    this.page.totalElements = this.fichaAtendimentoOdontologico.outrosProcedimentos.length;
  }

  getPage() {
    this.page.content = this.fichaAtendimentoOdontologico.outrosProcedimentos;
    this.page.totalElements = this.fichaAtendimentoOdontologico.outrosProcedimentos.length;
  }

  numProced(procedimento: Procedimento, event) {
    let index = this.fichaAtendimentoOdontologico.procedimentos.findIndex(x => x.proced != null && x.proced.id == procedimento.id);
    this.fichaAtendimentoOdontologico.procedimentos[index].quantidade = event.path[0].value;
  }

  valueNumProced(procedimento: Procedimento) {
    let index = this.fichaAtendimentoOdontologico.procedimentos.findIndex(x => x.proced != null && x.proced.id == procedimento.id);
    return this.fichaAtendimentoOdontologico.procedimentos[index].quantidade;
  }

}
