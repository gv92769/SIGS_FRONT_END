import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormAtendimentoOdontologicoComponent } from './form-atendimento-odontologico.component';

describe('FormAtendimentoOdontologicoComponent', () => {
  let component: FormAtendimentoOdontologicoComponent;
  let fixture: ComponentFixture<FormAtendimentoOdontologicoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormAtendimentoOdontologicoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormAtendimentoOdontologicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
