import { TestBed } from '@angular/core/testing';

import { AtendimentoOdontologicoService } from './atendimento-odontologico.service';

describe('AtendimentoOdontologicoService', () => {
  let service: AtendimentoOdontologicoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AtendimentoOdontologicoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
