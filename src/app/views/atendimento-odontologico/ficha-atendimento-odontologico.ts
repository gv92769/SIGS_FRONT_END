import { Profissional } from '../../core/model/profissional';
import { formatDate } from '@angular/common';
import { Cidadao } from '../cidadao/cidadao';
import { LocalAtendimento } from '../../core/model/local-atendimento';
import { TipoAtendimento } from '../../core/model/tipo-atendimento';
import { ProcedimentoRealizado } from './procedimento-realizado';
import { UnidadeBasicaSaude } from '../../core/model/unidade-basica-saude';
import { ObjectCodigoDescricao } from '../../core/model/object-codigo-descricao';
import { Cbo } from '../../core/model/cbo';

export class FichaAtendimentoOdontologico {
  id: number;
  uuid: string;
  ine: string;
  profissionalResponsavel: Profissional = new Profissional();
  ubs: UnidadeBasicaSaude = new UnidadeBasicaSaude();
  cbo: Cbo = new Cbo();
  data: string = formatDate(new Date(), 'yyyy-MM-dd', 'en-US');
  dataHorarioInicio: string;
  dataHorarioFinal: string;
  numProntuario: string;
  cidadao: Cidadao;
  peso: number;
  altura: number;
  turno: number;
  localAtendimento: LocalAtendimento = new LocalAtendimento();
  necessidadesEspeciais: boolean;
  gestante: boolean;
  tipoAtendimento: TipoAtendimento = new TipoAtendimento();
  tipoConsultaOdonto: ObjectCodigoDescricao = new ObjectCodigoDescricao();
  listVigilanciaSaudeBucal: ObjectCodigoDescricao[] = new Array;
  procedimentos: ProcedimentoRealizado[] = new Array;
  outrosProcedimentos: ProcedimentoRealizado[] = new Array;
  fornecimentos: ObjectCodigoDescricao[] = new Array;
  condutas: ObjectCodigoDescricao[] = new Array;
  importada: boolean;
}
