import { Component, OnInit, ViewChild } from '@angular/core';
import { FichaAtendimentoOdontologico } from '../ficha-atendimento-odontologico';
import { BehaviorSubject } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { AtendimentoOdontologicoService } from '../atendimento-odontologico.service';
import { map, mergeMap, finalize } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-editar-atendimento-odontologico',
  templateUrl: './editar-atendimento-odontologico.component.html',
  styleUrls: ['./editar-atendimento-odontologico.component.css']
})
export class EditarAtendimentoOdontologicoComponent implements OnInit {

  formData$: BehaviorSubject<FichaAtendimentoOdontologico> = new BehaviorSubject<FichaAtendimentoOdontologico>(new FichaAtendimentoOdontologico());

  @ViewChild('formAtendimentoOdontologico', { static: false }) formAtendimentoOdontologico: FichaAtendimentoOdontologico;

  isLoadingSubmit: boolean = false;

  importada: boolean = false;

  constructor(private route: ActivatedRoute, private atendimentoOdontologicoService: AtendimentoOdontologicoService, private toastrService: ToastrService,
    private router: Router) { }

  ngOnInit(): void {
    this.route.paramMap.pipe(
      map(params => {
        const id = +params.get('id');
        return id;
      }),
      mergeMap(id => {
        return this.atendimentoOdontologicoService.findById(id);
      }
      )
    )
      .subscribe({
        next: (response) => {
          this.importada = response.importada;
          this.formData$.next(response);
        },
        error: (err) => {
          console.log('Erro ao buscar Atendimento Odontológico');
          console.log(err);
        }
      });
  }

  onSubmitted(fichaAtendimentoOdontologico: FichaAtendimentoOdontologico) {
    if (this.importada == false) {
      this.isLoadingSubmit = true;

      this.atendimentoOdontologicoService.update(fichaAtendimentoOdontologico.id, fichaAtendimentoOdontologico)
        .pipe(finalize(() => this.isLoadingSubmit = false))
        .subscribe({
          next: (response) => {
            this.toastrService.success('Atendimento odontológico salvo com sucesso!', 'Sucesso');
            this.router.navigate(['atendimentoodontologico/adicionar']);
          },
          error: (err) => {
            this.toastrService.error('Error ao salvar atendimento odontológico.', 'Error');
            console.log(err);
          }
        });
    }
  }

}
