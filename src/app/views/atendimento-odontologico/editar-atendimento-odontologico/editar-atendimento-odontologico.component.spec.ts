import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarAtendimentoOdontologicoComponent } from './editar-atendimento-odontologico.component';

describe('EditarAtendimentoOdontologicoComponent', () => {
  let component: EditarAtendimentoOdontologicoComponent;
  let fixture: ComponentFixture<EditarAtendimentoOdontologicoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarAtendimentoOdontologicoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarAtendimentoOdontologicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
