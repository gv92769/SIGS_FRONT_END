import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PesquisarAtendimentoOdontologicoComponent } from './pesquisar-atendimento-odontologico.component';

describe('PesquisarAtendimentoOdontologicoComponent', () => {
  let component: PesquisarAtendimentoOdontologicoComponent;
  let fixture: ComponentFixture<PesquisarAtendimentoOdontologicoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PesquisarAtendimentoOdontologicoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PesquisarAtendimentoOdontologicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
