import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { AtendimentoOdontologicoService } from '../atendimento-odontologico.service';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { Router } from '@angular/router';
import { PageRequest } from '../../../core/model/page-request';
import { AtendimentoOdontologicoTabela } from '../atendimento-odontologico-tabela';
import { Page } from '../../../core/model/page';

@Component({
  selector: 'app-pesquisar-atendimento-odontologico',
  templateUrl: './pesquisar-atendimento-odontologico.component.html',
  styleUrls: ['./pesquisar-atendimento-odontologico.component.css']
})
export class PesquisarAtendimentoOdontologicoComponent implements OnInit {

  @ViewChild('form', { static: false }) form: FormGroup;

  @ViewChild('modalExcluir', { static: false }) modalExcluir: ModalDirective;

  atendimentoSelected: any;

  filter = {tipo: "Nome", valor: ""};

  pageRequest: PageRequest = new PageRequest();

  columns: any[];

  columnMode = ColumnMode;

  page: Page<AtendimentoOdontologicoTabela> = new Page();

  isLoading: boolean = false;

  isLoadingExcluir: boolean = false;

  constructor(private atendimentoOdontologicoService: AtendimentoOdontologicoService, private router: Router) { }

  ngOnInit(): void {
    this.createTable();
  }

  createTable() {
    this.columns = [
      { name: 'Data', sortable: true },
      { name: 'Nome', sortable: false },
      { name: 'Tipo Atendimento', sortable: false },
      { name: 'CPF', sortable: false },
      { name: 'CNS', sortable: false },
    ];
  }

  onEdit(data: any) {
    this.router.navigate(['atendimentoodontologico/editar/',data.id]);
  }

  onDeleteConfirm(data: any) {
    this.atendimentoSelected = data;
    this.modalExcluir.show();
  }

  delete() {
    this.isLoadingExcluir = true;

    this.atendimentoOdontologicoService.delete(this.atendimentoSelected.id).subscribe(
      response => this.search(),
      error => console.log('erro ao excluir atendimento odontológico'),
      () => {
        this.modalExcluir.hide();
        this.isLoadingExcluir = false;
        this.atendimentoSelected = null;
      }
    );
  }

  setPage(event) {
    this.page.pageNumber = event.offset;
    this.search();
  }

  onSort(event) {
    this.pageRequest.column = event.sorts[0].prop;
    this.pageRequest.sort = event.sorts[0].dir;
    this.search();
  }

  onSubmit() {
    this.page = new Page();
    this.pageRequest.column = null;
    this.pageRequest.sort = null;
    this.search();
  }

  search() {
    this.pageRequest.pageNumber = this.page.pageNumber;
    this.pageRequest.size = this.page.size;
    this.isLoading = true;

    if (this.filter.tipo == 'Nome') {
      this.atendimentoOdontologicoService.findAllByNome(this.filter.valor, this.pageRequest)
      .subscribe({
        next: (response) => {
          this.page.content = response.content;
          this.page.totalElements = response.totalElements;
          this.isLoading = false;
        },
        error: (err) => {
          console.log('erro ao pesquisar atendimento odontológico');
          console.log(err);
          this.isLoading = false;
        }
      });
    } else {
      this.atendimentoOdontologicoService.findAllByCpf(this.filter.valor, this.pageRequest)
      .subscribe({
        next: (response) => {
          this.page.content = response.content;
          this.page.totalElements = response.totalElements;
          this.isLoading = false;
        },
        error: (err) => {
          console.log('erro ao pesquisar atendimento odontológico');
          console.log(err);
          this.isLoading = false;
        }
      });
    }

  }

}
