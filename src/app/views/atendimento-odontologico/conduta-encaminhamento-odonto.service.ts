import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ObjectCodigoDescricao } from '../../core/model/object-codigo-descricao';

@Injectable({
  providedIn: 'root'
})
export class CondutaEncaminhamentoOdontoService {

  readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/contudaencaminhamentoodonto';
  }

  public findAllById(ids: number[]): Observable<ObjectCodigoDescricao[]> {
    return this.http.post<ObjectCodigoDescricao[]>(this.url+"/findallbyid", ids);
  }

}
