import { TestBed } from '@angular/core/testing';

import { ProcedimentoOdontoService } from './procedimento-odonto.service';

describe('ProcedimentoOdontoService', () => {
  let service: ProcedimentoOdontoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProcedimentoOdontoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
