import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { FichaAtendimentoOdontologico } from './ficha-atendimento-odontologico';
import { Observable } from 'rxjs';
import { PageRequest } from '../../core/model/page-request';
import { Page } from '../../core/model/page';
import { AtendimentoOdontologicoTabela } from './atendimento-odontologico-tabela';

@Injectable({
  providedIn: 'root'
})
export class AtendimentoOdontologicoService {

  readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/fichaatendimentoodontologico';
  }

  public findById(id: number): Observable<FichaAtendimentoOdontologico> {
    return this.http.get<FichaAtendimentoOdontologico>(this.url + '/' + id);
  }

  public create(fichaAtendimentoOdontologico: FichaAtendimentoOdontologico): Observable<FichaAtendimentoOdontologico> {
    return this.http.post<FichaAtendimentoOdontologico>(this.url, fichaAtendimentoOdontologico);
  }

  public update(id: number, fichaAtendimentoOdontologico: FichaAtendimentoOdontologico): Observable<FichaAtendimentoOdontologico> {
    return this.http.put<FichaAtendimentoOdontologico>(this.url+"/"+id, fichaAtendimentoOdontologico);
  }

  public delete(id: number) {
    return this.http.delete(this.url + '/' + id);
  }

  public findAllByNome(nome: string, pageRequest: PageRequest): Observable<Page<AtendimentoOdontologicoTabela>> {
    return this.http.post<Page<AtendimentoOdontologicoTabela>>(this.url + '/findallbynome/' + nome, pageRequest);
  }

  public findAllByCpf(cpf: string, pageRequest: PageRequest): Observable<Page<AtendimentoOdontologicoTabela>> {
    return this.http.post<Page<AtendimentoOdontologicoTabela>>(this.url + '/findallbycpf/' + cpf, pageRequest);
  }

}
