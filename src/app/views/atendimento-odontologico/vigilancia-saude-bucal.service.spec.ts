import { TestBed } from '@angular/core/testing';

import { VigilanciaSaudeBucalService } from './vigilancia-saude-bucal.service';

describe('VigilanciaSaudeBucalService', () => {
  let service: VigilanciaSaudeBucalService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VigilanciaSaudeBucalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
