import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdicionarAtendimentoOdontologicoComponent } from './adicionar-atendimento-odontologico/adicionar-atendimento-odontologico.component';
import { EditarAtendimentoOdontologicoComponent } from './editar-atendimento-odontologico/editar-atendimento-odontologico.component';
import { FormAtendimentoOdontologicoComponent } from './form-atendimento-odontologico/form-atendimento-odontologico.component';
import { IConfig, NgxMaskModule } from 'ngx-mask';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AtendimentoOdontologicoRoutingModule } from './atendimento-odontologico-routing.module';
import { CustomComponentsModule } from '../../custom-components/custom-components.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { ModalModule } from 'ngx-bootstrap/modal';
import { SelectDropDownModule } from 'ngx-select-dropdown'
import { PesquisarCidadaoBiometriaModule } from '../cidadao/pesquisar-cidadao-biometria/pesquisar-cidadao-biometria.module';
import { PesquisarAtendimentoOdontologicoComponent } from './pesquisar-atendimento-odontologico/pesquisar-atendimento-odontologico.component';

export const options: Partial<IConfig> | (() => Partial<IConfig>) = { validation: false };

@NgModule({
  declarations: [
    AdicionarAtendimentoOdontologicoComponent,
    EditarAtendimentoOdontologicoComponent,
    FormAtendimentoOdontologicoComponent,
    PesquisarAtendimentoOdontologicoComponent
  ],
  imports: [
    CommonModule,
    AtendimentoOdontologicoRoutingModule,
    FormsModule,
    TabsModule,
    AccordionModule.forRoot(),
    CustomComponentsModule,
    ModalModule.forRoot(),
    PesquisarCidadaoBiometriaModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot(options),
    NgxDatatableModule,
    SelectDropDownModule
  ]
})
export class AtendimentoOdontologicoModule { }
