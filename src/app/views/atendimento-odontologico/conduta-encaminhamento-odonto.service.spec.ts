import { TestBed } from '@angular/core/testing';

import { CondutaEncaminhamentoOdontoService } from './conduta-encaminhamento-odonto.service';

describe('CondutaEncaminhamentoOdontoService', () => {
  let service: CondutaEncaminhamentoOdontoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CondutaEncaminhamentoOdontoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
