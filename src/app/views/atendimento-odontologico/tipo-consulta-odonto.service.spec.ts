import { TestBed } from '@angular/core/testing';

import { TipoConsultaOdontoService } from './tipo-consulta-odonto.service';

describe('TipoConsultaOdontoService', () => {
  let service: TipoConsultaOdontoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TipoConsultaOdontoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
