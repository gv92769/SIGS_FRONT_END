import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { ObjectCodigoDescricao } from '../../core/model/object-codigo-descricao';

@Injectable({
  providedIn: 'root'
})
export class VigilanciaSaudeBucalService {

  readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/vigilanciasaudebucal';
  }

  public findAll(): Observable<ObjectCodigoDescricao[]> {
    return this.http.get<ObjectCodigoDescricao[]>(this.url);
  }

}
