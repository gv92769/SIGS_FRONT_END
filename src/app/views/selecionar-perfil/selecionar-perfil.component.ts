import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../auth/auth.service';
import { Config } from '../../core/model/config';
import { Lotacao } from '../../core/model/lotacao';
import { LotacaoDto } from '../../core/model/lotacao-dto';
import { LotacaoService } from '../../core/service/lotacao.service';
import { UsuarioService } from '../usuario/usuario.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './selecionar-perfil.component.html',
  styleUrls: ['./selecionar-perfil.component.css']
})
export class SelecionarPerfilComponent implements OnInit {

  lotacao: LotacaoDto = new LotacaoDto();

  config: Config = new Config();

  lotacoes: Lotacao[] = new Array;

  isLoading: boolean = false;

  constructor(private lotacaoService: LotacaoService, private usuarioService: UsuarioService, private authService: AuthService, private router: Router) {}

  ngOnInit() {
    this.config.placeholder = 'Lotação';
    this.config.displayKey = 'descricao';
    this.config.searchOnKey = 'descricao';

    this.usuarioService.getCurrentUserInfo()
      .subscribe(
        userDetails => {
          this.lotacaoService.searchPerfil(userDetails.id)
            .subscribe(
              data => this.lotacoes = data,
              error => console.log(error)
            )
        },
        error => console.log(error)
      );
  }

  onSubmit() {
    this.isLoading = true;
    this.lotacao.id = this.lotacao.lotacao.id;

    this.authService.lotacao(this.lotacao).subscribe(
      data => {
        this.router.navigate(['/']);
      },
      error => {
        this.isLoading = false;
        console.log(error);
      }
    );
  }

}
