import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { PesquisarUsuarioComponent } from './pesquisar-usuario/pesquisar-usuario.component';
import { EditarUsuarioComponent } from "./editar-usuario/editar-usuario.component";
import { AdicionarUsuarioComponent } from './adicionar-usuario/adicionar-usuario.component';

import { CustomComponentsModule } from '../../custom-components/custom-components.module';

import { ModalModule } from 'ngx-bootstrap/modal';

import { UsuarioRoutingModule } from './usuario-routing.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SelectDropDownModule } from 'ngx-select-dropdown';
import { LotacaoComponent } from './lotacao/lotacao.component'

@NgModule({
  imports: [
    CommonModule,
    UsuarioRoutingModule,
    FormsModule,
    CustomComponentsModule,
    ModalModule.forRoot(),
    NgxDatatableModule,
    SelectDropDownModule
  ],
  declarations: [
    PesquisarUsuarioComponent,
    EditarUsuarioComponent,
    AdicionarUsuarioComponent,
    LotacaoComponent
  ]
})
export class UsuarioModule { }
