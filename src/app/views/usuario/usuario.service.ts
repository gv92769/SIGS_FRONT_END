import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Usuario } from './usuario';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
import { UsuarioProfissional } from './usuario-profissional';
import { Page } from '../../core/model/page';
import { UsuarioFilter } from './usuario-filter';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/usuarios';
  }

  public getCurrentUserInfo(): Observable<any>{
    return this.http.get<any>(environment.apiBaseUrl + '/info/me');
  }
  
  public search(filter: UsuarioFilter) : Observable<Page<Usuario>> {
    return this.http.post<Page<Usuario>>(this.url+"/search", filter);
  }

  public findOne(id): Observable<Usuario> {
    return this.http.get<Usuario>(this.url+'/'+id);
  }

  public delete(id){
    return this.http.post(this.url+'/delete',id);
  }

  public create(usuarioProfissional: UsuarioProfissional): Observable<UsuarioProfissional> {
    return this.http.post<UsuarioProfissional>(this.url, usuarioProfissional);
  }

  public update(id, usuario: UsuarioProfissional): Observable<UsuarioProfissional> {
    return this.http.put<UsuarioProfissional>(this.url+'/'+id, usuario);
  }

}
