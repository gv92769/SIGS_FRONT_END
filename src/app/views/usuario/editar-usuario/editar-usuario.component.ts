import { Component, OnInit } from '@angular/core';
import { AdicionarUsuarioComponent } from '../adicionar-usuario/adicionar-usuario.component';
import { Router } from '@angular/router';
import { UsuarioService } from '../usuario.service';
import { ActivatedRoute } from '@angular/router';
import { CidadaoService } from '../../cidadao/cidadao.service';
import { ProfissionalService } from '../../../core/service/profissional.service';
import { ToastrService } from 'ngx-toastr';
import { ValidarDocumentoService } from '../../../core/service/validar-documento.service';

@Component({
  templateUrl: '../adicionar-editar-usuario.component.html'
})
export class EditarUsuarioComponent extends AdicionarUsuarioComponent implements OnInit {

  constructor(usuarioService: UsuarioService, router: Router, private route: ActivatedRoute, cidadaoService: CidadaoService,
    private profissionalService: ProfissionalService, toastrService: ToastrService, validarDocumentoService: ValidarDocumentoService) {
      super(usuarioService, router, cidadaoService, toastrService, validarDocumentoService);
      
      this.isEditar = true;
      
      this.textoBotaoSubmit = "Salvar";
      
      this.showSenha = false;
      
      this.tituloPagina = "Editar Usuário";
  }

  ngOnInit() {
    super.ngOnInit();
    
    this.route.paramMap.subscribe(
      params => {
        let selectedId = +params.get('id');
        this.usuarioService.findOne(selectedId).subscribe(
          data => this.usuario = data,
          error => console.log("error in retriveing user with id " + selectedId)
        );

        this.profissionalService.findById(selectedId).subscribe(
          data => {
            this.profissional = data
          },
          error => console.log("error ao buscar profissional por id" + selectedId)
        )
      }
    );
  }

  onSubmit(){
    this.isLoading = true;
    this.usuarioProfissional.usuario = this.usuario;
    this.usuarioProfissional.profissional = this.profissional;
    
    let id = this.usuario.id;
    this.usuarioService.update(id, this.usuarioProfissional).subscribe(
      data => {
        this.toastrService.success('Usuário salvo com sucesso!', 'Sucesso');
        this.router.navigate(['/usuario/pesquisar']);
      },
      error => {
        this.toastrService.error('Error ao salvar usuário.', 'Error');
        console.log(error);
      },
      () => this.isLoading = false
    );
  }

}
