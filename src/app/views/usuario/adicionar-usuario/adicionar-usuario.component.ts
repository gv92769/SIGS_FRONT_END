import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Usuario } from '../usuario';
import { UsuarioProfissional } from '../usuario-profissional';
import { UsuarioService } from '../usuario.service';
import { Profissional } from '../../../core/model/profissional';
import { CidadaoService } from '../../cidadao/cidadao.service';
import { ToastrService } from 'ngx-toastr';
import { ValidarDocumentoService } from '../../../core/service/validar-documento.service';

@Component({
  templateUrl: '../adicionar-editar-usuario.component.html'
})
export class AdicionarUsuarioComponent implements OnInit{

  tituloPagina = "Adicionar Usuário";

  usuario: Usuario;

  profissional: Profissional;

  showSenha = true;

  isEditar = false;

  textoBotaoSubmit = "Cadastrar";

  opcoesAtivo = [
    {name: "Sim", value: true},
    {name: "Não", value: false}
  ];
  
  opcoesEstados: any[] = new Array;

  isLoading: boolean = false;

  usuarioProfissional: UsuarioProfissional;

  cnsValido: boolean = true;

  cpfValido: boolean = true;

  constructor(protected usuarioService: UsuarioService, protected router: Router, protected cidadaoService: CidadaoService, 
    protected toastrService: ToastrService, protected validarDocumentoService: ValidarDocumentoService) {
    this.usuario = new Usuario();
    this.profissional = new Profissional();
    this.usuarioProfissional = new UsuarioProfissional();
  }

  ngOnInit() {
    this.cidadaoService.findAllOpcoesEstado().subscribe(data => this.opcoesEstados = data);
  }

  onSubmit(){
    this.isLoading = true;
    this.usuarioProfissional.usuario = this.usuario;
    this.usuarioProfissional.profissional = this.profissional;

    this.usuarioService.create(this.usuarioProfissional).subscribe(
      data => {
        this.toastrService.success('Usuário salvo com sucesso!', 'Sucesso');
        this.router.navigate(['/usuario/pesquisar']);
      },
      error => {
        this.toastrService.error('Error ao salvar usuário.', 'Error');
        console.log(error);
      },
      () => this.isLoading = false
    );
  }

  validaCns() {
    this.validarDocumentoService.validaCns(this.profissional.id != null ? this.profissional.id : 0, this.profissional.cns, 'PROFISSIONAL').subscribe(
      data => {
        if (data == false) {
          this.profissional.cns = null;
          this.cnsValido = false
        } else {
          this.cnsValido = true;
        }
      },
      error => {
        console.log(error);
      }
    )
  }

  validaCpf() {
    this.validarDocumentoService.validaCpf(this.profissional.id != null ? this.profissional.id : 0, this.profissional.cpf, 'PROFISSIONAL').subscribe(
      data => {
        if (data == false) {
          this.profissional.cpf = null;
          this.cpfValido = false
        } else {
          this.cpfValido = true;
        }
      },
      error => {
        console.log(error);
      }
    )
  }

}
