import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { UsuarioService } from '../usuario.service';
import { Usuario } from '../usuario';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Page } from '../../../core/model/page';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { UsuarioFilter } from '../usuario-filter';

@Component({
  templateUrl: './pesquisar-usuario.component.html'
})
export class PesquisarUsuarioComponent implements OnInit {

  @ViewChild('modalExcluir', { static: false }) modalExcluir: ModalDirective;

  usuarios: Usuario[];

  usuarioExcluir: Usuario;

  isLoadingExcluir: boolean = false;

  columns: any[];

  columnMode = ColumnMode;

  page: Page<Usuario> = new Page();

  filter: UsuarioFilter = new UsuarioFilter();

  constructor(private usuarioService : UsuarioService, private router: Router) { }

  ngOnInit() {
    this.createTable();
  }

  createTable() {
    this.columns = [
      { name: 'Id', sortable: true },
      { name: 'Nome', sortable: true },
      { name: 'Sobrenome', sortable: true },
      { name: 'Login', sortable: true },
      { name: 'Ativo', sortable: true }
    ];
  }

  onSubmit(){
    this.page = new Page();
    this.filter.column = null;
    this.filter.sort = null;
    this.search();
  }

  setPage(event) {
    this.page.pageNumber = event.offset;
    this.search();
  }

  onSort(event) {
    this.filter.column = event.sorts[0].prop;
    this.filter.sort = event.sorts[0].dir;
    this.search();
  }

  search(){
    this.filter.pageNumber = this.page.pageNumber;
    this.filter.size = this.page.size;

    this.usuarioService.search(this.filter)
      .subscribe(
        data => {
          this.page.content = data.content;
          this.page.totalElements = data.totalElements;
        },
        error => console.log("erro ao obter usuarios: " + error)
      );
  }

  onEditUsuario(id: number){
    this.router.navigate(['/usuario/editar/',id]);
  }

  onDeleteUsuario(usuario){
    this.usuarioExcluir = usuario;
    this.modalExcluir.show();
  }

  deleteUsuario(){
    this.isLoadingExcluir = true;
    this.usuarioExcluir.ativo = false;

    this.usuarioService.delete(this.usuarioExcluir.id).subscribe(
      response => this.search(),
      error => console.log('erro ao excluir usuário'),
      () => {this.isLoadingExcluir = false; this.modalExcluir.hide(); this.usuarioExcluir = null;}
    );
  }

  onLotacaoUsuario(id: number) {
    this.router.navigate(['/usuario/lotacao/',id]);
  }

}
