import { Lotacao } from "../../core/model/lotacao";

export class Usuario {
	id: number;
	nome: string;
	sobrenome: string;
	email: string;
	login: string;
	senha: string;
	ativo: boolean;
	lotacoes: Lotacao[] = new Array;
}
