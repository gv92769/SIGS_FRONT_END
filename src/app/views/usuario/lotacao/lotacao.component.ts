import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { Lotacao } from '../../../core/model/lotacao';
import { LotacaoService } from '../../../core/service/lotacao.service';
import { Page } from '../../../core/model/page';
import { UnidadeBasicaSaudeService } from '../../../core/service/unidade-basica-saude.service';
import { UnidadeBasicaSaude } from '../../../core/model/unidade-basica-saude';
import { Perfil } from '../../../core/model/perfil';
import { Cbo } from '../../../core/model/cbo';
import { Config } from '../../../core/model/config';
import { PerfilService } from '../../../core/service/perfil.service';
import { CboService } from '../../../core/service/cbo.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-lotacao',
  templateUrl: './lotacao.component.html',
  styleUrls: ['./lotacao.component.css']
})
export class LotacaoComponent implements OnInit {

  @ViewChild('modalExcluir', { static: false }) modalExcluir: ModalDirective;

  isLoadingExcluir: boolean = false;

  id: number;

  idLotacao: number;

  page: Page<any> = new Page<any>();

  columns: any[];

  columnMode = ColumnMode;

  lotacao: Lotacao = new Lotacao();

  unidades: UnidadeBasicaSaude[] = new Array;

  perfis: Perfil[] = new Array;

  optionsOcupacao: Cbo[] = new Array;

  config: Config = new Config();

  configUbs: Config = new Config();

  configPerfil: Config = new Config();

  constructor(private lotacaoService: LotacaoService, private ubsService: UnidadeBasicaSaudeService, private cboService: CboService,
    private perfilService: PerfilService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.columns = [
      { name: 'Unidade', sortable: true },
      { name: 'Ocupacao', sortable: true },
      { name: 'Nome Perfil', sortable: true},
      { name: 'Ativo', sortable: true }
    ];

    this.route.paramMap.subscribe(
      params => {
        this.id = +params.get('id');
      }
    );

    this.perfilService.findAllByAtivo()
      .subscribe(
        data => this.perfis = data,
        error => console.log(error)
      );

    this.config.placeholder = 'Selecione a Ocupação do Profissional';
    this.config.displayKey = 'titulo';
    this.config.searchOnKey = 'titulo';

    this.configUbs.placeholder = 'Vincule o Profissional a uma ou mais Ubs';
    this.configUbs.displayKey = 'nome';
    this.configUbs.searchOnKey = 'nome';

    this.configPerfil.placeholder = "Selecione o perfil do usuário";
    this.configPerfil.displayKey = 'descricao';
    this.configPerfil.searchOnKey = 'descricao';
  }

  onSubmit() {
    if (this.lotacao.id == null) {
      this.lotacao.profissional.id = this.id;
      
      this.lotacaoService.save(this.lotacao)
        .subscribe({
          next: (data) => {
            this.lotacao = new Lotacao();
            this.onSearch();
          },
          error: (err) => {
            console.log(err);
          }
        });
    } else {
      this.lotacaoService.update(this.lotacao.id, this.lotacao)
        .subscribe({
          next: (data) => {
            this.lotacao = new Lotacao();
            this.onSearch();
          },
          error: (err) => {
            console.log(err);
          }
        });
    }
  }

  findById(id: number) {
    this.lotacaoService.findById(id)
      .subscribe({
        next: (data) => {
          this.lotacao = data;
        },
        error: (err) => {
          console.log(err);
          this.lotacao = new Lotacao();
        }
      });
  }

  onSearch() {
    this.lotacaoService.search(this.id)
      .subscribe({
        next: (data) => {
          this.page.content = data;
          this.page.totalElements = data.length;
        },
        error: (err) => {
          console.log(err);
          this.page.content = [];
          this.page.totalElements = 0;
        }
      });
  }

  onDelete(id: number){
    this.idLotacao = id;
    this.modalExcluir.show();
  }

  onEdit(id: number) {
    this.lotacaoService.findById(id)
      .subscribe(
        data => this.lotacao = data,
        error => console.log(error)
      )
  }

  delete(){
    this.isLoadingExcluir = true;

    this.lotacaoService.delete(this.idLotacao).subscribe(
      response => this.onSearch(),
      error => console.log('erro ao excluir Lotação' + error),
      () => {this.isLoadingExcluir = false; this.modalExcluir.hide(); this.idLotacao = null;}
    );
  }

  limpar() {
    this.lotacao = new Lotacao();
  }

  searchUbsChange(nome: string) {
    this.ubsService.findAllByNome(nome)
      .subscribe({
        next: (data) => {
          this.unidades = data;
        },
        error: (err) => {
          console.log(err);
          this.unidades = new Array;
        }
      });
  }

  searchOcupacaoChange(titulo: string) {
    this.cboService.findAllByTituloByPerfil(titulo, this.lotacao.perfil.id)
      .subscribe(
        data => this.optionsOcupacao = data,
        error => console.log(error)
      );
  }

}
