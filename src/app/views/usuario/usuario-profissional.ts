import { Usuario } from './usuario';
import { Profissional } from '../../core/model/profissional';

export class UsuarioProfissional {
    usuario: Usuario = new Usuario();
    profissional: Profissional = new Profissional();
}
