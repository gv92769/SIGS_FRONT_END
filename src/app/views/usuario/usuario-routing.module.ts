import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PesquisarUsuarioComponent } from './pesquisar-usuario/pesquisar-usuario.component';
import { EditarUsuarioComponent } from "./editar-usuario/editar-usuario.component";
import { AdicionarUsuarioComponent } from './adicionar-usuario/adicionar-usuario.component';
import { LotacaoComponent } from './lotacao/lotacao.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Usuário'
    },
    children: [
      {
        path: '',
        redirectTo: 'pesquisar'
      },
      {
        path: 'pesquisar',
        component: PesquisarUsuarioComponent,
        data: {
          title: 'Pesquisar'
        }
      },
      {
        path: 'editar/:id',
        component: EditarUsuarioComponent,
        data: {
          title: 'Editar'
        }
      },
      {
        path: 'adicionar',
        component: AdicionarUsuarioComponent,
        data: {
          title: 'Adicionar'
        }
      },
      {
        path: 'lotacao/:id',
        component: LotacaoComponent,
        data: {
          title: 'Lotação'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsuarioRoutingModule {}
