import { ObjectCodigoDescricao } from '../../core/model/object-codigo-descricao';

export class FormAtividadeColetivaDto {
    atividades: ObjectCodigoDescricao[] = new Array;
	temasReuniao: ObjectCodigoDescricao[] = new Array;
	publicos: ObjectCodigoDescricao[] = new Array;
	temasSaude: ObjectCodigoDescricao[] = new Array;
	praticas: ObjectCodigoDescricao[] = new Array;
}
