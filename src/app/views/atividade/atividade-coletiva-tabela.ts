import { formatDate } from '@angular/common';

export class AtividadeColetivaTabela {
  id: number;
  data: string = formatDate(new Date(), 'yyyy-MM-dd', 'en-US');
  tipoAtividade: string;
  localidade: string;
  pesquisaTipo: string;
}
