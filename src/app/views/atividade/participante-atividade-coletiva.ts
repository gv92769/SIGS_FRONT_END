import { Cidadao } from '../cidadao/cidadao';

export class ParticipanteAtividadeColetiva {
  id: number;
  cidadao: Cidadao;
  altura: number;
  peso: number;
  cessouHabitoFumar: boolean;
  abandonouGrupo: boolean;
  avaliacaoAlterada: boolean;
}
