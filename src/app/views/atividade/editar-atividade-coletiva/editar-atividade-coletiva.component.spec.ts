import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarAtividadeColetivaComponent } from './editar-atividade-coletiva.component';

describe('EditarAtividadeColetivaComponent', () => {
  let component: EditarAtividadeColetivaComponent;
  let fixture: ComponentFixture<EditarAtividadeColetivaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarAtividadeColetivaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarAtividadeColetivaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
