import { Component, OnInit, ViewChild } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { AtividadeColetiva } from '../atividade-coletiva';
import { FormAtividadeColetivaComponent } from '../atividade-coletiva/form-atividade-coletiva/form-atividade-coletiva.component';
import { ActivatedRoute, Router } from '@angular/router';
import { AtividadeColetivaService } from '../atividade-coletiva.service';
import { map, mergeMap, finalize } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { UsuarioService } from '../../usuario/usuario.service';

@Component({
  selector: 'app-editar-atividade-coletiva',
  templateUrl: './editar-atividade-coletiva.component.html',
  styleUrls: ['./editar-atividade-coletiva.component.scss']
})
export class EditarAtividadeColetivaComponent implements OnInit {

  formData$: BehaviorSubject<AtividadeColetiva> = new BehaviorSubject<AtividadeColetiva>(new AtividadeColetiva());

  @ViewChild('formAtividadeColetiva', { static: false }) formAtividadeColetiva: FormAtividadeColetivaComponent;

  isLoadingSubmit: boolean = false;

  importada: boolean;

  profissional: boolean = false;

  constructor(private route: ActivatedRoute, private atividadeColetivaService: AtividadeColetivaService, private toastrService: ToastrService,
    private router: Router, private usuarioService: UsuarioService) { }

  ngOnInit() {
    this.route.paramMap.pipe(
      map(params => {
        const id = +params.get('id');
        return id;
      }),
      mergeMap(id => {
        return this.atividadeColetivaService.findById(id);
      }
      )
    )
      .subscribe({
        next: (response) => {
          this.importada = response.importada;
          this.usuarioService.getCurrentUserInfo().subscribe(
            data => this.profissional = data.id == response.profissionalResponsavel.id ? false : true,
            error => console.log(error)
          );
          this.formData$.next(response);
        },
        error: (err) => {
          console.log('Erro ao busca Atividade Coletiva');
          console.log(err);
        }
      });
  }

  onSubmitted(atividadeColetiva: AtividadeColetiva) {
    if (this.importada == false) {
      this.isLoadingSubmit = true;

      this.atividadeColetivaService.update(atividadeColetiva.id, atividadeColetiva)
        .pipe(finalize(() => this.isLoadingSubmit = false))
        .subscribe({
          next: (response) => {
            this.toastrService.success('Atividade coletiva salva com sucesso!', 'Sucesso');
            this.router.navigate(['atividadecoletiva/adicionar']);
          },
          error: (err) => {
            this.toastrService.error('Error ao salvar atividade coletiva.', 'Error');
            console.log(err);
          }
        });
    }
  }

}
