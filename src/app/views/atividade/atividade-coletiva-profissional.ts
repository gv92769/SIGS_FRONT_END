import { Profissional } from '../../core/model/profissional';
import { Cbo } from '../../core/model/cbo';

export class AtividadeColetivaProfissional {
    id: number;
    profissional: Profissional;
    cbo: Cbo;
}
