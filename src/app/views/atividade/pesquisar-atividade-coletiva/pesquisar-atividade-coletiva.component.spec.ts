import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PesquisarAtividadeColetivaComponent } from './pesquisar-atividade-coletiva.component';

describe('PesquisarAtividadeColetivaComponent', () => {
  let component: PesquisarAtividadeColetivaComponent;
  let fixture: ComponentFixture<PesquisarAtividadeColetivaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PesquisarAtividadeColetivaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PesquisarAtividadeColetivaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
