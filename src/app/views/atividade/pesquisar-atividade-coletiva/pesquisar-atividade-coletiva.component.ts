import { Component, OnInit, ViewChild } from '@angular/core';
import { PublicoAlvoService } from '../publico-alvo.service';
import { FormGroup } from '@angular/forms';
import { AtividadeColetivaFilter } from '../atividade-coletiva-filter';
import { AtividadeColetivaService } from '../atividade-coletiva.service';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Page } from '../../../core/model/page';
import { AtividadeColetivaTabela } from '../atividade-coletiva-tabela';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { ObjectCodigoDescricao } from '../../../core/model/object-codigo-descricao';
import * as jwt_decode from 'jwt-decode';
import { UsuarioService } from '../../usuario/usuario.service';

@Component({
  selector: 'app-pesquisar-atividade-coletiva',
  templateUrl: './pesquisar-atividade-coletiva.component.html',
  styleUrls: ['./pesquisar-atividade-coletiva.component.scss']
})
export class PesquisarAtividadeColetivaComponent implements OnInit {

  @ViewChild('form', { static: false }) form: FormGroup;

  @ViewChild('modalExcluir', { static: false }) modalExcluir: ModalDirective;

  atividadeSelected: any;

  filter: AtividadeColetivaFilter = new AtividadeColetivaFilter();

  publicos: ObjectCodigoDescricao[] = new Array;

  columns: any[];

  columnMode = ColumnMode;

  isLoading: boolean = false;

  isLoadingExcluir: boolean = false;

  page: Page<AtividadeColetivaTabela> = new Page();

  idProfissional: number;

  constructor(private publicoAlvoService: PublicoAlvoService, private atividadeColetivaService: AtividadeColetivaService, private router: Router,
    private usuarioService: UsuarioService) { }

  ngOnInit() {
    this.publicoAlvoService.findAll().subscribe(
      data => this.publicos = data,
      error => console.log("error in findAllPublicoAlvo()")
    );

    this.createTable();

    this.filter.ubs = jwt_decode( sessionStorage.getItem('access_token')).cnes;

    this.usuarioService.getCurrentUserInfo().subscribe(
      data => this.idProfissional = data.id,
      error => console.log(error)
    );
  }

  createTable() {
    this.columns = [
      { name: 'Data', sortable: true },
      { name: 'Tipo Atividade', sortable: false },
      { name: 'Localidade', sortable: false }
    ];
  }

  limpar() {
    this.filter = new AtividadeColetivaFilter();
    this.filter.ubs = jwt_decode( sessionStorage.getItem('access_token')).cnes;
  }

  onEdit(data: any) {
    this.router.navigate(['atividadecoletiva/editar/', data.id]);
  }

  onDeleteConfirm(data: any) {
    this.atividadeSelected = data;
    this.modalExcluir.show();
  }

  delete() {
    this.isLoadingExcluir = true;

    this.atividadeColetivaService.delete(this.atividadeSelected.id).subscribe(
      response => this.search(),
      error => console.log('erro ao excluir atividade coletiva'),
      () => {
        this.modalExcluir.hide();
        this.isLoadingExcluir = false;
        this.atividadeSelected = null;
      }
    );
  }

  setPage(event) {
    this.page.pageNumber = event.offset;
    this.search();
  }

  onSort(event) {
    this.filter.column = event.sorts[0].prop;
    this.filter.sort = event.sorts[0].dir;
    this.search();
  }

  onSubmit() {
    this.page = new Page();
    this.filter.column = null;
    this.filter.sort = null;
    this.search();
  }

  verificarCampo() {
    this.filter.inep = this.filter.inep != null && this.filter.inep.toString() == '' ? null : this.filter.inep;
    this.filter.cnes = this.filter.cnes == '' ? null : this.filter.cnes;
    this.filter.escola = this.filter.escola == true ? true : false
  }

  search() {
    this.filter.pageNumber = this.page.pageNumber;
    this.filter.size = this.page.size;
    this.isLoading = true;

    this.verificarCampo();
    this.atividadeColetivaService.search(this.filter)
      .subscribe({
        next: (response) => {
          this.page.content = response.content;
          this.page.totalElements = response.totalElements;
          this.isLoading = false;
        },
        error: (err) => {
          console.log('erro ao pesquisar atividade coletiva');
          console.log(err);
          this.isLoading = false;
        }
      });
  }

}
