import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { OutroProcedimentoColetivo } from './outro-procedimento-coletivo';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OutroProcedimentoColetivoService {

  readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/outroprocedimentocoletivo';
  }

  public findBySigtap(sigtap: string): Observable<OutroProcedimentoColetivo> {
    return this.http.get<OutroProcedimentoColetivo>(this.url + "/sigtap/" + sigtap);
  }

}
