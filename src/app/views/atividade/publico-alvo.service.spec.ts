import { TestBed } from '@angular/core/testing';

import { PublicoAlvoService } from './publico-alvo.service';

describe('PublicoAlvoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PublicoAlvoService = TestBed.get(PublicoAlvoService);
    expect(service).toBeTruthy();
  });
});
