import { Component, OnInit, ViewChild, Output, EventEmitter, Input } from '@angular/core';
import { AtividadeColetiva } from '../../atividade-coletiva';
import { ParticipanteAtividadeColetiva } from '../../participante-atividade-coletiva';
import { FormGroup } from '@angular/forms';
import { InstituicaoEnsino } from '../../instituicao-ensino';
import { UnidadeBasicaSaude } from '../../../../core/model/unidade-basica-saude';
import { FormInputIconStatusComponent } from '../../../../custom-components/form-input-icon-status/form-input-icon-status.component';
import { UnidadeBasicaSaudeService } from '../../../../core/service/unidade-basica-saude.service';
import { OutroProcedimentoColetivo } from '../../outro-procedimento-coletivo';
import { OutroProcedimentoColetivoService } from '../../outro-procedimento-coletivo.service';
import { CidadaoService } from '../../../cidadao/cidadao.service';
import { Cidadao } from '../../../cidadao/cidadao';
import { Observable } from 'rxjs';
import { InstituicaoEnsinoService } from '../../instituicao-ensino.service';
import { ProfissionalService } from '../../../../core/service/profissional.service';
import { ObjectCodigoDescricao } from '../../../../core/model/object-codigo-descricao';
import { Config } from '../../../../core/model/config';
import { AtividadeColetivaProfissional } from '../../atividade-coletiva-profissional';
import { Profissional } from '../../../../core/model/profissional';
import { Cbo } from '../../../../core/model/cbo';
import { CboService } from '../../../../core/service/cbo.service';
import { UsuarioService } from '../../../usuario/usuario.service';
import { FormAtividadeColetivaDto } from '../../form-atividade-coletiva-dto';
import { AtividadeColetivaService } from '../../atividade-coletiva.service';

@Component({
  selector: 'app-form-atividade-coletiva',
  templateUrl: './form-atividade-coletiva.component.html',
  styleUrls: ['./form-atividade-coletiva.component.scss']
})
export class FormAtividadeColetivaComponent implements OnInit {

  @Input('formData') formData$: Observable<AtividadeColetiva>;

  @ViewChild('form', { static: false }) form: FormGroup;

  @ViewChild('statusCnes', { static: false }) statusCnes: FormInputIconStatusComponent;

  @ViewChild('statusOutroProcedimento', { static: false }) statusOutroProcedimento: FormInputIconStatusComponent;

  @ViewChild('statusInep', { static: false }) statusInep: FormInputIconStatusComponent;

  @Output('onSubmit') onSubmitEventEmitter = new EventEmitter();

  atividadeColetiva: AtividadeColetiva = new AtividadeColetiva();

  opForm: FormAtividadeColetivaDto = new FormAtividadeColetivaDto();

  opCidadao: Cidadao[] = new Array;

  configCidadao: Config = new Config();

  profissionais: Profissional[] = new Array;

  configProfissional: Config = new Config();

  optionsCbo: Cbo[] = new Array;

  configCbo: Config = new Config();

  profissional: boolean = false;

  constructor(private profissionalService: ProfissionalService, private unidadeBasicaService: UnidadeBasicaSaudeService, private cidadaoService: CidadaoService,
    private outroProcedimentoColetivoService: OutroProcedimentoColetivoService, private cboService: CboService, private usuarioService: UsuarioService,
    private instituicaoEnsinoService: InstituicaoEnsinoService, private atividadeColetivaService: AtividadeColetivaService) { }

  ngOnInit() {
    this.formData$.subscribe({
      next: (value) => {
        this.atividadeColetiva = value;
        if (value.id != null) {
          this.usuarioService.getCurrentUserInfo().subscribe(
            data => this.profissional = data.id == value.profissionalResponsavel.id ? false : true,
            error => console.log(error)
          );
        }
        this.atividadeColetiva.instituicaoEnsino = this.atividadeColetiva.instituicaoEnsino == null ?
          new InstituicaoEnsino() : this.atividadeColetiva.instituicaoEnsino;
        this.atividadeColetiva.cnesLocalAtividade = this.atividadeColetiva.cnesLocalAtividade == null ?
          new UnidadeBasicaSaude() : this.atividadeColetiva.cnesLocalAtividade;
      },
      error: (err) => {
        console.log('erro behavior subject');
        console.log(err);
      }
    });

    this.atividadeColetivaService.init().subscribe(
      data => this.opForm = data,
      error => console.log("error in initAtividadeColetiva()")
    );

    this.configCidadao.displayKey = 'nome';
    this.configCidadao.placeholder = 'Digite o CPF/CNS';

    this.configProfissional.displayKey = 'nome';
    this.configProfissional.placeholder = 'Digite o Nome/CPF/CNS';

    this.configCbo.displayKey = 'titulo';
    this.configCbo.placeholder = 'Selecione o Cbo';
  }

  inepRequired() {
    return this.atividadeColetiva.pseSaude == true || this.atividadeColetiva.pseEducacao == true ? true : false;
  }

  inepDisabled() {
    if (this.atividadeColetiva.cnesLocalAtividade == null || this.atividadeColetiva.cnesLocalAtividade.id == null && this.atividadeColetiva.outraLocalidade == null || this.atividadeColetiva.outraLocalidade == '')
      return false;

    this.atividadeColetiva.instituicaoEnsino = new InstituicaoEnsino();
    return true;
  }

  cnesLocalAtividadeDisabled() {
    if ((this.atividadeColetiva.pseEducacao == false && this.atividadeColetiva.pseSaude == false) && (this.atividadeColetiva.instituicaoEnsino == null ||
      this.atividadeColetiva.instituicaoEnsino.inep == null) && (this.atividadeColetiva.outraLocalidade == null || this.atividadeColetiva.outraLocalidade == ''))
      return false;

    this.atividadeColetiva.cnesLocalAtividade = new UnidadeBasicaSaude();
    return true;
  }

  outraLocalidadeDisabled() {
    if (this.atividadeColetiva.pseEducacao == true || this.atividadeColetiva.pseSaude == true || this.atividadeColetiva.cnesLocalAtividade != null &&
      this.atividadeColetiva.cnesLocalAtividade.id != null || this.atividadeColetiva.instituicaoEnsino != null && this.atividadeColetiva.instituicaoEnsino.inep != null) {
      this.atividadeColetiva.outraLocalidade = null;
      return true;
    }
    return false;
  }

  buscarCnesLocalAtividade() {
    if (this.atividadeColetiva.cnesLocalAtividade.cnes.length > 0)
      if (!this.atividadeColetiva.cnesLocalAtividade.cnes) {
        this.atividadeColetiva.cnesLocalAtividade = new UnidadeBasicaSaude();
        this.statusCnes.ready();
      }
      else {
        this.statusCnes.loading();
        this.unidadeBasicaService.findByCnes(this.atividadeColetiva.cnesLocalAtividade.cnes).subscribe({
          next: (ubs) => {
            this.atividadeColetiva.cnesLocalAtividade = ubs;
            this.statusCnes.success();
          },
          error: (err) => {
            console.log(err);
            this.atividadeColetiva.cnesLocalAtividade = new UnidadeBasicaSaude();
            this.statusCnes.error();
          }
        });
      }
  }

  buscarOutroProcedimento() {
    if (!this.atividadeColetiva.procedimentoColetivo.sigtap) {
      this.atividadeColetiva.procedimentoColetivo = new OutroProcedimentoColetivo;
      this.statusOutroProcedimento.ready();
    }
    else {
      this.statusOutroProcedimento.loading();
      this.outroProcedimentoColetivoService.findBySigtap(this.atividadeColetiva.procedimentoColetivo.sigtap).subscribe({
        next: (procedimento) => {
          this.atividadeColetiva.procedimentoColetivo = procedimento;
          this.statusOutroProcedimento.success();
        },
        error: (err) => {
          console.log(err);
          this.atividadeColetiva.procedimentoColetivo = new OutroProcedimentoColetivo;
          this.statusOutroProcedimento.error();
        }
      });
    }
  }

  buscarInstituicao() {
    if (!this.atividadeColetiva.instituicaoEnsino.inep) {
      this.atividadeColetiva.instituicaoEnsino = new InstituicaoEnsino();
      this.statusInep.ready();
    }
    else {
      this.statusInep.loading();
      this.instituicaoEnsinoService.findByInep(this.atividadeColetiva.instituicaoEnsino.inep).subscribe({
        next: (instituicaoEnsino) => {
          this.atividadeColetiva.instituicaoEnsino = instituicaoEnsino;
          this.statusInep.success();
        },
        error: (err) => {
          console.log(err);
          this.atividadeColetiva.instituicaoEnsino = new InstituicaoEnsino();
          this.statusInep.error();
        }
      });
    }
  }

  minParticipantes() {
    let min = this.atividadeColetiva.participantes.length;
    return min == 0 ? 1 : min;
  }

  demaisProfissionai() {
    if (((this.atividadeColetiva.pseEducacao == true || this.atividadeColetiva.pseEducacao == false) && this.atividadeColetiva.pseSaude == true) ||
      (this.atividadeColetiva.pseEducacao == false && this.atividadeColetiva.pseSaude == false))
      return true;

    this.atividadeColetiva.profissionais = new Array;
    return false;
  }

  addProfissional() {
    let profissional: AtividadeColetivaProfissional = new AtividadeColetivaProfissional();
    this.atividadeColetiva.profissionais.push(profissional);
  }

  buscarProfissional(valor: string) {
    this.profissionalService.search(valor)
      .subscribe({
        next: (data) => {
          this.profissionais = data;
        },
        error: (err) => {
          console.log(err);
          this.profissionais = new Array;
        }
      });
  }

  buscarCboProfissional(id: number) {
    this.cboService.findAllByProfissional(id)
      .subscribe(
        data => this.optionsCbo = data
      )
  }

  removeProfissional(index) {
    this.atividadeColetiva.profissionais.splice(index, 1)
  }

  atividadeTipo(codigo) {
    let compare = [1, 2, 3, 5];

    if (this.atividadeColetiva.pseEducacao == true && this.atividadeColetiva.pseSaude == false && compare.findIndex(x => x == codigo) != -1) {
      if (compare.findIndex(x => x == this.atividadeColetiva.tipoAtividade.codigo) != -1)
        this.atividadeColetiva.tipoAtividade = new ObjectCodigoDescricao();

      return false;
    }

    return true;
  }

  temaReuniaoDisabled() {
    let compare = [4, 5, 6, 7];
    if (this.atividadeColetiva.tipoAtividade != null && compare.findIndex(x => x == this.atividadeColetiva.tipoAtividade.codigo) != -1 ||
      this.atividadeColetiva.pseEducacao == true && this.atividadeColetiva.pseSaude == false) {
      this.atividadeColetiva.listTemaReuniao = new Array;
      return false;
    }

    return true;
  }

  temaReuniaoRequired() {
    let compare = [1, 2, 3];
    if (this.atividadeColetiva.tipoAtividade != null && compare.findIndex(x => x == this.atividadeColetiva.tipoAtividade.codigo) == -1)
      return false;

    return true;
  }

  publicoAlvoRequired() {
    let compare = [4, 5, 6, 7];

    if (compare.findIndex(x => x == this.atividadeColetiva.tipoAtividade.codigo) == -1)
      return false;

    return true;
  }

  publicoAlvoDisabled() {
    let compare = [1, 2, 3];

    if (this.atividadeColetiva.tipoAtividade != null && compare.findIndex(x => x == this.atividadeColetiva.tipoAtividade.codigo) != -1) {
      this.atividadeColetiva.listPublicoAlvo = new Array;
      return false;
    }

    return true;
  }

  participanteRequired() {
    let compare = [5, 6];

    if (compare.findIndex(x => x == this.atividadeColetiva.tipoAtividade.codigo) != -1)
      return true;

    return false;
  }

  temaSaudeDisabled() {
    let compare = [1, 2, 3];

    if (this.atividadeColetiva.tipoAtividade != null && compare.findIndex(x => x == this.atividadeColetiva.tipoAtividade.codigo) != -1) {
      this.atividadeColetiva.listTemaSaude = new Array;
      return false;
    }

    return true;
  }

  temaSaudeChecked(temaSaude) {
    if (this.atividadeColetiva.listTemaSaude.findIndex(x => x.codigo == temaSaude.codigo) == -1)
      return false;

    return true;
  }

  temaSaudeRequired() {
    let compare = [4, 5, 7];

    if (compare.findIndex(x => x == this.atividadeColetiva.tipoAtividade.codigo) != -1)
      return true;

    return false;
  }

  pseEducacaoRequired() {
    if (this.atividadeColetiva.listTemaSaude != null && this.atividadeColetiva.listTemaSaude.findIndex(x => x.codigo == 18) != -1 && this.atividadeColetiva.pseSaude == false)
      return true;

    return false;
  }

  pseSaudeRequired() {
    if (this.atividadeColetiva.listTemaSaude != null && this.atividadeColetiva.listTemaSaude.findIndex(x => x.codigo == 18) != -1 && this.atividadeColetiva.pseEducacao == false)
      return true;

    return false;
  }

  praticaSaudeDisabled() {
    let compare = [1, 2, 3, 4, 7];

    if (this.atividadeColetiva.tipoAtividade != null && compare.findIndex(x => x == this.atividadeColetiva.tipoAtividade.codigo) != -1) {
      this.atividadeColetiva.praticaSaude = new ObjectCodigoDescricao();
      return false;
    }

    return true;
  }

  praticaSaudeRequired() {
    return this.atividadeColetiva.tipoAtividade.codigo == 6 ? true : false;
  }

  itemPraticaDisabled(codigo) {
    let compare = [2, 9, 25, 26, 27, 28, 24, 30];

    if (compare.findIndex(x => x == codigo) != -1 && this.atividadeColetiva.pseEducacao == true && this.atividadeColetiva.pseSaude == false) {
      if (this.atividadeColetiva.praticaSaude != null && compare.findIndex(x => x == this.atividadeColetiva.praticaSaude.codigo) != -1)
        this.atividadeColetiva.praticaSaude = new ObjectCodigoDescricao();
      return false;
    }

    return true;
  }

  addParticipante() {
    this.atividadeColetiva.participantes.push(new ParticipanteAtividadeColetiva());
    this.atividadeColetiva.numParticipantes = this.atividadeColetiva.numParticipantes < this.atividadeColetiva.participantes.length ?
      this.atividadeColetiva.participantes.length : this.atividadeColetiva.numParticipantes;
  }

  removeParticipante(index) {
    if (this.atividadeColetiva.participantes[index].avaliacaoAlterada == true)
      this.atividadeColetiva.numAvaliacoesAlteradas -= 1;
    this.atividadeColetiva.participantes.splice(index, 1)
  }

  buscarCidadao(valor: string) {
    if (valor != null && valor != '')
      this.cidadaoService.filtro(valor)
        .subscribe({
          next: (data) => {
            this.opCidadao = data;
          },
          error: (err) => {
            console.log(err);
          }
        });
  }

  temaReuniaoChange(temaReuniao) {
    let index = this.atividadeColetiva.listTemaReuniao.findIndex(x => x.codigo == temaReuniao.codigo);
    if (index == -1)
      this.atividadeColetiva.listTemaReuniao.push(temaReuniao);
    else {
      this.atividadeColetiva.listTemaReuniao.splice(index, 1);
    }
  }

  temaReuniaochecked(temaReuniao) {
    if (this.atividadeColetiva.listTemaReuniao.findIndex(x => x.codigo == temaReuniao.codigo) == -1)
      return false;

    return true;
  }

  publicoAlvoChange(publico) {
    let index = this.atividadeColetiva.listPublicoAlvo.findIndex(x => x.codigo == publico.codigo)
    if (index == -1)
      this.atividadeColetiva.listPublicoAlvo.push(publico);
    else {
      this.atividadeColetiva.listPublicoAlvo.splice(index, 1);
    }
  }

  publicoAlvoChecked(publico) {
    if (this.atividadeColetiva.listPublicoAlvo.findIndex(x => x.codigo == publico.codigo) == -1)
      return false;

    return true;
  }

  temaSaudeChange(temaSaude) {
    let index = this.atividadeColetiva.listTemaSaude.findIndex(x => x.codigo == temaSaude.codigo)
    if (index == -1) {
      this.atividadeColetiva.listTemaSaude.push(temaSaude);
      if (temaSaude.codigo == 18 && this.atividadeColetiva.pseSaude == false && this.atividadeColetiva.pseEducacao == false) {
        this.atividadeColetiva.pseEducacao = true;
        this.atividadeColetiva.pseSaude = true;
      }
    }
    else {
      this.atividadeColetiva.listTemaSaude.splice(index, 1);
    }
  }

  atividadeTipoChange(atividade: ObjectCodigoDescricao) {
    this.atividadeColetiva.tipoAtividade = atividade;
  }

  atividadeTipochecked(codigo) {
    return this.atividadeColetiva.tipoAtividade.codigo == codigo ? true : false;
  }

  validarCampoList() {
    let valido = true;

    if (this.temaReuniaoRequired() && this.atividadeColetiva.listTemaReuniao.length == 0)
      valido = false

    if (this.publicoAlvoRequired() && this.atividadeColetiva.listPublicoAlvo.length == 0)
      valido = false;

    if (this.participanteRequired() && this.atividadeColetiva.participantes.length == 0)
      valido = false;

    if (this.temaSaudeRequired() && this.atividadeColetiva.listTemaSaude.length == 0)
      valido = false;

    return valido;
  }

  avaliacaoAlteradaChange(i) {
    if (this.atividadeColetiva.participantes[i].avaliacaoAlterada == true)
      this.atividadeColetiva.numAvaliacoesAlteradas = this.atividadeColetiva.numAvaliacoesAlteradas == null ? 1 : this.atividadeColetiva.numAvaliacoesAlteradas + 1;

    if (this.atividadeColetiva.participantes[i].avaliacaoAlterada == false)
      this.atividadeColetiva.numAvaliacoesAlteradas -= 1;
  }

  onFormSubmit() {
    if (this.form.valid && this.validarCampoList()) {
      this.onSubmitEventEmitter.emit(this.atividadeColetiva);
    }
  }

}
