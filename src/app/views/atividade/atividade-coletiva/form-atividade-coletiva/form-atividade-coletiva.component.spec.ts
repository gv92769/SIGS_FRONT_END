import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormAtividadeColetivaComponent } from './form-atividade-coletiva.component';

describe('FormAtividadeColetivaComponent', () => {
  let component: FormAtividadeColetivaComponent;
  let fixture: ComponentFixture<FormAtividadeColetivaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormAtividadeColetivaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormAtividadeColetivaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
