import { PageRequest } from '../../core/model/page-request';

export class AtividadeColetivaFilter extends PageRequest {
  dataInicio: string;
  dataFim: string;
  publicoAlvo: number = null;
  cnes: string = null;
  inep: number = null;
  escola: boolean = false;
  ubs: string;
  exportada: boolean = false;
}
