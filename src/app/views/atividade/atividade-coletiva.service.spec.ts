import { TestBed } from '@angular/core/testing';

import { AtividadeColetivaService } from './atividade-coletiva.service';

describe('AtividadeColetivaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AtividadeColetivaService = TestBed.get(AtividadeColetivaService);
    expect(service).toBeTruthy();
  });
});
