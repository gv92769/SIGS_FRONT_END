import { TestBed } from '@angular/core/testing';

import { OutroProcedimentoColetivoService } from './outro-procedimento-coletivo.service';

describe('OutroProcedimentoColetivoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OutroProcedimentoColetivoService = TestBed.get(OutroProcedimentoColetivoService);
    expect(service).toBeTruthy();
  });
});
