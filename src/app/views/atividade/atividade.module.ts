// Angular
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
// Components Routing
import { AtividadeRoutingModule } from './atividade-routing.module';

import { FormsModule } from '@angular/forms';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { CustomComponentsModule } from '../../custom-components/custom-components.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { ModalModule } from 'ngx-bootstrap/modal';
import { SelectDropDownModule } from 'ngx-select-dropdown'

import { PesquisarCidadaoBiometriaModule } from '../cidadao/pesquisar-cidadao-biometria/pesquisar-cidadao-biometria.module';

import { ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule, IConfig } from 'ngx-mask';
import { FormAtividadeColetivaComponent } from './atividade-coletiva/form-atividade-coletiva/form-atividade-coletiva.component';
import { AdicionarAtividadeColetivaComponent } from './adicionar-atividade-coletiva/adicionar-atividade-coletiva.component';
import { EditarAtividadeColetivaComponent } from './editar-atividade-coletiva/editar-atividade-coletiva.component';
import { PesquisarAtividadeColetivaComponent } from './pesquisar-atividade-coletiva/pesquisar-atividade-coletiva.component';

export const options: Partial<IConfig> | (() => Partial<IConfig>) = { validation: false };

@NgModule({
  imports: [
    CommonModule,
    AtividadeRoutingModule,
    FormsModule,
    TabsModule,
    AccordionModule.forRoot(),
    CustomComponentsModule,
    ModalModule.forRoot(),
    PesquisarCidadaoBiometriaModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot(options),
    NgxDatatableModule,
    SelectDropDownModule
  ],
  declarations: [
    FormAtividadeColetivaComponent,
    AdicionarAtividadeColetivaComponent,
    EditarAtividadeColetivaComponent,
    PesquisarAtividadeColetivaComponent,
  ]
})
export class AtividadeModule { }
