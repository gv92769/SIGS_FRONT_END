import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormAtividadeColetivaComponent } from './atividade-coletiva/form-atividade-coletiva/form-atividade-coletiva.component';
import { AdicionarAtividadeColetivaComponent } from './adicionar-atividade-coletiva/adicionar-atividade-coletiva.component';
import { EditarAtividadeColetivaComponent } from './editar-atividade-coletiva/editar-atividade-coletiva.component';
import { PesquisarAtividadeColetivaComponent } from './pesquisar-atividade-coletiva/pesquisar-atividade-coletiva.component';


const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Atividade'
    },
    children: [
      {
        path: 'adicionar',
        component: AdicionarAtividadeColetivaComponent,
        data: {
          title: 'Adicionar'
        }
      },
      {
        path: 'editar/:id',
        component: EditarAtividadeColetivaComponent,
        data: {
          title: 'Editar'
        }
      },
      {
        path: 'pesquisar',
        component: PesquisarAtividadeColetivaComponent,
        data: {
          title: 'Pesquisar'
        }
      },
      {
        path: 'atividadecoletiva',
        data: {
          title: 'Atividade Coletiva'
        },
        children: [
          {
            path: 'adicionar',
            component: FormAtividadeColetivaComponent,
            data: {
              title: 'Adicionar'
            }
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AtividadeRoutingModule {}
