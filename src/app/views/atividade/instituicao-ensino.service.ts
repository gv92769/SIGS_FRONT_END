import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { InstituicaoEnsino } from './instituicao-ensino';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InstituicaoEnsinoService {

  readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/instituicaoensino';
  }

  public findByInep(inep: number): Observable<InstituicaoEnsino> {
    return this.http.get<InstituicaoEnsino>(this.url + "/inep/" + inep);
  }

}
