import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { AtividadeColetiva } from './atividade-coletiva';
import { Observable } from 'rxjs';
import { AtividadeColetivaFilter } from './atividade-coletiva-filter';
import { AtividadeColetivaTabela } from './atividade-coletiva-tabela';
import { Page } from '../../core/model/page';
import { FormAtividadeColetivaDto } from './form-atividade-coletiva-dto';

@Injectable({
  providedIn: 'root'
})
export class AtividadeColetivaService {

  readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/atividadecoletiva';
  }

  public findById(id: number): Observable<AtividadeColetiva> {
    return this.http.get<AtividadeColetiva>(this.url + '/' + id);
  }

  public create(atividadeColetiva: AtividadeColetiva): Observable<AtividadeColetiva> {
    return this.http.post<AtividadeColetiva>(this.url, atividadeColetiva);
  }

  public update(id: number, atividadeColetiva: AtividadeColetiva): Observable<AtividadeColetiva> {
    return this.http.put<AtividadeColetiva>(this.url+"/"+id, atividadeColetiva);
  }

  public search(atividade: AtividadeColetivaFilter): Observable<Page<AtividadeColetivaTabela>> {
    return this.http.post<Page<AtividadeColetivaTabela>>(this.url+'/search', atividade);
  }

  public delete(id: number) {
    return this.http.delete(this.url + '/' + id);
  }

  public init(): Observable<FormAtividadeColetivaDto> {
    return this.http.get<FormAtividadeColetivaDto>(this.url + '/init');
  }

}
