import { Profissional } from '../../core/model/profissional';
import { UnidadeBasicaSaude } from '../../core/model/unidade-basica-saude';
import { OutroProcedimentoColetivo } from './outro-procedimento-coletivo';
import { ParticipanteAtividadeColetiva } from './participante-atividade-coletiva';
import { InstituicaoEnsino } from './instituicao-ensino';
import { formatDate } from '@angular/common';
import { ObjectCodigoDescricao } from '../../core/model/object-codigo-descricao';
import { Cbo } from '../../core/model/cbo';
import { AtividadeColetivaProfissional } from './atividade-coletiva-profissional';

export class AtividadeColetiva {
  id: number;
  uuid: string;
  ine: string;
  profissionalResponsavel: Profissional = new Profissional();
  profissionais: AtividadeColetivaProfissional[] = new Array;
  ubs: UnidadeBasicaSaude = new UnidadeBasicaSaude();
  cbo: Cbo = new Cbo();
  data: string = formatDate(new Date(), 'yyyy-MM-dd', 'en-US');
  turno: number;
  pseEducacao: boolean = false;
  pseSaude: boolean = false;
  numParticipantes: number;
  numAvaliacoesAlteradas: number;
  cnesLocalAtividade: UnidadeBasicaSaude = new UnidadeBasicaSaude();
  instituicaoEnsino: InstituicaoEnsino = new InstituicaoEnsino();
  outraLocalidade: string;
  tipoAtividade: ObjectCodigoDescricao = new ObjectCodigoDescricao();
  listTemaReuniao: ObjectCodigoDescricao[] = new Array;
  listPublicoAlvo: ObjectCodigoDescricao[] = new Array;
  listTemaSaude: ObjectCodigoDescricao[] = new Array;
  praticaSaude: ObjectCodigoDescricao = new ObjectCodigoDescricao;
  procedimentoColetivo: OutroProcedimentoColetivo = new OutroProcedimentoColetivo();
  participantes: ParticipanteAtividadeColetiva[] = new Array;
  importada: boolean;
}
