import { TestBed } from '@angular/core/testing';

import { InstituicaoEnsinoService } from './instituicao-ensino.service';

describe('InstituicaoEnsinoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InstituicaoEnsinoService = TestBed.get(InstituicaoEnsinoService);
    expect(service).toBeTruthy();
  });
});
