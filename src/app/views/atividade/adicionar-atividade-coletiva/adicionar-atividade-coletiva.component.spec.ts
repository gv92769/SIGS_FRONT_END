import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdicionarAtividadeColetivaComponent } from './adicionar-atividade-coletiva.component';

describe('AdicionarAtividadeColetivaComponent', () => {
  let component: AdicionarAtividadeColetivaComponent;
  let fixture: ComponentFixture<AdicionarAtividadeColetivaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdicionarAtividadeColetivaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdicionarAtividadeColetivaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
