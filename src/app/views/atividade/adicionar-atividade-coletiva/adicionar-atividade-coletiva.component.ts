import { Component, OnInit, ViewChild } from '@angular/core';
import { AtividadeColetiva } from '../atividade-coletiva';
import { AtividadeColetivaService } from '../atividade-coletiva.service';
import { finalize } from 'rxjs/operators';
import { UsuarioService } from '../../usuario/usuario.service';
import { BehaviorSubject } from 'rxjs';
import { FormAtividadeColetivaComponent } from '../atividade-coletiva/form-atividade-coletiva/form-atividade-coletiva.component';
import { UnidadeBasicaSaudeService } from '../../../core/service/unidade-basica-saude.service';
import * as jwt_decode from 'jwt-decode';
import { ProfissionalService } from '../../../core/service/profissional.service';
import { ToastrService } from 'ngx-toastr';
import { CboService } from '../../../core/service/cbo.service';

@Component({
  selector: 'app-adicionar-atividade-coletiva',
  templateUrl: './adicionar-atividade-coletiva.component.html',
  styleUrls: ['./adicionar-atividade-coletiva.component.scss']
})
export class AdicionarAtividadeColetivaComponent implements OnInit {

  formData$: BehaviorSubject<AtividadeColetiva> = new BehaviorSubject<AtividadeColetiva>(new AtividadeColetiva());

  @ViewChild('formAtividadeColetiva', { static: false }) formAtividadeColetiva: FormAtividadeColetivaComponent;

  isLoadingSubmit: boolean = false;

  constructor(private atividadeColetivaService: AtividadeColetivaService, private usuarioService: UsuarioService, private toastrService: ToastrService,
    private profissionalService: ProfissionalService, private unidadeSaudeBasicaService: UnidadeBasicaSaudeService, private cboService: CboService) { }

  ngOnInit() {
    let atividadeColetiva: AtividadeColetiva = new AtividadeColetiva();

    this.usuarioService.getCurrentUserInfo().subscribe(
      data => this.profissionalService.findById(data.id)
        .subscribe(profissional => atividadeColetiva.profissionalResponsavel = profissional)
    );

    this.unidadeSaudeBasicaService.findByCnes(jwt_decode( sessionStorage.getItem('access_token')).cnes)
      .subscribe(ubs => atividadeColetiva.ubs = ubs)

    this.cboService.findByCodigo(jwt_decode(sessionStorage.getItem('access_token')).cbo)
      .subscribe(cbo => atividadeColetiva.cbo = cbo);

    this.formData$.next(atividadeColetiva);
  }

  onSubmitted(atividadeColetiva: AtividadeColetiva) {
    this.isLoadingSubmit = true;
    this.atividadeColetivaService.create(atividadeColetiva)
      .pipe(finalize(() => this.isLoadingSubmit = false))
      .subscribe({
        next: (response) => {
          this.toastrService.success('Atividade coletiva salva com sucesso!', 'Sucesso');
          this.ngOnInit();
        },
        error: (err) => {
          this.toastrService.error('Error ao salvar atividade coletiva.', 'Error');
          console.log(err);
        }
      });
  }

}
