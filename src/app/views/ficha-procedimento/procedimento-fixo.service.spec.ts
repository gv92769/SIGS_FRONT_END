import { TestBed } from '@angular/core/testing';

import { ProcedimentoFixoService } from './procedimento-fixo.service';

describe('ProcedimentoFixoService', () => {
  let service: ProcedimentoFixoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProcedimentoFixoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
