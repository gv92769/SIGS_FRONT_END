import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IConfig, NgxMaskModule } from 'ngx-mask';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { CustomComponentsModule } from '../../custom-components/custom-components.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PesquisarCidadaoBiometriaModule } from '../cidadao/pesquisar-cidadao-biometria/pesquisar-cidadao-biometria.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { FormFichaProcedimentoComponent } from './form-ficha-procedimento/form-ficha-procedimento.component';
import { AdicionarFichaProcedimentoComponent } from './adicionar-ficha-procedimento/adicionar-ficha-procedimento.component';
import { EditarFichaProcedimentoComponent } from './editar-ficha-procedimento/editar-ficha-procedimento.component';
import { FichaProcedimentoRoutingModule } from './ficha-procedimento-routing.module';
import { PesquisarFichaProcedimentoComponent } from './pesquisar-ficha-procedimento/pesquisar-ficha-procedimento.component';
import { SelectDropDownModule } from 'ngx-select-dropdown'
import { ExameModule } from '../exame/exame.module';

export const options: Partial<IConfig> | (() => Partial<IConfig>) = { validation: false };

@NgModule({
  declarations: [
    FormFichaProcedimentoComponent,
    AdicionarFichaProcedimentoComponent,
    EditarFichaProcedimentoComponent,
    PesquisarFichaProcedimentoComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    TabsModule,
    FichaProcedimentoRoutingModule,
    AccordionModule.forRoot(),
    CustomComponentsModule,
    ModalModule.forRoot(),
    PesquisarCidadaoBiometriaModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot(options),
    NgxDatatableModule,
    SelectDropDownModule,
    ExameModule
  ]
})
export class FichaProcedimentoModule { }
