import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { AdicionarFichaProcedimentoComponent } from './adicionar-ficha-procedimento/adicionar-ficha-procedimento.component';
import { EditarFichaProcedimentoComponent } from './editar-ficha-procedimento/editar-ficha-procedimento.component';
import { PesquisarFichaProcedimentoComponent } from './pesquisar-ficha-procedimento/pesquisar-ficha-procedimento.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Ficha Procedimentos'
    },
    children: [
      {
        path: 'adicionar',
        component: AdicionarFichaProcedimentoComponent,
        data: {
          title: 'Adicionar'
        }
      },
      {
        path: 'editar/:id',
        component: EditarFichaProcedimentoComponent,
        data: {
          title: 'Editar'
        }
      },
      {
        path: 'pesquisar',
        component: PesquisarFichaProcedimentoComponent,
        data: {
          title: 'Pesquisar'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FichaProcedimentoRoutingModule {}
