import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormFichaProcedimentoComponent } from './form-ficha-procedimento.component';

describe('FormFichaProcedimentoComponent', () => {
  let component: FormFichaProcedimentoComponent;
  let fixture: ComponentFixture<FormFichaProcedimentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormFichaProcedimentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormFichaProcedimentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
