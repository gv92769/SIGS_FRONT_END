import { Component, OnInit, Input, ViewChild, EventEmitter, Output } from '@angular/core';
import { FichaProcedimentos } from '../ficha-procedimentos';
import { Observable } from 'rxjs';
import { FormGroup } from '@angular/forms';
import { LocalAtendimento } from '../../../core/model/local-atendimento';
import { LocalAtendimentoService } from '../../../core/service/local-atendimento.service';
import { CidadaoService } from '../../cidadao/cidadao.service';
import { ProcedimentoFixoService } from '../procedimento-fixo.service';
import { Cidadao } from '../../cidadao/cidadao';
import { ProcedimentoFixo } from '../procedimento-fixo';
import { Config } from '../../../core/model/config';
import { ProcedimentoGrupo } from '../../../core/model/procedimento-grupo';
import { ProcedimentoGrupoService } from '../../../core/service/procedimento-grupo.service';
import { ProcedimentoSubGrupo } from '../../../core/model/procedimento-sub-grupo';
import { ProcedimentoSubGrupoService } from '../../../core/service/procedimento-sub-grupo.service';
import { ProcedimentoFormaOrganizacional } from '../../../core/model/procedimento-forma-organizacional';
import { ProcedimentoFormaOrganizacionalService } from '../../../core/service/procedimento-forma-organizacional.service';
import { ProcedimentoService } from '../../../core/service/procedimento.service';
import { Procedimento } from '../../../core/model/procedimento';
import { formatDate } from '@angular/common';
import { UsuarioService } from '../../usuario/usuario.service';

@Component({
  selector: 'app-form-ficha-procedimento',
  templateUrl: './form-ficha-procedimento.component.html',
  styleUrls: ['./form-ficha-procedimento.component.css']
})
export class FormFichaProcedimentoComponent implements OnInit {

  @Input('formData') formData$: Observable<FichaProcedimentos>;

  fichaProcedimentos: FichaProcedimentos = new FichaProcedimentos();

  @ViewChild('form', { static: false }) form: FormGroup;

  @Output('onSubmit') onSubmitEventEmitter = new EventEmitter();

  locais: LocalAtendimento[] = new Array;

  procedimentosFixo: ProcedimentoFixo[] = new Array;

  testes: ProcedimentoFixo[] = new Array;

  medicamentos: ProcedimentoFixo[] = new Array;

  configGrupo: Config = new Config();

  grupos: ProcedimentoGrupo[] = new Array;

  configSubGrupo: Config = new Config();

  subgrupos: ProcedimentoSubGrupo[] = new Array;

  configForma: Config = new Config();

  formas: ProcedimentoFormaOrganizacional[] = new Array;

  config: Config = new Config();

  procedimentos: Procedimento[] = new Array;

  opCidadao: Cidadao[] = new Array;

  configCidadao: Config = new Config();

  profissional: boolean = false;

  constructor(private localAtendimentoService: LocalAtendimentoService, private cidadaoService: CidadaoService,  
    private procedimentoFixoService: ProcedimentoFixoService, private grupoService: ProcedimentoGrupoService, 
    private procedimentoService: ProcedimentoService, private usuarioService: UsuarioService,
    private subGrupoService: ProcedimentoSubGrupoService, private formaService: ProcedimentoFormaOrganizacionalService) { }

  ngOnInit(): void {
    this.formData$.subscribe({
      next: (value) => {
        this.fichaProcedimentos = value;
        if (value.id != null) {
          this.usuarioService.getCurrentUserInfo().subscribe(
            data => this.profissional = data.id == value.profissionalResponsavel.id ? false : true,
            error => console.log(error)
          );
        }
        this.fichaProcedimentos.dataHorarioInicio = formatDate(new Date(), "yyyy-MM-dd'T'HH:mm:ss", 'en-US');
        this.fichaProcedimentos.grupo = new ProcedimentoGrupo();
        this.fichaProcedimentos.subgrupo = new ProcedimentoSubGrupo();
        this.fichaProcedimentos.forma = new ProcedimentoFormaOrganizacional();
      },
      error: (err) => {
        console.log('erro behavior subject');
        console.log(err);
      }
    });

    this.localAtendimentoService.findAll().subscribe(
      data => this.locais = data,
      error => console.log("error in findAllLocalAtendimento()")
    );

    let ids = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22];

    this.procedimentoFixoService.findAllById(ids).subscribe(
      data => this.procedimentosFixo = data,
      error => console.log("error in findAllById procedimento fixo()")
    );

    ids = [23, 24, 25, 26, 27];

    this.procedimentoFixoService.findAllById(ids).subscribe(
      data => this.testes = data,
      error => console.log("error in findAllById procedimento fixo()")
    );

    ids = [28, 29, 30, 31, 32, 33, 34];

    this.procedimentoFixoService.findAllById(ids).subscribe(
      data => this.medicamentos = data,
      error => console.log("error in findAllById procedimento fixo()")
    );

    this.configGrupo.search = false;
    this.configGrupo.displayKey = 'descricao';
    this.configGrupo.placeholder = 'Escolha um Grupo';

    ids = [1, 2, 3, 4];
    this.grupoService.findAllById(ids).subscribe(
      data => this.grupos = data,
      error => console.log("error in findAllById grupo()")
    );

    this.configSubGrupo.search = false;
    this.configSubGrupo.displayKey = 'descricao';
    this.configSubGrupo.placeholder = 'Escolha um SubGrupo';

    this.configForma.search = false;
    this.configForma.displayKey = 'descricao';
    this.configForma.placeholder = 'Escolha uma Forma Organizacional';

    this.config.displayKey = 'descricao';
    this.config.placeholder = 'Escolha um ou mais Procedimentos';
    this.config.searchOnKey = 'descricao';

    this.configCidadao.displayKey = 'nome';
    this.configCidadao.placeholder = 'Digite o CPF/CNS';
  }

  buscarCidadao(valor: string) {
    if (valor != null && valor != '')
      this.cidadaoService.filtro(valor)
        .subscribe({
          next: (data) => {
            this.opCidadao = data;
          },
          error: (err) => {
            console.log(err);
          }
        });
  }

  procedimentoChange(procedimento) {
    let index = this.fichaProcedimentos.procedimentosFixo.findIndex(x => x.id == procedimento.id)
    if (index == -1) {
      this.fichaProcedimentos.procedimentosFixo.push(procedimento);
    }
    else {
      this.fichaProcedimentos.procedimentosFixo.splice(index, 1);
    }
  }

  procedimentoChecked(procedimento) {
    if (this.fichaProcedimentos.procedimentosFixo.findIndex(x => x.id == procedimento.id) != -1)
      return true;

    return false;
  }

  validarCampoList() {
    if (this.fichaProcedimentos.escutaInicial == false && this.fichaProcedimentos.procedimentosFixo.length == 0 || 
      ((this.fichaProcedimentos.procedimento.length + this.fichaProcedimentos.procedimentosFixo.length) > 20))
      return false;

    return true;
  }

  onFormSubmit() {
    if (this.form.valid && this.validarCampoList()) {
      this.fichaProcedimentos.dataHorarioFinal = formatDate(new Date(), "yyyy-MM-dd'T'HH:mm:ss", 'en-US');
      this.onSubmitEventEmitter.emit(this.fichaProcedimentos);
    }
  }

  grupoChange(event: Event) {
    this.fichaProcedimentos.grupo = event['value'];
    this.fichaProcedimentos.subgrupo = new ProcedimentoSubGrupo();
    this.fichaProcedimentos.forma = new ProcedimentoFormaOrganizacional();
  }

  subgrupoOpen() {
    this.subGrupoService.findAllByGrupo(this.fichaProcedimentos.grupo)
      .subscribe(
        data => this.subgrupos = data,
        error => console.log("error in findAllByGrupo subgrupo()")
      )
  }

  subgrupoChange(event: Event) {
    this.fichaProcedimentos.subgrupo = event['value'];
    this.fichaProcedimentos.forma = new ProcedimentoFormaOrganizacional();
  }

  formaOpen() {
    this.formaService.findAllBySubGrupo(this.fichaProcedimentos.subgrupo)
      .subscribe(
        data => this.formas = data,
        error => console.log("error in findAllBySubGrupo formaOrganizacional()")
      );
  }

  formaChange(event: Event) {
    this.fichaProcedimentos.forma = event['value'];
  }

  clear() {
    this.fichaProcedimentos.grupo = new ProcedimentoGrupo();
    this.fichaProcedimentos.subgrupo = new ProcedimentoSubGrupo();
    this.fichaProcedimentos.forma =   new ProcedimentoFormaOrganizacional();
  }

  searchProcedimentoChange(texto: string) {
    let procedimentoFiltro = {
      grupo: this.fichaProcedimentos.grupo,
      subgrupo:this.fichaProcedimentos.subgrupo,
      forma: this.fichaProcedimentos.forma,
      texto: texto,
      sexo: this.fichaProcedimentos.cidadao.sexo == 'MASCULINO' ? true : false
    }

    this.procedimentoService.search(procedimentoFiltro)
      .subscribe(
        data => this.procedimentos = data,
        error => console.log("error in search Procedimento()"+error)
      );
  }

  limitarQuantidadeProcedimentos() {
    return this.fichaProcedimentos.procedimento.length + this.fichaProcedimentos.procedimentosFixo.length == 20 ? true : false;
  }

}
