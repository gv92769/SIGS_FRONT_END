import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PesquisarFichaProcedimentoComponent } from './pesquisar-ficha-procedimento.component';

describe('PesquisarFichaProcedimentoComponent', () => {
  let component: PesquisarFichaProcedimentoComponent;
  let fixture: ComponentFixture<PesquisarFichaProcedimentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PesquisarFichaProcedimentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PesquisarFichaProcedimentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
