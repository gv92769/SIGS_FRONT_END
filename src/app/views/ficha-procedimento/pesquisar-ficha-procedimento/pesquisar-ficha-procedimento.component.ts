import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FichaProcedimentoService } from '../ficha-procedimento.service';
import { PesquisaFichaComponent } from '../../pesquisa-ficha/pesquisa-ficha.component';
import { UsuarioService } from '../../usuario/usuario.service';
import { CidadaoService } from '../../cidadao/cidadao.service';
import { ExameService } from '../../exame/exame.service';
import { CboService } from '../../../core/service/cbo.service';
import { PerfilService } from '../../../core/service/perfil.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-pesquisar-ficha-procedimento',
  templateUrl: '../../pesquisa-ficha/pesquisa-ficha.component.html',
})
export class PesquisarFichaProcedimentoComponent extends PesquisaFichaComponent implements OnInit {

  constructor(exameService: ExameService, cidadaoService: CidadaoService, private router: Router, private fichaProcedimentoService: FichaProcedimentoService, usuarioService: UsuarioService, 
    cboService: CboService, perfilService: PerfilService, http: HttpClient) { 
      super(usuarioService, cidadaoService, cboService, perfilService, http);
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.createTable();

    this.titulo = "Pesquisar Ficha Procedimentos";

    this.tituloExcluir = "Ficha Procedimentos";

    this.textoExcluir = " a Ficha de Procedimentos";
  }

  createTable() {
    this.columns = [
      { name: 'Data', sortable: true },
      { name: 'Nome', sortable: false },
      { name: 'CPF', sortable: false },
      { name: 'CNS', sortable: false },
    ];
  }

  onEditarClick(id: any) {
    this.router.navigate(['fichaprocedimento/editar/', id]);
  }

  delete() {
    this.isLoadingExcluir = true;

    this.fichaProcedimentoService.delete(this.excluirId).subscribe(
      response => this.search(),
      error => console.log('erro ao excluir ficha procedimentos'),
      () => {
        this.modalExcluir.hide();
        this.isLoadingExcluir = false;
        this.excluirId = null;
      }
    );
  }

  setPage(event) {
    this.page.pageNumber = event.offset;
    this.search();
  }

  onSort(event) {
    this.getSort(event);
    this.search();
  }

  onSubmit() {
    this.getSumit()
    this.search();
  }

  search() {
    this.getSearch()

    this.fichaProcedimentoService.search(this.filter)
      .subscribe({
        next: (response) => {
          this.page.content = response.content;
          this.page.totalElements = response.totalElements;
          this.isLoading = false;
        },
        error: (err) => {
          console.log('erro ao pesquisar ficha procedimentos');
          console.log(err);
          this.isLoading = false;
        }
      });
  }

}
