import { TestBed } from '@angular/core/testing';

import { FichaProcedimentoService } from './ficha-procedimento.service';

describe('FichaProcedimentoService', () => {
  let service: FichaProcedimentoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FichaProcedimentoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
