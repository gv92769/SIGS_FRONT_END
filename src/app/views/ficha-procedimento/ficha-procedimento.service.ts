import { Injectable } from '@angular/core';
import { FichaProcedimentos } from './ficha-procedimentos';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { FichaProcedimentoTabela } from './ficha-procedimento-tabela';
import { Page } from '../../core/model/page';
import { FichaFiltro } from '../../core/model/ficha-filtro';

@Injectable({
  providedIn: 'root'
})
export class FichaProcedimentoService {

  readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/fichaprocedimento';
  }

  public findById(id: number): Observable<FichaProcedimentos> {
    return this.http.get<FichaProcedimentos>(this.url + '/' + id);
  }

  public create(fichaProcedimento: FichaProcedimentos): Observable<FichaProcedimentos> {
    return this.http.post<FichaProcedimentos>(this.url, fichaProcedimento);
  }

  public update(id: number, fichaProcedimento: FichaProcedimentos): Observable<FichaProcedimentos> {
    return this.http.put<FichaProcedimentos>(this.url+"/"+id, fichaProcedimento);
  }

  public delete(id: number) {
    return this.http.delete(this.url + '/' + id);
  }

  public search(filter: FichaFiltro): Observable<Page<FichaProcedimentoTabela>> {
    return this.http.post<Page<FichaProcedimentoTabela>>(this.url + '/search', filter);
  }

}
