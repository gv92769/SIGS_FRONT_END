export class FichaProcedimentoTabela {
  id: number;
  data: string;
  nome: string;
  cpf: string;
  cns: string;
}
