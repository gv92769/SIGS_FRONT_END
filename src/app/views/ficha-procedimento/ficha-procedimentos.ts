import { Profissional } from '../../core/model/profissional';
import { UnidadeBasicaSaude } from '../../core/model/unidade-basica-saude';
import { formatDate } from '@angular/common';
import { Cidadao } from '../cidadao/cidadao';
import { LocalAtendimento } from '../../core/model/local-atendimento';
import { ProcedimentoFixo } from './procedimento-fixo';
import { Procedimento } from '../../core/model/procedimento';
import { ProcedimentoGrupo } from '../../core/model/procedimento-grupo';
import { ProcedimentoSubGrupo } from '../../core/model/procedimento-sub-grupo';
import { ProcedimentoFormaOrganizacional } from '../../core/model/procedimento-forma-organizacional';
import { Cbo } from '../../core/model/cbo';

export class FichaProcedimentos {
  id: number;
  profissionalResponsavel: Profissional = new Profissional();
  ubs: UnidadeBasicaSaude = new UnidadeBasicaSaude();
  cbo: Cbo = new Cbo();
  ine: string;
  turno: number;
  data: string = formatDate(new Date(), 'yyyy-MM-dd', 'en-US');
  dataHorarioInicio: string;
  dataHorarioFinal: string;
  numProntuario: string;
  cidadao: Cidadao;
  localAtendimento: LocalAtendimento = new LocalAtendimento();
  escutaInicial: boolean;
  procedimento: Procedimento[] = new Array;
  procedimentosFixo: ProcedimentoFixo[] = new Array;
  grupo: ProcedimentoGrupo = new ProcedimentoGrupo();
  subgrupo: ProcedimentoSubGrupo = new ProcedimentoSubGrupo();
  forma: ProcedimentoFormaOrganizacional = new ProcedimentoFormaOrganizacional();
  importada: boolean;
}
