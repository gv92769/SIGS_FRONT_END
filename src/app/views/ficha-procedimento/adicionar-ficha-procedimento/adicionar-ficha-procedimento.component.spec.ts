import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdicionarFichaProcedimentoComponent } from './adicionar-ficha-procedimento.component';

describe('AdicionarFichaProcedimentoComponent', () => {
  let component: AdicionarFichaProcedimentoComponent;
  let fixture: ComponentFixture<AdicionarFichaProcedimentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdicionarFichaProcedimentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdicionarFichaProcedimentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
