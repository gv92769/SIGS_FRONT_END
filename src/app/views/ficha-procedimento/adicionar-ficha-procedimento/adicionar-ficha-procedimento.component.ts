import { Component, OnInit, ViewChild } from '@angular/core';
import { FichaProcedimentos } from '../ficha-procedimentos';
import { BehaviorSubject } from 'rxjs';
import { ProfissionalService } from '../../../core/service/profissional.service';
import { UsuarioService } from '../../usuario/usuario.service';
import { UnidadeBasicaSaudeService } from '../../../core/service/unidade-basica-saude.service';
import { FichaProcedimentoService } from '../ficha-procedimento.service';
import * as jwt_decode from 'jwt-decode';
import { finalize } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { CboService } from '../../../core/service/cbo.service';

@Component({
  selector: 'app-adicionar-ficha-procedimento',
  templateUrl: './adicionar-ficha-procedimento.component.html',
  styleUrls: ['./adicionar-ficha-procedimento.component.css']
})
export class AdicionarFichaProcedimentoComponent implements OnInit {

  formData$: BehaviorSubject<FichaProcedimentos> = new BehaviorSubject<FichaProcedimentos>(new FichaProcedimentos());

  @ViewChild('formFichaProcedimento', { static: false }) formFichaProcedimento: FichaProcedimentos;

  isLoadingSubmit: boolean = false;

  constructor(private usuarioService: UsuarioService, private profissionalService: ProfissionalService, private toastrService: ToastrService,
    private fichaProcedimentoService: FichaProcedimentoService, private unidadeSaudeBasicaService: UnidadeBasicaSaudeService, 
    private cboService: CboService) { }

  ngOnInit(): void {
    let fichaProcedimento: FichaProcedimentos = new FichaProcedimentos();

    this.usuarioService.getCurrentUserInfo().subscribe(
      data => this.profissionalService.findById(data.id)
        .subscribe(profissional => fichaProcedimento.profissionalResponsavel = profissional)
    );

    this.unidadeSaudeBasicaService.findByCnes(jwt_decode( sessionStorage.getItem('access_token')).cnes)
      .subscribe(ubs => fichaProcedimento.ubs = ubs);

    this.cboService.findByCodigo(jwt_decode(sessionStorage.getItem('access_token')).cbo)
      .subscribe(cbo => fichaProcedimento.cbo = cbo);

    this.formData$.next(fichaProcedimento);
  }

  onSubmitted(fichaProcedimentos: FichaProcedimentos) {
    this.isLoadingSubmit = true;
    this.fichaProcedimentoService.create(fichaProcedimentos)
      .pipe(finalize(() => this.isLoadingSubmit = false))
      .subscribe({
        next: (response) => {
          this.toastrService.success('Ficha de procedimentos salva com sucesso!', 'Sucesso');
          this.ngOnInit();
        },
        error: (err) => {
          this.toastrService.error('Error ao salvar ficha de procedimentos.', 'Error');
          console.log(err);
        }
      });
  }

}
