import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ProcedimentoFixo } from './procedimento-fixo';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProcedimentoFixoService {

  readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/procedimetofixo';
  }

  public findAllById(ids: number[]): Observable<ProcedimentoFixo[]> {
    return this.http.post<ProcedimentoFixo[]>(this.url+"/findallbyid", ids);
  }

}
