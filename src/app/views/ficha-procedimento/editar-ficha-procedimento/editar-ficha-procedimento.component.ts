import { Component, OnInit, ViewChild } from '@angular/core';
import { FichaProcedimentos } from '../ficha-procedimentos';
import { BehaviorSubject } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { FichaProcedimentoService } from '../ficha-procedimento.service';
import { map, mergeMap, finalize } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { UsuarioService } from '../../usuario/usuario.service';

@Component({
  selector: 'app-editar-ficha-procedimento',
  templateUrl: './editar-ficha-procedimento.component.html',
  styleUrls: ['./editar-ficha-procedimento.component.css']
})
export class EditarFichaProcedimentoComponent implements OnInit {

  formData$: BehaviorSubject<FichaProcedimentos> = new BehaviorSubject<FichaProcedimentos>(new FichaProcedimentos());

  @ViewChild('formFichaProcedimento', { static: false }) formFichaProcedimento: FichaProcedimentos;

  isLoadingSubmit: boolean = false;

  importada: boolean = false;

  profissional: boolean = false;

  constructor(private route: ActivatedRoute, private fichaProcedimentoService: FichaProcedimentoService, private toastrService: ToastrService,
    private router: Router, private usuarioService: UsuarioService) { }

  ngOnInit(): void {
    this.route.paramMap.pipe(
      map(params => {
        const id = +params.get('id');
        return id;
      }),
      mergeMap(id => {
        return this.fichaProcedimentoService.findById(id);
      }
      )
    )
      .subscribe({
        next: (response) => {
          this.importada = response.importada;
          this.usuarioService.getCurrentUserInfo().subscribe(
            data => this.profissional = data.id == response.profissionalResponsavel.id ? false : true,
            error => console.log(error)
          );
          this.formData$.next(response);
        },
        error: (err) => {
          console.log('Erro ao buscar Ficha Procedimentos');
          console.log(err);
        }
      });
  }

  onSubmitted(fichaProcedimentos: FichaProcedimentos) {
    if (this.importada == false) {
      this.isLoadingSubmit = true;

      this.fichaProcedimentoService.update(fichaProcedimentos.id, fichaProcedimentos)
        .pipe(finalize(() => this.isLoadingSubmit = false))
        .subscribe({
          next: (response) => {
            this.toastrService.success('Ficha de procedimentos salva com sucesso!', 'Sucesso');
            this.router.navigate(['fichaprocedimento/adicionar']);
          },
          error: (err) => {
            this.toastrService.error('Error ao salvar ficha de procedimentos.', 'Error');
            console.log(err);
          }
        });
    }
  }

}
