import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarFichaProcedimentoComponent } from './editar-ficha-procedimento.component';

describe('EditarFichaProcedimentoComponent', () => {
  let component: EditarFichaProcedimentoComponent;
  let fixture: ComponentFixture<EditarFichaProcedimentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarFichaProcedimentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarFichaProcedimentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
