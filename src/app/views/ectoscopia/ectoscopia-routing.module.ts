import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PesquisarEctoscopiaComponent } from './pesquisar-ectoscopia/pesquisar-ectoscopia.component';
import { AdicionarEctoscopiaComponent } from './adicionar-ectoscopia/adicionar-ectoscopia.component';
import { EditarEctoscopiaComponent } from './editar-ectoscopia/editar-ectoscopia.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Ectoscopia'
    },
    children: [
      {
        path: '',
        redirectTo: 'pesquisar'
      },
      {
        path: 'pesquisar',
        component: PesquisarEctoscopiaComponent,
        data: {
          title: 'Pesquisar'
        }
      },
      {
        path: 'editar/:id',
        component: EditarEctoscopiaComponent,
        data: {
          title: 'Editar'
        }
      },
      {
        path: 'adicionar',
        component: AdicionarEctoscopiaComponent,
        data: {
          title: 'Adicionar'
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EctoscopiaRoutingModule {}
