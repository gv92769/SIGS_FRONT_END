import { Injectable } from '@angular/core';
import { EctoscopiaItem } from './ectoscopia-item';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EctoscopiaItensService {

  readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/ectoscopiaitem';
  }

  public findAllByGrupo(grupo: string): Observable<EctoscopiaItem[]> {
    return this.http.get<EctoscopiaItem[]>(this.url+'/grupo/'+grupo);
  }

}
