import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EctoscopiaService } from '../../../core/service/ectoscopia.service';
import { Ectoscopia } from '../../../core/model/ectoscopia';
import { ItemEctoscopia } from '../../../core/model/item-ectoscopia';
import { ActivatedRoute } from '@angular/router';
import { formatDate } from '@angular/common';
import { EctoscopiaItem } from '../ectoscopia-item';
import { EctoscopiaItensService } from '../ectoscopia-itens.service';
import { ToastrService } from 'ngx-toastr';
import { UsuarioService } from '../../usuario/usuario.service';
import { ProfissionalService } from '../../../core/service/profissional.service';
import { Cidadao } from '../../cidadao/cidadao';
import { Config } from '../../../core/model/config';
import { CidadaoService } from '../../cidadao/cidadao.service';
import { Cid10Service } from '../../../core/service/cid10.service';
import { Cid10 } from '../../../core/model/cid10';

@Component({
  selector: 'app-adicionar-ectoscopia',
  templateUrl: '../adicionar-editar-ectoscopia.component.html'
})
export class AdicionarEctoscopiaComponent implements OnInit {

  isLoading: boolean = false;
  
  tituloPagina: string = 'Adicionar Ectoscopia';
  
  idCidadao: number;
  
  ectoscopia: Ectoscopia;

  itensPele: EctoscopiaItem[] = new Array;
  
  itensLinfoAx: EctoscopiaItem[] = new Array;
  
  itensLinfoIng: EctoscopiaItem[] = new Array;
  
  itensOlhos: EctoscopiaItem[] = new Array;
  
  itensNariz: EctoscopiaItem[] = new Array;
  
  itensOuvido: EctoscopiaItem[] = new Array;
  
  itensCardiovascular: EctoscopiaItem[] = new Array;
  
  itensRespiratorio: EctoscopiaItem[] = new Array;
  
  itensDigestivo: EctoscopiaItem[] = new Array;
  
  itensGinecologico: EctoscopiaItem[] = new Array;
  
  itensUrinario: EctoscopiaItem[] = new Array;
  
  itensProctologico: EctoscopiaItem[] = new Array;

  opCidadao: Cidadao[] = new Array;

  configCidadao: Config = new Config();

  profissional: boolean = false;

  optionsCid: Cid10[] = new Array;

  config: Config = new Config();

  constructor(protected ectoscopiaService: EctoscopiaService, protected router: Router, protected route: ActivatedRoute, 
    protected ectoscopiaItemService: EctoscopiaItensService, protected toastrService: ToastrService, protected usuarioService: UsuarioService,
    protected profissionalService: ProfissionalService, protected cidadaoService: CidadaoService, protected cid10Service: Cid10Service) {
  }

  ngOnInit() {
    this.ectoscopia = new Ectoscopia();

    this.usuarioService.getCurrentUserInfo().subscribe(
      data => this.profissionalService.findById(data.id)
        .subscribe(profissional => this.ectoscopia.profissional = profissional)
    );
   
    this.ectoscopia.dataRealizacao = formatDate(new Date(), 'yyyy-MM-dd', 'en-US');

    this.ectoscopia.dataHorarioInicio = formatDate(new Date(), "yyyy-MM-dd'T'HH:mm:ss", 'en-US');

    this.ectoscopiaItemService.findAllByGrupo('pele')
      .subscribe(data => this.itensPele = data);

    this.ectoscopiaItemService.findAllByGrupo('linfonodos axilares')
      .subscribe(data => this.itensLinfoAx = data);
    
    this.ectoscopiaItemService.findAllByGrupo('linfonodos inguinais')
      .subscribe(data => this.itensLinfoIng = data);
  
    this.ectoscopiaItemService.findAllByGrupo('olhos')
      .subscribe(data => this.itensOlhos = data);

    this.ectoscopiaItemService.findAllByGrupo('nariz')
      .subscribe(data => this.itensNariz = data);

    this.ectoscopiaItemService.findAllByGrupo('ouvido')
      .subscribe(data => this.itensOuvido = data);

    this.ectoscopiaItemService.findAllByGrupo('sistema cardiovascular')
      .subscribe(data => this.itensCardiovascular = data);

    this.ectoscopiaItemService.findAllByGrupo('sistema respiratório')
      .subscribe(data => this.itensRespiratorio = data);

    this.ectoscopiaItemService.findAllByGrupo('sistema digestivo')
      .subscribe(data => this.itensDigestivo = data);

    this.ectoscopiaItemService.findAllByGrupo('ginecológico')
      .subscribe(data => this.itensGinecologico = data);

    this.ectoscopiaItemService.findAllByGrupo('sistema urinário')
      .subscribe(data => this.itensUrinario = data);

    this.ectoscopiaItemService.findAllByGrupo('proctológico')
      .subscribe(data => this.itensProctologico = data);

    this.configCidadao.displayKey = 'nome';

    this.configCidadao.placeholder = 'Digite o CPF/CNS ou Nome';

    this.config.placeholder = 'Selecione Cid10';
    this.config.displayKey = 'descricao';
    this.config.searchOnKey = 'descricao';
  }

  buscarCidadao(valor: string) {
    if (valor != null && valor != '')
      this.cidadaoService.filtro(valor)
        .subscribe({
          next: (data) => {
            this.opCidadao = data;
          },
          error: (err) => {
            console.log(err);
          }
        });
  }

  onSaveButtonClick() : void {
    this.isLoading = true;
    
    if(!this.ectoscopia.id){
      this.ectoscopia.dataHorarioFinal = formatDate(new Date(), "yyyy-MM-dd'T'HH:mm:ss", 'en-US');
      this.ectoscopiaService.create(this.ectoscopia).subscribe(
        data => {
          this.toastrService.success('Ectoscopia salva com sucesso!', 'Sucesso');
          this.ngOnInit();
        },
        error => {
          this.toastrService.error('Error ao salvar ectoscopia.', 'Error');
          console.log(error);
        },
        () => this.isLoading = false
      );
    }
    else{
      this.ectoscopiaService.update(this.ectoscopia.id, this.ectoscopia).subscribe(
        data => {
          console.log('UPDATED');
          window.scrollTo({left: 0, top: 0, behavior: 'smooth'});
        },
        error => console.log(error)
      );
    }

  }

  onCancelButtonClick() : void {
    console.log('CANCEL');
    this.router.navigate(['/']);
  }

  onFinishButtonClick() : void {
    console.log('FINISH');
  }

  itemChange(op: EctoscopiaItem) {
    let index = this.itemPosicao(op);
    if (index == -1) {
      let item: ItemEctoscopia = new ItemEctoscopia();
      item.item = op;
      this.ectoscopia.itensEctoscopia.push(item);
    }
    else {
      this.ectoscopia.itensEctoscopia.splice(index, 1);
    }
  }

  itemChecked(op: EctoscopiaItem) {
    if (this.itemPosicao(op) != -1)
      return true;

    return false;
  }

  itemPosicao(op: EctoscopiaItem) {
    return this.ectoscopia.itensEctoscopia.findIndex(x => x.item.id == op.id);
  }

  searchCid10(valor: string) {
    if (valor != null && valor != '')
      this.cid10Service.search(valor, this.ectoscopia.cidadao.sexo == 'FEMININO' ? 1 : 0)
        .subscribe(
          data => this.optionsCid = data,
          error => console.log("Error ao buscar Cid10", error)
        );
  }

}
