import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdicionarEctoscopiaComponent } from './adicionar-ectoscopia.component';

describe('AdicionarEctoscopiaComponent', () => {
  let component: AdicionarEctoscopiaComponent;
  let fixture: ComponentFixture<AdicionarEctoscopiaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdicionarEctoscopiaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdicionarEctoscopiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
