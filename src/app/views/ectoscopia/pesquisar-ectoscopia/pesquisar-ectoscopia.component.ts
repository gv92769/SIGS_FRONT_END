import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TipoAvaliacaoMedica } from '../../../core/model/tipo-avaliacao-medica.enum';
import { AvaliacaoMedicaService } from '../../../core/service/avaliacao-medica.service';
import { CboService } from '../../../core/service/cbo.service';
import { PerfilService } from '../../../core/service/perfil.service';
import { CidadaoService } from '../../cidadao/cidadao.service';
import { PesquisaFichaComponent } from '../../pesquisa-ficha/pesquisa-ficha.component';
import { UsuarioService } from '../../usuario/usuario.service';

@Component({
  selector: 'app-pesquisar-ectoscopia',
  templateUrl: '../../pesquisa-ficha/pesquisa-ficha.component.html',
})
export class PesquisarEctoscopiaComponent extends PesquisaFichaComponent implements OnInit {

  constructor(cidadaoService: CidadaoService, private router: Router, private avaliacaoMedicaService: AvaliacaoMedicaService, usuarioService: UsuarioService, 
    cboService: CboService, perfilService: PerfilService, http: HttpClient) { 
      super(usuarioService, cidadaoService, cboService, perfilService, http);
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.createTable();

    this.titulo = 'Pesquisar Ectoscopia';

    this.tituloExcluir = "Ectoscopia";

    this.textoExcluir = " a Ectoscopia";
  }

  createTable() {
    this.columns = [
      { name: 'Data', sortable: true },
      { name: 'Horario', sortable: true },
      { name: 'Profissional', sortable: false },
      { name: 'Paciente', sortable: false },
    ];
  }

  onEditarClick(id: number) {
    this.router.navigate(['/ectoscopia/editar/', id]);
  }

  delete() {
    this.isLoadingExcluir = true;

    this.avaliacaoMedicaService.delete(this.excluirId).subscribe(
      response => this.search(),
      error => console.log('erro ao excluir ectoscopia'),
      () => {
        this.modalExcluir.hide();
        this.isLoadingExcluir = false;
        this.excluirId = null;
      }
    );
  }

  setPage(event) {
    this.page.pageNumber = event.offset;
    this.search();
  }

  onSort(event) {
    this.getSort(event);
    this.search();
  }

  onSubmit() {
    this.getSumit()
    this.search();
  }

  search() {
    this.getSearch()

    this.avaliacaoMedicaService.search(this.filter, TipoAvaliacaoMedica.ECTOSCOPIA)
      .subscribe({
        next: (response) => {
          this.page.content = response.content;
          this.page.totalElements = response.totalElements;
          this.isLoading = false;
        },
        error: (err) => {
          console.log('erro ao pesquisar ectoscopia');
          console.log(err);
          this.isLoading = false;
        }
      });
  }


}
