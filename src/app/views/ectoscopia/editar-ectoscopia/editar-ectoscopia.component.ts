import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EctoscopiaService } from '../../../core/service/ectoscopia.service';
import { ActivatedRoute } from '@angular/router';
import { EctoscopiaItensService } from '../ectoscopia-itens.service';
import { AdicionarEctoscopiaComponent } from '../adicionar-ectoscopia/adicionar-ectoscopia.component';
import { ToastrService } from 'ngx-toastr';
import { ProfissionalService } from '../../../core/service/profissional.service';
import { UsuarioService } from '../../usuario/usuario.service';
import { CidadaoService } from '../../cidadao/cidadao.service';
import { Cid10Service } from '../../../core/service/cid10.service';

@Component({
  selector: 'app-editar-ectoscopia',
  templateUrl: '../adicionar-editar-ectoscopia.component.html'
})
export class EditarEctoscopiaComponent extends AdicionarEctoscopiaComponent implements OnInit {
  
  tituloPagina: string = 'Visualizar/Editar Ectoscopia';
  
  id: number;

  constructor(protected ectoscopiaService: EctoscopiaService, protected router: Router, protected route: ActivatedRoute, 
    ectoscopiaItemService: EctoscopiaItensService, toastrService: ToastrService, usuarioService: UsuarioService,
    profissionalService: ProfissionalService, cidadaoService: CidadaoService, cid10Service: Cid10Service) {
    super(ectoscopiaService, router, route, ectoscopiaItemService, toastrService, usuarioService, profissionalService, cidadaoService, cid10Service);
  }

  ngOnInit() {
    super.ngOnInit();
    this.route.paramMap.subscribe(
      params => {
        // (+) before `params.get()` turns the string into a number
        this.id = +params.get('id');
        this.ectoscopiaService.findOne(this.id).subscribe(
          data => {
            this.ectoscopia = data;
            this.usuarioService.getCurrentUserInfo().subscribe(
              user => this.profissional = user.id == data.profissional.id ? false : true,
              error => console.log(error)
            );
          },
          error => console.log(error)
        );
      }
    );
  }

  onSaveButtonClick() : void {
    this.isLoading = true;

    this.ectoscopiaService.update(this.id, this.ectoscopia).subscribe(
      data => {
        this.toastrService.success('Ectoscopia salva com sucesso!', 'Sucesso');
        this.router.navigate(['/ectoscopia/adicionar']);
      },
      error => {
        this.toastrService.error('Error ao salvar ectoscopia.', 'Error');
        console.log(error);
      },
      () => this.isLoading = false
    );
  }

  onCancelButtonClick() : void {
    console.log('CANCEL');
    this.router.navigate(['/']);
  }

  onFinishButtonClick() : void {
    console.log('FINISH');
  }

}
