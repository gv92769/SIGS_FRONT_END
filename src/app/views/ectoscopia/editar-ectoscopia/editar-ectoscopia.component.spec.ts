import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarEctoscopiaComponent } from './editar-ectoscopia.component';

describe('EditarEctoscopiaComponent', () => {
  let component: EditarEctoscopiaComponent;
  let fixture: ComponentFixture<EditarEctoscopiaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarEctoscopiaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarEctoscopiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
