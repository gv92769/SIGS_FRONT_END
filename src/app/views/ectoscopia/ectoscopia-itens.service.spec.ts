import { TestBed } from '@angular/core/testing';

import { EctoscopiaItensService } from './ectoscopia-itens.service';

describe('EctoscopiaItensService', () => {
  let service: EctoscopiaItensService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EctoscopiaItensService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
