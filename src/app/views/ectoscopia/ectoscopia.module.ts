// Angular
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
// Components Routing
import { EctoscopiaRoutingModule } from './ectoscopia-routing.module';

import { FormsModule } from '@angular/forms';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { CustomComponentsModule } from '../../custom-components/custom-components.module';

import { AdicionarEctoscopiaComponent } from './adicionar-ectoscopia/adicionar-ectoscopia.component';
import { EditarEctoscopiaComponent } from './editar-ectoscopia/editar-ectoscopia.component';

import { ModalModule } from 'ngx-bootstrap/modal';

import { PesquisarCidadaoBiometriaModule } from '../cidadao/pesquisar-cidadao-biometria/pesquisar-cidadao-biometria.module';

import { ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule, IConfig } from 'ngx-mask';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SelectDropDownModule } from 'ngx-select-dropdown'
import { PesquisarEctoscopiaComponent } from './pesquisar-ectoscopia/pesquisar-ectoscopia.component';
import { ExameModule } from '../exame/exame.module';

export const options: Partial<IConfig> | (() => Partial<IConfig>) = { validation: false };

@NgModule({
  imports: [
    CommonModule,
    EctoscopiaRoutingModule,
    FormsModule,
    TabsModule,
    AccordionModule.forRoot(),
    CustomComponentsModule,
    ModalModule.forRoot(),
    PesquisarCidadaoBiometriaModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot(options), 
    NgxDatatableModule,
    SelectDropDownModule,
    ExameModule
  ],
  declarations: [
    AdicionarEctoscopiaComponent,
    EditarEctoscopiaComponent,
    PesquisarEctoscopiaComponent
  ]
})
export class EctoscopiaModule { }
