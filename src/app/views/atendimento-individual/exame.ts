import { ListaExames } from './lista-exames';

export class Exame {
    id: number;
    exame: ListaExames = new ListaExames();
    situacao?: Exame.Situacao;
}

export namespace Exame {
    export enum Situacao {
        S, A
    }
}
