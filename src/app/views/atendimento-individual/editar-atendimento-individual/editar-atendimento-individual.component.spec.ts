import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarAtendimentoIndividualComponent } from './editar-atendimento-individual.component';

describe('EditarAtendimentoIndividualComponent', () => {
  let component: EditarAtendimentoIndividualComponent;
  let fixture: ComponentFixture<EditarAtendimentoIndividualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarAtendimentoIndividualComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarAtendimentoIndividualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
