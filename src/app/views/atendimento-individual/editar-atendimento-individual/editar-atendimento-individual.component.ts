import { Component, OnInit, ViewChild } from '@angular/core';
import { AtendimentoIndividualService } from '../../../core/service/atendimento-individual.service';
import { BehaviorSubject } from 'rxjs';
import { AtendimentoIndividual } from '../../../core/model/atendimento-individual';
import { FormAtendimentoIndividualComponent } from '../form-atendimento-individual/form-atendimento-individual.component';
import { ActivatedRoute, Router } from '@angular/router';
import { map, mergeMap, finalize } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { FichaAtendimentoIndividual } from '../ficha-atendimento-individual';
import { UsuarioService } from '../../usuario/usuario.service';

@Component({
  selector: 'app-editar-atendimento-individual',
  templateUrl: './editar-atendimento-individual.component.html',
  styleUrls: ['./editar-atendimento-individual.component.scss']
})
export class EditarAtendimentoIndividualComponent implements OnInit {

  isLoadingSubmit: boolean = false;

  id: number;

  formData$: BehaviorSubject<AtendimentoIndividual> = new BehaviorSubject<AtendimentoIndividual>(new AtendimentoIndividual());

  @ViewChild('formAtendimentoIndividual', { static: false }) formAtendimentoIndividual: FormAtendimentoIndividualComponent;

  fichaAtendimentoIndividual: FichaAtendimentoIndividual = new FichaAtendimentoIndividual;

  profissional: boolean = false;

  constructor(private route: ActivatedRoute, private atendimentoIndividualService: AtendimentoIndividualService, private toastrService: ToastrService,
    private router: Router, private usuarioService: UsuarioService) { }

  ngOnInit() {
    this.fetchFormData();
  }

  private fetchFormData() {
    this.route.paramMap.pipe(
      map(params => {
        // (+) before `params.get()` turns the string into a number
        const id = +params.get('id');
        this.id = id;
        return id;
      }),
      mergeMap(id => {
        return this.atendimentoIndividualService.findbyId(id);
      }
      )
    )
      .subscribe({
        next: (response) => {
          this.fichaAtendimentoIndividual = response;
          this.usuarioService.getCurrentUserInfo().subscribe(
            data => this.profissional = data.id == response.atendimento.profissional.id ? false : true,
            error => console.log(error)
          );
          this.formData$.next(response.atendimento);
        },
        error: (err) => {
          console.log('Erro ao busca Atendimento Individual');
          console.log(err);
        }
      });
  }

  onSubmitted(atendimento: AtendimentoIndividual) {
    if (this.fichaAtendimentoIndividual.importada == false) {
      this.isLoadingSubmit = true;
      
      this.atendimentoIndividualService.update(this.id, atendimento)
        .pipe(finalize(() => this.isLoadingSubmit = false))
        .subscribe({
          next: (response) => {
            this.toastrService.success('Atendimento individual salvo com sucesso!', 'Sucesso');
            this.router.navigate(['atendimentoindividual/adicionar']);
          },
          error: (err) => {
            this.toastrService.error('Error ao salvar atendimento individual.', 'Error');
            console.log(err);
          }
        });
    }
  }

  onCanceled() {
    console.log('Cancelar');
  }

}
