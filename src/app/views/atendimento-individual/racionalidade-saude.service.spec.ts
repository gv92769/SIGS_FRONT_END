import { TestBed } from '@angular/core/testing';

import { RacionalidadeSaudeService } from './racionalidade-saude.service';

describe('RacionalidadeSaudeService', () => {
  let service: RacionalidadeSaudeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RacionalidadeSaudeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
