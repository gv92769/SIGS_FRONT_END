import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdicionarAtendimentoIndividualComponent } from './adicionar-atendimento-individual.component';

describe('AdicionarAtendimentoIndividualComponent', () => {
  let component: AdicionarAtendimentoIndividualComponent;
  let fixture: ComponentFixture<AdicionarAtendimentoIndividualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdicionarAtendimentoIndividualComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdicionarAtendimentoIndividualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
