import { Component, OnInit, ViewChild } from '@angular/core';
import { FormAtendimentoIndividualComponent } from '../form-atendimento-individual/form-atendimento-individual.component';
import { UsuarioService } from '../../usuario/usuario.service';
import { finalize } from 'rxjs/operators';
import { formatDate } from '@angular/common';
import { BehaviorSubject } from 'rxjs';
import { AtendimentoIndividual } from '../../../core/model/atendimento-individual';
import { AtendimentoIndividualService } from '../../../core/service/atendimento-individual.service';
import { UnidadeBasicaSaudeService } from '../../../core/service/unidade-basica-saude.service';
import * as jwt_decode from 'jwt-decode';
import { ToastrService } from 'ngx-toastr';
import { ProfissionalService } from '../../../core/service/profissional.service';
import { CboService } from '../../../core/service/cbo.service';

@Component({
  selector: 'app-adicionar-atendimento-individual',
  templateUrl: './adicionar-atendimento-individual.component.html',
  styleUrls: ['./adicionar-atendimento-individual.component.scss']
})
export class AdicionarAtendimentoIndividualComponent implements OnInit {

  isLoadingSubmit: boolean = false;

  idCidadao: number;
  
  formData$: BehaviorSubject<AtendimentoIndividual> = new BehaviorSubject<AtendimentoIndividual>(new AtendimentoIndividual());

  @ViewChild('formAtendimentoIndividual', { static: false }) formAtendimentoIndividual: FormAtendimentoIndividualComponent;

  constructor(private usuarioService: UsuarioService, private profissionalSerice: ProfissionalService, private toastrService: ToastrService,
    private atendimentoIndividualService: AtendimentoIndividualService, private unidadeSaudeBasicaService: UnidadeBasicaSaudeService,
    private cboService: CboService) {
  }

  ngOnInit() {
    this.fetchFormData();
  }

  private fetchFormData() {
    let atendimentoIndividual: AtendimentoIndividual = new AtendimentoIndividual();

    atendimentoIndividual.dataRealizacao = formatDate(new Date(), 'yyyy-MM-dd', 'en-US');

    atendimentoIndividual.dataHorarioInicio = formatDate(new Date(), "yyyy-MM-dd'T'HH:mm:ss", 'en-US');
    
    this.usuarioService.getCurrentUserInfo().subscribe(
      data => {
        this.profissionalSerice.findById(data.id).subscribe(
          profissional => atendimentoIndividual.profissional = profissional,
          error => console.log(error)
        );
      },
      error => console.log(error)
    );

    this.unidadeSaudeBasicaService.findByCnes(jwt_decode(sessionStorage.getItem('access_token')).cnes)
      .subscribe(ubs => atendimentoIndividual.ubs = ubs)

    this.cboService.findByCodigo(jwt_decode(sessionStorage.getItem('access_token')).cbo)
      .subscribe(cbo => atendimentoIndividual.cbo = cbo);

    this.formData$.next(atendimentoIndividual);
  }

  onSubmitted(atendimento: AtendimentoIndividual) {
    this.isLoadingSubmit = true;
    this.atendimentoIndividualService.create(atendimento)
      .pipe(finalize(() => this.isLoadingSubmit = false))
      .subscribe({
        next: (response) => {
          this.toastrService.success('Atendimento individual salvo com sucesso!', 'Sucesso');
          this.ngOnInit();
        },
        error: (err) => {
          this.toastrService.error('Error ao salvar atendimento individual.', 'Error');
          console.log(err);
        }
      });
  }

  onCanceled(){
    console.log('Cancelar');
  }

}
