import { Injectable } from '@angular/core';
import { CiapCondicaoAvaliada } from './ciap-condicao-avaliada';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CiapCondicaoAvaliadaService {

  readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/ciapcondicaoavaliada';
  }

  public findAll(): Observable<CiapCondicaoAvaliada[]> {
    return this.http.get<CiapCondicaoAvaliada[]>(this.url);
  }

  public findAllById(ids: number[]): Observable<CiapCondicaoAvaliada[]> {
    return this.http.post<CiapCondicaoAvaliada[]>(this.url, ids);
  }

}
