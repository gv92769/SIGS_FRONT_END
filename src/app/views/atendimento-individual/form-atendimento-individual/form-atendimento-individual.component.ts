import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { formatDate } from '@angular/common';
import { Cid10Service } from '../../../core/service/cid10.service';
import { Ciap2Service } from '../../../core/service/ciap2.service';
import { AtendimentoIndividual } from '../../../core/model/atendimento-individual';
import { Observable } from 'rxjs';
import { LocalAtendimentoService } from '../../../core/service/local-atendimento.service';
import { ObjectCodigoDescricao } from '../../../core/model/object-codigo-descricao';
import { TipoAtendimentoService } from '../../../core/service/tipo-atendimento.service';
import { AleitamentoMaternoService } from '../aleitamento-materno.service';
import { CiapCondicaoAvaliadaService } from '../ciap-condicao-avaliada.service';
import { CiapCondicaoAvaliada } from '../ciap-condicao-avaliada';
import { NasfService } from '../nasf.service';
import { RacionalidadeSaudeService } from '../racionalidade-saude.service';
import { CondutaEncaminhamentoService } from '../conduta-encaminhamento.service';
import { ListaExamesService } from '../lista-exames.service';
import { ListaExames } from '../lista-exames';
import { Exame } from '../exame';
import { Ciap2 } from '../../../core/model/ciap2';
import { Cid10 } from '../../../core/model/cid10';
import { CidadaoService } from '../../cidadao/cidadao.service';
import { Cidadao } from '../../cidadao/cidadao';
import { Config } from '../../../core/model/config';
import { Procedimento } from '../../../core/model/procedimento';
import { ProcedimentoService } from '../../../core/service/procedimento.service';
import { AtendimentoIndividualService } from '../../../core/service/atendimento-individual.service';
import { UsuarioService } from '../../usuario/usuario.service';

@Component({
  selector: 'app-form-atendimento-individual',
  templateUrl: './form-atendimento-individual.component.html',
  styleUrls: ['./form-atendimento-individual.component.scss']
})
export class FormAtendimentoIndividualComponent implements OnInit {

  @ViewChild('form', { static: false }) form: FormGroup;

  atendimento: AtendimentoIndividual = new AtendimentoIndividual();

  idCidadao: number;

  @Input('formData') formData$: Observable<AtendimentoIndividual>;

  @Output('onSubmit') onSubmitEventEmitter = new EventEmitter();

  locais: ObjectCodigoDescricao[] = new Array;

  tiposAtendimento: ObjectCodigoDescricao[] = new Array;

  aleitamento: ObjectCodigoDescricao[] = new Array;

  ciaps: CiapCondicaoAvaliada[] = new Array;

  nasfs: ObjectCodigoDescricao[] = new Array;

  opRacionalidade: ObjectCodigoDescricao[] = new Array;

  condutas: ObjectCodigoDescricao[] = new Array;

  encaminhamentos: ObjectCodigoDescricao[] = new Array;

  exames: ListaExames[] = new Array;

  opNeonatal: ListaExames[] = new Array;

  ciapTransmissiveis: CiapCondicaoAvaliada[] = new Array;

  ciapRastreamento: CiapCondicaoAvaliada[] = new Array;

  optionsCiap: Ciap2[] = new Array;

  config: Config = new Config();

  selectedCiap: Ciap2[] = new Array;

  optionsCid: Cid10[] = new Array;

  selectedCid: Cid10[] = new Array;

  opCidadao: Cidadao[] = new Array;

  configCidadao: Config = new Config();

  optionsProced: Procedimento[] = new Array;

  configExame: Config = new Config();

  importada: boolean = false;

  profissional: boolean = false;

  constructor(private cid10Service: Cid10Service, private ciap2Service: Ciap2Service, private nasfService: NasfService,
    private localAtendimentoService: LocalAtendimentoService, private tipoAtendimentoService: TipoAtendimentoService,
    private aleitamentoMaternoService: AleitamentoMaternoService, private ciapCondicaoService: CiapCondicaoAvaliadaService,
    private racionalidadeService: RacionalidadeSaudeService, private condutaService: CondutaEncaminhamentoService,
    private exameService: ListaExamesService, private cidadaoService: CidadaoService, private procedimentoService: ProcedimentoService,
    private atendimentoService: AtendimentoIndividualService, private usuarioService: UsuarioService) {
  }

  ngOnInit() {
    this.fillForm();

    this.localAtendimentoService.findAll()
      .subscribe(data => this.locais = data);

    let ids = [1, 2, 3, 4, 5, 6];
    this.tipoAtendimentoService.findAllById(ids)
      .subscribe(data => this.tiposAtendimento = data);

    this.aleitamentoMaternoService.findAll()
      .subscribe(data => this.aleitamento = data);

    this.nasfService.findAll()
      .subscribe(data => this.nasfs = data);

    this.racionalidadeService.findAll()
      .subscribe(data => this.opRacionalidade = data);

    ids = [1, 2, 12, 3, 9];
    this.condutaService.findAllById(ids)
      .subscribe(data => this.condutas = data);

    ids = [11, 4, 5, 6, 7, 8, 10];
    this.condutaService.findAllById(ids)
      .subscribe(data => this.encaminhamentos = data);

    ids = [1, 3, 4, 5, 8, 9, 10, 11, 12, 16, 17, 18, 19, 20, 21, 22];
    this.ciapCondicaoService.findAllById(ids)
      .subscribe(data => this.ciaps = data);

    ids = [20, 6, 2, 7];
    this.ciapCondicaoService.findAllById(ids)
      .subscribe(data => this.ciapTransmissiveis = data);

    ids = [13, 14, 15];
    this.ciapCondicaoService.findAllById(ids)
      .subscribe(data => this.ciapRastreamento = data);

    this.config.placeholder = 'Selecione até 2 opções';
    this.config.displayKey = 'descricao';
    this.config.searchOnKey = 'descricao';

    this.configCidadao.displayKey = 'nome';
    this.configCidadao.placeholder = 'Digite o CPF/CNS ou Nome';

    this.configExame.placeholder = 'Selecione o procedimento';
    this.configExame.displayKey = 'descricao';
    this.configExame.searchOnKey = 'descricao';
  }

  private fillForm() {
    this.formData$.subscribe({
      next: (value) => {
        this.atendimento = value;
        this.verificarCampos();
        if (value.id != null) {
          this.usuarioService.getCurrentUserInfo().subscribe(
            data => this.profissional = data.id == value.profissional.id ? false : true,
            error => console.log(error)
          );

          this.atendimentoService.importada(value.id)
            .subscribe(
              data => this.importada = data,
              error => console.log(error)
            );
        }
      },
      error: (err) => {
        console.log('erro behavior subject');
        console.log(err);
      }
    });
  }

  verificarCampos() {
    if (this.atendimento.ciap2_1 != null) {
      this.atendimento.ciap2_1.descricao = this.atendimento.ciap2_1.codigo + " - " + this.atendimento.ciap2_1.descricao;
      this.selectedCiap.push(this.atendimento.ciap2_1);
    }

    if (this.atendimento.ciap2_2 != null) {
      this.atendimento.ciap2_2.descricao = this.atendimento.ciap2_2.codigo + " - " + this.atendimento.ciap2_2.descricao;
      this.selectedCiap.push(this.atendimento.ciap2_2);
    }

    if (this.atendimento.cid10_1 != null) {
      this.atendimento.cid10_1.descricao = this.atendimento.cid10_1.codigo + " - " + this.atendimento.cid10_1.descricao;
      this.selectedCid.push(this.atendimento.cid10_1);
    }

    if (this.atendimento.cid10_2 != null) {
      this.atendimento.cid10_2.descricao = this.atendimento.cid10_2.codigo + " - " + this.atendimento.cid10_2.descricao;
      this.selectedCid.push(this.atendimento.cid10_2);
    }

    if (this.atendimento.outrosSia1Codigo != null)
      this.atendimento.outrosSia1Codigo.descricao = this.atendimento.outrosSia1Codigo.codigo + " - " + this.atendimento.outrosSia1Codigo.descricao;

    if (this.atendimento.outrosSia2Codigo != null)
      this.atendimento.outrosSia2Codigo.descricao = this.atendimento.outrosSia2Codigo.codigo + " - " + this.atendimento.outrosSia2Codigo.descricao;

    if (this.atendimento.outrosSia3Codigo != null)
      this.atendimento.outrosSia3Codigo.descricao = this.atendimento.outrosSia3Codigo.codigo + " - " + this.atendimento.outrosSia3Codigo.descricao;

    if (this.atendimento.outrosSia4Codigo != null)
      this.atendimento.outrosSia4Codigo.descricao = this.atendimento.outrosSia4Codigo.codigo + " - " + this.atendimento.outrosSia4Codigo.descricao;

    this.atendimento.aleitamentoMaterno = this.atendimento.aleitamentoMaterno == null ? new ObjectCodigoDescricao() : this.atendimento.aleitamentoMaterno;
    this.atendimento.ciaps = this.atendimento.ciaps == null ? new Array : this.atendimento.ciaps;
    this.atendimento.listExame = this.atendimento.listExame || new Array;
    this.atendimento.nasfs = this.atendimento.nasfs == null ? new Array : this.atendimento.nasfs;
    this.atendimento.racionalidadeSaude = this.atendimento.racionalidadeSaude == null ? new ObjectCodigoDescricao() : this.atendimento.racionalidadeSaude;

    let exs: Exame[] = this.atendimento.listExame;

    let ids = [18, 20, 21];

    this.atendimento.listExame = [];

    this.exameService.findAllByNotInId(ids)
      .subscribe(data => {
        this.exames = data;
        data.forEach(exame => {
          let ex: Exame = new Exame();
          ex.exame = exame;
          let index = exs.findIndex(e => e.exame.id == exame.id);
          if (index != -1) {
            ex.id = exs[index].id;
            ex.situacao = exs[index].situacao;
          }
          this.atendimento.listExame.push(ex);
        });
      });

    this.exameService.findAllById(ids)
      .subscribe(data => {
        this.opNeonatal = data;
        data.forEach(exame => {
          let ex: Exame = new Exame();
          ex.exame = exame;
          let index = exs.findIndex(e => e.exame.id == exame.id);
          if (index != -1) {
            ex.id = exs[index].id;
            ex.situacao = exs[index].situacao;
          }
          this.atendimento.listExame.push(ex);
        });
      });
  }

  onFormSubmit() {
    if (this.form.valid && this.selectedCiap.length <= 2 && this.selectedCid.length <= 2 && this.atendimento.condutas.length > 0) {
      this.atendimento.listExame = this.atendimento.listExame.filter(exame => exame.situacao != null &&
        (exame.situacao.toString() == "S" || exame.situacao.toString() == "A"));
      this.atendimento.dataHorarioFinal = formatDate(new Date(), "yyyy-MM-dd'T'HH:mm:ss", 'en-US');

      if (this.selectedCiap.length > 0) {
        if (this.selectedCiap.length > 1)
          this.atendimento.ciap2_2 = this.selectedCiap[1];
        this.atendimento.ciap2_1 = this.selectedCiap[0];
      }

      if (this.selectedCid.length > 0) {
        if (this.selectedCid.length > 1)
          this.atendimento.cid10_2 = this.selectedCid[1];
        this.atendimento.cid10_1 = this.selectedCid[0];
      }

      if (this.atendimento.outrosSia1 == null)
        this.atendimento.outrosSia1Codigo = null;

      if (this.atendimento.outrosSia2 == null)
        this.atendimento.outrosSia2Codigo = null;

      if (this.atendimento.outrosSia3 == null)
        this.atendimento.outrosSia3Codigo = null;

      if (this.atendimento.outrosSia4 == null)
        this.atendimento.outrosSia4Codigo = null;

      this.selectedCid = new Array;
      this.selectedCiap = new Array;

      this.onSubmitEventEmitter.emit(this.atendimento);
    }
  }

  buscarCidadao(valor: string) {
    if (valor != null && valor != '')
      this.cidadaoService.filtro(valor)
        .subscribe({
          next: (data) => {
            this.opCidadao = data;
          },
          error: (err) => {
            console.log(err);
          }
        });
  }

  searchChangeExame(texto: string) {
    let procedimentoFiltro = {
      texto: texto,
      sexo: this.atendimento.cidadao.sexo == 'MASCULINO' ? true : false
    }

    this.procedimentoService.searchExame(procedimentoFiltro)
      .subscribe(
        data => this.optionsProced = data,
        error => console.log(error)
      );
  }

  searchCiap2(valor: string) {
    if (valor != null && valor != '')
      this.ciap2Service.search(valor, this.atendimento.cidadao.sexo == 'FEMININO' ? 1 : 0)
        .subscribe(
          data => this.optionsCiap = data,
          error => console.log("Error ao Buscar Ciap2", error)
        );
  }

  searchCid10(valor: string) {
    if (valor != null && valor != '')
      this.cid10Service.search(valor, this.atendimento.cidadao.sexo == 'FEMININO' ? 1 : 0)
        .subscribe(
          data => this.optionsCid = data,
          error => console.log("Error ao buscar Cid10", error)
        );
  }

  nasfChange(nasf: ObjectCodigoDescricao) {
    let index = this.atendimento.nasfs.findIndex(x => x.codigo == nasf.codigo)
    if (index == -1) {
      this.atendimento.nasfs.push(nasf);
    }
    else {
      this.atendimento.nasfs.splice(index, 1);
    }
  }

  nasfChecked(nasf: ObjectCodigoDescricao) {
    if (this.atendimento.nasfs.findIndex(x => x.codigo == nasf.codigo) != -1)
      return true;

    return false;
  }

  condutasEncaminhamentoChange(op: ObjectCodigoDescricao) {
    let index = this.atendimento.condutas.findIndex(x => x.codigo == op.codigo)
    if (index == -1) {
      this.atendimento.condutas.push(op);
    }
    else {
      this.atendimento.condutas.splice(index, 1);
    }
  }

  condutasEncaminhamentoChecked(op: ObjectCodigoDescricao) {
    if (this.atendimento.condutas.findIndex(x => x.codigo == op.codigo) != -1)
      return true;

    return false;
  }

  ciapChange(op: CiapCondicaoAvaliada) {
    let index = this.atendimento.ciaps.findIndex(x => x.id == op.id);
    if (index == -1) {
      this.atendimento.ciaps.push(op);
    }
    else {
      this.atendimento.ciaps.splice(index, 1);
    }
  }

  ciapChecked(op: CiapCondicaoAvaliada) {
    if (this.atendimento.ciaps.findIndex(x => x.id == op.id) != -1)
      return true;

    return false;
  }

}
