import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormAtendimentoIndividualComponent } from './form-atendimento-individual.component';

describe('FormAtendimentoIndividualComponent', () => {
  let component: FormAtendimentoIndividualComponent;
  let fixture: ComponentFixture<FormAtendimentoIndividualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormAtendimentoIndividualComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormAtendimentoIndividualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
