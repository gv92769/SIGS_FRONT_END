import { AtendimentoIndividual } from '../../core/model/atendimento-individual';

export class FichaAtendimentoIndividual {
  uuid: string;
  atendimento: AtendimentoIndividual;
  importada: boolean;
}
