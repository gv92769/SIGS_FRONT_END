import { TestBed } from '@angular/core/testing';

import { CiapCondicaoAvaliadaService } from './ciap-condicao-avaliada.service';

describe('CiapCondicaoAvaliadaService', () => {
  let service: CiapCondicaoAvaliadaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CiapCondicaoAvaliadaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
