import { Ciap2 } from '../../core/model/ciap2';

export class CiapCondicaoAvaliada {
    id: number;
    ciap2: Ciap2;
    descricao: string;
    codigoab: string;
    sexo: string;
}
