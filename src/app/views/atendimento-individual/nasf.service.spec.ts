import { TestBed } from '@angular/core/testing';

import { NasfService } from './nasf.service';

describe('NasfService', () => {
  let service: NasfService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NasfService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
