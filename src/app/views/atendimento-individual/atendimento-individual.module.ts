import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdicionarAtendimentoIndividualComponent } from './adicionar-atendimento-individual/adicionar-atendimento-individual.component';
import { EditarAtendimentoIndividualComponent } from './editar-atendimento-individual/editar-atendimento-individual.component'
import { FormAtendimentoIndividualComponent } from './form-atendimento-individual/form-atendimento-individual.component';
import { FormsModule } from '@angular/forms';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { CustomComponentsModule } from '../../custom-components/custom-components.module';
import { ModalModule } from 'ngx-bootstrap/modal';

import { PesquisarCidadaoBiometriaModule } from '../cidadao/pesquisar-cidadao-biometria/pesquisar-cidadao-biometria.module';

import { ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule, IConfig } from 'ngx-mask';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { AtendimentoIndividualRoutingModule } from './atendimento-individual-routing.module';
import { PesquisarAtendimentoIndividualComponent } from './pesquisar-atendimento-individual/pesquisar-atendimento-individual.component';
import { SelectDropDownModule } from 'ngx-select-dropdown';
import { ExameModule } from '../exame/exame.module';

export const options: Partial<IConfig> | (() => Partial<IConfig>) = { validation: false };

@NgModule({
  declarations: [
    AdicionarAtendimentoIndividualComponent,
    EditarAtendimentoIndividualComponent,
    FormAtendimentoIndividualComponent,
    PesquisarAtendimentoIndividualComponent
  ],
  imports: [
    CommonModule,
    AtendimentoIndividualRoutingModule,
    FormsModule,
    TabsModule,
    AccordionModule.forRoot(),
    CustomComponentsModule,
    ModalModule.forRoot(),
    PesquisarCidadaoBiometriaModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot(options), 
    NgxDatatableModule,
    SelectDropDownModule,
    ExameModule
  ]
})
export class AtendimentoIndividualModule { }
