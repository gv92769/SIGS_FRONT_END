import { Injectable } from '@angular/core';
import { ObjectCodigoDescricao } from '../../core/model/object-codigo-descricao';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CondutaEncaminhamentoService {

  readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/condutaencaminhamento';
  }

  public findAll(): Observable<ObjectCodigoDescricao[]> {
    return this.http.get<ObjectCodigoDescricao[]>(this.url);
  }

  public findAllById(ids: number[]): Observable<ObjectCodigoDescricao[]> {
    return this.http.post<ObjectCodigoDescricao[]>(this.url, ids);
  }
  
}
