import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PesquisarAtendimentoIndividualComponent } from './pesquisar-atendimento-individual.component';

describe('PesquisarAtendimentoIndividualComponent', () => {
  let component: PesquisarAtendimentoIndividualComponent;
  let fixture: ComponentFixture<PesquisarAtendimentoIndividualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PesquisarAtendimentoIndividualComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PesquisarAtendimentoIndividualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
