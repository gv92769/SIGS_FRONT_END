import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { ObjectCodigoDescricao } from '../../core/model/object-codigo-descricao';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NasfService {

  readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/nasf';
  }

  public findAll(): Observable<ObjectCodigoDescricao[]> {
    return this.http.get<ObjectCodigoDescricao[]>(this.url);
  }

}
