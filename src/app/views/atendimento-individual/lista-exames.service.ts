import { Injectable } from '@angular/core';
import { ListaExames } from './lista-exames';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ListaExamesService {

  readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/listaexames';
  }

  public findAll(): Observable<ListaExames[]> {
    return this.http.get<ListaExames[]>(this.url);
  }

  public findAllById(ids: number[]): Observable<ListaExames[]> {
    return this.http.post<ListaExames[]>(this.url+'/findAllById', ids);
  }

  public findAllByNotInId(ids: number[]): Observable<ListaExames[]> {
    return this.http.post<ListaExames[]>(this.url+'/findAllByNotInId', ids);
  }

}
