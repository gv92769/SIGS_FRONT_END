import { TestBed } from '@angular/core/testing';

import { AleitamentoMaternoService } from './aleitamento-materno.service';

describe('AleitamentoMaternoService', () => {
  let service: AleitamentoMaternoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AleitamentoMaternoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
