import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { ObjectCodigoDescricao } from '../../core/model/object-codigo-descricao';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AleitamentoMaternoService {

  readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/aleitamentomaterno';
  }

  public findAll(): Observable<ObjectCodigoDescricao[]> {
    return this.http.get<ObjectCodigoDescricao[]>(this.url);
  }

}
