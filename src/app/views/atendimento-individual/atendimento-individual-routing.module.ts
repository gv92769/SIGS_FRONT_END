import { NgModule } from '@angular/core';
import { AdicionarAtendimentoIndividualComponent } from './adicionar-atendimento-individual/adicionar-atendimento-individual.component';
import { EditarAtendimentoIndividualComponent } from './editar-atendimento-individual/editar-atendimento-individual.component';
import { RouterModule, Routes } from '@angular/router';
import { PesquisarAtendimentoIndividualComponent } from './pesquisar-atendimento-individual/pesquisar-atendimento-individual.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Atendimento Individual'
    },
    children: [
      {
        path: 'pesquisar',
        component: PesquisarAtendimentoIndividualComponent,
        data: {
          title: 'Pesquisar'
        }
      },
      {
        path: 'adicionar',
        component: AdicionarAtendimentoIndividualComponent,
        data: {
          title: 'Adicionar'
        }
      },
      {
        path: 'editar/:id',
        component: EditarAtendimentoIndividualComponent,
        data: {
          title: 'Editar'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AtendimentoIndividualRoutingModule { }
