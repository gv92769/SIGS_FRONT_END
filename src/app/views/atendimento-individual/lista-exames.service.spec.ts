import { TestBed } from '@angular/core/testing';

import { ListaExamesService } from './lista-exames.service';

describe('ListaExamesService', () => {
  let service: ListaExamesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ListaExamesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
