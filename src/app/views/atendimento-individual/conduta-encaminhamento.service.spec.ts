import { TestBed } from '@angular/core/testing';

import { CondutaEncaminhamentoService } from './conduta-encaminhamento.service';

describe('CondutaEncaminhamentoService', () => {
  let service: CondutaEncaminhamentoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CondutaEncaminhamentoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
