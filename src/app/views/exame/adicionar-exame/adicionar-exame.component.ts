import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject } from 'rxjs';
import { ProfissionalService } from '../../../core/service/profissional.service';
import { UsuarioService } from '../../usuario/usuario.service';
import { Solicitacao } from '../solicitacao';
import { ExameService } from '../exame.service';
import { finalize } from 'rxjs/operators';
import { UnidadeBasicaSaudeService } from '../../../core/service/unidade-basica-saude.service';
import * as jwt_decode from 'jwt-decode';
import { Profissional } from '../../../core/model/profissional';

import jsPDF from 'jspdf';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import htmlToPdfmake from 'html-to-pdfmake';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-adicionar-exame',
  templateUrl: './adicionar-exame.component.html',
  styleUrls: ['./adicionar-exame.component.css']
})
export class AdicionarExameComponent implements OnInit {

  formData$: BehaviorSubject<Solicitacao> = new BehaviorSubject<Solicitacao>(new Solicitacao());

  @ViewChild('examepdf', {static: false}) pdfTable: ElementRef;

  @ViewChild('formExame', { static: false }) formExame: Solicitacao;

  isLoadingSubmit: boolean = false;

  solicitacao: Solicitacao = new Solicitacao();

  profissional: Profissional = new Profissional();

  usuario: any

  geraPdf:boolean = false;

  pdfImageBase64: any;

  constructor(private usuarioService: UsuarioService, private profissionalService: ProfissionalService, private toastrService: ToastrService,
    private exameService: ExameService, private unidadeSaudeBasicaService: UnidadeBasicaSaudeService, private http: HttpClient) { }

  ngOnInit(): void {
    

    this.usuarioService.getCurrentUserInfo().subscribe(
      data =>{
        this.usuario = data; 
        this.profissionalService.findById(data.id) .subscribe(
          profissional =>{
            this.profissional = this.solicitacao.profissional = profissional;
            this.profissional.nome = this.usuario.nome;
          } );
      }
    );

    this.unidadeSaudeBasicaService.findByCnes(jwt_decode(sessionStorage.getItem('access_token')).cnes)
      .subscribe(ubs => this.solicitacao.ubs = ubs);

    this.formData$.next(this.solicitacao);

    this.http.get('assets/img/brand/sigs-logo-pdf.jpeg', { responseType: 'blob' })
        .subscribe(res => {
          const reader = new FileReader();
          reader.onloadend = () => {
            var base64data = reader.result;                
                this.pdfImageBase64 = base64data;
          }
  
          reader.readAsDataURL(res);
    });
  }

  onSubmitted(solicitacao: Solicitacao) {
    var activeButton = document.activeElement.id;

    this.isLoadingSubmit = true;
    this.exameService.saveSolicitacao(solicitacao)
      .pipe(finalize(() => this.isLoadingSubmit = false))
      .subscribe({
        next: (response) => {

          if(activeButton=="submitPdf"){
            this.geraPDF();
          }

          this.toastrService.success('Solicitação de Exame salva com sucesso!', 'Sucesso');
          this.ngOnInit();
        },
        error: (err) => {
          this.toastrService.error('Error ao salvar solicitação de Exame.', 'Error');
          console.log(err);
        }
      });
  }

  geraPDF() {
    const doc = new jsPDF();
   
    const pdfTable = this.pdfTable.nativeElement;
   
    var html = htmlToPdfmake(pdfTable.innerHTML);
     
    const documentDefinition = { 
      content: [html,
        {
          image:  this.pdfImageBase64,
          fit: [75,75],
          absolutePosition: {x: 500, y: 0}
        }]
    };

    pdfMake.createPdf(documentDefinition).open(); 
     
  }

}
