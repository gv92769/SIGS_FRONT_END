import { formatDate } from "@angular/common";
import { Documento } from "./documento";

export class ResultadoExame {
  id: number;
  dataResultado: string = formatDate(new Date(), 'yyyy-MM-dd', 'en-US');
  dataRealizacao: string;
  observacaoResultado: string;
  documentos: Documento[];
}
