import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarExameComponent } from './editar-exame.component';

describe('EditarExameComponent', () => {
  let component: EditarExameComponent;
  let fixture: ComponentFixture<EditarExameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarExameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarExameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
