import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject } from 'rxjs';
import { finalize, map, mergeMap } from 'rxjs/operators';
import { ExameService } from '../exame.service';
import { Solicitacao } from '../solicitacao';

@Component({
  selector: 'app-editar-exame',
  templateUrl: './editar-exame.component.html',
  styleUrls: ['./editar-exame.component.css']
})
export class EditarExameComponent implements OnInit {

  formData$: BehaviorSubject<Solicitacao> = new BehaviorSubject<Solicitacao>(new Solicitacao());

  @ViewChild('formExame', { static: false }) formExame: Solicitacao;

  isLoadingSubmit: boolean = false;

  id: number;

  constructor(private route: ActivatedRoute, private exameService: ExameService, private toastrService: ToastrService,
    private router: Router) { }

  ngOnInit(): void {
    this.route.paramMap.pipe(
      map(params => {
        const id = +params.get('id');
        this.id = id;
        return id;
      }),
      mergeMap(id => {
        return this.exameService.solicitacaoFindById(id);
      }
      )
    )
      .subscribe({
        next: (response) => {
          this.formData$.next(response);
        },
        error: (err) => {
          console.log('Erro ao buscar Solicitação de Exame');
          console.log(err);
        }
      });
  }

  onSubmitted(solicitacao: Solicitacao) {
    this.isLoadingSubmit = true;

    this.exameService.updateSolicitacao(this.id, solicitacao)
      .pipe(finalize(() => this.isLoadingSubmit = false))
      .subscribe({
        next: (response) => {
          this.toastrService.success('Solicitação de Exame salva com sucesso!', 'Sucesso');
          this.router.navigate(['exame/solicitacao/adicionar']);
        },
        error: (err) => {
          this.toastrService.error('Error ao salvar Solicitação de Exame.', 'Error');
          console.log(err);
        }
      });
  }

}
