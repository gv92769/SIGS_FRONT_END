import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Profissional } from '../../../core/model/profissional';
import { Solicitacao } from '../solicitacao';

@Component({
  selector: 'app-pdf-exame',
  templateUrl: './pdf-exame.component.html',
  styleUrls: ['./pdf-exame.component.css']
})
export class PdfExameComponent implements OnInit {

  @Input() solicitacao: Solicitacao;
  @Input() profissional: Profissional

  today= new Date();

  pdfImageBase64: any;

  constructor(
    private http: HttpClient,
    private sanitizer:DomSanitizer ) { }

  ngOnInit(): void {
    this.http.get('assets/img/brand/sigs-logo-pdf.jpeg', { responseType: 'blob' })
      .subscribe(res => {
        const reader = new FileReader();
        reader.onloadend = () => {
          var base64data = reader.result;                
              this.pdfImageBase64 = base64data;
        }

        reader.readAsDataURL(res);
      });
  }
  
  idade() {

    if(!this.solicitacao.cidadao.dataNascimento){
      return "";
    }

    var d = new Date,
        ano_atual = d.getFullYear(),
        mes_atual = d.getMonth() + 1,
        dia_atual = d.getDate(),

        ano_aniversario = +this.solicitacao.cidadao.dataNascimento.split('-')[0],
        mes_aniversario = +this.solicitacao.cidadao.dataNascimento.split('-')[1],
        dia_aniversario = +this.solicitacao.cidadao.dataNascimento.split('-')[2],

        quantos_anos = ano_atual - ano_aniversario;

    if (mes_atual < mes_aniversario || mes_atual == mes_aniversario && dia_atual < dia_aniversario) {
        quantos_anos--;
    }

      return quantos_anos < 0 ? 0 : quantos_anos;
  }

  transform(){
    return this.sanitizer.bypassSecurityTrustResourceUrl(this.pdfImageBase64);
  }

}
