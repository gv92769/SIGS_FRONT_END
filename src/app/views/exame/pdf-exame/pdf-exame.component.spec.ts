import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PdfExameComponent } from './pdf-exame.component';

describe('PdfExameComponent', () => {
  let component: PdfExameComponent;
  let fixture: ComponentFixture<PdfExameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PdfExameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PdfExameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
