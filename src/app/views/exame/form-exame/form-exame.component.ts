import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { Config } from '../../../core/model/config';
import { Procedimento } from '../../../core/model/procedimento';
import { ProcedimentoSubGrupo } from '../../../core/model/procedimento-sub-grupo';
import { ProcedimentoGrupoService } from '../../../core/service/procedimento-grupo.service';
import { ProcedimentoSubGrupoService } from '../../../core/service/procedimento-sub-grupo.service';
import { ProcedimentoService } from '../../../core/service/procedimento.service';
import { Cidadao } from '../../cidadao/cidadao';
import { CidadaoService } from '../../cidadao/cidadao.service';
import { UsuarioService } from '../../usuario/usuario.service';
import { Solicitacao } from '../solicitacao';

@Component({
  selector: 'app-form-exame',
  templateUrl: './form-exame.component.html',
  styleUrls: ['./form-exame.component.css']
})
export class FormExameComponent implements OnInit {

  @Input('formData') formData$: Observable<Solicitacao>;

  solicitacao: Solicitacao = new Solicitacao();

  @ViewChild('form', { static: false }) form: FormGroup;

  @Output('onSubmit') onSubmitEventEmitter = new EventEmitter();

  configSubGrupo: Config = new Config();

  subgrupos: ProcedimentoSubGrupo[] = new Array;
  
  subgruposSearch: ProcedimentoSubGrupo[] = new Array;

  config: Config = new Config();

  procedimentos: Procedimento[] = new Array;

  opCidadao: Cidadao[] = new Array;

  configCidadao: Config = new Config();

  profissional: boolean = false;

  constructor(private cidadaoService: CidadaoService, private procedimentoService: ProcedimentoService, private grupoService: ProcedimentoGrupoService,
    private subGrupoService: ProcedimentoSubGrupoService, private usuarioService: UsuarioService) { }

  ngOnInit(): void {
    this.formData$.subscribe({
      next: (value) => {
        if (value.id != null) {
          this.usuarioService.getCurrentUserInfo().subscribe(
            data => this.profissional = data.id == value.profissional.id ? false : true,
            error => console.log(error)
          );
        }
        this.solicitacao = value;
      },
      error: (err) => {
        console.log('erro behavior subject');
        console.log(err);
      }
    });

    let ids = [2];
    this.grupoService.findAllById(ids).subscribe(
      data => {
        this.subGrupoService.findAllByGrupo(data[0])
          .subscribe(
            subgrupos => this.subgrupos = this.subgruposSearch = subgrupos,
            error => console.log("error in findAllByGrupo subgrupo()")
          );
      },
      error => console.log("error in findAllById grupo()")
    );

    this.configSubGrupo.search = true;
    this.configSubGrupo.displayKey = 'descricao';
    this.configSubGrupo.placeholder = 'Escolha um SubGrupo';

    this.config.displayKey = 'descricao';
    this.config.placeholder = 'Escolha um ou mais Procedimentos';
    this.config.searchOnKey = 'descricao';

    this.configCidadao.displayKey = 'nome';
    this.configCidadao.placeholder = 'Digite o CPF/CNS ou Nome';
  }

  buscarCidadao(valor: string) {
    if (valor != null && valor != '') {
      this.cidadaoService.filtro(valor)
        .subscribe({
          next: (data) => {
            this.opCidadao = data;
          },
          error: (err) => {
            console.log(err);
          }
        });
    }
  }

  subgrupoChange(event: Event) {
    this.solicitacao.subGrupo = event['value'];
    this.solicitacao.procedimentos = new Array;
  }

  searchSubGrupoChange(texto: string) {
    this.subgruposSearch =  this.subgrupos.filter(function(item){
      return item.descricao.toUpperCase().includes(texto.toUpperCase());         
    })
    
  }

  searchProcedimentoChange(texto: string) {
    let procedimentoFiltro = {
      grupo: null,
      subgrupo: this.solicitacao.subGrupo,
      texto: texto,
      sexo: this.solicitacao.cidadao.sexo == 'MASCULINO' ? true : false
    }

    this.procedimentoService.searchExame(procedimentoFiltro)
      .subscribe(
        data => this.procedimentos = data,
        error => console.log("error in search Procedimento()" + error)
      );
  }

  onFormSubmit() {
    this.solicitacao.tipoSolicitacao = "EXAME";
    if (this.form.valid)
      this.onSubmitEventEmitter.emit(this.solicitacao);
    }
  }