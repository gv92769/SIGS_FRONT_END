import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Config } from '../../../core/model/config';
import { ProcedimentoSubGrupo } from '../../../core/model/procedimento-sub-grupo';
import { CboService } from '../../../core/service/cbo.service';
import { StorageService } from '../../../core/service/storage.service';
import { PerfilService } from '../../../core/service/perfil.service';
import { ProcedimentoGrupoService } from '../../../core/service/procedimento-grupo.service';
import { ProcedimentoSubGrupoService } from '../../../core/service/procedimento-sub-grupo.service';
import { CidadaoService } from '../../cidadao/cidadao.service';
import { PesquisaFichaComponent } from '../../pesquisa-ficha/pesquisa-ficha.component';
import { UsuarioService } from '../../usuario/usuario.service';
import { ExameService } from '../exame.service';
import jsPDF from 'jspdf';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import htmlToPdfmake from 'html-to-pdfmake';
import * as fileSaver from 'file-saver';

@Component({
  selector: 'app-pesquisar-exame',
  templateUrl: '../../pesquisa-ficha/pesquisa-ficha.component.html',
})
export class PesquisarExameComponent extends PesquisaFichaComponent implements OnInit {

  configSubGrupo: Config = new Config();

  subgrupos: ProcedimentoSubGrupo[] = new Array;

  config: Config = new Config();

  pdfImageBase64: any;

  tiposAtend = [
    "Ectoscopia",
  ];

  constructor(cidadaoService: CidadaoService, private router: Router, private exameService: ExameService, usuarioService: UsuarioService, 
    private grupoService: ProcedimentoGrupoService, private subGrupoService: ProcedimentoSubGrupoService, cboService: CboService, 
    perfilService: PerfilService, http: HttpClient, private storageService: StorageService) {
    super(usuarioService, cidadaoService, cboService, perfilService, http);
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.createTable();

    this.titulo = "Pesquisar Exames";

    this.tituloExcluir = "Exame";

    this.textoExcluir = " a solicitação de exame";

    this.exportar = true;

    this.configSubGrupo.search = false;
    this.configSubGrupo.displayKey = 'descricao';
    this.configSubGrupo.placeholder = 'Escolha um SubGrupo';

    let ids = [2];
    this.grupoService.findAllById(ids).subscribe(
      data => {
        this.subGrupoService.findAllByGrupo(data[0])
          .subscribe(
            subgrupos => this.subgrupos = subgrupos,
            error => console.log("error in findAllByGrupo subgrupo()")
          );
      },
      error => console.log("error in findAllById grupo()")
    );

    this.http.get('assets/img/brand/sigs-logo-pdf.jpeg', { responseType: 'blob' })
        .subscribe(res => {
          const reader = new FileReader();
          reader.onloadend = () => {
            var base64data = reader.result;                
                this.pdfImageBase64 = base64data;
          }
  
          reader.readAsDataURL(res);
    });
  }

  createTable() {
    this.columns = [
      { name: 'Data', sortable: true },
      { name: 'Nome', sortable: false },
      { name: 'CPF', sortable: false },
      { name: 'CNS', sortable: false },
      { name: 'Arquivo', sortable: false },
    ];
  }

  downloadFile(value){
    this.storageService.downloadFile(value)
      .subscribe(
        response => {
          debugger
          fileSaver.saveAs(response.body, value);
          console.log(response);
        },
        error => console.log(error)
      );
  }

  geraPDF(exameId) {

    this.exameService.solicitacaoFindById(exameId).subscribe(
      res=>{

        this.solicitacao = res;

        var that = this;
        setTimeout(function(){
          const doc = new jsPDF();
   
          const pdfTable = that.pdfTable.nativeElement;
        
          var html = htmlToPdfmake(pdfTable.innerHTML);
          
          const documentDefinition = { 
            content: [html,
              {
                image:  that.pdfImageBase64,
                fit: [75,75],
                absolutePosition: {x: 500, y: 0}
              }]
          };
      
          pdfMake.createPdf(documentDefinition).open();
        },100);

      },
      err=>{
        console.log('Erro ao buscar Solicitação de Exame!');
        console.log(err);
      }
    )
   
  }

  onEditarClick(id: any) {
    this.router.navigate(['exame/solicitacao/editar/', id]);
  }

  onResultadoClick(id: any) {
    this.router.navigate(['exame/resultado/', id]);
  }

  delete() {
    this.isLoadingExcluir = true;

    this.exameService.delete(this.excluirId).subscribe(
      response => this.search(),
      error => console.log('erro ao excluir solicitação de exame'),
      () => {
        this.modalExcluir.hide();
        this.isLoadingExcluir = false;
        this.excluirId = null;
      }
    );
  }

  setPage(event) {
    this.page.pageNumber = event.offset;
    this.search();
  }

  onSort(event) {
    this.getSort(event);
    this.search();
  }

  onSubmit() {
    this.getSumit()
    this.search();
  }

  search() {
    this.getSearch()

    this.filter.tipoAtendimento = this.filter.tipoAtendimento.length == 0 ? null : this.filter.tipoAtendimento;
    this.filter.tipoExame = this.filter.tipoExame.id == null ? null : this.filter.tipoExame;

    this.exameService.search(this.filter)
      .subscribe({
        next: (response) => {
          this.page.content = response.content;
          this.page.totalElements = response.totalElements;
          this.isLoading = false;
        },
        error: (err) => {
          console.log('erro ao pesquisar exame');
          console.log(err);
          this.isLoading = false;
        }
      });
  }

}
