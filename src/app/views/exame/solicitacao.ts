import { Cidadao } from '../cidadao/cidadao';
import { Profissional } from '../../core/model/profissional';
import { ProcedimentoSubGrupo } from '../../core/model/procedimento-sub-grupo';
import { Procedimento } from '../../core/model/procedimento';
import { formatDate } from '@angular/common';
import { UnidadeBasicaSaude } from '../../core/model/unidade-basica-saude';

export class Solicitacao {
  id: number;
  cidadao: Cidadao;
  profissional: Profissional = new Profissional();
  ubs: UnidadeBasicaSaude = new UnidadeBasicaSaude();
  subGrupo: ProcedimentoSubGrupo;
  procedimentos: Procedimento[];
  tipoAtendimento: SolicitacaoExame.TipoAtendimento;
  idAtendimento: number;
  data: string = formatDate(new Date(), 'yyyy-MM-dd', 'en-US');
  observacao: string;
  file: any;
  tipoSolicitacao: string;
}

export namespace SolicitacaoExame {
  export enum TipoAtendimento {
    ECTOSCOPIA
  }
}
