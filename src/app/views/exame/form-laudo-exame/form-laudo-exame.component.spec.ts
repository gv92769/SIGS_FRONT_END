import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormLaudoExameComponent } from './form-laudo-exame.component';

describe('FormLaudoExameComponent', () => {
  let component: FormLaudoExameComponent;
  let fixture: ComponentFixture<FormLaudoExameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormLaudoExameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormLaudoExameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
