import { Component, OnInit, ViewChild } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject } from 'rxjs';
import { ProfissionalService } from '../../../core/service/profissional.service';
import { UsuarioService } from '../../usuario/usuario.service';
import { ExameService } from '../exame.service';
import { finalize } from 'rxjs/operators';
import { UnidadeBasicaSaudeService } from '../../../core/service/unidade-basica-saude.service';
import * as jwt_decode from 'jwt-decode';
import { Solicitacao } from '../solicitacao';

@Component({
  selector: 'app-laudo-exame',
  templateUrl: './laudo-exame.component.html',
  styleUrls: ['./laudo-exame.component.css']
})
export class LaudoExameComponent implements OnInit {

  formData$: BehaviorSubject<Solicitacao> = new BehaviorSubject<Solicitacao>(new Solicitacao());

  @ViewChild('formExame', { static: false }) formExame: Solicitacao;

  isLoadingSubmit: boolean = false;

  constructor(private usuarioService: UsuarioService, private profissionalService: ProfissionalService, private toastrService: ToastrService,
    private exameService: ExameService, private unidadeSaudeBasicaService: UnidadeBasicaSaudeService) { }

  ngOnInit(): void {
    let solicitacao: Solicitacao = new Solicitacao();

    this.usuarioService.getCurrentUserInfo().subscribe(
      data => this.profissionalService.findById(data.id)
        .subscribe(profissional => solicitacao.profissional = profissional)
    );

    this.unidadeSaudeBasicaService.findByCnes(jwt_decode(sessionStorage.getItem('access_token')).cnes)
      .subscribe(ubs => solicitacao.ubs = ubs);

    this.formData$.next(solicitacao);
  }

  onSubmitted(solicitacao: Solicitacao) {
    this.isLoadingSubmit = true;

    var form_data = new FormData();

    form_data.append("file", solicitacao.file);
    
    solicitacao.file = null;
    form_data.append('properties', new Blob([JSON.stringify(solicitacao)], 
          {
                    type: "application/json"
                }));

    this.exameService.saveLaudo(form_data)
      .pipe(finalize(() => this.isLoadingSubmit = false))
      .subscribe({
        next: (response) => {
          this.toastrService.success('Laudo de Exame salvo com sucesso!', 'Sucesso');
          this.ngOnInit();
        },
        error: (err) => {
          this.toastrService.error('Error ao salvar Laudo de Exame.', 'Error');
          console.log(err);
        }
      });
  }

}
