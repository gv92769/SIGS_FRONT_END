import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LaudoExameComponent } from './laudo-exame.component';

describe('LaudoExameComponent', () => {
  let component: LaudoExameComponent;
  let fixture: ComponentFixture<LaudoExameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaudoExameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaudoExameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
