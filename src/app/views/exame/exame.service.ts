import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Solicitacao } from './solicitacao';
import { ResultadoExame } from './resultado-exame';
import { FichaFiltro } from '../../core/model/ficha-filtro';
import { Page } from '../../core/model/page';

@Injectable({
  providedIn: 'root'
})
export class ExameService {

  readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/exame';
  }

  public solicitacaoFindById(id: number): Observable<Solicitacao> {
    return this.http.get<Solicitacao>(this.url + '/solicitacao/' + id);
  }

  public resultadoFindById(id: number): Observable<ResultadoExame> {
    return this.http.get<ResultadoExame>(this.url + '/resultado/' + id);
  }

  public saveSolicitacao(solicitacao: Solicitacao): Observable<Solicitacao> {
    return this.http.post<Solicitacao>(this.url + '/solicitacao', solicitacao);
  }

  public saveLaudo(solicitacao: FormData): Observable<Solicitacao> {
    return this.http.post<Solicitacao>(this.url + '/laudo', solicitacao);
  }

  public updateResultado(id: number, resultado: ResultadoExame): Observable<ResultadoExame> {
    return this.http.put<ResultadoExame>(this.url + '/' + id + '/resultado', resultado);
  }

  public updateSolicitacao(id: number, solicitacao: Solicitacao): Observable<Solicitacao> {
    return this.http.put<Solicitacao>(this.url + '/' + id + '/solicitacao', solicitacao);
  }

  public delete(id: number) {
    return this.http.delete(this.url + '/solicitacao/' + id);
  }

  public search(filter: FichaFiltro): Observable<Page<any>> {
    return this.http.post<Page<any>>(this.url + '/search', filter);
  }

}
