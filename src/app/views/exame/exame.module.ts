import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IConfig, NgxMaskModule } from 'ngx-mask';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { CustomComponentsModule } from '../../custom-components/custom-components.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PesquisarCidadaoBiometriaModule } from '../cidadao/pesquisar-cidadao-biometria/pesquisar-cidadao-biometria.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ExameRoutingModule } from './exame-routing.module';
import { SelectDropDownModule } from 'ngx-select-dropdown';
import { FormExameComponent } from './form-exame/form-exame.component';
import { AdicionarExameComponent } from './adicionar-exame/adicionar-exame.component';
import { EditarExameComponent } from './editar-exame/editar-exame.component';
import { PesquisarExameComponent } from './pesquisar-exame/pesquisar-exame.component';
import { FormResultadoComponent } from './form-resultado/form-resultado.component';
import { LaudoExameComponent } from './laudo-exame/laudo-exame.component';
import { FormLaudoExameComponent } from './form-laudo-exame/form-laudo-exame.component';
import { PdfExameComponent } from './pdf-exame/pdf-exame.component'

export const options: Partial<IConfig> | (() => Partial<IConfig>) = { validation: false };

@NgModule({
  declarations: [
    FormExameComponent,
    AdicionarExameComponent,
    EditarExameComponent,
    PesquisarExameComponent,
    FormResultadoComponent,
    LaudoExameComponent,
    FormLaudoExameComponent,
    PdfExameComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    TabsModule,
    ExameRoutingModule,
    AccordionModule.forRoot(),
    CustomComponentsModule,
    ModalModule.forRoot(),
    PesquisarCidadaoBiometriaModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot(options),
    NgxDatatableModule,
    SelectDropDownModule
  ],
  exports:[
    PdfExameComponent
  ]
})
export class ExameModule { }