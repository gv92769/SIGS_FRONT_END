import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { finalize, map, mergeMap } from 'rxjs/operators';
import { ExameService } from '../exame.service';
import { ResultadoExame } from '../resultado-exame';

@Component({
  selector: 'app-form-resultado',
  templateUrl: './form-resultado.component.html',
  styleUrls: ['./form-resultado.component.css']
})
export class FormResultadoComponent implements OnInit {

  @ViewChild('formResultado', { static: false }) form: FormGroup;

  resultado: ResultadoExame;

  id: number;

  isLoadingSubmit: boolean = false;

  constructor(private toastrService: ToastrService, private exameService: ExameService, private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.paramMap.pipe(
      map(params => {
        const id = +params.get('id');
        this.id = id;
        return id;
      }),
      mergeMap(id => {
        return this.exameService.resultadoFindById(id);
      }
      )
    )
      .subscribe({
        next: (response) => {
          this.resultado = response || new ResultadoExame();
        },
        error: (err) => {
          console.log('Erro ao buscar Resultado de Exame');
          console.log(err);
        }
      });
  }

  onFormSubmit() {
    if (this.form.valid) {
      this.isLoadingSubmit = true;
      this.exameService.updateResultado(this.id, this.resultado)
        .pipe(finalize(() => this.isLoadingSubmit = false))
        .subscribe({
          next: (response) => {
            this.toastrService.success('Resultado do Exame salva com sucesso!', 'Sucesso');
            this.router.navigate(['exame/pesquisar']);
          },
          error: (err) => {
            this.toastrService.error('Error ao salvar resultado do Exame.', 'Error');
            console.log(err);
          }
        });
    }
  }

}
