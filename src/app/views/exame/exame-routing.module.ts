import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { AdicionarExameComponent } from './adicionar-exame/adicionar-exame.component';
import { EditarExameComponent } from './editar-exame/editar-exame.component';
import { PesquisarExameComponent } from './pesquisar-exame/pesquisar-exame.component';
import { FormResultadoComponent } from './form-resultado/form-resultado.component';
import { LaudoExameComponent } from './laudo-exame/laudo-exame.component';
import { PdfExameComponent } from './pdf-exame/pdf-exame.component';

const routes: Routes = [

  {
    path: 'aaa',
    component: PdfExameComponent,
    pathMatch: 'full',
  },

  {
    path: 'solicitacao',
    data: {
      title: 'Solicitacao Exame'
    },
    children: [
      {
        path: 'adicionar',
        component: AdicionarExameComponent,
        data: {
          title: 'Adicionar'
        }
      },
      {
        path: 'editar/:id',
        component: EditarExameComponent,
        data: {
          title: 'Editar'
        }
      },
      {
        path: 'laudo',
        component: LaudoExameComponent,
        data: {
          title: 'Laudo'
        }
      },
    ]
  },
  {
    path: '',
    data: {
      title: 'Exame'
    },
    children: [
      {
        path: 'pesquisar',
        component: PesquisarExameComponent,
        data: {
          title: 'Pesquisar'
        }
      },
      {
        path: 'resultado/:id',
        component: FormResultadoComponent,
        data: {
          title: 'Resultado Exame'
        }
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExameRoutingModule { }
