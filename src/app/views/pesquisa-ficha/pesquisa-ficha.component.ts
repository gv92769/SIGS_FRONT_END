import { ElementRef, OnInit, ViewChild } from '@angular/core';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Page } from '../../core/model/page';
import { FichaFiltro } from '../../core/model/ficha-filtro';
import { UsuarioService } from '../usuario/usuario.service';
import { Cidadao } from '../cidadao/cidadao';
import { Config } from '../../core/model/config';
import { CidadaoService } from '../cidadao/cidadao.service';
import { Solicitacao } from '../exame/solicitacao';
import { Cbo } from '../../core/model/cbo';
import { CboService } from '../../core/service/cbo.service';
import { Lotacao } from '../../core/model/lotacao';
import { Perfil } from '../../core/model/perfil';
import { PerfilService } from '../../core/service/perfil.service';
import { HttpClient } from '@angular/common/http';

export class PesquisaFichaComponent implements OnInit {

  @ViewChild('examepdf', {static: false}) pdfTable: ElementRef;

  solicitacao: Solicitacao = new Solicitacao();

  filter: FichaFiltro = new FichaFiltro();

  excluirId: any;

  tipoSolicitacao: string;

  columns: any[];

  page: Page<any> = new Page();

  @ViewChild('modalExcluir', { static: false }) modalExcluir: ModalDirective;

  isLoadingExcluir: boolean = false;

  exportar: boolean = false;

  isLoading: boolean = false;

  columnMode = ColumnMode;

  titulo: string;

  tituloExcluir: string;

  textoExcluir: string;

  idProfissional: number;

  opCidadao: Cidadao[] = new Array;

  cidadao: Cidadao;

  configCidadao: Config = new Config();

  configOcupacao: Config = new Config();

  configPerfil: Config = new Config();
  
  optionsOcupacao: Cbo[] = new Array;

  lotacao: Lotacao = new Lotacao();

  perfis: Perfil[] = new Array;

  constructor(
    protected usuarioService: UsuarioService,
    protected cidadaoService: CidadaoService,
    protected cboService: CboService,
    protected perfilService: PerfilService,
    protected http: HttpClient) {
  }

  ngOnInit(): void {
    this.usuarioService.getCurrentUserInfo().subscribe(
      data => this.idProfissional = data.id,
      error => console.log(error)
    );

    this.perfilService.findAllByAtivo()
      .subscribe(
        data => this.perfis = data,
        error => console.log(error)
    );

    this.configCidadao.displayKey = 'nome';
    this.configCidadao.placeholder = 'Digite o CPF/CNS ou Nome';

    this.configOcupacao.placeholder = 'Selecione a Ocupação do Profissional';
    this.configOcupacao.displayKey = 'titulo';
    this.configOcupacao.searchOnKey = 'titulo';

    this.configPerfil.placeholder = "Selecione o perfil do usuário";
    this.configPerfil.displayKey = 'descricao';
    this.configPerfil.searchOnKey = 'descricao';
  }

  limpar() {
    this.filter = new FichaFiltro();
    this.cidadao = null;
  }

  onApagarClick(solicitacao) {
    this.excluirId = solicitacao.id;
    this.tipoSolicitacao = solicitacao.tipo;
    this.modalExcluir.show();
  }

  getSumit() {
    this.page = new Page();
    this.filter.column = null;
    this.filter.sort = null;
  }

  getSort(event) {
    this.filter.column = event.sorts[0].prop;
    this.filter.sort = event.sorts[0].dir;
  }

  getSearch() {
    this.filter.pageNumber = this.page.pageNumber;
    this.filter.size = this.page.size;
    this.isLoading = true;
  }

  buscarCidadao(valor: string) {
    if (valor != null && valor != '') {
      this.cidadaoService.filtro(valor)
        .subscribe({
          next: (data) => {
            this.opCidadao = data;
          },
          error: (err) => {
            console.log(err);
          }
        });
    }
  }

  selectionChanged(event){
    this.filter.nome = event.value.nome;
    this.filter.cns = event.value.cns;
    this.filter.cpf = event.value.cpf;
  }

  searchOcupacaoChange(titulo: string) {
    this.cboService.findAllByTituloByPerfil(titulo, this.filter.perfil.id)
      .subscribe(
        data => this.optionsOcupacao = data,
        error => console.log(error)
      );
  }
}
