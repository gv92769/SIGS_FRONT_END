import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PesquisaFichaComponent } from './pesquisa-ficha.component';

describe('PesquisaFichaComponent', () => {
  let component: PesquisaFichaComponent;
  let fixture: ComponentFixture<PesquisaFichaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PesquisaFichaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PesquisaFichaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
