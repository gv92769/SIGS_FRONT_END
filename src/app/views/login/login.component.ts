import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../auth/auth.service';
import { UsuarioLogin } from './usuario-login';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit {

  messageAlert = "";

  showAlert = false;

  usuario: UsuarioLogin;

  isLoading: boolean = false;

  constructor(private authService: AuthService, private router: Router) {
    this.usuario = new UsuarioLogin();
  }

  ngOnInit() {
    this.authService.deleteTokens();
  }

  onSubmit() {
    this.isLoading = true;
    this.showAlert = false;
    
    this.authService.login(this.usuario).subscribe(
      data => {
        this.router.navigate(['lotacao/selecionar-perfil']);
      },
      error => {
        console.log(error);
        this.isLoading = false;
        if (error.status == 401 || error.status == 403)
          this.showErrorForUnauthorized();
        else
          this.showErrorForOthers();
      }
    );
  }

  private showErrorForUnauthorized() {
    this.messageAlert = 'Login e/ou senha inválidos.';
    this.showAlert = true;
  }

  private showErrorForOthers() {
    this.messageAlert = 'Não foi possível realizar o login. Tente novamente mais tarde.';
    this.showAlert = true;
  }
}
