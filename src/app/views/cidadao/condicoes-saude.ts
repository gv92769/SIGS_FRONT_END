import { ObjectCodigoDescricao } from "../../core/model/object-codigo-descricao";
import { CondicoesSaudeTemporaria } from "./condicoes-saude-temporaria";

export class CondicoesSaude {
  id: number;
  listDoencaRins: ObjectCodigoDescricao[] = new Array;
  listDoencaRespiratoria: ObjectCodigoDescricao[] = new Array;
  listDoencaCardiaca: ObjectCodigoDescricao[] = new Array;
  statusTemDiabetes: boolean = false;
  statusTemDoencaRespiratoria: boolean = false;
  statusTemHanseniase: boolean = false;
  statusTemHipertensaoArterial: boolean = false;
  statusTemTeveCancer: boolean = false;
  statusTemTeveDoencasRins: boolean = false;
  statusTemTuberculose: boolean = false;
  statusTeveAvcDerrame: boolean = false;
  statusTeveDoencaCardiaca: boolean = false;
  statusTeveInfarto: boolean = false;
  statusTratamentoPsiquicoOuProblemaMental: boolean = false;
  statusUsaOutrasPraticasIntegrativasOuComplementares: boolean = false;
  condicaoTemporaria: CondicoesSaudeTemporaria = new CondicoesSaudeTemporaria();
}
