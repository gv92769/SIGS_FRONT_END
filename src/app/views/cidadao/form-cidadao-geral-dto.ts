import { ObjectCodigoDescricao } from '../../core/model/object-codigo-descricao';

export class FormCidadaoGeralDto {
    opcoesSexo: string[];
	opcoesEstadoCivil: string[];
	opcoesEstadosBrasil: string[];
	opcoesNacionalidade: string[];
	opcoesRacaCor: ObjectCodigoDescricao[] = new Array;
}
