import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdicionarBiometriaCidadaoComponent } from './adicionar-biometria-cidadao.component';

describe('AdicionarBiometriaCidadaoComponent', () => {
  let component: AdicionarBiometriaCidadaoComponent;
  let fixture: ComponentFixture<AdicionarBiometriaCidadaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdicionarBiometriaCidadaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdicionarBiometriaCidadaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
