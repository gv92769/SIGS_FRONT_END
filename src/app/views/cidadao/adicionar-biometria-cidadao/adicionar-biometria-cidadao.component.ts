import { Component, OnInit, Input, Output, ViewChild, EventEmitter } from '@angular/core';
import { LeitorBiometrico } from '../../../biometria/leitor-biometrico';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { BiometriaCidadaoService } from '../biometria-cidadao.service';

declare var Fingerprint: any;

@Component({
  selector: 'app-adicionar-biometria-cidadao',
  templateUrl: './adicionar-biometria-cidadao.component.html',
  styleUrls: ['./adicionar-biometria-cidadao.component.scss']
})
export class AdicionarBiometriaCidadaoComponent implements OnInit {

  @ViewChild('cadastrarBiometriaModal', { static: false }) childModal: ModalDirective;
  @Input() inIdCidadao: number;
  isLoading: boolean = false;

  leitorBiometrico: LeitorBiometrico;
  leitorBiometricoUid: string;

  mensagemDireita: string;
  dedos = [
    {numero: 0, nome: 'Mínimo Esquerdo'},
    {numero: 1, nome: 'Anelar Esquerdo'},
    {numero: 2, nome: 'Médio Esquerdo'},
    {numero: 3, nome: 'Indicador Esquerdo'},
    {numero: 4, nome: 'Polegar Esquerdo'},
    {numero: 5, nome: 'Polegar Direito'},
    {numero: 6, nome: 'Indicador Direito'},
    {numero: 7, nome: 'Médio Direito'},
    {numero: 8, nome: 'Anelar Direito'},
    {numero: 9, nome: 'Mínimo Direito'}
  ];
  dedoCadastro: number;
  textoBotao: string;
  numeroDigitaisColetadas: number;
  cadastroDigitalPrincipal: boolean;
  statusLeitor: string;
  qualidadeLeitura: string;
  digitais: string[];
  cadastroDigitalPrincipalTexto: string;
  cadastroConcluido: boolean;

  @Output() outCloseModal = new EventEmitter();
  outCloseModalData = {concluido: false, dedo: -1, principal: true};


  constructor(private biometriaCidadaoService: BiometriaCidadaoService) {
    console.log(this.leitorBiometrico);
    this.leitorBiometrico = LeitorBiometrico.getInstance();
  }

  ngOnInit() {
  }

  showModal(cadastroDigitalPrincipal: boolean): void {
    this.cadastroDigitalPrincipal = cadastroDigitalPrincipal;
    this.cadastroDigitalPrincipalTexto = cadastroDigitalPrincipal ? "Principal" : "Secundária";
    this.cadastroConcluido = false;
    this.leitorBiometricoUid = null;
    this.dedoCadastro = -1;
    this.mensagemDireita = 'Para iniciar o cadastro, selecione o dedo para coleta da digital.';
    this.textoBotao = 'Cancelar';
    this.numeroDigitaisColetadas = 0;
    this.childModal.show();
    this.configurarLeitorBiometrico();
  }

  hideModal(): void {
    this.outCloseModal.emit(this.outCloseModalData);
    this.childModal.hide();
  }

  onDedoCadastroSelected(){
    this.configurarLeitorBiometricoIniciarCadastro();
  }

  configurarLeitorBiometrico(){
    this.statusLeitor = "Procurando...";
    this.leitorBiometrico = LeitorBiometrico.getInstance();
    this.leitorBiometrico.enumerateDevices().subscribe(
      leitores => {
        if(leitores.length > 0){
          this.leitorBiometricoUid = leitores[0];
          this.statusLeitor = "Pronto";
        }
        else{
          this.leitorBiometricoUid = "";
          this.statusLeitor = "Não encontrado";
        }
      },
      error => {
        console.log(error);
        this.leitorBiometricoUid = "";
        this.statusLeitor = "Erro";
      }
    );
  }


  configurarLeitorBiometricoIniciarCadastro() {
    this.leitorBiometrico.onSamplesAcquired = (e) => this.sampleAcquired(e);
    this.leitorBiometrico.onQualityReported = (e) => this.qualityReported(e);
    this.digitais = new Array<string>(4);
    let formatoBiometria = LeitorBiometrico.SAMPLE_FORMAT_PNG;
    this.leitorBiometrico.startAcquisition(formatoBiometria, this.leitorBiometricoUid).subscribe(
      () => {
        console.log('START CAPTURE');
        this.mensagemDireita = 'Posicione o dedo no leitor biométrico e aguarde.';
      },
      error => {
        console.log(error);
        this.mensagemDireita = 'Erro com o leitor biométrico!';
      }
    );

  }

  onButtonClick() {
    this.leitorBiometrico.stopAcquisition(this.leitorBiometricoUid).subscribe(
      () => console.log("Capturing stopped !!!"),
      error => console.log(error.ssage)
    );
    if (this.cadastroConcluido){
      this.outCloseModalData.concluido = true;
      this.outCloseModalData.dedo = this.dedoCadastro;
    }
    else{
      this.outCloseModalData.concluido = false;
      this.outCloseModalData.dedo = -1;
    }
    this.outCloseModalData.principal = this.cadastroDigitalPrincipal;
    this.hideModal();
  }


  qualityReported(e){
    this.qualidadeLeitura = LeitorBiometrico.qualityCode(e);
  }

  sampleAcquired(s){
    console.log('sampleAcquiredEnrollment idCId:' + this.inIdCidadao + '  count:'+this.numeroDigitaisColetadas);
    // If sample acquired format is PNG- perform following call on object recieved
    // Get samples from the object - get 0th element of samples as base 64 encoded PNG image
    //localStorage.setItem("imageSrc", "");
    var samples = JSON.parse(s.samples);
    //localStorage.setItem("imageSrc", "data:image/png;base64," + Fingerprint.b64UrlTo64(samples[0]));
    var vDiv = document.getElementById('imagediv');
    vDiv.innerHTML = "";
    var image = document.createElement("img");
    image.id = "image";
    image.src = "data:image/png;base64," + Fingerprint.b64UrlTo64(samples[0]);//localStorage.getItem("imageSrc");
    image.width = 170;
    vDiv.appendChild(image);
    let di = Fingerprint.b64UrlTo64(samples[0]);
    this.digitais[this.numeroDigitaisColetadas] = di;
    this.numeroDigitaisColetadas++;

    this.afterSampleAcquired();
  }

  afterSampleAcquired(){
    if(this.numeroDigitaisColetadas < 4){
      this.mensagemDireita = 'Digital coletada com sucesso. Posicione novamente o dedo no leitor biométrico e aguarde.';
    }
    else{
      this.mensagemDireita = 'Digital coletada com sucesso. Enviando dados.';
      this.leitorBiometrico.stopAcquisition(this.leitorBiometricoUid).subscribe(
        () => console.log("Capturing stopped !!!"),
        error => console.log(error.ssage)
      );
      this.isLoading = true;
      this.sendData();
    }
  }

  sendData(){
    let dataToSend = {
      digital1: this.digitais[0],
      digital2: this.digitais[1],
      digital3: this.digitais[2],
      digital4: this.digitais[3],
      dedo: this.dedoCadastro,
      principal: this.cadastroDigitalPrincipal
    };

    this.biometriaCidadaoService.postCadastro(this.inIdCidadao, dataToSend).subscribe(
      data => {
        this.mensagemDireita = "Cadastro realizado com sucesso.";
        this.textoBotao = 'Fechar';
        this.cadastroConcluido = true;
      },
      error => {
        console.log(error);
        this.mensagemDireita = "Erro no envio dos dados!";
      },
      () => this.isLoading = false
    );
  }

}
