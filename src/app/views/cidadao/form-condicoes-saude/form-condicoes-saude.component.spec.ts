import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormCondicoesSaudeComponent } from './form-condicoes-saude.component';

describe('FormCondicoesSaudeComponent', () => {
  let component: FormCondicoesSaudeComponent;
  let fixture: ComponentFixture<FormCondicoesSaudeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormCondicoesSaudeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormCondicoesSaudeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
