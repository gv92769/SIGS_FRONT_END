import { Component, Input, OnInit } from '@angular/core';
import { ProfissionalService } from '../../../core/service/profissional.service';
import { UsuarioService } from '../../usuario/usuario.service';
import { CidadaoService } from '../cidadao.service';
import { CondicoesSaude } from '../condicoes-saude';
import { FichaCadastroIndividual } from '../ficha-cadastro-individual';
import { InformacoesCondicoesSaude } from '../informacoes-condicoes-saude';
import * as jwt_decode from 'jwt-decode';
import { UnidadeBasicaSaudeService } from '../../../core/service/unidade-basica-saude.service';
import { ToastrService } from 'ngx-toastr';
import { CboService } from '../../../core/service/cbo.service';
import { Cidadao } from '../cidadao';



@Component({
  selector: 'app-form-condicoes-saude',
  templateUrl: './form-condicoes-saude.component.html',
  styleUrls: ['./form-condicoes-saude.component.css']
})

export class FormCondicoesSaudeComponent implements OnInit {
  @Input() inIdCidadao: number;

  condicoesSaude: CondicoesSaude = new CondicoesSaude();

  informacao: InformacoesCondicoesSaude = new InformacoesCondicoesSaude();

  ficha: FichaCadastroIndividual = new FichaCadastroIndividual();

  isLoadingSubmit: boolean = false;

  cidadao: Cidadao = new Cidadao();

  obrigatorio = true;

  constructor(
    private cidadaoService: CidadaoService,
    private usuarioService: UsuarioService,
    private profissionalService: ProfissionalService,
    private unidadeSaudeBasicaService: UnidadeBasicaSaudeService,
    private toastrService: ToastrService,
    private cboService: CboService
  ){ }

  ngOnInit(): void {
    if (this.inIdCidadao) {
      this.cidadaoService.findByIdCondicaoSaude(this.inIdCidadao).subscribe(
        data => {
          Object.assign(this.condicoesSaude, data);
        },
        error => console.log(error)
      );

      this.cidadaoService.findOne(this.inIdCidadao).subscribe(
         data => {
          this.cidadao = data;
        },
        error => console.log(error)
      );
    }

    this.usuarioService.getCurrentUserInfo().subscribe(
      data => this.profissionalService.findById(data.id)
        .subscribe(profissional => this.ficha.profissionalResponsavel = profissional)
    );

    this.unidadeSaudeBasicaService.findByCnes(jwt_decode( sessionStorage.getItem('access_token')).cnes)
      .subscribe(ubs => this.ficha.ubs = ubs);

    this.cboService.findByCodigo(jwt_decode(sessionStorage.getItem('access_token')).cbo)
      .subscribe(cbo => this.ficha.cbo = cbo);


    this.cidadaoService.initCondicaosaude().subscribe(
      data => {
        this.informacao = data;
      },
      error => console.log(error)
    );
  }

  cardiacaChange(opcao) {
    let index = this.condicoesSaude.listDoencaCardiaca.findIndex(x => x.codigo == opcao.codigo)
    if (index == -1)
      this.condicoesSaude.listDoencaCardiaca.push(opcao);
    else
      this.condicoesSaude.listDoencaCardiaca.splice(index, 1);
  }

  cardiacaChecked(opcao) {
    if (this.condicoesSaude.listDoencaCardiaca.findIndex(x => x.codigo == opcao.codigo) != -1)
      return true;

    return false;

  }

  respiratoriaChange(opcao) {
    let index = this.condicoesSaude.listDoencaRespiratoria.findIndex(x => x.codigo == opcao.codigo)
    if (index == -1)
      this.condicoesSaude.listDoencaRespiratoria.push(opcao);
    else
      this.condicoesSaude.listDoencaRespiratoria.splice(index, 1);
  }

  respiratoriaChecked(opcao) {
    if (this.condicoesSaude.listDoencaRespiratoria.findIndex(x => x.codigo == opcao.codigo) != -1)
      return true;

    return false;
  }

  rinsChange(opcao) {
    let index = this.condicoesSaude.listDoencaRins.findIndex(x => x.codigo == opcao.codigo)
    if (index == -1)
      this.condicoesSaude.listDoencaRins.push(opcao);
    else
      this.condicoesSaude.listDoencaRins.splice(index, 1);
  }

  rinsChecked(opcao) {
    if (this.condicoesSaude.listDoencaRins.findIndex(x => x.codigo == opcao.codigo) != -1)
      return true;

    return false;
  }

  verificarCampos() {

    if (!this.condicoesSaude.statusTemDoencaRespiratoria) {
      this.condicoesSaude.listDoencaRespiratoria = new Array;
    }

    if (!this.condicoesSaude.statusTemTeveDoencasRins) {
      this.condicoesSaude.listDoencaRins = new Array;
    }

    if (!this.condicoesSaude.statusTeveDoencaCardiaca) {
      this.condicoesSaude.listDoencaCardiaca = new Array;
    }

    if (!this.condicoesSaude.condicaoTemporaria.statusTeveInternadoem12Meses) {
      this.condicoesSaude.condicaoTemporaria.descricaoCausaInternacaoEm12Meses = "";
    }

    if (!this.condicoesSaude.condicaoTemporaria.statusUsaPlantasMedicinais) {
      this.condicoesSaude.condicaoTemporaria.descricaoPlantasMedicinaisUsadas = "";
    }

    if (!this.condicoesSaude.condicaoTemporaria.statusEhGestante) {
      this.condicoesSaude.condicaoTemporaria.maternidadeDeReferencia = "";
    }

  }

  onSubmit() {
    this.isLoadingSubmit = true;
    this.verificarCampos();
    this.ficha.condicaoSaude = this.condicoesSaude;

    this.cidadaoService.updateCondicaoSaude(this.inIdCidadao, this.ficha).subscribe(
      data => {
        this.toastrService.success('Condição de Saúde salva com sucesso!', 'Sucesso');
        this.isLoadingSubmit = false;
        this.condicoesSaude = data;
      },
      error => {
        console.log(error);
        this.toastrService.error('Error ao salvar Condição de Saúde.', 'Error');
        this.isLoadingSubmit = false;
      }
    );
  }

}
