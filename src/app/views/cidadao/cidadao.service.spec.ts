import { TestBed } from '@angular/core/testing';

import { CidadaoService } from './cidadao.service';

describe('CidadaoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CidadaoService = TestBed.get(CidadaoService);
    expect(service).toBeTruthy();
  });
});
