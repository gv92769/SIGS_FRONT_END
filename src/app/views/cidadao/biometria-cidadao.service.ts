import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BiometriaCidadaoService {

  readonly baseUrl: string;

  constructor(private http: HttpClient) {
    this.baseUrl = environment.apiBaseUrl + '/cidadaos/biometria';
  }

  public verificarCadastroBiometria(idCidadao: number) : Observable<any>{
    let url = this.baseUrl + '/possui/' + idCidadao;
    return  this.http.get<any>(url);
  }

  public postVerificacao(idCidadao: number, digital) : Observable<any>{
    let url = this.baseUrl + '/verificar/' + idCidadao;
    return this.http.post<any>(url, digital);
  }

  public postCadastro(idCidadao: number, digitais) : Observable<any>{
    console.log('ID CIDADAO:' + idCidadao);
    let url = this.baseUrl + '/cadastrar/' + idCidadao;
    return this.http.post<any>(url, digitais);
  }

  public delete(idCidadao: number, principal: boolean) : Observable<any>{
    let url = this.baseUrl + '/'+ idCidadao + '/' + principal;
    return this.http.delete<any>(url);
  }


}
