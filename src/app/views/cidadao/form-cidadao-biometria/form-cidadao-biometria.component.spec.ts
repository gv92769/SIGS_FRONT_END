import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormCidadaoBiometriaComponent } from './form-cidadao-biometria.component';

describe('FormCidadaoBiometriaComponent', () => {
  let component: FormCidadaoBiometriaComponent;
  let fixture: ComponentFixture<FormCidadaoBiometriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormCidadaoBiometriaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormCidadaoBiometriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
