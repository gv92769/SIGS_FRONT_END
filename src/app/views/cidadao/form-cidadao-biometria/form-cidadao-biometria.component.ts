import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { BiometriaCidadaoService } from '../biometria-cidadao.service';
import { AdicionarBiometriaCidadaoComponent } from '../adicionar-biometria-cidadao/adicionar-biometria-cidadao.component';
import { VerificarBiometriaCidadaoComponent } from '../verificar-biometria-cidadao/verificar-biometria-cidadao.component';

@Component({
  selector: 'app-form-cidadao-biometria',
  templateUrl: './form-cidadao-biometria.component.html',
  styleUrls: ['./form-cidadao-biometria.component.scss']
})
export class FormCidadaoBiometriaComponent implements OnInit {

  @ViewChild(AdicionarBiometriaCidadaoComponent, { static: false }) modalCadastrarBiometria: AdicionarBiometriaCidadaoComponent;

  @ViewChild(VerificarBiometriaCidadaoComponent, { static: false }) modalVerificarBiometria: VerificarBiometriaCidadaoComponent;

  @Input() inIdCidadao: number;

  possuiCadastroDigitalPrincipal: boolean = false;

  possuiCadastroDigitalSecundaria: boolean = false;

  dedoCadastroDigitalPrincipal: number;

  dedoCadastroDigitalSecundaria: number;

  constructor(private biometriaCidadaoService : BiometriaCidadaoService) { }

  ngOnInit() {
    this.biometriaCidadaoService.verificarCadastroBiometria(this.inIdCidadao).subscribe(
      data => {
        this.possuiCadastroDigitalPrincipal = data.possuiDigitalPrincipal;
        this.possuiCadastroDigitalSecundaria = data.possuiDigitalSecundaria;
        this.dedoCadastroDigitalPrincipal = data.dedoDigitalPrincipal;
        this.dedoCadastroDigitalSecundaria = data.dedoDigitalSecundaria;
      },
      error => {
        console.log(error);
      }
    );
  }

  onButtonCadastrarPrincipalClick(){
    this.modalCadastrarBiometria.showModal(true);
  }

  onButtonVerificarPrincipalClick(){
    this.modalVerificarBiometria.showModal();
  }

  onButtonExcluirPrincipalClick(){
    this.biometriaCidadaoService.delete(this.inIdCidadao,true).subscribe(
      data => {
        this.possuiCadastroDigitalPrincipal = false;
        this.dedoCadastroDigitalPrincipal = -1;
      },
      error => {
        console.log(error);
      }
    );
  }

  onButtonCadastrarSecundariaClick(){
    this.modalCadastrarBiometria.showModal(false);
  }

  onButtonVerificarSecundariaClick(){
    this.modalVerificarBiometria.showModal();
  }

  onButtonExcluirSecundariaClick(){
    this.biometriaCidadaoService.delete(this.inIdCidadao,false).subscribe(
      data => {
        this.possuiCadastroDigitalSecundaria = false;
        this.dedoCadastroDigitalSecundaria = -1;
      },
      error => {
        console.log(error);
      }
    );
  }

  onCloseModalCadastrarBiometria(resultado){
    if(resultado.concluido){
      if(resultado.principal){
        this.possuiCadastroDigitalPrincipal = true;
        this.dedoCadastroDigitalPrincipal = resultado.dedo;
      }
      else{
        this.possuiCadastroDigitalSecundaria = true;
        this.dedoCadastroDigitalSecundaria = resultado.dedo;
      }
    }
  }

}
