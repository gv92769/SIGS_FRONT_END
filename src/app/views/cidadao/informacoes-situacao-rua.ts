import { ObjectCodigoDescricao } from "../../core/model/object-codigo-descricao";

export class InformacoesSituacaoRua {
  listTempoSituacaoRua: ObjectCodigoDescricao[] = new Array;
  listOrigemAlimentacao: ObjectCodigoDescricao[] = new Array;
  QuantasAlimentacao: ObjectCodigoDescricao[] = new Array;
  listAcessoHigiene: ObjectCodigoDescricao[] = new Array;
}
