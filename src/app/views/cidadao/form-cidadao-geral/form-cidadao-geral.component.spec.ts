import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormCidadaoGeralComponent } from './form-cidadao-geral.component';

describe('FormCidadaoGeralComponent', () => {
  let component: FormCidadaoGeralComponent;
  let fixture: ComponentFixture<FormCidadaoGeralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormCidadaoGeralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormCidadaoGeralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
