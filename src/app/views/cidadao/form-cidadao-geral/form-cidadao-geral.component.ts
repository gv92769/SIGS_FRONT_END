import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Cidadao } from '../cidadao';
import { CidadaoService } from '../cidadao.service';
import { MunicipioService } from '../../../core/service/municipio.service';
import { Municipio } from '../../../core/model/municipio';
import { PaisService } from '../../../core/service/pais.service';
import { Pais } from '../../../core/model/pais';
import { EtniaService } from '../etnia.service';
import { FichaCadastroIndividual } from '../ficha-cadastro-individual';
import { UsuarioService } from '../../usuario/usuario.service';
import { ProfissionalService } from '../../../core/service/profissional.service';
import { UnidadeBasicaSaudeService } from '../../../core/service/unidade-basica-saude.service';
import * as jwt_decode from 'jwt-decode';
import { ValidarDocumentoService } from '../../../core/service/validar-documento.service';
import { ObjectCodigoDescricao } from '../../../core/model/object-codigo-descricao';
import { Config } from '../../../core/model/config';
import { ToastrService } from 'ngx-toastr';
import { CboService } from '../../../core/service/cbo.service';
import { FormCidadaoGeralDto } from '../form-cidadao-geral-dto';

@Component({
  selector: 'app-form-cidadao-geral',
  templateUrl: './form-cidadao-geral.component.html'
})
export class FormCidadaoGeralComponent implements OnInit {

  @Input() inIdCidadao: number;

  @Output() outSubmitSuccess: EventEmitter<number> = new EventEmitter();

  cidadao: Cidadao;

  ficha: FichaCadastroIndividual = new FichaCadastroIndividual();

  opcaoForm: FormCidadaoGeralDto = new FormCidadaoGeralDto();

  isLoading: boolean = false;

  cnsValido: boolean = true;

  cnsResponsavel: boolean = true;

  cpfValido: boolean = true;

  cpfResponsavel: boolean = true;

  municipios: Municipio[] = new Array;

  config: Config = new Config();

  paises: Pais[] = new Array;

  etnias: ObjectCodigoDescricao[] = new Array;

  configEtnia: Config = new Config();

  constructor(private cidadaoService: CidadaoService, private municipioService: MunicipioService, private paisService: PaisService,
    private etniaService: EtniaService, private usuarioService: UsuarioService, private profissionalService: ProfissionalService,
    private unidadeSaudeBasicaService: UnidadeBasicaSaudeService, private validarDocumentoService: ValidarDocumentoService, 
    private toastrService: ToastrService, private cboService: CboService) {
    this.cidadao = new Cidadao();
  }

  emitSubmitSuccessEvent(id: number) {
    this.outSubmitSuccess.emit(id);
  }

  ngOnInit() {
    if (this.inIdCidadao) {
      console.log("inIdCidadao: " + this.inIdCidadao);
      this.cidadaoService.findOne(this.inIdCidadao).subscribe(
        data => {
          Object.assign(this.cidadao, data);
        },
        error => console.log(error)
      );
    }

    this.cidadaoService.initCidadaoGeral().subscribe(data => this.opcaoForm = data);

    this.usuarioService.getCurrentUserInfo().subscribe(
      data => this.profissionalService.findById(data.id)
        .subscribe(profissional => this.ficha.profissionalResponsavel = profissional)
    );

    this.unidadeSaudeBasicaService.findByCnes(jwt_decode(sessionStorage.getItem('access_token')).cnes)
      .subscribe(ubs => this.ficha.ubs = ubs);

    this.cboService.findByCodigo(jwt_decode(sessionStorage.getItem('access_token')).cbo)
      .subscribe(cbo => this.ficha.cbo = cbo);

    this.config.displayKey = 'nome';
    this.config.placeholder = 'Selecione';
    this.config.searchOnKey = 'nome';

    this.configEtnia.displayKey = 'descricao';
    this.configEtnia.placeholder = 'Selecione';
    this.configEtnia.searchOnKey = 'descricao';
  }

  desconheceMae() {
    this.cidadao.nomeMae = this.cidadao.desconheceNomeMae == true ? null : this.cidadao.nomeMae;
  }

  desconhecePai() {
    this.cidadao.nomePai = this.cidadao.desconheceNomePai == true ? null : this.cidadao.nomePai;
  }

  buscarMunicipio(nome: string) {
    let uf = this.cidadao.estadoNascimento;
    this.municipioService.findByUfAndNome(uf, nome)
      .subscribe({
        next: (municipios) => {
          this.municipios = municipios;
        },
        error: (err) => {
          console.log(err);
          this.municipios = new Array;
        }
      });
  }

  municipioChange(event: Event) {
    this.cidadao.municipioNascimento = event['value'];
  }

  ufChange() {
    this.cidadao.municipioNascimento = new Municipio();
  }

  buscarPais(nome: string) {
    this.paisService.findAllByNome(nome)
      .subscribe({
        next: (paises) => {
          this.paises = paises;
          if (this.cidadao.nacionalidade == 'Brasileira')
            this.cidadao.paisNascimento = this.paises[0];
        },
        error: (err) => {
          console.log(err);
          this.paises = new Array;
        }
      });
  }

  paisChange(event: Event) {
    this.cidadao.paisNascimento = event['value'];
  }

  nacionalidadeChange() {
    if (this.cidadao.nacionalidade == 'Brasileira') {
      this.buscarPais('Brasil');
    } else {
      if (this.cidadao.paisNascimento.codigo == 31) {
        this.cidadao.paisNascimento = new Pais();
        this.cidadao.estadoNascimento = null;
        this.cidadao.municipioNascimento = new Municipio();
      }
    }
  }

  racaChange() {
    this.cidadao.etnia = this.cidadao.racaCor.codigo != 5 ? new ObjectCodigoDescricao() : this.cidadao.etnia;
  }

  buscarEtnia(descricao: string) {
    this.etniaService.findAllByNome(descricao)
      .subscribe({
        next: (etnias) => {
          this.etnias = etnias;
        },
        error: (err) => {
          console.log(err);
          this.etnias = new Array;
        }
      });
  }

  etniaChange(event: Event) {
    this.cidadao.etnia = event['value'];
  }

  validaCns() {
    this.validarDocumentoService.validaCns(this.cidadao.id != null ? this.cidadao.id : 0, this.cidadao.cns, 'CIDADAO').subscribe(
      data => {
        if (data == false) {
          this.cidadao.cns = null;
          this.cnsValido = false
        } else {
          this.cnsValido = true;
        }
      },
      error => {
        console.log(error);
      }
    )
  }

  validarCnsResponsavel() {
    this.validarDocumentoService.validaCns(-1, this.cidadao.cnsResponsavelFamiliar, 'CIDADAO').subscribe(
      data => {
        if (data == false) {
          this.cidadao.cnsResponsavelFamiliar = null;
          this.cnsResponsavel = false
        } else {
          this.cnsResponsavel = true;
        }
      },
      error => {
        console.log(error);
      }
    )
  }

  validaCpf() {
    this.validarDocumentoService.validaCpf(this.cidadao.id != null ? this.cidadao.id : 0, this.cidadao.cpf, 'CIDADAO').subscribe(
      data => {
        if (data == false) {
          this.cidadao.cpf = null;
          this.cpfValido = false
        } else {
          this.cpfValido = true;
        }
      },
      error => {
        console.log(error);
      }
    )
  }

  validaCpfResponsavel() {
    this.validarDocumentoService.validaCpf(-1, this.cidadao.cpfResponsavelFamiliar, 'CIDADAO').subscribe(
      data => {
        if (data == false) {
          this.cidadao.cpfResponsavelFamiliar = null;
          this.cpfValido = false
        } else {
          this.cpfValido = true;
        }
      },
      error => {
        console.log(error);
      }
    )
  }

  onSubmit() {
    this.isLoading = true;
    this.cidadao.ativo = true;
    this.ficha.cidadao = this.cidadao;
    
    this.cidadaoService.create(this.ficha).subscribe(
      data => {
        this.toastrService.success('Cidadão salvo com sucesso!', 'Sucesso');
        let id = data.cidadao.id;
        this.emitSubmitSuccessEvent(id);
      },
      error => {
        console.log(error);
        this.toastrService.error('Error ao salvar cidadao.', 'Error');
      },
      () => this.isLoading = false
    );
  }

}
