import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Cidadao } from './cidadao';
import { FinanceiroCidadao } from './financeiro-cidadao';
import { MoradiaCidadao } from './moradia-cidadao';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
import { Page } from '../../core/model/page';
import { FichaCadastroIndividual } from './ficha-cadastro-individual';
import { InformacaoSocioDemografica } from './informacao-socio-demografica';
import { CidadaoFilter } from './cidadao-filter';
import { CondicoesSaude } from './condicoes-saude';
import { InformacoesCondicoesSaude } from './informacoes-condicoes-saude';
import { SituacaoRua } from './situacao-rua';
import { InformacoesSituacaoRua } from './informacoes-situacao-rua';
import { FormInformacaoSociodemograficaDto } from './form-informacao-sociodemografica-dto';
import { FormCidadaoGeralDto } from './form-cidadao-geral-dto';
import { FormMoradiaCidadaoDto } from './form-moradia-cidadao';

@Injectable({
  providedIn: 'root'
})
export class CidadaoService {

  readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/cidadaos';
  }

  public findOne(id): Observable<Cidadao> {
    return this.http.get<Cidadao>(this.url+'/'+id);
  }

  public filtro(valor: string): Observable<Cidadao[]> {
    return this.http.get<Cidadao[]>(this.url+'/filtro/'+valor);
  }

  public search(filter: CidadaoFilter): Observable<Page<Cidadao>> {
    return this.http.post<Page<Cidadao>>(this.url+'/search', filter);
  }

  public delete(id){
    return this.http.post(this.url+'/delete', id);
  }

  public create(ficha: FichaCadastroIndividual): Observable<FichaCadastroIndividual> {
    return this.http.post<FichaCadastroIndividual>(this.url, ficha);
  }

  public update(id, ficha: FichaCadastroIndividual): Observable<FichaCadastroIndividual> {
    return this.http.put<FichaCadastroIndividual>(this.url+'/'+id, ficha);
  }

  public findAllOpcoesEstadoSigla(): Observable<string[]> {
    return this.http.get<string[]>(this.url+'/opcoesEstadoSigla');
  }

  public findAllOpcoesEstado(): Observable<string[]> {
    return this.http.get<string[]>(this.url+'/opcoesEstado')
  }

  public findAllOpcoesGrauInstrucao(): Observable<string[]> {
    return this.http.get<string[]>(this.url+'/opcoesGrauInstrucao');
  }

  public findAllOpcoesSituacaoImovelTipo(): Observable<string[]> {
    return this.http.get<string[]>(this.url+'/opcoesSituacaoImovelTipo');
  }

  public findOneMoradia(id: number): Observable<MoradiaCidadao> {
    return this.http.get<MoradiaCidadao>(this.url+'/'+id+'/moradia');
  }

  public updateMoradia(id: number, moradia: MoradiaCidadao): Observable<MoradiaCidadao> {
    return this.http.put<MoradiaCidadao>(this.url+'/'+id+'/moradia', moradia);
  }

  public findOneInformacaoSocioDemografica(id: number): Observable<FinanceiroCidadao> {
    return this.http.get<FinanceiroCidadao>(this.url+'/'+id+'/informacaosociodemografica');
  }

  public updateInformacaoSocioDemografica(id: number, ficha: FichaCadastroIndividual): Observable<InformacaoSocioDemografica> {
    return this.http.put<InformacaoSocioDemografica>(this.url+'/'+id+'/informacaosociodemografica', ficha);
  }

  public updateCondicaoSaude(id: number, ficha: FichaCadastroIndividual): Observable<CondicoesSaude> {
    return this.http.put<CondicoesSaude>(this.url+'/'+id+'/condicaosaude', ficha);
  }

  public findByIdCondicaoSaude(id: number): Observable<CondicoesSaude> {
    return this.http.get<CondicoesSaude>(this.url + '/' + id + '/condicaosaude/');
  }

  public initCondicaosaude(): Observable<InformacoesCondicoesSaude> {
    return this.http.get<InformacoesCondicoesSaude>(this.url + '/initCondicaosaude');
  }

  public findByIdSituacaoRua(id: number): Observable<SituacaoRua> {
    return this.http.get<SituacaoRua>(this.url + '/' + id + '/situacaorua/');
  }

  public updateSituacaoRua(id: number, ficha: FichaCadastroIndividual): Observable<SituacaoRua> {
    return this.http.put<SituacaoRua>(this.url+'/'+id+'/situacaorua', ficha);
  }

  public initSituacaoRua(): Observable<InformacoesSituacaoRua> {
    return this.http.get<InformacoesSituacaoRua>(this.url + '/initSituacaoRua');
  }

  public initInformacaoSociodemografica(): Observable<FormInformacaoSociodemograficaDto> {
    return this.http.get<FormInformacaoSociodemograficaDto>(this.url + '/initInformacaosociodemografica');
  }

  public initCidadaoGeral(): Observable<FormCidadaoGeralDto> {
    return this.http.get<FormCidadaoGeralDto>(this.url + '/initCidadaogeral');
  }

  public initMoradiaCidadao(): Observable<FormMoradiaCidadaoDto> {
    return this.http.get<FormMoradiaCidadaoDto>(this.url + '/initMoradiaCidadao');
  }
}
