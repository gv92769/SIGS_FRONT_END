import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormCidadaoMoradiaComponent } from './form-cidadao-moradia.component';

describe('FormCidadaoMoradiaComponent', () => {
  let component: FormCidadaoMoradiaComponent;
  let fixture: ComponentFixture<FormCidadaoMoradiaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormCidadaoMoradiaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormCidadaoMoradiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
