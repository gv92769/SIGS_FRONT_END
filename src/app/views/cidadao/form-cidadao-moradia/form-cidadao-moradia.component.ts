import { Component, OnInit, Input } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Config } from '../../../core/model/config';
import { Municipio } from '../../../core/model/municipio';
import { MunicipioService } from '../../../core/service/municipio.service';
import { CidadaoService } from '../cidadao.service';
import { FormMoradiaCidadaoDto } from '../form-moradia-cidadao';
import { MoradiaCidadao } from '../moradia-cidadao';

@Component({
  selector: 'app-form-cidadao-moradia',
  templateUrl: './form-cidadao-moradia.component.html'
})
export class FormCidadaoMoradiaComponent implements OnInit {

  @Input() inIdCidadao: number;

  moradia: MoradiaCidadao = new MoradiaCidadao();

  opForm: FormMoradiaCidadaoDto = new FormMoradiaCidadaoDto();

  municipios: Municipio[] = new Array;

  isLoadingSubmit: boolean = false;

  config: Config = new Config();

  configCidadao: Config = new Config();

  constructor(
    private cidadaoService: CidadaoService,
    private toastrService: ToastrService,
    private municipioService: MunicipioService
    ) {}

  ngOnInit() {
    if (this.inIdCidadao) {
      this.cidadaoService.findOneMoradia(this.inIdCidadao).subscribe(
        data => {
          Object.assign(this.moradia, data);
        },
        error => console.log(error)
      );
    }

    this.cidadaoService.initMoradiaCidadao().subscribe(
      data => {
        this.opForm = data;
      },
      error => console.log(error)
    );

    this.config.displayKey = 'nome';
    this.config.placeholder = 'Selecione um Municipio';
    this.config.searchOnKey = 'nome';

    this.configCidadao.displayKey = 'nome';
    this.configCidadao.placeholder = 'Digite o CPF/CNS';
  }

  buscarMunicipio(nome: string) {
    let uf = this.moradia.estado;

    this.municipioService.findByUfAndNome(uf, nome)
      .subscribe({
        next: (municipios) => {
          this.municipios = municipios;
        },
        error: (err) => {
          console.log(err);
          this.municipios = new Array;
        }
      });
  }

  municipioChange(event: Event) {
    this.moradia.municipio = event['value'];
  }

  estadoChange() {
    this.moradia.municipio = new Municipio();
  }

  onSubmit(){
    this.isLoadingSubmit = true;

    this.cidadaoService.updateMoradia(this.inIdCidadao,this.moradia).subscribe(
      data => {
        this.toastrService.success('Moradia do cidadão salva com sucesso!', 'Sucesso');
        this.moradia = data;
        this.isLoadingSubmit = false;
      },
      error => {
        console.log(error);
        this.toastrService.error('Error ao salvar moradia.', 'Error');
        this.isLoadingSubmit = false;
      },
    );
  }

}
