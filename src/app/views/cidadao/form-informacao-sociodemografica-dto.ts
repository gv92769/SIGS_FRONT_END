import { ObjectCodigoDescricao } from '../../core/model/object-codigo-descricao';
import { PovoComunidadeTradicional } from './povo-comunidade-tradicional';

export class FormInformacaoSociodemograficaDto {
    opcaoRelacaoParentesco: ObjectCodigoDescricao[] = new Array;
	opcaoCurso: ObjectCodigoDescricao[] = new Array;	
	opcaoSituacaoMercado: ObjectCodigoDescricao[] = new Array;
	opcaoResponsavel: ObjectCodigoDescricao[] = new Array;
    opcaoComunidade: PovoComunidadeTradicional[] = new Array;
    opcaoOrientacaoSexual: ObjectCodigoDescricao[] = new Array;
	opcaoIdentidadeGenero: ObjectCodigoDescricao[] = new Array;
    opcaoDeficiencia: ObjectCodigoDescricao[] = new Array;
}
