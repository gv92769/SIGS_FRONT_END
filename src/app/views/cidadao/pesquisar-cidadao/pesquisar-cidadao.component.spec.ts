import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PesquisarCidadaoComponent } from './pesquisar-cidadao.component';

describe('PesquisarCidadaoComponent', () => {
  let component: PesquisarCidadaoComponent;
  let fixture: ComponentFixture<PesquisarCidadaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PesquisarCidadaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PesquisarCidadaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
