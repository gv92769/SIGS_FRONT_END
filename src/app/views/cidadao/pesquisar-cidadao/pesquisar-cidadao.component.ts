import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Cidadao } from '../cidadao';
import { CidadaoService } from '../cidadao.service';
import { PesquisarCidadaoBiometriaComponent } from '../pesquisar-cidadao-biometria/pesquisar-cidadao-biometria.component';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { Page } from '../../../core/model/page';
import { CidadaoFilter } from '../cidadao-filter';

@Component({
  templateUrl: './pesquisar-cidadao.component.html'
})
export class PesquisarCidadaoComponent implements OnInit {

  @ViewChild(PesquisarCidadaoBiometriaComponent, { static: false }) modalBiometria: PesquisarCidadaoBiometriaComponent;

  @ViewChild('modalExcluir', { static: false }) modalExcluir: ModalDirective;

  isLoading: boolean = false;

  isLoadingExcluir: boolean = false;

  cidadaoExcluir: Cidadao;

  cidadaoExcluirNome: string;

  columns: any[];

  columnMode = ColumnMode;

  page: Page<Cidadao> = new Page();

  filter: CidadaoFilter = new CidadaoFilter();

  constructor(protected cidadaoService: CidadaoService, protected router: Router) { }

  ngOnInit() {
    this.createTable();
  }

  createTable() {
    this.columns = [
      { name: 'Nome', sortable: true },
      { name: 'Cpf', sortable: true },
      { name: 'Cns', sortable: true }
    ];
  }

  limpar() {
    this.filter = new CidadaoFilter();
  }

  onSubmit() {
    this.page = new Page();
    this.filter.column = null;
    this.filter.sort = null;
    this.search();
  }

  search(){
    this.isLoading = true;
    this.filter.pageNumber = this.page.pageNumber;
    this.filter.size = this.page.size;

    this.cidadaoService.search(this.filter)
      .subscribe(
        data => {
          this.page.content = data.content;
          this.page.totalElements = data.totalElements;
          this.isLoading = false;
        },
        error => {console.log(error); this.isLoading = false;}
      );
  }

  showModalBiometria(){
    console.log('outter show');
    this.modalBiometria.showModal();
  }

  onCloseModalBiometria(resultadoPesquisa){
    console.log('resultdo pesquisa pesquisar cidadao:');
    console.log(resultadoPesquisa);
    if(resultadoPesquisa.encontrado){
      //Pesquisar o cidadao e atualizar a tabela
      this.isLoading = true;
      this.cidadaoService.findOne(resultadoPesquisa.idCidadao).subscribe({
        next: (response) => {
          this.page.content = [response];
          this.page.totalElements = 1;
          this.isLoading = false;
        },
        error: (err) => {
          console.log(err);
          this.isLoading = false;
          this.page = new Page();
        }
      });
    }
    else{
      if(resultadoPesquisa.idCidadao==-1){
        //Foi cancelado, não faz nada
      }
      else{
        // Não encontrou cidadao com biometria, limpa tabela (exibe mensagem?)
        this.page = new Page();
        alert('Digital não cadastrada.');
      }
    }
  }

  onEditarCidadaoClick(id: number){
    this.router.navigate(['/cidadao/editar/',id]);
  }

  onExcluirCidadaoClick(cidadao){
    this.cidadaoExcluir = cidadao;
    this.cidadaoExcluirNome = this.cidadaoExcluir.nome;
    this.modalExcluir.show();
  }

  excluirCidadao(){
    this.isLoadingExcluir = true;
    //apagar no banco
    this.cidadaoService.delete(this.cidadaoExcluir.id)
      .subscribe(
        response => {
          this.search();
        },
        error => console.log('erro ao excluir cidadao'),
        () => {
          this.modalExcluir.hide();
          this.cidadaoExcluir = null;
        }
      );
  }

  onSort(event) {
    if (this.page.content.length > 1) {
      this.filter.column = event.sorts[0].prop;
      this.filter.sort = event.sorts[0].dir;
      this.search();
    }
  }

  setPage(event) {
    this.page.pageNumber = event.offset;
    this.search();
  }

}
