import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  templateUrl: './adicionar-cidadao.component.html'
})
export class AdicionarCidadaoComponent implements OnInit {

  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  onSubmitSuccess($event){
    let id = $event;
    this.router.navigate(['/cidadao/editar/'+id]);
  }

}
