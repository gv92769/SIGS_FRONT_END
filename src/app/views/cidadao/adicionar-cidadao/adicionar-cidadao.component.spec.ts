import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdicionarCidadaoComponent } from './adicionar-cidadao.component';

describe('AdicionarCidadaoComponent', () => {
  let component: AdicionarCidadaoComponent;
  let fixture: ComponentFixture<AdicionarCidadaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdicionarCidadaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdicionarCidadaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
