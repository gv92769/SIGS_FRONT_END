import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { LeitorBiometrico } from '../../../biometria/leitor-biometrico';
import { environment } from '../../../../environments/environment';
import { HttpClient } from '@angular/common/http';

declare var Fingerprint: any;

@Component({
  selector: 'app-pesquisar-cidadao-biometria',
  templateUrl: './pesquisar-cidadao-biometria.component.html',
  styleUrls: ['./pesquisar-cidadao-biometria.component.scss']
})
export class PesquisarCidadaoBiometriaComponent implements OnInit {

  @ViewChild('staticModal', { static: false }) childModal: ModalDirective;

  @Output() outCloseModal = new EventEmitter();

  leitorBiometrico: LeitorBiometrico;

  leitorBiometricoUid: string;

  resultadoPesquisa = {encontrado: false, idCidadao: undefined};

  statusLeitor: string;

  qualidadeDigitalTexto: string;

  qualidadeDigitalCodigo: number;

  mensagem: string;

  isLoading: boolean = false;

  di;

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
  }

  reinicializarResultadoPesquisa(){
    this.resultadoPesquisa.encontrado = false;
    this.resultadoPesquisa.idCidadao = null;
  }

  reinicializarImagemDiv(){
    var vDiv = document.getElementById('imagediv');
    vDiv.innerHTML = "";
  }

  showModal(): void {
    this.reinicializarResultadoPesquisa();
    this.reinicializarImagemDiv();
    this.di = undefined;
    this.leitorBiometricoUid=undefined;
    this.statusLeitor = 'Procurando...';
    this.qualidadeDigitalTexto = undefined;
    this.qualidadeDigitalCodigo = undefined;
    this.mensagem = undefined;

    console.log(this.leitorBiometricoUid);
    console.log(this.resultadoPesquisa);
    console.log(this.statusLeitor);
    console.log('show');
    this.childModal.show();

    this.leitorBiometrico = LeitorBiometrico.getInstance();
    this.leitorBiometrico.enumerateDevices().subscribe(
      leitores => {
        if(leitores.length > 0){
          this.leitorBiometricoUid = leitores[0];
          console.log(this.leitorBiometricoUid);
          this.statusLeitor = "Pronto";
          this.mensagem = "Aguardando leitor biométrico estar disponível"
          this.leitorBiometrico.onSamplesAcquired = (e) => this.sampleAcquired(e);
          this.leitorBiometrico.onQualityReported = (e) => this.qualityReported(e);
          let formatoBiometria = LeitorBiometrico.SAMPLE_FORMAT_PNG;
          this.leitorBiometrico.startAcquisition(formatoBiometria, this.leitorBiometricoUid).subscribe(
            () => {
              this.mensagem = 'Posicione o dedo no leitor biométrico e aguarde.';
            },
            error => {
              console.log(error);
              this.mensagem = "Erro";
            }
          );
        }
        else{
          this.statusLeitor = "Não encontrado";
        }
      },
      error => {
        console.log(error);
        this.leitorBiometricoUid = "";
        this.statusLeitor = "Erro";
      }
    );
  }

  hideModal(cancelar: boolean): void {
    this.leitorBiometrico.stopAcquisition(this.leitorBiometricoUid).subscribe(
      () => console.log("Capturing stopped !!!"),
      error => console.log(error.ssage)
    );
    this.childModal.hide();
    if(cancelar){
      this.resultadoPesquisa.idCidadao = -1;
    }
    this.outCloseModal.emit(this.resultadoPesquisa);
  }


  qualityReported(e){
    this.qualidadeDigitalTexto = LeitorBiometrico.qualityCode(e);
    this.qualidadeDigitalCodigo = e.quality;
  }

  sampleAcquired(s){
    // If sample acquired format is PNG- perform following call on object recieved
    // Get samples from the object - get 0th element of samples as base 64 encoded PNG image
    if(this.qualidadeDigitalCodigo == 0){
      var samples = JSON.parse(s.samples);
      var vDiv = document.getElementById('imagediv');
      vDiv.innerHTML = "";
      var image = document.createElement("img");
      image.id = "image";
      image.src = "data:image/png;base64," + Fingerprint.b64UrlTo64(samples[0]);
      image.width = 170;
      vDiv.appendChild(image);
      this.di = Fingerprint.b64UrlTo64(samples[0]);
      this.sendData();
    }
    else{
      this.mensagem = "Leitura mal sucedida. Posicione novamente o dedo no leitor biométrico e aguarde."
    }
  }

  sendData(){
    this.isLoading = true;
    let dataToSend = {
      digital: this.di
    };

    let url = environment.apiBaseUrl + '/cidadaos/biometria/identificar';
    this.http.post<any>(url, dataToSend).subscribe(
      data => {
        this.isLoading = false;
        this.mensagem = "Concluído";
        console.log(data);
        if(data.matched){
          //alert('Digital verificada com sucesso. ID:' + data.idCidadao);
          this.resultadoPesquisa.encontrado = true;
          this.resultadoPesquisa.idCidadao = data.idCidadao;
        }
        else{
          //alert('Não foi possível identificar a digital.');
          this.reinicializarResultadoPesquisa();
        }
      },
      error => {
        console.log(error);
        this.isLoading = false;
        this.mensagem = "Erro";
      },
      () => this.hideModal(false)
    );

  }

}
