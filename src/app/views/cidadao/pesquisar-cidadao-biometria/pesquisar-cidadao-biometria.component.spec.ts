import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PesquisarCidadaoBiometriaComponent } from './pesquisar-cidadao-biometria.component';

describe('PesquisarCidadaoBiometriaComponent', () => {
  let component: PesquisarCidadaoBiometriaComponent;
  let fixture: ComponentFixture<PesquisarCidadaoBiometriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PesquisarCidadaoBiometriaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PesquisarCidadaoBiometriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
