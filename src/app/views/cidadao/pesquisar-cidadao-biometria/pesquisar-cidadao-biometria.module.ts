// Angular
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PesquisarCidadaoBiometriaComponent } from './pesquisar-cidadao-biometria.component';
import { ModalModule } from 'ngx-bootstrap/modal';

@NgModule({
  imports: [
    CommonModule,
    ModalModule.forRoot(),
  ],
  declarations: [
    PesquisarCidadaoBiometriaComponent
  ],
  exports: [
    PesquisarCidadaoBiometriaComponent
  ]
})
export class PesquisarCidadaoBiometriaModule { }
