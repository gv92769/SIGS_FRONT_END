import { Municipio } from '../../core/model/municipio';
import { Pais } from '../../core/model/pais';
import { ObjectCodigoDescricao } from '../../core/model/object-codigo-descricao';

export class Cidadao {
  id: number;
  nomeSocial: string;
  nome: string;
  numProntuario: string;
  cpf: string;
  cns: string;
  numeroNisPisPasep: string;
  dataNascimento: string;
  sexo: string;
  estadoCivil: string;
  nacionalidade: string;
  paisNascimento: Pais = new Pais();
  estadoNascimento: string;
  municipioNascimento: Municipio = new Municipio();
  nomeMae: string;
  desconheceNomeMae: boolean = false;
  nomePai: string;
  desconheceNomePai: boolean = false;
  dtNaturalizacao: string;
  portariaNaturalizacao: string;
  dtEntradaBrasil: string;
  racaCor: ObjectCodigoDescricao = new ObjectCodigoDescricao();
  etnia: ObjectCodigoDescricao = new ObjectCodigoDescricao();
  statusEhResponsavel: boolean = true;
  cnsResponsavelFamiliar: string;
  cpfResponsavelFamiliar: string;
  stForaArea: boolean = false;
  microarea: string;
  email: string;
  celular: string;
  telefone: string;
  ativo: boolean = true;

  rg = new class {
      numero: string;
      orgaoEmissor: string;
      uf: string;
  };

  tituloEleitor = new class{
    numero: string;
    zona: string;
    secao: string;
    uf: string;
  };
}
