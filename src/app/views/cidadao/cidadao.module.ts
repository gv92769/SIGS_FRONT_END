// Angular
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
// Components Routing
import { CidadaoRoutingModule } from './cidadao-routing.module';

import { FormsModule } from '@angular/forms';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { PesquisarCidadaoComponent } from './pesquisar-cidadao/pesquisar-cidadao.component';
import { AdicionarCidadaoComponent } from './adicionar-cidadao/adicionar-cidadao.component';
import { EditarCidadaoComponent } from './editar-cidadao/editar-cidadao.component';
import { FormCidadaoGeralComponent } from './form-cidadao-geral/form-cidadao-geral.component';
import { FormCidadaoMoradiaComponent } from './form-cidadao-moradia/form-cidadao-moradia.component';
import { CustomComponentsModule } from '../../custom-components/custom-components.module';
import { AdicionarBiometriaCidadaoComponent } from './adicionar-biometria-cidadao/adicionar-biometria-cidadao.component';

import { ModalModule } from 'ngx-bootstrap/modal';
import { FormCidadaoBiometriaComponent } from './form-cidadao-biometria/form-cidadao-biometria.component';
import { VerificarBiometriaCidadaoComponent } from './verificar-biometria-cidadao/verificar-biometria-cidadao.component';

import { PesquisarCidadaoBiometriaModule } from './pesquisar-cidadao-biometria/pesquisar-cidadao-biometria.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { FormInformacaoSocioDemograficaComponent } from './form-informacao-socio-demografica/form-informacao-socio-demografica.component';
import { SelectDropDownModule } from 'ngx-select-dropdown';
import { FormCondicoesSaudeComponent } from './form-condicoes-saude/form-condicoes-saude.component';
import { FormSituacaoRuaComponent } from './form-situacao-rua/form-situacao-rua.component'

@NgModule({
  imports: [
    CommonModule,
    CidadaoRoutingModule,
    FormsModule,
    TabsModule,
    TooltipModule.forRoot(),
    CustomComponentsModule,
    ModalModule.forRoot(),
    PesquisarCidadaoBiometriaModule,
    NgxDatatableModule,
    SelectDropDownModule
  ],
  declarations: [
    PesquisarCidadaoComponent,
    AdicionarCidadaoComponent,
    EditarCidadaoComponent,
    FormCidadaoGeralComponent,
    FormCidadaoMoradiaComponent,
    AdicionarBiometriaCidadaoComponent,
    FormCidadaoBiometriaComponent,
    VerificarBiometriaCidadaoComponent,
    FormInformacaoSocioDemograficaComponent,
    FormCondicoesSaudeComponent,
    FormSituacaoRuaComponent
  ]
})
export class CidadaoModule { }
