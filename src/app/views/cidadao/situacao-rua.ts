import { ObjectCodigoDescricao } from "../../core/model/object-codigo-descricao";
import { QuantasAlimentacao } from "./quantas-alimentacao";
import { TempoSituacaoRua } from "./tempo-situacao-rua";

export class SituacaoRua {
  id: number;
  grauParentescoFamiliarFrequentado: string;
  outraInstituicaoAcompanha: string;
  listAcessoHigiene: ObjectCodigoDescricao[] = new Array;
  listOrigemAlimentacao: ObjectCodigoDescricao[] = new Array;
  quantasAlimentacao: QuantasAlimentacao;
  statusAcompanhadoOutraInstituicao: boolean;
  statusPossuiReferenciaFamiliar: boolean;
  statusRecebeBeneficio: boolean;
  statusSituacaoRua: boolean = false;
  statusAcessoHigienePessoal: boolean;
  statusVisitaFamiliarFrequentemente: boolean;
  tempoSituacaoRua: TempoSituacaoRua;
}
