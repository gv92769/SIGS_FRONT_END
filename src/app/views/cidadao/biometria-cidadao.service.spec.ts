import { TestBed } from '@angular/core/testing';

import { BiometriaCidadaoService } from './biometria-cidadao.service';

describe('BiometriaCidadaoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BiometriaCidadaoService = TestBed.get(BiometriaCidadaoService);
    expect(service).toBeTruthy();
  });
});
