import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormSituacaoRuaComponent } from './form-situacao-rua.component';

describe('FormSituacaoRuaComponent', () => {
  let component: FormSituacaoRuaComponent;
  let fixture: ComponentFixture<FormSituacaoRuaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormSituacaoRuaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormSituacaoRuaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
