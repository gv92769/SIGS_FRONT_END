import { Component, Input, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { CboService } from '../../../core/service/cbo.service';
import { ProfissionalService } from '../../../core/service/profissional.service';
import { UnidadeBasicaSaudeService } from '../../../core/service/unidade-basica-saude.service';
import { UsuarioService } from '../../usuario/usuario.service';
import { Cidadao } from '../cidadao';
import { CidadaoService } from '../cidadao.service';
import { FichaCadastroIndividual } from '../ficha-cadastro-individual';
import { InformacoesSituacaoRua } from '../informacoes-situacao-rua';
import { SituacaoRua } from '../situacao-rua';
import * as jwt_decode from 'jwt-decode';

@Component({
  selector: 'app-form-situacao-rua',
  templateUrl: './form-situacao-rua.component.html',
  styleUrls: ['./form-situacao-rua.component.css']
})

export class FormSituacaoRuaComponent implements OnInit {
  @Input() inIdCidadao: number;

  situacaoRua: SituacaoRua = new SituacaoRua();

  informacao: InformacoesSituacaoRua = new InformacoesSituacaoRua();

  ficha: FichaCadastroIndividual = new FichaCadastroIndividual();

  isLoadingSubmit: boolean = false;

  cidadao: Cidadao = new Cidadao();

  obrigatorio = true;
  constructor(
    private cidadaoService: CidadaoService,
    private usuarioService: UsuarioService,
    private profissionalService: ProfissionalService,
    private unidadeSaudeBasicaService: UnidadeBasicaSaudeService,
    private toastrService: ToastrService,
    private cboService: CboService
  ) { }

  ngOnInit(): void {
    if (this.inIdCidadao) {
      this.cidadaoService.findByIdSituacaoRua(this.inIdCidadao).subscribe(
        data => {
          Object.assign(this.situacaoRua, data);
        },
        error => console.log(error)
      );

      this.cidadaoService.findOne(this.inIdCidadao).subscribe(
         data => {
          this.cidadao = data;
        },
        error => console.log(error)
      );
    }

    this.usuarioService.getCurrentUserInfo().subscribe(
      data => this.profissionalService.findById(data.id)
        .subscribe(profissional => this.ficha.profissionalResponsavel = profissional)
    );

    this.unidadeSaudeBasicaService.findByCnes(jwt_decode( sessionStorage.getItem('access_token')).cnes)
      .subscribe(ubs => this.ficha.ubs = ubs);

    this.cboService.findByCodigo(jwt_decode(sessionStorage.getItem('access_token')).cbo)
      .subscribe(cbo => this.ficha.cbo = cbo);


    this.cidadaoService.initSituacaoRua().subscribe(
      data => {
        this.informacao = data;
      },
      error => console.log(error)
    );
  }

  acessoHigieneChecked(opcao) {
    if (this.situacaoRua.listAcessoHigiene.findIndex(x => x.codigo == opcao.codigo) != -1)
      return true;

    return false;
  }

  acessoHigieneChange(opcao) {
    let index = this.situacaoRua.listAcessoHigiene.findIndex(x => x.codigo == opcao.codigo)
    if (index == -1)
      this.situacaoRua.listAcessoHigiene.push(opcao);
    else
      this.situacaoRua.listAcessoHigiene.splice(index, 1);
  }

  origemAlimentacaoChecked(opcao) {
    if (this.situacaoRua.listOrigemAlimentacao.findIndex(x => x.codigo == opcao.codigo) != -1)
      return true;

    return false;
  }

  origemAlimentacaoChange(opcao) {
    let index = this.situacaoRua.listOrigemAlimentacao.findIndex(x => x.codigo == opcao.codigo)
    if (index == -1)
      this.situacaoRua.listOrigemAlimentacao.push(opcao);
    else
      this.situacaoRua.listOrigemAlimentacao.splice(index, 1);
  }

  verificarCampos() {

    if (!this.situacaoRua.statusSituacaoRua) {
      this.informacao.listTempoSituacaoRua = new Array;
      this.informacao.QuantasAlimentacao = new Array;
      this.informacao.listAcessoHigiene = new Array;
      this.informacao.listOrigemAlimentacao = new Array;

      this.situacaoRua.statusRecebeBeneficio = false;
      this.situacaoRua.statusPossuiReferenciaFamiliar = false;
      this.situacaoRua.statusAcompanhadoOutraInstituicao = false;
      this.situacaoRua.outraInstituicaoAcompanha = "";
      this.situacaoRua.statusVisitaFamiliarFrequentemente = false;
      this.situacaoRua.grauParentescoFamiliarFrequentado = "";
      this.situacaoRua.statusAcessoHigienePessoal = false;
    }
  }

  onSubmit() {
    this.isLoadingSubmit = true;
    this.verificarCampos();
    this.ficha.situacaoRua = this.situacaoRua;
    this.cidadaoService.updateSituacaoRua(this.inIdCidadao, this.ficha).subscribe(
      data => {
        this.toastrService.success('Situação de Rua salva com sucesso!', 'Sucesso');
        this.isLoadingSubmit = false;
        this.situacaoRua = data;
      },
      error => {
        console.log(error);
        this.toastrService.error('Error ao salvar Situação de Rua.', 'Error');
        this.isLoadingSubmit = false;
      }
    );
  }

}
