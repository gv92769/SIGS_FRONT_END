import { Cbo } from '../../core/model/cbo';
import { PovoComunidadeTradicional } from './povo-comunidade-tradicional';
import { ObjectCodigoDescricao } from '../../core/model/object-codigo-descricao';

export class InformacaoSocioDemografica {
  id: number;
  deficiencias: ObjectCodigoDescricao[] = new Array;
  grauInstrucaoCidadao: ObjectCodigoDescricao = new ObjectCodigoDescricao();
  cbo: Cbo = null;
  orientacaoSexual: ObjectCodigoDescricao = new ObjectCodigoDescricao();
  povoComunidadeTradicional: PovoComunidadeTradicional = new PovoComunidadeTradicional();
  relacaoParentesco: ObjectCodigoDescricao = new ObjectCodigoDescricao();
  situacaoMercadoTrabalho: ObjectCodigoDescricao = new ObjectCodigoDescricao();
  statusDesejaInformarOrientacaoSexual: boolean = false;
  statusFrequentaBenzedeira: boolean = false;
  statusFrequentaEscola: boolean = false;
  statusMembroPovoComunidadeTradicional: boolean = false;
  statusParticipaGrupoComunitario: boolean = false;
  statusPossuiPlanoSaudePrivado: boolean = false;
  statusTemAlgumaDeficiencia: boolean = false;
  identidadeGenero: ObjectCodigoDescricao = new ObjectCodigoDescricao();
  statusDesejaInformarIdentidadeGenero: boolean = false;
  responsaveis: ObjectCodigoDescricao[] = new Array;
  ehResponsavel: boolean;
  idade: number;
}
