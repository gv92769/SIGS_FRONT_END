import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PesquisarCidadaoComponent } from './pesquisar-cidadao/pesquisar-cidadao.component';
import { AdicionarCidadaoComponent } from './adicionar-cidadao/adicionar-cidadao.component';
import { EditarCidadaoComponent } from './editar-cidadao/editar-cidadao.component';


const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Cidadão'
    },
    children: [
      {
        path: '',
        redirectTo: 'pesquisar'
      },
      {
        path: 'pesquisar',
        component: PesquisarCidadaoComponent,
        data: {
          title: 'Pesquisar'
        }
      },
      {
        path: 'adicionar',
        component: AdicionarCidadaoComponent,
        data: {
          title: 'Adicionar'
        }
      },
      {
        path: 'editar/:id',
        component: EditarCidadaoComponent,
        data: {
          title: 'Editar'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CidadaoRoutingModule {}
