import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { LeitorBiometrico } from '../../../biometria/leitor-biometrico';
import { BiometriaCidadaoService } from '../biometria-cidadao.service';

declare var Fingerprint: any;

@Component({
  selector: 'app-verificar-biometria-cidadao',
  templateUrl: './verificar-biometria-cidadao.component.html',
  styleUrls: ['./verificar-biometria-cidadao.component.scss']
})
export class VerificarBiometriaCidadaoComponent implements OnInit {

  @ViewChild('verificarModalBiometria', { static: false }) childModal: ModalDirective;
  @Input() inIdCidadao: number;

  leitorBiometrico: LeitorBiometrico;
  leitorBiometricoUid: string;
  resultadoVerificacao: boolean = false;
  statusLeitor: string;
  qualidadeDigitalTexto: string;
  qualidadeDigitalCodigo: number;
  mensagem: string;
  isLoading: boolean = false;
  di;

  constructor(private biometriaCidadaoServico: BiometriaCidadaoService) {
  }

  ngOnInit() {
  }

  reinicializarResultadoPesquisa(){
    this.resultadoVerificacao = false;
    this.di = undefined;
  }

  reinicializarImagemDiv(){
    var vDiv = document.getElementById('imagedivver');
    vDiv.innerHTML = "";
  }

  showModal(): void {
    this.reinicializarResultadoPesquisa();
    this.reinicializarImagemDiv();
    this.di = undefined;
    this.leitorBiometricoUid=undefined;
    this.statusLeitor = 'Procurando...';
    this.qualidadeDigitalTexto = undefined;
    this.qualidadeDigitalCodigo = undefined;
    this.mensagem = undefined;

    this.childModal.show();

    this.leitorBiometrico = LeitorBiometrico.getInstance();
    this.leitorBiometrico.enumerateDevices().subscribe(
      leitores => {
        if(leitores.length > 0){
          this.leitorBiometricoUid = leitores[0];
          console.log(this.leitorBiometricoUid);
          this.statusLeitor = "Pronto";
          this.mensagem = "Aguardando leitor biométrico estar disponível"
          this.leitorBiometrico.onSamplesAcquired = (e) => this.sampleAcquired(e);
          this.leitorBiometrico.onQualityReported = (e) => this.qualityReported(e);
          let formatoBiometria = LeitorBiometrico.SAMPLE_FORMAT_PNG;
          this.leitorBiometrico.startAcquisition(formatoBiometria, this.leitorBiometricoUid).subscribe(
            () => {
              this.mensagem = 'Posicione o dedo no leitor biométrico e aguarde.';
            },
            error => {
              console.log(error);
              this.mensagem = "Erro";
            }
          );
        }
        else{
          this.statusLeitor = "Não encontrado";
        }
      },
      error => {
        console.log(error);
        this.leitorBiometricoUid = "";
        this.statusLeitor = "Erro";
      }
    );
  }

  hideModal(): void {
    this.leitorBiometrico.stopAcquisition(this.leitorBiometricoUid).subscribe(
      () => console.log("Capturing stopped !!!"),
      error => console.log(error.ssage)
    );
    this.childModal.hide();
  }


  qualityReported(e){
    this.qualidadeDigitalTexto = LeitorBiometrico.qualityCode(e);
    this.qualidadeDigitalCodigo = e.quality;
  }

  sampleAcquired(s){
    // If sample acquired format is PNG- perform following call on object recieved
    // Get samples from the object - get 0th element of samples as base 64 encoded PNG image
    if(this.qualidadeDigitalCodigo == 0){
      var samples = JSON.parse(s.samples);
      var vDiv = document.getElementById('imagedivver');
      vDiv.innerHTML = "";
      var image = document.createElement("img");
      image.id = "image";
      image.src = "data:image/png;base64," + Fingerprint.b64UrlTo64(samples[0]);
      image.width = 170;
      vDiv.appendChild(image);
      this.di = Fingerprint.b64UrlTo64(samples[0]);
      this.sendData();
    }
    else{
      this.mensagem = "Leitura mal sucedida. Posicione novamente o dedo no leitor biométrico e aguarde."
    }
  }

  sendData(){
    this.isLoading = true;
    let dataToSend = {
      digital: this.di
    };

    this.biometriaCidadaoServico.postVerificacao(this.inIdCidadao,dataToSend).subscribe(
      data => {
        console.log(data);
        if(data.matched){
          this.resultadoVerificacao = true;
        }
        else{
          this.resultadoVerificacao = false;
        }
      },
      error => {
        console.log(error);
        this.mensagem = "Erro";
      },
      () => {
        this.isLoading = false;
        this.mensagem = 'Posicione o dedo no leitor biométrico e aguarde.';
      }
    );

  }

}
