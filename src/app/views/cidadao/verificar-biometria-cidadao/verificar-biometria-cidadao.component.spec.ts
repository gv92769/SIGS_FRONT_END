import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerificarBiometriaCidadaoComponent } from './verificar-biometria-cidadao.component';

describe('VerificarBiometriaCidadaoComponent', () => {
  let component: VerificarBiometriaCidadaoComponent;
  let fixture: ComponentFixture<VerificarBiometriaCidadaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerificarBiometriaCidadaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerificarBiometriaCidadaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
