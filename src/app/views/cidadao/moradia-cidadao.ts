import { Municipio } from "../../core/model/municipio";
import { ObjectCodigoDescricao } from "../../core/model/object-codigo-descricao";

export class MoradiaCidadao {
  id: number;
  bairro: string;
  cep: string;
  municipio: Municipio = null;
  complemento: string;
  nomeLogradouro: string;
  numero: string;
  estado: string;
  telefoneContato: string;
  telefoneResidencia: string;
  tipoLogradouro: ObjectCodigoDescricao = new ObjectCodigoDescricao();
  stSemNumero: boolean= false;
  pontoReferencia: string;
  microarea: string;
  stForaArea: boolean = false;
}
