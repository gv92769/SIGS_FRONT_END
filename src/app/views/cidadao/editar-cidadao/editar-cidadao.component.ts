import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  templateUrl: './editar-cidadao.component.html'
})
export class EditarCidadaoComponent implements OnInit {

  idCidadao: number;

  constructor(private route: ActivatedRoute) {
  }

  ngOnInit() {
      this.route.paramMap.subscribe(
        params => {
          let id = +params.get('id');
          this.idCidadao = id;
        }
      );
  }

  onGeralSubmitSuccess($event){
    window.scrollTo(0,0);
  }
}
