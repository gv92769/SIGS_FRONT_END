import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarCidadaoComponent } from './editar-cidadao.component';

describe('EditarCidadaoComponent', () => {
  let component: EditarCidadaoComponent;
  let fixture: ComponentFixture<EditarCidadaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarCidadaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarCidadaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
