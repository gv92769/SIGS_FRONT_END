import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormInformacaoSocioDemograficaComponent } from './form-informacao-socio-demografica.component';

describe('FormInformacaoSocioDemograficaComponent', () => {
  let component: FormInformacaoSocioDemograficaComponent;
  let fixture: ComponentFixture<FormInformacaoSocioDemograficaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormInformacaoSocioDemograficaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormInformacaoSocioDemograficaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
