import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { InformacaoSocioDemografica } from '../informacao-socio-demografica';
import { CboService } from '../../../core/service/cbo.service';
import { Cbo } from '../../../core/model/cbo';
import { FormInputIconStatusComponent } from '../../../custom-components/form-input-icon-status/form-input-icon-status.component';
import { FormInformacaoSociodemograficaDto } from '../form-informacao-sociodemografica-dto';
import { FichaCadastroIndividual } from '../ficha-cadastro-individual';
import { CidadaoService } from '../cidadao.service';
import { UsuarioService } from '../../usuario/usuario.service';
import { ProfissionalService } from '../../../core/service/profissional.service';
import { UnidadeBasicaSaudeService } from '../../../core/service/unidade-basica-saude.service';
import * as jwt_decode from 'jwt-decode';
import { ToastrService } from 'ngx-toastr';
import { Config } from '../../../core/model/config';

@Component({
  selector: 'app-form-informacao-socio-demografica',
  templateUrl: './form-informacao-socio-demografica.component.html',
  styleUrls: ['./form-informacao-socio-demografica.component.css']
})
export class FormInformacaoSocioDemograficaComponent implements OnInit {

  @Input() inIdCidadao: number;

  informacao: InformacaoSocioDemografica = new InformacaoSocioDemografica();

  opcaoForm: FormInformacaoSociodemograficaDto = new FormInformacaoSociodemograficaDto();

  @ViewChild('statusOcupacao', { static: false }) statusOcupacao: FormInputIconStatusComponent;

  isLoading: boolean = false;

  ficha: FichaCadastroIndividual = new FichaCadastroIndividual();

  config: Config = new Config();

  optionsOcupacao: Cbo[] = new Array;

  constructor(private cboService: CboService, private cidadaoService: CidadaoService, private usuarioService: UsuarioService, 
    private profissionalService: ProfissionalService, private unidadeSaudeBasicaService: UnidadeBasicaSaudeService, private toastrService: ToastrService) { }

  ngOnInit(): void {
    if (this.inIdCidadao) {
      this.cidadaoService.findOneInformacaoSocioDemografica(this.inIdCidadao).subscribe(
        data => {
          Object.assign(this.informacao, data);
        },
        error => console.log(error)
      );
    }

    this.usuarioService.getCurrentUserInfo().subscribe(
      data => this.profissionalService.findById(data.id)
        .subscribe(profissional => this.ficha.profissionalResponsavel = profissional)
    );

    this.unidadeSaudeBasicaService.findByCnes(jwt_decode( sessionStorage.getItem('access_token')).cnes)
      .subscribe(ubs => this.ficha.ubs = ubs);

    this.cboService.findByCodigo(jwt_decode(sessionStorage.getItem('access_token')).cbo)
      .subscribe(cbo => this.ficha.cbo = cbo);

    this.cidadaoService.initInformacaoSociodemografica().subscribe(
      data => this.opcaoForm = data
    );

    this.config.placeholder = 'Selecione a Ocupação';
    this.config.displayKey = 'titulo';
    this.config.searchOnKey = 'titulo';
  }

  buscarOcupacao() {
    if (!this.informacao.cbo.codigo) {
      this.informacao.cbo = new Cbo();
      this.statusOcupacao.ready();
    } else {
      let codigo = this.informacao.cbo.codigo;
      this.statusOcupacao.loading();
      this.cboService.findByCodigo(codigo)
        .subscribe({
          next: (cbo) => {
            this.informacao.cbo = cbo != null ? cbo : new Cbo();
            this.statusOcupacao.success();
          },
          error: (err) => {
            console.log(err);
            this.informacao.cbo = new Cbo();
            this.statusOcupacao.error();
          }
        });
    }
  }

  responsavelChange(opcao) {
    let index = this.informacao.responsaveis.findIndex(x => x.codigo == opcao.codigo)
    if (index == -1)
      this.informacao.responsaveis.push(opcao);
    else
      this.informacao.responsaveis.splice(index, 1);
  }

  responsavelChecked(opcao) {
    if (this.informacao.responsaveis.findIndex(x => x.codigo == opcao.codigo) == -1)
      return false;

    return true;
  }

  deficienciaChange(opcao) {
    let index = this.informacao.deficiencias.findIndex(x => x.codigo == opcao.codigo)
    if (index == -1)
      this.informacao.deficiencias.push(opcao);
    else
      this.informacao.deficiencias.splice(index, 1);
  }

  deficienciaChecked(opcao) {
    if (this.informacao.deficiencias.findIndex(x => x.codigo == opcao.codigo) == -1)
      return false;

    return true;
  }

  onSubmit() {
    this.isLoading = true;
    this.ficha.informacao = this.informacao;

    this.cidadaoService.updateInformacaoSocioDemografica(this.inIdCidadao, this.ficha).subscribe(
      data => {
        this.toastrService.success('Informações sociodemográfica salva com sucesso!', 'Sucesso');
        this.isLoading = false;
      },
      error => {
        console.log(error);
        this.toastrService.error('Error ao salvar informações sociodemográfica.', 'Error');
        this.isLoading = false;
      }
    );
  }

  searchOcupacaoChange(titulo: string) {
    this.cboService.findAllByTitulo(titulo)
      .subscribe({
        next: (data) => {
          this.optionsOcupacao = data;
        },
        error: (err) => {
          console.log(err);
          this.optionsOcupacao = new Array;
        }
      });
  }

}
