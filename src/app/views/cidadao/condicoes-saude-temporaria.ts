import { ObjectCodigoDescricao } from "../../core/model/object-codigo-descricao";

export class CondicoesSaudeTemporaria {

  id: number;
  consideracaoPeso: ObjectCodigoDescricao;
  statusEhDependenteAlcool: boolean = false;
  statusEhFumante: boolean = false;
  statusTemDiabetes: boolean = false;
  statusEhDependenteOutrasDrogas: boolean = false;
  descricaoCausaInternacaoEm12Meses: string;
  statusTeveInternadoem12Meses: boolean = false;
  descricaoOutraCondicao1: string;
  descricaoOutraCondicao2: string;
  descricaoOutraCondicao3: string;
  descricaoPlantasMedicinaisUsadas: string;
  maternidadeDeReferencia: string;
  statusEhGestante: boolean = false;
  statusEstaAcamado: boolean = false;
  statusUsaPlantasMedicinais: boolean = false;
  statusEstaDomiciliado: boolean = false;
}
