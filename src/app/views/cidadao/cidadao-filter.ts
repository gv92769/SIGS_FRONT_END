import { PageRequest } from '../../core/model/page-request';

export class CidadaoFilter extends PageRequest {
  nome: string;
  cpf: string;
  cns: string;
  ativo: boolean = true;
}
