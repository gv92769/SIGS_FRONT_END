export class FinanceiroCidadao {
  id: number;
  rendaFamiliar: string;
  inssAuxilio = new class {
      possui: boolean;
      descricao: string;
  };
  bolsaFamilia = new class {
      possui: boolean;
      descricao: string;
  };
  outraAssistencia = new class {
      possui: boolean;
      descricao: string;
  };
}
