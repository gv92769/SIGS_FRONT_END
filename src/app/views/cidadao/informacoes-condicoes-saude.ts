import { ObjectCodigoDescricao } from "../../core/model/object-codigo-descricao";

export class InformacoesCondicoesSaude {
  listRins: ObjectCodigoDescricao[] = new Array;
  listRespiratoria: ObjectCodigoDescricao[] = new Array;
  listCardiaca: ObjectCodigoDescricao[] = new Array;
  consideracaoPeso: ObjectCodigoDescricao[] = new Array;
}
