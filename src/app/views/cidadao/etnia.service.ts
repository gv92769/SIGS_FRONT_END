import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { ObjectCodigoDescricao } from '../../core/model/object-codigo-descricao';

@Injectable({
  providedIn: 'root'
})
export class EtniaService {

  readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/etnia';
  }

  public findAllByNome(descricao: string): Observable<ObjectCodigoDescricao[]> {
    return this.http.get<ObjectCodigoDescricao[]>(this.url+'/descricao/'+descricao);
  }

}
