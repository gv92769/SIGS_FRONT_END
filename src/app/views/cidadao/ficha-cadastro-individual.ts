import { UnidadeBasicaSaude } from '../../core/model/unidade-basica-saude';
import { Cidadao } from './cidadao';
import { Profissional } from '../../core/model/profissional';
import { formatDate } from '@angular/common';
import { InformacaoSocioDemografica } from './informacao-socio-demografica';
import { Cbo } from '../../core/model/cbo';
import { CondicoesSaude } from './condicoes-saude';
import { SituacaoRua } from './situacao-rua';

export class FichaCadastroIndividual {
  uuid: string;
  profissionalResponsavel: Profissional = new Profissional();
  ubs: UnidadeBasicaSaude = new UnidadeBasicaSaude();
  cbo: Cbo = new Cbo();
  cidadao: Cidadao = new Cidadao();
  ine: string;
  informacao: InformacaoSocioDemografica = new InformacaoSocioDemografica();
  condicaoSaude: CondicoesSaude = new CondicoesSaude();
  situacaoRua: SituacaoRua = new SituacaoRua();
  uuidFichaOriginadora: string;
  fichaAtualizada: boolean = false;
  data: string = formatDate(new Date(), 'yyyy-MM-dd', 'en-US');
  importada: boolean = false;
}
