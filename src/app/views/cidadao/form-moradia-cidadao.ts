import { ObjectCodigoDescricao } from '../../core/model/object-codigo-descricao';

export class FormMoradiaCidadaoDto {
  opImovel: ObjectCodigoDescricao[] = new Array;
	opLogradouro: ObjectCodigoDescricao[] = new Array;
 	opcoesEstados: string[];
}
