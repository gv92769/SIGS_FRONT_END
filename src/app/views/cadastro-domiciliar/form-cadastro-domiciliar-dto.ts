import { ObjectCodigoDescricao } from '../../core/model/object-codigo-descricao';

export class FormCadastroDomiciliarDto {
    opAbastecimentoAgua: ObjectCodigoDescricao[] = new Array;	
	opAguaConsumo: ObjectCodigoDescricao[] = new Array;	
	opAnimal: ObjectCodigoDescricao[] = new Array;
	opCondicaoPosse: ObjectCodigoDescricao[] = new Array;
	opDestinoLixo: ObjectCodigoDescricao[] = new Array;
	opFormaEscoamento: ObjectCodigoDescricao[] = new Array;
	opMaterialPredominante: ObjectCodigoDescricao[] = new Array;
	opRendaFamiliar: ObjectCodigoDescricao[] = new Array;
	opSituacaoMoradia: ObjectCodigoDescricao[] = new Array;
	opAcessoDomicilio: ObjectCodigoDescricao[] = new Array;	
	opDomicilio: ObjectCodigoDescricao[] = new Array;	
	opImovel: ObjectCodigoDescricao[] = new Array;	
	opLogradouro: ObjectCodigoDescricao[] = new Array;	
	opcoesEstados: string[];
}
