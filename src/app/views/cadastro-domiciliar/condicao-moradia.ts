export class CondicaoMoradia {
  id: number;
  codAbastecimentoAgua: number;
  codPosseUso: number;
  codDestinoLixo: number;
  codFormaEscoamentoBanheiro: number;
  localizacaoMoradia: string;
  codMaterialPredominante: number;
  nuComodos: string;
  nuMoradores: string;
  codSituacaoMoradia: number;
  stDisponibilidadeEnergiaEletrica: boolean = true;
  codTipoAcessoDomicilio: number;
  codTipoDomicilio: number;
  codAguaDomicilio: number;
}
