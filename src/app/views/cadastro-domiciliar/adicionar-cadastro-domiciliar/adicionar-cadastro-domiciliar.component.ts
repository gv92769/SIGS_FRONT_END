import { Component, OnInit, ViewChild } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Domicilio } from '../domicilio';
import { FormCadastroDomiciliarComponent } from '../form-cadastro-domiciliar/form-cadastro-domiciliar.component';
import { CadastroDomiciliarService } from '../cadastro-domiciliar.service';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FichaCadastroDomiciliar } from '../ficha-cadastro-domiciliar';

@Component({
  selector: 'app-adicionar-cadastro-domiciliar',
  templateUrl: './adicionar-cadastro-domiciliar.component.html',
  styleUrls: ['./adicionar-cadastro-domiciliar.component.css']
})
export class AdicionarCadastroDomiciliarComponent implements OnInit {

  formData$: BehaviorSubject<Domicilio> = new BehaviorSubject<Domicilio>(new Domicilio());

  @ViewChild('formCadastroDomiciliar', { static: false }) formCadastroDomiciliar: FormCadastroDomiciliarComponent;

  isLoadingSubmit: boolean = false;

  constructor(private toastrService: ToastrService, private cadastroDomiciliarService: CadastroDomiciliarService, private router: Router) { }

  ngOnInit(): void {
    this.formData$.next(new Domicilio());
  }

  onSubmitted(ficha: FichaCadastroDomiciliar) {
    this.isLoadingSubmit = true;

    this.cadastroDomiciliarService.create(ficha)
      .pipe(finalize(() => this.isLoadingSubmit = false))
      .subscribe({
        next: (response) => {
          this.toastrService.success('Cadastro domiciliar salvo com sucesso!', 'Sucesso');
          this.ngOnInit();
          this.router.navigate(['cadastrodomiciliar/adicionar']);
        },
        error: (err) => {
          this.toastrService.error('Error ao salvar cadastro domiciliar.', 'Error');
          console.log(err);
        }
      });
  }

}
