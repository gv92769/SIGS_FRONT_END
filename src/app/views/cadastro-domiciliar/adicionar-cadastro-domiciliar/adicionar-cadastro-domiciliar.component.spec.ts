import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdicionarCadastroDomiciliarComponent } from './adicionar-cadastro-domiciliar.component';

describe('AdicionarCadastroDomiciliarComponent', () => {
  let component: AdicionarCadastroDomiciliarComponent;
  let fixture: ComponentFixture<AdicionarCadastroDomiciliarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdicionarCadastroDomiciliarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdicionarCadastroDomiciliarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
