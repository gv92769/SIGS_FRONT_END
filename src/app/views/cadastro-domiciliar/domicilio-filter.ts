import { PageRequest } from '../../core/model/page-request';

export class DomicilioFilter extends PageRequest {
  logradouro: string;
  numero: string;
  complemento: string;
  bairro: string;
  microarea: string;
  cep: string;
  cpfCns: string;
}
