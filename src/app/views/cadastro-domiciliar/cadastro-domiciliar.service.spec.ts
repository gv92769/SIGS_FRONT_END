import { TestBed } from '@angular/core/testing';

import { CadastroDomiciliarService } from './cadastro-domiciliar.service';

describe('CadastroDomiciliarService', () => {
  let service: CadastroDomiciliarService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CadastroDomiciliarService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
