import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { FichaCadastroDomiciliar } from './ficha-cadastro-domiciliar';
import { Observable } from 'rxjs';
import { Domicilio } from './domicilio';
import { DomicilioFilter } from './domicilio-filter';
import { EnderecoLocalPermanencia } from './endereco-local-permanencia';
import { Page } from '../../core/model/page';
import { FormCadastroDomiciliarDto } from './form-cadastro-domiciliar-dto';

@Injectable({
  providedIn: 'root'
})
export class CadastroDomiciliarService {

  readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/cadastrodomiciliar';
  }

  public findById(id: number): Observable<Domicilio> {
    return this.http.get<Domicilio>(this.url + '/' + id);
  }

  public create(ficha: FichaCadastroDomiciliar): Observable<FichaCadastroDomiciliar> {
    return this.http.post<FichaCadastroDomiciliar>(this.url, ficha);
  }

  public update(id: number, ficha: FichaCadastroDomiciliar): Observable<FichaCadastroDomiciliar> {
    return this.http.put<FichaCadastroDomiciliar>(this.url+"/"+id, ficha);
  }

  public delete(id: number) {
    return this.http.delete(this.url + '/' + id);
  }

  public search(filter: DomicilioFilter): Observable<Page<EnderecoLocalPermanencia>> {
    return this.http.post<Page<EnderecoLocalPermanencia>>(this.url+'/search', filter);
  }

  public init(): Observable<FormCadastroDomiciliarDto> {
    return this.http.get<FormCadastroDomiciliarDto>(this.url + '/init');
  }

}
