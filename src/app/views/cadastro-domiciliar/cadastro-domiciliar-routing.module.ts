import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdicionarCadastroDomiciliarComponent } from './adicionar-cadastro-domiciliar/adicionar-cadastro-domiciliar.component';
import { EditarCadastroDomiciliarComponent } from './editar-cadastro-domiciliar/editar-cadastro-domiciliar.component';
import { PesquisarCadastroDomiciliarComponent } from './pesquisar-cadastro-domiciliar/pesquisar-cadastro-domiciliar.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Cadastro Domiciliar'
    },
    children: [
      {
        path: 'adicionar',
        component: AdicionarCadastroDomiciliarComponent,
        data: {
          title: 'Adicionar'
        }
      },
      {
        path: 'editar/:id',
        component: EditarCadastroDomiciliarComponent,
        data: {
          title: 'Editar'
        }
      },
      {
        path: 'pesquisar',
        component: PesquisarCadastroDomiciliarComponent,
        data: {
          title: 'Pesquisar'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CadastroDomiciliarRoutingModule {}
