import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormCadastroDomiciliarComponent } from './form-cadastro-domiciliar.component';

describe('FormCadastroDomiciliarComponent', () => {
  let component: FormCadastroDomiciliarComponent;
  let fixture: ComponentFixture<FormCadastroDomiciliarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormCadastroDomiciliarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormCadastroDomiciliarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
