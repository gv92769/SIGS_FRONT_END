import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { Domicilio } from '../domicilio';
import { Observable } from 'rxjs';
import { FormGroup } from '@angular/forms';
import { ObjectCodigoDescricao } from '../../../core/model/object-codigo-descricao';
import { CidadaoService } from '../../cidadao/cidadao.service';
import { MunicipioService } from '../../../core/service/municipio.service';
import { Municipio } from '../../../core/model/municipio';
import { Familia } from '../familia';
import { Cidadao } from '../../cidadao/cidadao';
import { InstituicaoPermanencia } from '../instituicao-permanencia';
import { Config } from '../../../core/model/config';
import { UnidadeBasicaSaudeService } from '../../../core/service/unidade-basica-saude.service';
import { UsuarioService } from '../../usuario/usuario.service';
import { ProfissionalService } from '../../../core/service/profissional.service';
import { FichaCadastroDomiciliar } from '../ficha-cadastro-domiciliar';
import * as jwt_decode from 'jwt-decode';
import { CboService } from '../../../core/service/cbo.service';
import { FormCadastroDomiciliarDto } from '../form-cadastro-domiciliar-dto';
import { CondicaoMoradia } from '../condicao-moradia';
import { CadastroDomiciliarService } from '../cadastro-domiciliar.service';

@Component({
  selector: 'app-form-cadastro-domiciliar',
  templateUrl: './form-cadastro-domiciliar.component.html',
  styleUrls: ['./form-cadastro-domiciliar.component.css']
})
export class FormCadastroDomiciliarComponent implements OnInit {

  @Input('formData') formData$: Observable<Domicilio>;

  @ViewChild('form', { static: false }) form: FormGroup;

  @Output('onSubmit') onSubmitEventEmitter = new EventEmitter();

  ficha: FichaCadastroDomiciliar = new FichaCadastroDomiciliar();

  domicilio: Domicilio = new Domicilio();

  opForm: FormCadastroDomiciliarDto = new FormCadastroDomiciliarDto();

  municipios: Municipio[] = new Array;

  config: Config = new Config();

  opCidadao: Cidadao[] = new Array;

  configCidadao: Config = new Config();

  constructor(private cidadaoService: CidadaoService,private municipioService: MunicipioService, private usuarioService: UsuarioService,
    private profissionalService: ProfissionalService,private unidadeSaudeBasicaService: UnidadeBasicaSaudeService, private cboService: CboService,
    private cadastroDomiciliarService: CadastroDomiciliarService) { }

  ngOnInit(): void {
    this.formData$.subscribe({
      next: (value) => {
        this.domicilio = value;
        this.domicilio.instituicao = this.domicilio.instituicao == null ? new InstituicaoPermanencia() : this.domicilio.instituicao;
        this.domicilio.condicao = this.domicilio.condicao == null ? new CondicaoMoradia() : this.domicilio.condicao;
        this.domicilio.tipoImovel = this.domicilio.tipoImovel == null ? new ObjectCodigoDescricao() : this.domicilio.tipoImovel;
      },
      error: (err) => {
        console.log('erro behavior subject');
        console.log(err);
      }
    });

    this.usuarioService.getCurrentUserInfo().subscribe(
      data => this.profissionalService.findById(data.id)
        .subscribe(profissional => this.ficha.profissionalResponsavel = profissional)
    );

    this.unidadeSaudeBasicaService.findByCnes(jwt_decode( sessionStorage.getItem('access_token')).cnes)
      .subscribe(ubs => this.ficha.ubs = ubs);

    this.cboService.findByCodigo(jwt_decode(sessionStorage.getItem('access_token')).cbo)
      .subscribe(cbo => this.ficha.cbo = cbo);

    this.cadastroDomiciliarService.init().subscribe(
      data => this.opForm = data,
      error => console.log("error in initCadastroDomiciliar()")
    );

    this.config.displayKey = 'nome';
    this.config.placeholder = 'Selecione um Municipio';
    this.config.searchOnKey = 'nome';

    this.configCidadao.displayKey = 'nome';
    this.configCidadao.placeholder = 'Digite o CPF/CNS';
  }

  buscarMunicipio(nome: string) {
    let uf = this.domicilio.endereco.estado;

    this.municipioService.findByUfAndNome(uf, nome)
      .subscribe({
        next: (municipios) => {
          this.municipios = municipios;
        },
        error: (err) => {
          console.log(err);
          this.municipios = new Array;
        }
      });
  }

  municipioChange(event: Event) {
    this.domicilio.endereco.municipio = event['value'];
  }

  estadoChange() {
    this.domicilio.endereco.municipio = new Municipio();
  }

  disabled() {
    let compare = [7, 8, 9, 10, 11];

    if (compare.findIndex(x => x == this.domicilio.tipoImovel.codigo) == -1)
      return false;

    return true;
  }

  animalChange(op) {
    let index = this.domicilio.animaisDomicilio.findIndex(x => x.codigo == op.codigo)
    if (index == -1)
      this.domicilio.animaisDomicilio.push(op);
    else
      this.domicilio.animaisDomicilio.splice(index, 1);
  }

  animalChecked(op) {
    if (this.domicilio.animaisDomicilio.findIndex(x => x.codigo == op.codigo) == -1)
      return false;

    return true;
  }

  addFamilia() {
    this.domicilio.familias.push(new Familia());
  }

  removeFamilia(i) {
    this.domicilio.familias.splice(i, 1);
  }

  buscarCidadao(valor: string) {
    if (valor != null && valor != '')
      this.cidadaoService.filtro(valor)
        .subscribe({
          next: (data) => {
            this.opCidadao = data;
          },
          error: (err) => {
            console.log(err);
          }
        });
  }

  onFormSubmit() {
    if (this.form.valid) {
      if (this.domicilio.tipoImovel.codigo == 1)
        this.domicilio.quantosAnimaisDomicilio = this.domicilio.animaisDomicilio.length;
      if (this.domicilio.id != null)
        this.ficha.fichaAtualizada = true;
      this.ficha.domicilio = this.domicilio;
      this.onSubmitEventEmitter.emit(this.ficha);
    }
  }

}
