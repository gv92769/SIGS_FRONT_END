export class InstituicaoPermanencia {
  id: number;
  nomeInstituicaoPermanencia: string;
  stOutrosProfissionaisVinculados: boolean = false;
  nomeResponsavelTecnico: string;
  cnsResponsavelTecnico: string;
  cargoInstituicao: string;
  telefoneResponsavelTecnico: string;
}
