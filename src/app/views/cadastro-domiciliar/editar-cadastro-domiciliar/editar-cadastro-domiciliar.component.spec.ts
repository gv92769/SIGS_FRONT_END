import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarCadastroDomiciliarComponent } from './editar-cadastro-domiciliar.component';

describe('EditarCadastroDomiciliarComponent', () => {
  let component: EditarCadastroDomiciliarComponent;
  let fixture: ComponentFixture<EditarCadastroDomiciliarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarCadastroDomiciliarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarCadastroDomiciliarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
