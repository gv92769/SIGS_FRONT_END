import { Component, OnInit, ViewChild } from '@angular/core';
import { Domicilio } from '../domicilio';
import { BehaviorSubject } from 'rxjs';
import { FormCadastroDomiciliarComponent } from '../form-cadastro-domiciliar/form-cadastro-domiciliar.component';
import { ActivatedRoute, Router } from '@angular/router';
import { map, mergeMap, finalize } from 'rxjs/operators';
import { CadastroDomiciliarService } from '../cadastro-domiciliar.service';
import { FichaCadastroDomiciliar } from '../ficha-cadastro-domiciliar';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-editar-cadastro-domiciliar',
  templateUrl: './editar-cadastro-domiciliar.component.html',
  styleUrls: ['./editar-cadastro-domiciliar.component.css']
})
export class EditarCadastroDomiciliarComponent implements OnInit {

  formData$: BehaviorSubject<Domicilio> = new BehaviorSubject<Domicilio>(new Domicilio());

  @ViewChild('formCadastroDomiciliar', { static: false }) formCadastroDomiciliar: FormCadastroDomiciliarComponent;

  isLoadingSubmit: boolean = false;

  constructor(private toastrService: ToastrService, private route: ActivatedRoute, private cadastroDomiciliarService: CadastroDomiciliarService, 
    private router: Router) { }

  ngOnInit(): void {
    this.route.paramMap.pipe(
      map(params => {
        const id = +params.get('id');
        return id;
      }),
      mergeMap(id => {
        return this.cadastroDomiciliarService.findById(id);
        }
      )
    )
    .subscribe({
      next: (response) => {
        this.formData$.next(response);
      },
      error: (err) => {
        console.log('Erro ao busca Domicilio');
        console.log(err);
      }
    });
  }

  onSubmitted(ficha: FichaCadastroDomiciliar) {
    this.isLoadingSubmit = true;

    this.cadastroDomiciliarService.update(ficha.domicilio.id, ficha)
      .pipe(finalize(() => this.isLoadingSubmit = false))
      .subscribe({
        next: (response) => {
          this.toastrService.success('Cadastro domiciliar salvo com sucesso!', 'Sucesso');
          this.router.navigate(['cadastrodomiciliar/adicionar']);
        },
        error: (err) => {
          this.toastrService.error('Error ao salvar cadastro domiciliar.', 'Error');
          console.log(err);
        }
      });
  }

}
