import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PesquisarCadastroDomiciliarComponent } from './pesquisar-cadastro-domiciliar.component';

describe('PesquisarCadastroDomiciliarComponent', () => {
  let component: PesquisarCadastroDomiciliarComponent;
  let fixture: ComponentFixture<PesquisarCadastroDomiciliarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PesquisarCadastroDomiciliarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PesquisarCadastroDomiciliarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
