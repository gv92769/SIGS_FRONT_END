import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Page } from '../../../core/model/page';
import { EnderecoLocalPermanencia } from '../endereco-local-permanencia';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { Router } from '@angular/router';
import { CadastroDomiciliarService } from '../cadastro-domiciliar.service';
import { DomicilioFilter } from '../domicilio-filter';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-pesquisar-cadastro-domiciliar',
  templateUrl: './pesquisar-cadastro-domiciliar.component.html',
  styleUrls: ['./pesquisar-cadastro-domiciliar.component.css']
})
export class PesquisarCadastroDomiciliarComponent implements OnInit {

  @ViewChild('form', { static: false }) form: FormGroup;

  @ViewChild('modalExcluir', { static: false }) modalExcluir: ModalDirective;

  domicilioSelected: any;

  filter: DomicilioFilter = new DomicilioFilter();

  columns: any[];

  columnMode = ColumnMode;

  page: Page<EnderecoLocalPermanencia> = new Page();

  isLoading: boolean = false;

  isLoadingExcluir: boolean = false;

  constructor(private router: Router, private cadastroDomiciliarService: CadastroDomiciliarService) { }

  ngOnInit(): void {
    this.createTable();
  }

  createTable() {
    this.columns = [
      { name: 'Nome Logradouro', sortable: true },
      { name: 'Numero', sortable: true },
      { name: 'Complemento', sortable: true },
      { name: 'Bairro', sortable: true },
      { name: 'Microarea', sortable: true }
    ];
  }

  limpar() {
    this.filter = new DomicilioFilter();
  }

  onEdit(data: any) {
    this.router.navigate(['cadastrodomiciliar/editar/',data.id]);
  }

  onDeleteConfirm(data: any) {
    this.domicilioSelected = data;
    this.modalExcluir.show();
  }

  delete() {
    this.isLoadingExcluir = true;

    this.cadastroDomiciliarService.delete(this.domicilioSelected.id).subscribe(
      response => this.search(),
      error => console.log('erro ao excluir atividade coletiva'),
      () => {
        this.modalExcluir.hide();
        this.isLoadingExcluir = false;
        this.domicilioSelected = null;
      }
    );
  }

  setPage(event) {
    this.page.pageNumber = event.offset;
    this.search();
  }

  onSort(event) {
    this.filter.column = event.sorts[0].prop;
    this.filter.sort = event.sorts[0].dir;
    this.search();
  }

  onSubmit() {
    this.page = new Page();
    this.filter.column = null;
    this.filter.sort = null;
    this.search();
  }

  search() {
    this.filter.pageNumber = this.page.pageNumber;
    this.filter.size = this.page.size;
    this.isLoading = true;

    this.cadastroDomiciliarService.search(this.filter)
      .pipe(finalize(() => this.isLoading = false))
      .subscribe({
        next: (response) => {
          this.page.content = response.content;
          this.page.totalElements = response.totalElements;
        },
        error: (err) => {
          console.log('erro ao pesquisar cadastro domiciliar');
          console.log(err);
        }
      });
  }

}
