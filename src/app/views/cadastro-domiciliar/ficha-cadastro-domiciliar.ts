import { Profissional } from '../../core/model/profissional';
import { UnidadeBasicaSaude } from '../../core/model/unidade-basica-saude';
import { Domicilio } from './domicilio';
import { formatDate } from '@angular/common';
import { Cbo } from '../../core/model/cbo';

export class FichaCadastroDomiciliar {
  uuid: string;
  profissionalResponsavel: Profissional = new Profissional();
  ubs: UnidadeBasicaSaude = new UnidadeBasicaSaude();
  cbo: Cbo = new Cbo();
  ine: string;
  domicilio: Domicilio = new Domicilio();
  uuidFichaOriginadora: string;
  fichaAtualizada: boolean = false;
  data: string = formatDate(new Date(), 'yyyy-MM-dd', 'en-US');
  statusTermoRecusa: boolean = false;
  importada: boolean = false;
}
