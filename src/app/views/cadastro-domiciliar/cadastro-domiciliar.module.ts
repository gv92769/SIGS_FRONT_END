import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdicionarCadastroDomiciliarComponent } from './adicionar-cadastro-domiciliar/adicionar-cadastro-domiciliar.component';
import { EditarCadastroDomiciliarComponent } from './editar-cadastro-domiciliar/editar-cadastro-domiciliar.component';
import { FormCadastroDomiciliarComponent } from './form-cadastro-domiciliar/form-cadastro-domiciliar.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxMaskModule, IConfig } from 'ngx-mask';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CustomComponentsModule } from '../../custom-components/custom-components.module';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { CadastroDomiciliarRoutingModule } from './cadastro-domiciliar-routing.module';
import { PesquisarCadastroDomiciliarComponent } from './pesquisar-cadastro-domiciliar/pesquisar-cadastro-domiciliar.component';
import { SelectDropDownModule } from 'ngx-select-dropdown'

export const options: Partial<IConfig> | (() => Partial<IConfig>) = { validation: false };

@NgModule({
  declarations: [
    AdicionarCadastroDomiciliarComponent,
    EditarCadastroDomiciliarComponent,
    FormCadastroDomiciliarComponent,
    PesquisarCadastroDomiciliarComponent
  ],
  imports: [
    CommonModule,
    CadastroDomiciliarRoutingModule,
    FormsModule,
    TabsModule,
    AccordionModule.forRoot(),
    CustomComponentsModule,
    ModalModule.forRoot(),
    ReactiveFormsModule,
    NgxMaskModule.forRoot(options),
    NgxDatatableModule,
    SelectDropDownModule
  ]
})
export class CadastroDomiciliarModule { }
