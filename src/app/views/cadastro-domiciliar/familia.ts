import { Cidadao } from '../cidadao/cidadao';

export class Familia {
  id: number;
  cidadao: Cidadao;
  numeroMembrosFamilia: number;
  numeroProntuario: string;
  codRendaFamiliar: number;
  resideDesde: string;
  stMudanca: boolean = false;
}
