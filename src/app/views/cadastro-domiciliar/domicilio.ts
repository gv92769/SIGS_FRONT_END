import { ObjectCodigoDescricao } from '../../core/model/object-codigo-descricao';
import { EnderecoLocalPermanencia } from './endereco-local-permanencia';
import { Familia } from './familia';
import { InstituicaoPermanencia } from './instituicao-permanencia';
import { CondicaoMoradia } from './condicao-moradia';

export class Domicilio {
  id: number;
  animaisDomicilio: ObjectCodigoDescricao[] = new Array;
  quantosAnimaisDomicilio: number;
  stAnimaisDomicilio: boolean;
  tipoImovel: ObjectCodigoDescricao = new ObjectCodigoDescricao();
  ficha: string;
  ultimaficha: string;
  endereco: EnderecoLocalPermanencia = new EnderecoLocalPermanencia();
  condicao: CondicaoMoradia = new CondicaoMoradia();
  familias: Familia[] = new Array;
  instituicao: InstituicaoPermanencia = new InstituicaoPermanencia();
}
