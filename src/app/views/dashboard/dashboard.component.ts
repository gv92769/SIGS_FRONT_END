import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, map} from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';

@Component({
  templateUrl: 'dashboard.component.html'
})
export class DashboardComponent implements OnInit {

  ncidadaosCadastrados:  number;
  ncidadaosComBiometria:  number;
  ncidadaosSemBiometria: number;
  navaliacoes: number;
  navaliacoesHoje: number;
  navaliacoes7dias: number;
  navaliacoes30dias: number;

  pAvaliacoesHoje: number;
  pAvaliacoes7dias: number;
  pAvaliacoes30dias: number;


  constructor(private http: HttpClient){

  }

  ngOnInit(): void {
    let url = environment.apiBaseUrl + '/estatisticas/inicio';
    this.http.get<any>(url).subscribe(
      data => {
        console.log(data);
        this.navaliacoes = data.navaliacoes;
        this.navaliacoesHoje = data.navaliacoesHoje;
        this.navaliacoes7dias = data.navaliacoes7dias;
        this.navaliacoes30dias = data.navaliacoes30dias;
        this.ncidadaosCadastrados = data.ncidadaos;
        this.ncidadaosComBiometria = data.ncidadaosComBiometria;
        this.ncidadaosSemBiometria = this.ncidadaosCadastrados - this.ncidadaosComBiometria;
        this.calcularDados();
      },
      error => console.log(error)
    );
  }

  calcularDados(): void {
    let totalRef = this.navaliacoes30dias + 10;
    this.pAvaliacoesHoje = this.navaliacoesHoje * 100.0 / totalRef;
    this.pAvaliacoes7dias = this.navaliacoes7dias * 100.0 / totalRef;
    this.pAvaliacoes30dias = this.navaliacoes30dias * 100.0 / totalRef;
  }
}
