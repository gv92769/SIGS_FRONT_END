import { Component, OnInit, ViewChild } from '@angular/core';
import { FormEvolucaoComponent } from '../form-evolucao/form-evolucao.component';
import { UsuarioService } from '../../usuario/usuario.service';
import { finalize } from 'rxjs/operators';
import { formatDate } from '@angular/common';
import { BehaviorSubject } from 'rxjs';
import { UnidadeBasicaSaudeService } from '../../../core/service/unidade-basica-saude.service';
import * as jwt_decode from 'jwt-decode';
import { ToastrService } from 'ngx-toastr';
import { ProfissionalService } from '../../../core/service/profissional.service';
import { CboService } from '../../../core/service/cbo.service';
import { Evolucao } from '../../../core/model/evolucao';
import { EvolucaoService } from '../../../core/service/evolucao.service';

@Component({
  selector: 'app-adicionar-evolucao',
  templateUrl: './adicionar-evolucao.component.html',
  styleUrls: ['./adicionar-evolucao.component.css']
})
export class AdicionarEvolucaoComponent implements OnInit {

  isLoadingSubmit: boolean = false;

  idCidadao: number;
  
  formData$: BehaviorSubject<Evolucao> = new BehaviorSubject<Evolucao>(new Evolucao());

  @ViewChild('formEvolucao', { static: false }) formEvolucaoComponent: FormEvolucaoComponent;

  constructor(private usuarioService: UsuarioService, private profissionalSerice: ProfissionalService, private toastrService: ToastrService,
    private evolucaoService: EvolucaoService, private unidadeSaudeBasicaService: UnidadeBasicaSaudeService,
    private cboService: CboService) {
  }

  ngOnInit() {
    this.fetchFormData();
  }

  private fetchFormData() {
    let evolucao: Evolucao = new Evolucao();

    evolucao.dataRealizacao = formatDate(new Date(), 'yyyy-MM-dd', 'en-US');

    evolucao.dataHorarioInicio = formatDate(new Date(), "yyyy-MM-dd'T'HH:mm:ss", 'en-US');
    
    this.usuarioService.getCurrentUserInfo().subscribe(
      data => {
        this.profissionalSerice.findById(data.id).subscribe(
          profissional => evolucao.profissional = profissional,
          error => console.log(error)
        );
      },
      error => console.log(error)
    );

    this.unidadeSaudeBasicaService.findByCnes(jwt_decode(sessionStorage.getItem('access_token')).cnes)
      .subscribe(ubs => evolucao.ubs = ubs)

    this.cboService.findByCodigo(jwt_decode(sessionStorage.getItem('access_token')).cbo)
      .subscribe(cbo => evolucao.cbo = cbo);

    this.formData$.next(evolucao);
  }

  onSubmitted(evolucao: Evolucao) {
    this.isLoadingSubmit = true;
    this.evolucaoService.create(evolucao)
      .pipe(finalize(() => this.isLoadingSubmit = false))
      .subscribe({
        next: (response) => {
          this.toastrService.success('Evolução salva com sucesso!', 'Sucesso');
          this.ngOnInit();
        },
        error: (err) => {
          this.toastrService.error('Error ao salvar evolução.', 'Error');
          console.log(err);
        }
      });
  }

  onCanceled(){
    console.log('Cancelar');
  }

}

