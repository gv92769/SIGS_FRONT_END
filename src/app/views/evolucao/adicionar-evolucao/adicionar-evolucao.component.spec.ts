import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdicionarEvolucaoComponent } from './adicionar-evolucao.component';

describe('AdicionarEvolucaoComponent', () => {
  let component: AdicionarEvolucaoComponent;
  let fixture: ComponentFixture<AdicionarEvolucaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdicionarEvolucaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdicionarEvolucaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
