import { Evolucao } from '../../core/model/evolucao';

export class FichaEvolucao {
  uuid: string;
  evolucao: Evolucao;
  importada: boolean;
}
