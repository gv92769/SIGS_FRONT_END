import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PesquisarEvolucaoComponent } from './pesquisar-evolucao/pesquisar-evolucao.component';
import { AdicionarEvolucaoComponent } from './adicionar-evolucao/adicionar-evolucao.component';
import { EditarEvolucaoComponent } from './editar-evolucao/editar-evolucao.component';
import { VisualizarEvolucaoComponent } from './visualizar-evolucao/visualizar-evolucao.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Evolução'
    },
    children: [
      {
        path: 'pesquisar',
        component: PesquisarEvolucaoComponent,
        data: {
          title: 'Pesquisar'
        }
      },
      {
        path: 'adicionar',
        component: AdicionarEvolucaoComponent,
        data: {
          title: 'Adicionar'
        }
      },
      {
        path: 'visualizar/:id',
        component: VisualizarEvolucaoComponent,
        data: {
          title: 'Visualizar'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EvolucaoRoutingModule { }
