import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { CustomComponentsModule } from '../../custom-components/custom-components.module';
import { ModalModule } from 'ngx-bootstrap/modal';

import { PesquisarCidadaoBiometriaModule } from '../cidadao/pesquisar-cidadao-biometria/pesquisar-cidadao-biometria.module';

import { ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule, IConfig } from 'ngx-mask';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SelectDropDownModule } from 'ngx-select-dropdown';
import { ExameModule } from '../exame/exame.module';
import { AdicionarEvolucaoComponent } from './adicionar-evolucao/adicionar-evolucao.component';
import { EditarEvolucaoComponent } from './editar-evolucao/editar-evolucao.component';
import { FormEvolucaoComponent } from './form-evolucao/form-evolucao.component';
import { PesquisarEvolucaoComponent } from './pesquisar-evolucao/pesquisar-evolucao.component';
import { EvolucaoRoutingModule } from './evolucao-routing.module';
import { VisualizarEvolucaoComponent } from './visualizar-evolucao/visualizar-evolucao.component';

export const options: Partial<IConfig> | (() => Partial<IConfig>) = { validation: false };

@NgModule({
  declarations: [
    AdicionarEvolucaoComponent, 
    EditarEvolucaoComponent, 
    PesquisarEvolucaoComponent, 
    FormEvolucaoComponent, 
    VisualizarEvolucaoComponent
  ],
  imports: [
    CommonModule,
    EvolucaoRoutingModule,
    FormsModule,
    TabsModule,
    AccordionModule.forRoot(),
    CustomComponentsModule,
    ModalModule.forRoot(),
    PesquisarCidadaoBiometriaModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot(options), 
    NgxDatatableModule,
    SelectDropDownModule,
    ExameModule
  ]
})
export class EvolucaoModule { }
