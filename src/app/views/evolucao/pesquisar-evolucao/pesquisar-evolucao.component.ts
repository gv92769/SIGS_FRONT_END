import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AvaliacaoMedicaService } from '../../../core/service/avaliacao-medica.service';
import { CboService } from '../../../core/service/cbo.service';
import { EvolucaoService } from '../../../core/service/evolucao.service';
import { PerfilService } from '../../../core/service/perfil.service';
import { CidadaoService } from '../../cidadao/cidadao.service';
import { PesquisaFichaComponent } from '../../pesquisa-ficha/pesquisa-ficha.component';
import { UsuarioService } from '../../usuario/usuario.service';

@Component({
  selector: 'app-pesquisar-evolucao',
  templateUrl: '../../pesquisa-ficha/pesquisa-ficha.component.html',
})
export class PesquisarEvolucaoComponent extends PesquisaFichaComponent implements OnInit {

  constructor(
    private router: Router,
    private avaliacaoMedicaService: AvaliacaoMedicaService,
    private evolucaoService: EvolucaoService,
    usuarioService: UsuarioService,
    cidadaoService: CidadaoService,
    cboService: CboService,
    perfilService: PerfilService,
    http: HttpClient
    ) { super(usuarioService, cidadaoService, cboService, perfilService, http); }

  ngOnInit(): void {
    super.ngOnInit();

    this.createTable();

    this.titulo = 'Pesquisar Evolução';

    this.tituloExcluir = "Evolução";

    this.textoExcluir = " a Evolução";

    this.exportar = true;
  }

  createTable() {
    this.columns = [
      { name: 'Data', sortable: true },
      { name: 'Horario', sortable: true },
      { name: 'Profissional', sortable: false },
      { name: 'Paciente', sortable: false },
    ];
  }

  onEditarClick(id: number){
   this.router.navigate(['/evolucao/visualizar/',id]);
  }

  delete() {
    this.isLoadingExcluir = true;

    this.avaliacaoMedicaService.delete(this.excluirId).subscribe(
      response => this.search(),
      error => console.log('erro ao excluir evolucao'),
      () => {
        this.modalExcluir.hide();
        this.isLoadingExcluir = false;
        this.excluirId = null;
      }
    );
  }

  setPage(event) {
    this.page.pageNumber = event.offset;
    this.search();
  }

  onSort(event) {
    this.getSort(event);
    this.search();
  }

  onSubmit() {
    this.getSumit()
    this.search();
  }

  search() {
    this.getSearch()

    if(Array.isArray(this.filter.cbo))
      this.filter.cbo = null;

    if(Array.isArray(this.filter.perfil))
      this.filter.perfil = null;

    this.evolucaoService.search(this.filter)
      .subscribe({
        next: (response) => {
          this.page.content = response.content;
          this.page.totalElements = response.totalElements;
          this.isLoading = false;
        },
        error: (err) => {
          console.log('erro ao pesquisar evolução');
          console.log(err);
          this.isLoading = false;
        }
      });
  }

}
