import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizarEvolucaoComponent } from './visualizar-evolucao.component';

describe('VisualizarEvolucaoComponent', () => {
  let component: VisualizarEvolucaoComponent;
  let fixture: ComponentFixture<VisualizarEvolucaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualizarEvolucaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizarEvolucaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
