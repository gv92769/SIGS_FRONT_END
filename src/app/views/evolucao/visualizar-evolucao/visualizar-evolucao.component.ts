import { Component, OnInit, ViewChild } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { FormEvolucaoComponent } from '../form-evolucao/form-evolucao.component';
import { ActivatedRoute, Router } from '@angular/router';
import { map, mergeMap, finalize } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { FichaEvolucao } from '../ficha-evolucao';
import { UsuarioService } from '../../usuario/usuario.service';
import { Evolucao } from '../../../core/model/evolucao';
import { EvolucaoService } from '../../../core/service/evolucao.service';

@Component({
  selector: 'app-visualizar-evolucao',
  templateUrl: './visualizar-evolucao.component.html',
  styleUrls: ['./visualizar-evolucao.component.css']
})
export class VisualizarEvolucaoComponent implements OnInit {

  isLoadingSubmit: boolean = false;

  id: number;

  formData$: BehaviorSubject<Evolucao> = new BehaviorSubject<Evolucao>(new Evolucao());

  @ViewChild('formEvolucao', { static: false }) formEvolucao: FormEvolucaoComponent;

  fichaEvolucao: FichaEvolucao = new FichaEvolucao;

  profissional: boolean = false;

  constructor(private route: ActivatedRoute, private evolucaoService: EvolucaoService, private toastrService: ToastrService,
    private router: Router, private usuarioService: UsuarioService) { }

  ngOnInit() {
    this.fetchFormData();
  }

  private fetchFormData() {
    this.route.paramMap.pipe(
      map(params => {
        // (+) before `params.get()` turns the string into a number
        const id = +params.get('id');
        this.id = id;
        return id;
      }),
      mergeMap(id => {
        return this.evolucaoService.findbyId(id);
      }
      )
    )
      .subscribe({
        next: (response) => {
          this.fichaEvolucao = response;
          this.usuarioService.getCurrentUserInfo().subscribe(
            data => this.profissional = data.id == response.evolucao.profissional.id ? false : true,
            error => console.log(error)
          );
          this.formData$.next(response.evolucao);
        },
        error: (err) => {
          console.log('Erro ao busca Evolucao');
          console.log(err);
        }
      });
  }

  onSubmitted(evolucao: Evolucao) {
    if (this.fichaEvolucao.importada == false) {
      this.isLoadingSubmit = true;
      
      this.evolucaoService.update(this.id, evolucao)
        .pipe(finalize(() => this.isLoadingSubmit = false))
        .subscribe({
          next: (response) => {
            this.toastrService.success('Evolucao salvo com sucesso!', 'Sucesso');
            this.router.navigate(['evolucao/adicionar']);
          },
          error: (err) => {
            this.toastrService.error('Error ao salvar evolução.', 'Error');
            console.log(err);
          }
        });
    }
  }

  onCanceled() {
    console.log('Cancelar');
  }

}
