import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormEvolucaoComponent } from './form-evolucao.component';

describe('FormEvolucaoComponent', () => {
  let component: FormEvolucaoComponent;
  let fixture: ComponentFixture<FormEvolucaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormEvolucaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormEvolucaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
