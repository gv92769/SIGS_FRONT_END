import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { formatDate } from '@angular/common';
import { Cid10Service } from '../../../core/service/cid10.service';
import { Ciap2Service } from '../../../core/service/ciap2.service';
import { Observable } from 'rxjs';
import { LocalAtendimentoService } from '../../../core/service/local-atendimento.service';
import { ObjectCodigoDescricao } from '../../../core/model/object-codigo-descricao';
import { TipoAtendimentoService } from '../../../core/service/tipo-atendimento.service';
import { AleitamentoMaternoService } from '../../atendimento-individual/aleitamento-materno.service';
import { CiapCondicaoAvaliadaService } from '../../atendimento-individual/ciap-condicao-avaliada.service';
import { CiapCondicaoAvaliada } from '../../atendimento-individual/ciap-condicao-avaliada';
import { NasfService } from '../../atendimento-individual/nasf.service';
import { RacionalidadeSaudeService } from '../../atendimento-individual/racionalidade-saude.service';
import { CondutaEncaminhamentoService } from '../../atendimento-individual/conduta-encaminhamento.service';
import { ListaExamesService } from '../../atendimento-individual/lista-exames.service';
import { ListaExames } from '../../atendimento-individual/lista-exames';
import { Exame } from '../../atendimento-individual/exame';
import { Ciap2 } from '../../../core/model/ciap2';
import { Cid10 } from '../../../core/model/cid10';
import { CidadaoService } from '../../cidadao/cidadao.service';
import { Cidadao } from '../../cidadao/cidadao';
import { Config } from '../../../core/model/config';
import { Procedimento } from '../../../core/model/procedimento';
import { ProcedimentoService } from '../../../core/service/procedimento.service';
import { UsuarioService } from '../../usuario/usuario.service';
import { Evolucao } from '../../../core/model/evolucao';
import { EvolucaoService } from '../../../core/service/evolucao.service';

@Component({
  selector: 'app-form-evolucao',
  templateUrl: './form-evolucao.component.html',
  styleUrls: ['./form-evolucao.component.css']
})
export class FormEvolucaoComponent implements OnInit {

  @ViewChild('form', { static: false }) form: FormGroup;

  @Input('isVisualizacao') isVisualizacao = null;

  evolucao: Evolucao = new Evolucao();

  idCidadao: number;

  @Input('formData') formData$: Observable<Evolucao>;

  @Output('onSubmit') onSubmitEventEmitter = new EventEmitter();

  locais: ObjectCodigoDescricao[] = new Array;

  tiposAtendimento: ObjectCodigoDescricao[] = new Array;

  aleitamento: ObjectCodigoDescricao[] = new Array;

  ciaps: CiapCondicaoAvaliada[] = new Array;

  nasfs: ObjectCodigoDescricao[] = new Array;

  opRacionalidade: ObjectCodigoDescricao[] = new Array;

  condutas: ObjectCodigoDescricao[] = new Array;

  encaminhamentos: ObjectCodigoDescricao[] = new Array;

  exames: ListaExames[] = new Array;

  opNeonatal: ListaExames[] = new Array;

  ciapTransmissiveis: CiapCondicaoAvaliada[] = new Array;

  ciapRastreamento: CiapCondicaoAvaliada[] = new Array;

  optionsCiap: Ciap2[] = new Array;

  config: Config = new Config();

  selectedCiap: Ciap2[] = new Array;

  optionsCid: Cid10[] = new Array;

  selectedCid: Cid10[] = new Array;

  opCidadao: Cidadao[] = new Array;

  configCidadao: Config = new Config();

  optionsProced: Procedimento[] = new Array;

  configExame: Config = new Config();

  configProcedimentos: Config = new Config();

  importada: boolean = false;

  profissional: boolean = false;

  procedimentos: Procedimento[] = new Array;

  constructor(private cid10Service: Cid10Service, private ciap2Service: Ciap2Service, private nasfService: NasfService,
    private localAtendimentoService: LocalAtendimentoService, private tipoAtendimentoService: TipoAtendimentoService,
    private aleitamentoMaternoService: AleitamentoMaternoService, private ciapCondicaoService: CiapCondicaoAvaliadaService,
    private racionalidadeService: RacionalidadeSaudeService, private condutaService: CondutaEncaminhamentoService,
    private exameService: ListaExamesService, private cidadaoService: CidadaoService, private procedimentoService: ProcedimentoService,
    private evolucaoService: EvolucaoService, private usuarioService: UsuarioService) {
  }

  ngOnInit() {
    this.fillForm();

    this.localAtendimentoService.findAll()
      .subscribe(data => this.locais = data);

    let ids = [1, 2, 3, 4, 5, 6];
    this.tipoAtendimentoService.findAllById(ids)
      .subscribe(data => this.tiposAtendimento = data);

    this.aleitamentoMaternoService.findAll()
      .subscribe(data => this.aleitamento = data);

    this.nasfService.findAll()
      .subscribe(data => this.nasfs = data);

    this.racionalidadeService.findAll()
      .subscribe(data => this.opRacionalidade = data);

    ids = [1, 2, 12, 3, 9];
    this.condutaService.findAllById(ids)
      .subscribe(data => this.condutas = data);

    ids = [11, 4, 5, 6, 7, 8, 10];
    this.condutaService.findAllById(ids)
      .subscribe(data => this.encaminhamentos = data);

    ids = [1, 3, 4, 5, 8, 9, 10, 11, 12, 16, 17, 18, 19, 20, 21, 22];
    this.ciapCondicaoService.findAllById(ids)
      .subscribe(data => this.ciaps = data);

    ids = [20, 6, 2, 7];
    this.ciapCondicaoService.findAllById(ids)
      .subscribe(data => this.ciapTransmissiveis = data);

    ids = [13, 14, 15];
    this.ciapCondicaoService.findAllById(ids)
      .subscribe(data => this.ciapRastreamento = data);

    this.config.placeholder = 'Selecione até 2 opções';
    this.config.displayKey = 'descricao';
    this.config.searchOnKey = 'descricao';

    this.configProcedimentos.displayKey = 'descricao';
    this.configProcedimentos.placeholder = 'Escolha um ou mais Procedimentos';
    this.configProcedimentos.searchOnKey = 'descricao';

    this.configCidadao.displayKey = 'nome';
    this.configCidadao.placeholder = 'Digite o CPF/CNS ou Nome';

    this.configExame.placeholder = 'Selecione o procedimento';
    this.configExame.displayKey = 'descricao';
    this.configExame.searchOnKey = 'descricao';
  }

  private fillForm() {
    this.formData$.subscribe({
      next: (value) => {
        this.evolucao = value;
        this.verificarCampos();
        if (value.id != null) {
          this.usuarioService.getCurrentUserInfo().subscribe(
            data => this.profissional = data.id == value.profissional.id ? false : true,
            error => console.log(error)
          );

          this.evolucaoService.importada(value.id)
            .subscribe(
              data => this.importada = data,
              error => console.log(error)
            );
        }
      },
      error: (err) => {
        console.log('erro behavior subject');
        console.log(err);
      }
    });
  }

  verificarCampos() {
    if (this.evolucao.ciap2_1 != null) {
      this.evolucao.ciap2_1.descricao = this.evolucao.ciap2_1.codigo + " - " + this.evolucao.ciap2_1.descricao;
      this.selectedCiap.push(this.evolucao.ciap2_1);
    }

    if (this.evolucao.ciap2_2 != null) {
      this.evolucao.ciap2_2.descricao = this.evolucao.ciap2_2.codigo + " - " + this.evolucao.ciap2_2.descricao;
      this.selectedCiap.push(this.evolucao.ciap2_2);
    }

    if (this.evolucao.cid10_1 != null) {
      this.evolucao.cid10_1.descricao = this.evolucao.cid10_1.codigo + " - " + this.evolucao.cid10_1.descricao;
      this.selectedCid.push(this.evolucao.cid10_1);
    }

    if (this.evolucao.cid10_2 != null) {
      this.evolucao.cid10_2.descricao = this.evolucao.cid10_2.codigo + " - " + this.evolucao.cid10_2.descricao;
      this.selectedCid.push(this.evolucao.cid10_2);
    }


    this.evolucao.aleitamentoMaterno = this.evolucao.aleitamentoMaterno == null ? new ObjectCodigoDescricao() : this.evolucao.aleitamentoMaterno;
    this.evolucao.ciaps = this.evolucao.ciaps == null ? new Array : this.evolucao.ciaps;
    this.evolucao.nasfs = this.evolucao.nasfs == null ? new Array : this.evolucao.nasfs;
    this.evolucao.racionalidadeSaude = this.evolucao.racionalidadeSaude == null ? new ObjectCodigoDescricao() : this.evolucao.racionalidadeSaude;

    let ids = [18, 20, 21];

  }

  onFormSubmit() {
    if (this.form.valid && this.selectedCiap.length <= 2 && this.selectedCid.length <= 2 && this.evolucao.condutas.length > 0) {

      this.evolucao.dataHorarioFinal = formatDate(new Date(), "yyyy-MM-dd'T'HH:mm:ss", 'en-US');

      if (this.selectedCiap.length > 0) {
        if (this.selectedCiap.length > 1)
          this.evolucao.ciap2_2 = this.selectedCiap[1];
        this.evolucao.ciap2_1 = this.selectedCiap[0];
      }

      if (this.selectedCid.length > 0) {
        if (this.selectedCid.length > 1)
          this.evolucao.cid10_2 = this.selectedCid[1];
        this.evolucao.cid10_1 = this.selectedCid[0];
      }

      this.selectedCid = new Array;
      this.selectedCiap = new Array;

      this.onSubmitEventEmitter.emit(this.evolucao);
    }
  }

  buscarCidadao(valor: string) {
    if (valor != null && valor != '')
      this.cidadaoService.filtro(valor)
        .subscribe({
          next: (data) => {
            this.opCidadao = data;
          },
          error: (err) => {
            console.log(err);
          }
        });
  }

  searchChangeExame(texto: string) {
    let procedimentoFiltro = {
      texto: texto,
      sexo: this.evolucao.cidadao.sexo == 'MASCULINO' ? true : false
    }

    this.procedimentoService.searchExame(procedimentoFiltro)
      .subscribe(
        data => this.optionsProced = data,
        error => console.log(error)
      );
  }

  searchCiap2(valor: string) {
    if (valor != null && valor != '')
      this.ciap2Service.search(valor, this.evolucao.cidadao.sexo == 'FEMININO' ? 1 : 0)
        .subscribe(
          data => this.optionsCiap = data,
          error => console.log("Error ao Buscar Ciap2", error)
        );
  }

  searchCid10(valor: string) {
    if (valor != null && valor != '')
      this.cid10Service.search(valor, this.evolucao.cidadao.sexo == 'FEMININO' ? 1 : 0)
        .subscribe(
          data => this.optionsCid = data,
          error => console.log("Error ao buscar Cid10", error)
        );
  }

  nasfChange(nasf: ObjectCodigoDescricao) {
    let index = this.evolucao.nasfs.findIndex(x => x.codigo == nasf.codigo)
    if (index == -1) {
      this.evolucao.nasfs.push(nasf);
    }
    else {
      this.evolucao.nasfs.splice(index, 1);
    }
  }

  nasfChecked(nasf: ObjectCodigoDescricao) {
    if (this.evolucao.nasfs.findIndex(x => x.codigo == nasf.codigo) != -1)
      return true;

    return false;
  }

  condutasEncaminhamentoChange(op: ObjectCodigoDescricao) {
    let index = this.evolucao.condutas.findIndex(x => x.codigo == op.codigo)
    if (index == -1) {
      this.evolucao.condutas.push(op);
    }
    else {
      this.evolucao.condutas.splice(index, 1);
    }
  }

  condutasEncaminhamentoChecked(op: ObjectCodigoDescricao) {
    if (this.evolucao.condutas.findIndex(x => x.codigo == op.codigo) != -1)
      return true;

    return false;
  }

  ciapChange(op: CiapCondicaoAvaliada) {
    let index = this.evolucao.ciaps.findIndex(x => x.id == op.id);
    if (index == -1) {
      this.evolucao.ciaps.push(op);
    }
    else {
      this.evolucao.ciaps.splice(index, 1);
    }
  }

  ciapChecked(op: CiapCondicaoAvaliada) {
    if (this.evolucao.ciaps.findIndex(x => x.id == op.id) != -1)
      return true;

    return false;
  }

  searchProcedimentoChange(texto: string) {
    let procedimentoFiltro = {
      grupo: {},
      subgrupo: {},
      forma: {},
      texto: texto,
      sexo: this.evolucao.cidadao.sexo == 'MASCULINO' ? true : false
    }

    this.procedimentoService.search(procedimentoFiltro)
      .subscribe(
        data => this.procedimentos = data,
        error => console.log("error in search Procedimento()"+error)
      );
  }

  limitarQuantidadeProcedimentos() {
    return this.evolucao.procedimentos.length >= 20 ? true : false;
  }
}
