import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarEvolucaoComponent } from './editar-evolucao.component';

describe('EditarEvolucaoComponent', () => {
  let component: EditarEvolucaoComponent;
  let fixture: ComponentFixture<EditarEvolucaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarEvolucaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarEvolucaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
