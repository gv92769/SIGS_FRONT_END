import { TestBed } from '@angular/core/testing';

import { DesfechoService } from './desfecho.service';

describe('DesfechoService', () => {
  let service: DesfechoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DesfechoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
