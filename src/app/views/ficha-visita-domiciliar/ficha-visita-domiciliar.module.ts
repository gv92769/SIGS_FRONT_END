import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FichaVisitaDomiciliarRoutingModule } from './ficha-visita-domiciliar-routing.module';
import { PesquisarFichaVisitaDomiciliarComponent } from './pesquisar-ficha-visita-domiciliar/pesquisar-ficha-visita-domiciliar.component';
import { FormFichaVisitaDomiciliarComponent } from './form-ficha-visita-domiciliar/form-ficha-visita-domiciliar.component';
import { EditarFichaVisitaDomiciliarComponent } from './editar-ficha-visita-domiciliar/editar-ficha-visita-domiciliar.component';
import { AdicionarFichaVisitaDomiciliarComponent } from './adicionar-ficha-visita-domiciliar/adicionar-ficha-visita-domiciliar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { CustomComponentsModule } from '../../custom-components/custom-components.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NgxMaskModule } from 'ngx-mask';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SelectDropDownModule } from 'ngx-select-dropdown';
import { options } from '../cadastro-domiciliar/cadastro-domiciliar.module';


@NgModule({
  declarations: [
    AdicionarFichaVisitaDomiciliarComponent,
    EditarFichaVisitaDomiciliarComponent,
    FormFichaVisitaDomiciliarComponent,
    PesquisarFichaVisitaDomiciliarComponent
  ],
  imports: [
    FormsModule,
    TabsModule,
    AccordionModule.forRoot(),
    CustomComponentsModule,
    ModalModule.forRoot(),
    ReactiveFormsModule,
    NgxMaskModule.forRoot(options), 
    NgxDatatableModule,
    SelectDropDownModule,
    CommonModule,
    FichaVisitaDomiciliarRoutingModule
  ]
})
export class FichaVisitaDomiciliarModule { }
