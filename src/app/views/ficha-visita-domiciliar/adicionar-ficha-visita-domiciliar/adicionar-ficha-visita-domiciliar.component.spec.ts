import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdicionarFichaVisitaDomiciliarComponent } from './adicionar-ficha-visita-domiciliar.component';

describe('AdicionarFichaVisitaDomiciliarComponent', () => {
  let component: AdicionarFichaVisitaDomiciliarComponent;
  let fixture: ComponentFixture<AdicionarFichaVisitaDomiciliarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdicionarFichaVisitaDomiciliarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdicionarFichaVisitaDomiciliarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
