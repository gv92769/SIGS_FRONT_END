import { Component, OnInit, ViewChild } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { FichaVisitaDomiciliar } from '../../../core/model/ficha-visita-domiciliar';
import { CboService } from '../../../core/service/cbo.service';
import { ProfissionalService } from '../../../core/service/profissional.service';
import { UnidadeBasicaSaudeService } from '../../../core/service/unidade-basica-saude.service';
import { UsuarioService } from '../../usuario/usuario.service';
import * as jwt_decode from 'jwt-decode';
import { ToastrService } from 'ngx-toastr';
import { FormFichaVisitaDomiciliarComponent } from '../form-ficha-visita-domiciliar/form-ficha-visita-domiciliar.component';
import { finalize } from 'rxjs/operators';
import { FichaVisitaDomiciliarService } from '../ficha-visita-domiciliar.service';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-adicionar-ficha-visita-domiciliar',
  templateUrl: './adicionar-ficha-visita-domiciliar.component.html',
  styleUrls: ['./adicionar-ficha-visita-domiciliar.component.css']
})
export class AdicionarFichaVisitaDomiciliarComponent implements OnInit {
  
  isLoadingSubmit: boolean = false;

  formData$: BehaviorSubject<FichaVisitaDomiciliar> = new BehaviorSubject<FichaVisitaDomiciliar>(new FichaVisitaDomiciliar());

  @ViewChild('formVisitaDomiciliar', { static: false }) formVisitaDomiciliar: FormFichaVisitaDomiciliarComponent;

  constructor(
    private usuarioService: UsuarioService, 
    private profissionalSerice: ProfissionalService, 
    private toastrService: ToastrService,
    private unidadeSaudeBasicaService: UnidadeBasicaSaudeService, 
    private cboService: CboService, 
    private fichaVisitaDomiciliarService: FichaVisitaDomiciliarService
  ) { }

  ngOnInit(): void {
    this.fetchFormData();
  }

  private fetchFormData() {

    let fichaVisitaDomiciliar: FichaVisitaDomiciliar = new FichaVisitaDomiciliar();

    fichaVisitaDomiciliar.data = formatDate(new Date(), 'yyyy-MM-dd', 'en-BR');

    let ficha: FichaVisitaDomiciliar = new FichaVisitaDomiciliar();

    this.usuarioService.getCurrentUserInfo().subscribe(
      data => {
        this.profissionalSerice.findById(data.id).subscribe(
          profissional => ficha.profissionalResponsavel = profissional,
          error => console.log(error)
        );
      },
      error => console.log(error)
    );

    this.unidadeSaudeBasicaService.findByCnes(jwt_decode(sessionStorage.getItem('access_token')).cnes)
      .subscribe(ubs => ficha.ubs = ubs)

    this.cboService.findByCodigo(jwt_decode(sessionStorage.getItem('access_token')).cbo)
      .subscribe(cbo => ficha.cbo = cbo);

    this.formData$.next(ficha);

  }

  onSubmitted(ficha: FichaVisitaDomiciliar) {
    this.isLoadingSubmit = true;
    this.fichaVisitaDomiciliarService.create(ficha)
      .pipe(finalize(() => this.isLoadingSubmit = false))
      .subscribe({
        next: (response) => {
          this.toastrService.success('Ficha de Visita Domiciliar salvo com sucesso!', 'Sucesso');
          this.ngOnInit();
        },
        error: (err) => {
          this.toastrService.error('Error ao salvar Ficha de Visita Domiciliar.', 'Error');
          console.log(err);
        }
      });
  }

  onCanceled(){
    console.log('Cancelar');
  }
  

}
