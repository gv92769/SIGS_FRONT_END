import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CboService } from '../../../core/service/cbo.service';
import { PerfilService } from '../../../core/service/perfil.service';
import { CidadaoService } from '../../cidadao/cidadao.service';
import { PesquisaFichaComponent } from '../../pesquisa-ficha/pesquisa-ficha.component';
import { UsuarioService } from '../../usuario/usuario.service';
import { FichaVisitaDomiciliarService } from '../ficha-visita-domiciliar.service';

@Component({
  selector: 'app-pesquisar-ficha-visita-domiciliar',
  templateUrl: '../../pesquisa-ficha/pesquisa-ficha.component.html',
})

export class PesquisarFichaVisitaDomiciliarComponent extends PesquisaFichaComponent implements OnInit {

  constructor (
    private router: Router,
    private fichaVisitaDomiciliarService: FichaVisitaDomiciliarService,
    usuarioService: UsuarioService,
    cidadaoService: CidadaoService,
    cboService: CboService,
    perfilService: PerfilService,
    http: HttpClient
    ) {  super(usuarioService, cidadaoService, cboService, perfilService, http); }

  ngOnInit(): void {
    super.ngOnInit();

    this.createTable();

    this.titulo = 'Pesquisar Ficha de Visita Domiciliar';

    this.tituloExcluir = "Ficha de Visita Domiciliar";

    this.textoExcluir = " o Ficha de Visita Domiciliar";

    // this.exportar = true;
  }

  createTable() {
    this.columns = [
      { name: 'Data', sortable: true },
      { name: 'Profissional', sortable: false },
      { name: 'Cidadao', sortable: true }
    ];
  }

  onEditarClick(id: number){
   this.router.navigate(['/fichavisitadomiciliar/editar/',id]);
  }

  delete() {
    this.isLoadingExcluir = true;

    this.fichaVisitaDomiciliarService.delete(this.excluirId).subscribe(
      response => this.search(),
      error => console.log('erro ao excluir Ficha de Visita Domiciliar'),
      () => {
        this.modalExcluir.hide();
        this.isLoadingExcluir = false;
        this.excluirId = null;
      }
    );
  }

  setPage(event) {
    this.page.pageNumber = event.offset;
    this.search();
  }

  onSort(event) {
    this.getSort(event);
    this.search();
  }

  onSubmit() {
    this.getSumit()
    this.search();
  }

  search() {
    this.getSearch()

    this.fichaVisitaDomiciliarService.search(this.filter)
      .subscribe({
        next: (response) => {
          this.page.content = response.content;
          this.page.totalElements = response.totalElements;
          this.isLoading = false;
        },
        error: (err) => {
          console.log('erro ao pesquisar Ficha de Visita Domiciliar');
          console.log(err);
          this.isLoading = false;
        }
      });
  }
}
