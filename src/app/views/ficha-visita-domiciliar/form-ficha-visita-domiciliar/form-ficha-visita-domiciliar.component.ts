import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { Config } from '../../../core/model/config';
import { FichaVisitaDomiciliar } from '../../../core/model/ficha-visita-domiciliar';
import { ObjectCodigoDescricao } from '../../../core/model/object-codigo-descricao';
import { Cidadao } from '../../cidadao/cidadao';
import { CidadaoService } from '../../cidadao/cidadao.service';
import { UsuarioService } from '../../usuario/usuario.service';
import { DesfechoService } from '../desfecho.service';
import { FichaVisitaDomiciliarService } from '../ficha-visita-domiciliar.service';
import { MotivoVisitaService } from '../motivo-visita.service';

@Component({
  selector: 'app-form-ficha-visita-domiciliar',
  templateUrl: './form-ficha-visita-domiciliar.component.html',
  styleUrls: ['./form-ficha-visita-domiciliar.component.css']
})
export class FormFichaVisitaDomiciliarComponent implements OnInit {

  @ViewChild('form', { static: false }) form: FormGroup;

  idCidadao: number;

  opCidadao: Cidadao[] = new Array;

  configCidadao: Config = new Config();

  ficha: FichaVisitaDomiciliar = new FichaVisitaDomiciliar();

  motivos: ObjectCodigoDescricao[] = new Array;

  desfechos: ObjectCodigoDescricao[] = new Array;

  motivosBuscaAtv: ObjectCodigoDescricao[] = new Array;

  motivosAcompanhamento: ObjectCodigoDescricao[] = new Array;

  @Input('formData') formData$: Observable<FichaVisitaDomiciliar>;

  @Output('onSubmit') onSubmitEventEmitter = new EventEmitter();

  importada: boolean = false;

  permiteMotivo: boolean = true;

  profissional: boolean = false;

  constructor(
    private motivoService: MotivoVisitaService,
    private desfechoService: DesfechoService,
    private usuarioService: UsuarioService,
    private cidadaoService: CidadaoService,
    private fichaVisitaDomiciliarService: FichaVisitaDomiciliarService
    ) {}

  ngOnInit(): void {
    this.fillForm();

    let ids = [1, 29];
    this.motivoService.findAllById(ids)
      .subscribe(data => this.motivos = data);

    ids = [2, 3, 4, 30];
    this.motivoService.findAllById(ids)
      .subscribe(data => this.motivosBuscaAtv = data);

    ids = [5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 32, 33, 19, 20, 21, 22, 23, 24, 25, 26, 27, 31, 28];
    this.motivoService.findAllById(ids)
      .subscribe(data => this.motivosAcompanhamento = data);

    this.desfechoService.findAll().subscribe(data => this.desfechos = data);
    this.configCidadao.displayKey = 'nome';
    this.configCidadao.placeholder = 'Digite o CPF/CNS';
  }

  private fillForm() {
    console.log(this.formData$);
    this.formData$.subscribe({
      next: (value) => {
        this.ficha = value;
        // this.verificarCampos();
        if (value.id != null) {
          this.usuarioService.getCurrentUserInfo().subscribe(
            data => this.profissional = data.id == value.profissionalResponsavel.id ? false : true,
            error => console.log(error)
          );

          this.fichaVisitaDomiciliarService.importada(value.id)
            .subscribe(
              data => this.importada = data,
              error => console.log(error)
            );
        }
      },
      error: (err) => {
        console.log('erro behavior subject');
        console.log(err);
      }
    });
  }

  desfechoChange(desfecho: ObjectCodigoDescricao) {
    this.ficha.desfecho = desfecho;
    if (desfecho.codigo == 2 || this.ficha.motivos == new Array(desfecho.codigo)) {
      this.permiteMotivo == false;
    }
    else {
      this.permiteMotivo == true;
    }

  }

  desfechoChecked(desfecho: ObjectCodigoDescricao) {
    if (this.ficha.desfecho.codigo == desfecho.codigo) {

      if (desfecho.codigo == 2 || this.ficha.motivos == new Array(desfecho.codigo)) {
        this.permiteMotivo == false;
      }
      else {
        this.permiteMotivo == true;
      }

      return true;
    }

    return false;
  }

  motivoChange(motivo: ObjectCodigoDescricao) {
    let index = this.ficha.motivos.findIndex(x => x.codigo == motivo.codigo)
    if (index == -1) {
      this.ficha.motivos.push(motivo);
      console.log(this.ficha.motivos)
    }
    else {
      this.ficha.motivos.splice(index, 1);
      console.log(this.ficha.motivos)
    }
  }

  motivoChecked(motivo: ObjectCodigoDescricao) {
    if (this.ficha.motivos.findIndex(x => x.codigo == motivo.codigo) != -1)
      return true;

    return false;
  }

  buscarCidadao(valor: string) {
    if (valor != null && valor != '')
      this.cidadaoService.filtro(valor)
        .subscribe({
          next: (data) => {
            this.opCidadao = data;
          },
          error: (err) => {
            console.log(err);
          }
        });
  }

  onFormSubmit() {
    console.log(this.ficha);
    if (this.form.valid)
      this.onSubmitEventEmitter.emit(this.ficha);
  }

}
