import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormFichaVisitaDomiciliarComponent } from './form-ficha-visita-domiciliar.component';

describe('FormFichaVisitaDomiciliarComponent', () => {
  let component: FormFichaVisitaDomiciliarComponent;
  let fixture: ComponentFixture<FormFichaVisitaDomiciliarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormFichaVisitaDomiciliarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormFichaVisitaDomiciliarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
