import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { finalize, map, mergeMap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject } from 'rxjs';
import { FichaVisitaDomiciliar } from '../../../core/model/ficha-visita-domiciliar';
import { FichaVisitaDomiciliarService } from '../ficha-visita-domiciliar.service';
import { FormFichaVisitaDomiciliarComponent } from '../form-ficha-visita-domiciliar/form-ficha-visita-domiciliar.component';

@Component({
  selector: 'app-editar-ficha-visita-domiciliar',
  templateUrl: './editar-ficha-visita-domiciliar.component.html',
  styleUrls: ['./editar-ficha-visita-domiciliar.component.css']
})

export class EditarFichaVisitaDomiciliarComponent implements OnInit {
  isLoadingSubmit: boolean = false;

  formData$: BehaviorSubject<FichaVisitaDomiciliar> = new BehaviorSubject<FichaVisitaDomiciliar>(new FichaVisitaDomiciliar());

  @ViewChild('formFichaVisitaDomiciliar', { static: false }) formFichaVisitaDomiciliar: FormFichaVisitaDomiciliarComponent;

  constructor(
    private route: ActivatedRoute, 
    private fichaVisitaDomiciliarService: FichaVisitaDomiciliarService, 
    private toastrService: ToastrService,
    private router: Router
  ) { }

  ngOnInit(){
    this.fetchFormData();  
  }

  private fetchFormData() {
    let ficha: FichaVisitaDomiciliar = new FichaVisitaDomiciliar();

    this.route.paramMap.pipe(
      map(params => {
        // (+) before `params.get()` turns the string into a number
        const id = +params.get('id');
        return id;
      }),
      mergeMap(id => {
        return this.fichaVisitaDomiciliarService.findbyId(id);
      }
      )
    )
      .subscribe({
        next: (response) => {
          this.formData$.next(response);
        },
        error: (err) => {
          console.log('Erro ao buscar Ficha de Visita Domiciliar');
          console.log(err);
        }
      });
  }

  onSubmitted(ficha: FichaVisitaDomiciliar) {
    if (ficha.importada == false) {
      this.isLoadingSubmit = true;
      
      this.fichaVisitaDomiciliarService.update(ficha.id, ficha)
        .pipe(finalize(() => this.isLoadingSubmit = false))
        .subscribe({
          next: (response) => {
            this.toastrService.success('Ficha de Visita Domiciliar salva com sucesso!', 'Sucesso');
            this.router.navigate(['fichavisitadomiciliar/adicionar']);
          },
          error: (err) => {
            this.toastrService.error('Error ao salvar Ficha de Visita Domiciliar.', 'Error');
            console.log(err);
          }
        });
    }
  }

  onCanceled() {
    console.log('Cancelar');
  }

}
