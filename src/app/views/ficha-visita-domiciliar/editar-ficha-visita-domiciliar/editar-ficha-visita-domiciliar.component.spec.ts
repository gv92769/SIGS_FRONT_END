import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarFichaVisitaDomiciliarComponent } from './editar-ficha-visita-domiciliar.component';

describe('EditarFichaVisitaDomiciliarComponent', () => {
  let component: EditarFichaVisitaDomiciliarComponent;
  let fixture: ComponentFixture<EditarFichaVisitaDomiciliarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarFichaVisitaDomiciliarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarFichaVisitaDomiciliarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
