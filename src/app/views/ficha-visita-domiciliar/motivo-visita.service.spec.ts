import { TestBed } from '@angular/core/testing';

import { MotivoVisitaService } from './motivo-visita.service';

describe('MotivoVisitaService', () => {
  let service: MotivoVisitaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MotivoVisitaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
