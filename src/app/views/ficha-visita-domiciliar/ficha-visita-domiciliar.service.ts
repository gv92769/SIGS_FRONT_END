import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { FichaFiltro } from '../../core/model/ficha-filtro';
import { Page } from '../../core/model/page';
import { FichaVisitaDomiciliar } from '../../core/model/ficha-visita-domiciliar';

@Injectable({
  providedIn: 'root'
})

export class FichaVisitaDomiciliarService {

  readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/fichavisitadomiciliar';
  }

  create(ficha: FichaVisitaDomiciliar): Observable<FichaVisitaDomiciliar> {
    const url = this.url;
    return this.http.post<FichaVisitaDomiciliar>(url, ficha);
  }

  findbyId(id: number): Observable<FichaVisitaDomiciliar> {
    const url = this.url + '/' + id;
    return this.http.get<FichaVisitaDomiciliar>(url);
  }

  update(id: number, ficha: FichaVisitaDomiciliar): Observable<FichaVisitaDomiciliar> {
    const url = this.url + '/' + id;
    return this.http.put<FichaVisitaDomiciliar>(url, ficha);
  }


  public delete(id: number): Observable<void>{
    return this.http.delete<void>(this.url+"/"+id);
  }

  public search(filter: FichaFiltro): Observable<Page<any>> {
    return this.http.post<Page<any>>(this.url+"/search", filter);
  }

  importada(id: number): Observable<boolean> {
    return this.http.get<boolean>(this.url+'/exportada/'+id);
  }

}
