import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdicionarFichaVisitaDomiciliarComponent } from './adicionar-ficha-visita-domiciliar/adicionar-ficha-visita-domiciliar.component';
import { EditarFichaVisitaDomiciliarComponent } from './editar-ficha-visita-domiciliar/editar-ficha-visita-domiciliar.component';
import { PesquisarFichaVisitaDomiciliarComponent } from './pesquisar-ficha-visita-domiciliar/pesquisar-ficha-visita-domiciliar.component';


const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Ficha Visita Domiciliar'
    },
    children: [
      {
        path: 'pesquisar',
        component: PesquisarFichaVisitaDomiciliarComponent,
        data: {
          title: 'Pesquisar'
        }
      },
      {
        path: 'adicionar',
        component: AdicionarFichaVisitaDomiciliarComponent,
        data: {
          title: 'Adicionar'
        }
      },
      {
        path: 'editar/:id',
        component: EditarFichaVisitaDomiciliarComponent,
        data: {
          title: 'Editar'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FichaVisitaDomiciliarRoutingModule { }
