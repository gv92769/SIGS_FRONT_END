import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { ObjectCodigoDescricao } from '../../core/model/object-codigo-descricao';

@Injectable({
  providedIn: 'root'
})
export class MotivoVisitaService {
  readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/motivovisita';
  }

  public findAllById(ids: number[]): Observable<ObjectCodigoDescricao[]> {
    return this.http.post<ObjectCodigoDescricao[]>(this.url, ids);
  }
}
