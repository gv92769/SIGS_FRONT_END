import { TestBed } from '@angular/core/testing';

import { FichaVisitaDomiciliarService } from './ficha-visita-domiciliar.service';

describe('FichaVisitaDomiciliarService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FichaVisitaDomiciliarService = TestBed.get(FichaVisitaDomiciliarService);
    expect(service).toBeTruthy();
  });
});
