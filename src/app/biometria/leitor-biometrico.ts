import { from, Observable } from 'rxjs';


declare var Fingerprint: any;

export class LeitorBiometrico {
  static readonly SAMPLE_FORMAT_PNG: any = Fingerprint.SampleFormat.PngImage;
  static readonly SAMPLE_FORMAT_RAW: any = Fingerprint.SampleFormat.Raw;
  static readonly SAMPLE_FORMAT_COMPRESSED_WSQ: any = Fingerprint.SampleFormat.Compressed;
  static readonly SAMPLE_FORMAT_INTERMEDIATE_FEATURE_SET: any = Fingerprint.SampleFormat.Intermediate;

  private sdk: any;
  onSamplesAcquired: Function = function(e){};
  onDeviceConnected: Function = function(e){};
  onDeviceDisconnected: Function = function(e){};
  onCommunicationFailed: Function = function(e){};
  onQualityReported: Function = function(e){};

  //teste
  private static instance: LeitorBiometrico;
  static getInstance(): LeitorBiometrico{
    if (!LeitorBiometrico.instance){
      LeitorBiometrico.instance = new LeitorBiometrico();
    }
    return LeitorBiometrico.instance;
  }

  private constructor(){
      this.sdk = new Fingerprint.WebApi;
      this.sdk.onSamplesAcquired = (e) => this.onSamplesAcquired(e);
      this.sdk.onDeviceConnected = (e) => this.onDeviceConnected(e);
      this.sdk.onDeviceDisconnected = (e) => this.onDeviceDisconnected(e);
      this.sdk.onCommunicationFailed = (e) => this.onCommunicationFailed(e);
      this.sdk.onQualityReported = (e) => this.onQualityReported(e);

  }

  enumerateDevices(): Observable<any> {
    return from(this.sdk.enumerateDevices());
  }

  startAcquisition(sampleFormat, deviceUid?): Observable<any> {
    return from(this.sdk.startAcquisition(sampleFormat, deviceUid));
  }

  stopAcquisition(deviceUid?): Observable<any> {
    return from(this.sdk.stopAcquisition(deviceUid));
  }

  getDeviceInfo(deviceUid): Observable<any> {
    return from(this.sdk.getDeviceInfo(deviceUid));
  }

  static qualityCode(e){
    return Fingerprint.QualityCode[(e.quality)];
  }

}
