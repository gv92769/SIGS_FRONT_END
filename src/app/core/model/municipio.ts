export class Municipio {
  codigoIbge: number;
  nome: string;
  uf: string;
}
