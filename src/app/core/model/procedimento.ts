import { ProcedimentoFormaOrganizacional } from './procedimento-forma-organizacional';
import { ProcedimentoGrupo } from './procedimento-grupo';
import { ProcedimentoSubGrupo } from './procedimento-sub-grupo';

export class Procedimento {
    id: number;
    grupo: ProcedimentoGrupo;
    subGrupo: ProcedimentoSubGrupo;
    formaOrganizacional: ProcedimentoFormaOrganizacional;
    descricao: string;
    codigo: string;
    dtcompetencia: string;
    ativo: boolean;
    feminino: boolean;
    masculino: boolean;
}
