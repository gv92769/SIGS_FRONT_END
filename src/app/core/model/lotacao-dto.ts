import { Lotacao } from './lotacao';

export class LotacaoDto {
    token: string;
    lotacao: Lotacao
    id: number;
}
