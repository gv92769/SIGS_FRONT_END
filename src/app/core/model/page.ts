export class Page<T> {
  content: T [];
  totalElements: number;
  pageNumber: number = 0;
  size: number = 10;
}
