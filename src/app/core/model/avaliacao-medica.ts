import { TipoAvaliacaoMedica } from './tipo-avaliacao-medica.enum';
import { Cidadao } from '../../views/cidadao/cidadao';
import { Profissional } from './profissional';

export class AvaliacaoMedica {
    id?: number;
    dataRealizacao: string;
    dataHorarioInicio: string;
    dataHorarioFinal: string;
    cidadao: Cidadao;
    profissional: Profissional = new Profissional();
    tipoAvaliacaoMedica: TipoAvaliacaoMedica;
}
