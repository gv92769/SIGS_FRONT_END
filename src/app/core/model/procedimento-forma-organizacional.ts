import { ProcedimentoGrupo } from './procedimento-grupo';
import { ProcedimentoSubGrupo } from './procedimento-sub-grupo';

export class ProcedimentoFormaOrganizacional {
    id: number;
    codigo: string;
    subGrupo: ProcedimentoSubGrupo;
    descricao: string;
    dtcompetencia: string;
}
