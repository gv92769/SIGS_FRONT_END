import { ProcedimentoGrupo } from './procedimento-grupo';

export class ProcedimentoSubGrupo {
    id: number;
    codigo: string;
    grupo: ProcedimentoGrupo;
    descricao: string;
    dtcompetencia: string;
}
