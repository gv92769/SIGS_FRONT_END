import { UnidadeBasicaSaude } from './unidade-basica-saude';
import { Cbo } from './cbo';
import { Perfil } from './perfil';
import { Profissional } from './profissional';

export class Lotacao {
    id: number;
    ubs: UnidadeBasicaSaude;
    unidade: string;
    cbo: Cbo;
    ocupacao: string;
    profissional: Profissional = new Profissional();
    perfil: Perfil;
    nomePerfil: string;
    descricao: string;
    ativo: boolean = true;
}
