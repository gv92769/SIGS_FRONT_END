export class PageRequest {
  pageNumber: number;
  size: number;
  column: string;
  sort: string;
}
