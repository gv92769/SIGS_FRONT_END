import { Cidadao } from "../../views/cidadao/cidadao";
import { Cbo } from "./cbo";
import { ObjectCodigoDescricao } from "./object-codigo-descricao";
import { Profissional } from "./profissional";
import { UnidadeBasicaSaude } from "./unidade-basica-saude";

export class FichaVisitaDomiciliar  {
    id: number;
    uuid: string;
    statusOutroProfissional: boolean = false;
    turno: number;
    cidadao: Cidadao;
    data: string;
    ine: string;
    importada: boolean = false;
    ubs: UnidadeBasicaSaude = new UnidadeBasicaSaude();
    profissionalResponsavel: Profissional = new Profissional();
    cbo: Cbo = new Cbo();
    motivos:  ObjectCodigoDescricao[] = new Array;
    desfecho:  ObjectCodigoDescricao = new ObjectCodigoDescricao();
}
