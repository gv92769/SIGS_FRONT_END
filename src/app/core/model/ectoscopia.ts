import {ItemEctoscopia} from './item-ectoscopia';
import { AvaliacaoMedica } from './avaliacao-medica';
import { Cid10 } from './cid10';

export class Ectoscopia extends AvaliacaoMedica{
  itensEctoscopia: ItemEctoscopia[] = new Array;
  listCid: Cid10[] = new Array;
  observacao: string;
}
