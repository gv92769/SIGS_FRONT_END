export enum TipoAvaliacaoMedica {
    ECTOSCOPIA = 'Ectoscopia',
    ATENDIMENTO_INDIVIDUAL = 'Atendimento Individual'
}
