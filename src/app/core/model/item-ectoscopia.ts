import { EctoscopiaItem } from '../../views/ectoscopia/ectoscopia-item';

export class ItemEctoscopia {
  id?: number;
  item: EctoscopiaItem;
  observacao?: string;
  auxiliar?: string;
  opcao?: string;
}
