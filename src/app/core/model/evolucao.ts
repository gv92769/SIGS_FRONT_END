import { AvaliacaoMedica } from './avaliacao-medica';
import { UnidadeBasicaSaude } from './unidade-basica-saude';
import { Ciap2 } from './ciap2';
import { Cid10 } from './cid10';
import { LocalAtendimento } from './local-atendimento';
import { TipoAtendimento } from './tipo-atendimento';
import { ObjectCodigoDescricao } from './object-codigo-descricao';
import { CiapCondicaoAvaliada } from '../../views/atendimento-individual/ciap-condicao-avaliada';
import { Cbo } from './cbo';
import { Procedimento } from './procedimento';

export class Evolucao extends AvaliacaoMedica {
    ubs: UnidadeBasicaSaude = new UnidadeBasicaSaude();
    cbo: Cbo = new Cbo();
    ine: string;
    turno: Evolucao.Turno;
    localAtendimento: LocalAtendimento = new LocalAtendimento();
    tipoAtendimento: TipoAtendimento = new TipoAtendimento();
    peso: number;
    altura: number;
    aleitamentoMaterno: ObjectCodigoDescricao = new ObjectCodigoDescricao();
    dum: Date;
    idadeGestacional: number;
    modalidadeAD: Evolucao.ModalidadeAD;
    ciaps: CiapCondicaoAvaliada[] = new Array;
    ciap2_1: Ciap2 = null;
    ciap2_2: Ciap2 = null;
    cid10_1: Cid10 = null;
    cid10_2: Cid10 = null;
    vacinacaoEmDia: boolean;
    ficouObservacao: boolean;
    nasfs: ObjectCodigoDescricao[] = new Array;
    condutas: ObjectCodigoDescricao[] = new Array;
    gravidezPlanejada: boolean;
    gestasPrevias: number;
    partos: number;
    racionalidadeSaude: ObjectCodigoDescricao = new ObjectCodigoDescricao();
    perimetroCefalico: number;
    procedimentos: Procedimento[] = new Array;
    observacao: string;
    importada:any;
}

export namespace Evolucao {
    export enum Turno {
        M, T, N
    }

    export enum ModalidadeAD {
        AD1, AD2, AD3
    }

    export enum Situacao {
        S, A
    }
}
