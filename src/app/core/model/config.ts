export class Config {
    displayKey: string = 'description';
    search: boolean = true;
    height: string = 'auto';
    placeholder: string;
    limitTo: number = 10;
    moreText: string;
    noResultsFound: string = 'No results found!';
    searchPlaceholder: string = 'Search';
    searchOnKey: string;
}
