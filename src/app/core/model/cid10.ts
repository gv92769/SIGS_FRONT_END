export class Cid10 {
    id: number;
    codigo: string;
    descricao: string;
    sexo: number;
    ativo: boolean;
}
