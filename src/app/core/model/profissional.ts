export class Profissional {
    id?: number;
    nome: string;
    cpf: string;
    celular: string;
    cns: string;
    crmCorem: string;
}
