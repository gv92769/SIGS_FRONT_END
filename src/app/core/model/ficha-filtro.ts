import { Cbo } from './cbo';
import { PageRequest } from './page-request';
import { Perfil } from './perfil';
import { ProcedimentoSubGrupo } from './procedimento-sub-grupo';

export class FichaFiltro extends PageRequest {
  nome: string;
  cpf: string;
  cns: string;
  dataInicio: Date;
  dataFinal: Date;
  exportada: boolean = false;
  tipoExame: ProcedimentoSubGrupo;
  tipoAtendimento: string;
  perfil: Perfil = null;
  cbo: Cbo = null;
}
