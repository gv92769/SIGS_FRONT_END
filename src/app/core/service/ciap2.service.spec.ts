import { TestBed } from '@angular/core/testing';

import { Ciap2Service } from './ciap2.service';

describe('Ciap2Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Ciap2Service = TestBed.get(Ciap2Service);
    expect(service).toBeTruthy();
  });
});
