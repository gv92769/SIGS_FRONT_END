import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Lotacao } from '../model/lotacao';
import { environment } from '../../../environments/environment';
import { UnidadeBasicaSaude } from '../model/unidade-basica-saude';

@Injectable({
  providedIn: 'root'
})
export class LotacaoService {

  readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/lotacao';
  }

  public search(id: number): Observable<Lotacao[]> {
    return this.http.get<Lotacao[]>(this.url+'/search/'+id);
  }

  public findById(id: number): Observable<Lotacao> {
    return this.http.get<Lotacao>(this.url+'/'+id);
  }

  public save(lotacao: Lotacao): Observable<Lotacao> {
    return this.http.post<Lotacao>(this.url, lotacao);
  }

  public update(id: number, lotacao: Lotacao): Observable<Lotacao> {
    return this.http.put<Lotacao>(this.url+'/'+id, lotacao);
  }

  public delete(id: number) {
    return this.http.get(this.url+'/delete/'+id);
  }

  public searchPerfil(id: number): Observable<Lotacao[]> {
    return this.http.get<Lotacao[]>(this.url+'/perfis/'+id);
  }

  public searchFindAllByLotacao(valor: string, ubs: UnidadeBasicaSaude): Observable<Lotacao[]> {
    return this.http.post<Lotacao[]>(this.url+'/search/'+valor,ubs);
  }

}
