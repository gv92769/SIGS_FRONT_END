import { TestBed } from '@angular/core/testing';

import { ProcedimentoSubGrupoService } from './procedimento-sub-grupo.service';

describe('ProcedimentoSubGrupoService', () => {
  let service: ProcedimentoSubGrupoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProcedimentoSubGrupoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
