import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { ProcedimentoFormaOrganizacional } from '../model/procedimento-forma-organizacional';
import { ProcedimentoSubGrupo } from '../model/procedimento-sub-grupo';

@Injectable({
  providedIn: 'root'
})
export class ProcedimentoFormaOrganizacionalService {

  readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/procedimentoformaorganizacional';
  }

  public findAllBySubGrupo(subgrupo: ProcedimentoSubGrupo): Observable<ProcedimentoFormaOrganizacional[]> {
    return this.http.post<ProcedimentoFormaOrganizacional[]>(this.url, subgrupo);
  }

}
