import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/file';
  }

  downloadFile(file: string): Observable<HttpResponse<Blob>> {
    return this.http.post(this.url+'/download', file, {
      observe: 'response',
      responseType: 'blob'
    });
  }

}
