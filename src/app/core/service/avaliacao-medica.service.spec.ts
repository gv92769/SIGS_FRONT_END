import { TestBed } from '@angular/core/testing';

import { AvaliacaoMedicaService } from './avaliacao-medica.service';

describe('AvaliacaoMedicaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AvaliacaoMedicaService = TestBed.get(AvaliacaoMedicaService);
    expect(service).toBeTruthy();
  });
});
