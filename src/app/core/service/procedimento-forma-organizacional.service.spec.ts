import { TestBed } from '@angular/core/testing';

import { ProcedimentoFormaOrganizacionalService } from './procedimento-forma-organizacional.service';

describe('ProcedimentoFormaOrganizacionalService', () => {
  let service: ProcedimentoFormaOrganizacionalService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProcedimentoFormaOrganizacionalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
