import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
import { TipoAvaliacaoMedica } from '../model/tipo-avaliacao-medica.enum';
import { Page } from '../model/page';
import { FichaFiltro } from '../model/ficha-filtro';

@Injectable({
  providedIn: 'root'
})
export class AvaliacaoMedicaService {

  readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/avaliacoesmedicas';
  }

  public search(filter: FichaFiltro, tipo: TipoAvaliacaoMedica): Observable<Page<any>> {
    return this.http.post<Page<any>>(this.url+"/search/"+tipo, filter);
  }

  public delete(id: number): Observable<void>{
    return this.http.delete<void>(this.url+'/'+id);
  }

}
