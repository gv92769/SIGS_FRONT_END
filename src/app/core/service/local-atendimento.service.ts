import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LocalAtendimento } from '../model/local-atendimento';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LocalAtendimentoService {

  readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/localatendimento';
  }

  public findAll(): Observable<LocalAtendimento[]> {
    return this.http.get<LocalAtendimento[]>(this.url);
  }

}
