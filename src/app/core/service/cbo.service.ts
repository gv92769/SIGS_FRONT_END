import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Cbo } from '../model/cbo';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CboService {

  readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/cbo';
  }

  public findByCodigo(codigo: string): Observable<Cbo> {
    return this.http.get<Cbo>(this.url + '/codigo/' + codigo);
  }

  public findAllByTitulo(titulo: string): Observable<Cbo[]> {
    return this.http.get<Cbo[]>(this.url + '/titulo/' + titulo);
  }

  public findAllByTituloByPerfil(titulo: string, id: number): Observable<Cbo[]> {
    return this.http.get<Cbo[]>(this.url+'/titulo/'+titulo+'/id/'+id);
  }

  public findAllByProfissional(id: number): Observable<Cbo[]> {
    return this.http.get<Cbo[]>(this.url+'/findallbyprofissional/'+id);
  }

}
