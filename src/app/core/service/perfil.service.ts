import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { Perfil } from '../model/perfil';

@Injectable({
  providedIn: 'root'
})
export class PerfilService {

  readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/perfil';
  }

  public findAllByAtivo(): Observable<Perfil[]> {
    return this.http.get<Perfil[]>(this.url+'/findallbyativo');
  }

}
