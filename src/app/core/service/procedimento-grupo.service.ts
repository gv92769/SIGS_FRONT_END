import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { ProcedimentoGrupo } from '../model/procedimento-grupo';

@Injectable({
  providedIn: 'root'
})
export class ProcedimentoGrupoService {

  readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/procedimentogrupo';
  }

  public findAllById(ids: number[]): Observable<ProcedimentoGrupo[]> {
    return this.http.post<ProcedimentoGrupo[]>(this.url, ids);
  }

}
