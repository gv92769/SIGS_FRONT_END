import { TestBed } from '@angular/core/testing';

import { ValidarDocumentoService } from './validar-documento.service';

describe('ValidarDocumentoService', () => {
  let service: ValidarDocumentoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ValidarDocumentoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
