import { Injectable } from '@angular/core';
import { Profissional } from '../../core/model/profissional';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { UnidadeBasicaSaude } from '../model/unidade-basica-saude';

@Injectable({
  providedIn: 'root'
})
export class ProfissionalService {

  readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/profissional';
  }

  public findById(id: number): Observable<Profissional> {
    return this.http.get<Profissional>(this.url + '/' + id);
  }

  public search(valor: string): Observable<Profissional[]> {
    return this.http.get<Profissional[]>(this.url+'/search/'+valor);
  }

  public searchFindAllByLotacao(valor: string, ubs: UnidadeBasicaSaude): Observable<Profissional[]> {
    return this.http.post<Profissional[]>(this.url+'/search/'+valor, ubs)
  }

}
