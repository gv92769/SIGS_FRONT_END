import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ValidarDocumentoService {

  readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/validardocumentos';
  }

  public validaCns(id: number, cns: string, tipo: string): Observable<boolean> {
    return this.http.get<boolean>(this.url+'/id/'+id+'/cns/'+cns+'/tipo/'+tipo);
  }

  public validaCpf(id: number, cpf: string, tipo: string): Observable<boolean> {
    return this.http.get<boolean>(this.url+'/id/'+id+'/cpf/'+cpf+'/tipo/'+tipo);
  }

}
