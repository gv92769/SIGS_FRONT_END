import { TestBed } from '@angular/core/testing';

import { UnidadeBasicaSaudeService } from './unidade-basica-saude.service';

describe('UnidadeBasicaSaudeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UnidadeBasicaSaudeService = TestBed.get(UnidadeBasicaSaudeService);
    expect(service).toBeTruthy();
  });
});
