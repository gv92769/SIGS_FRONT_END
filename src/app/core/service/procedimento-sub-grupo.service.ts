import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { ProcedimentoGrupo } from '../model/procedimento-grupo';
import { ProcedimentoSubGrupo } from '../model/procedimento-sub-grupo';

@Injectable({
  providedIn: 'root'
})
export class ProcedimentoSubGrupoService {

  readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/procedimentosubgrupo';
  }

  public findAllByGrupo(grupo: ProcedimentoGrupo): Observable<ProcedimentoSubGrupo[]> {
    return this.http.post<ProcedimentoSubGrupo[]>(this.url, grupo);
  }

  public searchSubGrupo(procedimentoFiltro: any): Observable<ProcedimentoSubGrupo[]> {
    return this.http.post<ProcedimentoSubGrupo[]>(this.url+'/search/subgrupo', procedimentoFiltro);
  }

}
