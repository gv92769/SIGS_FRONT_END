import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { AtendimentoIndividual } from '../model/atendimento-individual';
import { Cidadao } from '../../views/cidadao/cidadao';
import { FichaFiltro } from '../model/ficha-filtro';
import { Page } from '../model/page';
import { FichaAtendimentoIndividual } from '../../views/atendimento-individual/ficha-atendimento-individual';

@Injectable({
  providedIn: 'root'
})
export class AtendimentoIndividualService {

  readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/atendimentosindividuais';
  }

  public search(filter: FichaFiltro): Observable<Page<any>> {
    return this.http.post<Page<any>>(this.url+"/search", filter);
  }

  findbyId(id: number): Observable<FichaAtendimentoIndividual> {
    const url = this.url + '/' + id;
    return this.http.get<FichaAtendimentoIndividual>(url);
  }

  findCidadaoByAtendimentoId(id: number): Observable<Cidadao> {
    const url = this.url + '/' + id + '/cidadao';
    return this.http.get<Cidadao>(url);
  }

  create(atendimento: AtendimentoIndividual): Observable<AtendimentoIndividual> {
    const url = this.url;
    return this.http.post<AtendimentoIndividual>(url, atendimento);
  }

  update(id: number, atendimento: AtendimentoIndividual): Observable<AtendimentoIndividual> {
    const url = this.url + '/' + id;
    return this.http.put<AtendimentoIndividual>(url, atendimento); 
  }

  importada(id: number): Observable<boolean> {
    return this.http.get<boolean>(this.url+'/exportada/'+id);
  }
  
}
