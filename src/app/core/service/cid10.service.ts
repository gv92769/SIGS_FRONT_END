import { Injectable } from '@angular/core';
import { Cid10 } from '../model/cid10';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class Cid10Service {

  readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/cid10';
  }

  public search(valor: string, sexo: number): Observable<Cid10[]> {
    return this.http.get<Cid10[]>(this.url+'/search/'+valor+'/'+sexo);
  }
}
