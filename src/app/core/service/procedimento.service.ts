import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { Procedimento } from '../model/procedimento';

@Injectable({
  providedIn: 'root'
})
export class ProcedimentoService {

  readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/procedimento';
  }

  public search(procedimentoFiltro: any): Observable<Procedimento[]> {
    return this.http.post<Procedimento[]>(this.url+'/search', procedimentoFiltro);
  }

  public searchExame(procedimentoFiltro: any): Observable<Procedimento[]> {
    return this.http.post<Procedimento[]>(this.url+'/search/exame', procedimentoFiltro);
  }
  
  public searchOdonto(procedimentoFiltro: any): Observable<Procedimento[]> {
    return this.http.post<Procedimento[]>(this.url+"/search/odonto", procedimentoFiltro);
  }

}
