import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { Cidadao } from '../../views/cidadao/cidadao';
import { FichaFiltro } from '../model/ficha-filtro';
import { Page } from '../model/page';
import { FichaEvolucao } from '../../views/evolucao/ficha-evolucao';
import { Evolucao } from '../model/evolucao';

@Injectable({
  providedIn: 'root'
})
export class EvolucaoService {

  readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/evolucao';
  }

  public search(filter: FichaFiltro): Observable<Page<any>> {
    return this.http.post<Page<any>>(this.url+"/search", filter);
  }

  findbyId(id: number): Observable<FichaEvolucao> {
    const url = this.url + '/' + id;
    return this.http.get<FichaEvolucao>(url);
  }

  findCidadaoByAtendimentoId(id: number): Observable<Cidadao> {
    const url = this.url + '/' + id + '/cidadao';
    return this.http.get<Cidadao>(url);
  }

  create(atendimento: Evolucao): Observable<Evolucao> {
    const url = this.url;
    return this.http.post<Evolucao>(url, atendimento);
  }

  update(id: number, atendimento: Evolucao): Observable<Evolucao> {
    const url = this.url + '/' + id;
    return this.http.put<Evolucao>(url, atendimento); 
  }

  importada(id: number): Observable<boolean> {
    return this.http.get<boolean>(this.url+'/exportada/'+id);
  }
  
}
