import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { Ciap2 } from '../model/ciap2';

@Injectable({
  providedIn: 'root'
})
export class Ciap2Service {

  readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/ciap2';
  }

  public search(valor: string, sexo: number): Observable<Ciap2[]> {
    return this.http.get<Ciap2[]>(this.url+'/search/'+valor+'/'+sexo);
  }
  
}
