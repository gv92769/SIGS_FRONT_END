import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
import { Ectoscopia } from '../model/ectoscopia';

@Injectable({
  providedIn: 'root'
})
export class EctoscopiaService {

  readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/avaliacoesmedicas/ectoscopias';
  }

  public findOne(id): Observable<Ectoscopia> {
    return this.http.get<Ectoscopia>(this.url+'/'+id);
  }

  public create(ectoscopia: any): Observable<any> {
    return this.http.post<any>(this.url, ectoscopia);
  }

  public update(id, ectoscopia: Ectoscopia): Observable<Ectoscopia> {
    return this.http.put<Ectoscopia>(this.url+'/'+id, ectoscopia);
  }
}
