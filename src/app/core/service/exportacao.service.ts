import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ExportacaoService {

  readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/exportacao';
  }

  gerarExportacao(): Observable<HttpResponse<Blob>> {
    return this.http.get(this.url, {
      observe: 'response',
      responseType: 'blob'
    });
  }

  countByImportada(): Observable<number> {
    return this.http.get<number>(this.url+'/count');
  }

}
