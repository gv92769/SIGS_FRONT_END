import { TestBed } from '@angular/core/testing';

import { LocalAtendimentoService } from './local-atendimento.service';

describe('LocalAtendimentoService', () => {
  let service: LocalAtendimentoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LocalAtendimentoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
