import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { TipoAtendimento } from '../model/tipo-atendimento';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TipoAtendimentoService {

  readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/tipoatendimento';
  }

  public findAllById(ids: number[]): Observable<TipoAtendimento[]> {
    return this.http.post<TipoAtendimento[]>(this.url+'/findallbyid', ids);
  }

}
