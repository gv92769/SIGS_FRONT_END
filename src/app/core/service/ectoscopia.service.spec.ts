import { TestBed } from '@angular/core/testing';

import { EctoscopiaService } from './ectoscopia.service';

describe('EctoscopiaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EctoscopiaService = TestBed.get(EctoscopiaService);
    expect(service).toBeTruthy();
  });
});
