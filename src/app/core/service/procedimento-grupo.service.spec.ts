import { TestBed } from '@angular/core/testing';

import { ProcedimentoGrupoService } from './procedimento-grupo.service';

describe('ProcedimentoGrupoService', () => {
  let service: ProcedimentoGrupoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProcedimentoGrupoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
