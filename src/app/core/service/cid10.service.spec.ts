import { TestBed } from '@angular/core/testing';

import { Cid10Service } from './cid10.service';

describe('Cid10Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Cid10Service = TestBed.get(Cid10Service);
    expect(service).toBeTruthy();
  });
});
