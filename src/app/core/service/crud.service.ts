import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export abstract class CrudService<T,K> {

  constructor(protected http: HttpClient) { }

  abstract getBaseUrl(): string;

  findAll(): Observable<T[]> {
    const url = this.getBaseUrl();
    return this.http.get<T[]>(url);
  }

  findOne(id: K): Observable<T> {
    const url = this.getBaseUrl() + '/' + id;
    return this.http.get<T>(url);
  }
}
