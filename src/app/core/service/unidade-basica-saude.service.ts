import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { UnidadeBasicaSaude } from '../model/unidade-basica-saude';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UnidadeBasicaSaudeService {

  readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/ubs';
  }

  public findByCnes(cnes: string): Observable<UnidadeBasicaSaude> {
    return this.http.get<UnidadeBasicaSaude>(this.url + "/cnes/" + cnes);
  }

  public findAllByNome(nome: string): Observable<UnidadeBasicaSaude[]> {
    return this.http.get<UnidadeBasicaSaude[]>(this.url + "/nome/" + nome);
  }

  public findAll(): Observable<UnidadeBasicaSaude[]> {
    return this.http.get<UnidadeBasicaSaude[]>(this.url);
  }

}
