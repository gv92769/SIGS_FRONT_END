import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { Municipio } from '../model/municipio';

@Injectable({
  providedIn: 'root'
})
export class MunicipioService {

  readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/municipio';
  }

  public findByUfAndNome(uf: string, nome: string): Observable<Municipio[]> {
    return this.http.get<Municipio[]>(this.url + '/uf/' + uf + '/nome/' + nome);
  }

}
