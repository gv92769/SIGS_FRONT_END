import { TestBed } from '@angular/core/testing';

import { AtendimentoIndividualService } from './atendimento-individual.service';

describe('AtendimentoIndividualService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AtendimentoIndividualService = TestBed.get(AtendimentoIndividualService);
    expect(service).toBeTruthy();
  });
});
