import { Injectable } from '@angular/core';
import { Pais } from '../model/pais';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PaisService {

  readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/pais';
  }

  public findAllByNome(nome: string): Observable<Pais[]> {
    return this.http.get<Pais[]>(this.url + '/nome/' + nome);
  }

}
